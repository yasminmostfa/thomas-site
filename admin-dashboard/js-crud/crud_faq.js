$(document).ready(function (){
var checkbox=[];
 $("#form_crud").submit(function (){
	 //disable button
	//get value of checkbox
	$('input:checkbox[name="category[]"]:checked').each(function(){
	   checkbox.push($(this).val());
		 
	});
	$('#submit').attr("disabled", "true");
	var type = 'POST';
	var url = $('#form_crud').attr('action');
	//send json data
	var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			question: $('#question').val(),
			alias: $('#alias').val(),
			status: $('#status').val(),
			author_id:$('#author').val(),
			lang: $('#lang').val(),
			description: tinymce.get('faq_description').getContent(),
			answer: tinymce.get('answer').getContent(),
			categories:checkbox
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		 if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				window.location.href = "full_info.php?id="+$('#record').val()+"&lang="+$('#lang').val();
			 }else{
				window.location.href = "full_info.php?id="+data.inserted_id;
			 }
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 
  
	
 //do not go to any where
  return false;     
 });
    
	
})