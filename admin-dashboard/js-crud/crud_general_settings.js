$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			title: $('#title').val(), 
			enable_website:$('input[name=enable_website]:checked').val(), 
			offline_messages: $('#offile_messeges').val(),
			meta_key: $('#meta_key').val(),
			site_url :$('#site_url').val(),
			email: $('#email').val(),
			time_zone: $('#time_zone').val(),
			translate_lang: $('#translate_lang').val(),
			google_analitic: $('#google_analitic').val(),
			front_lang: $('#front_lang').val(),
			description:  $('#description').val(),
			vufind_main: $('#vufind_main').val(),
			vufind_advanced: $('#vufind_advanced').val()
			
		};
		
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		if(data.status == 'work'){
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('Successful Updated Process');
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');			
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})