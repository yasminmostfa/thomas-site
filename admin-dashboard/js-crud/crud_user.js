$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			user_name: $('#user_name').val(), 
			first_name: $('#first_name').val(), 
			last_name: $('#last_name').val(),
			email:  $('#email').val(),
			password: $('#pwd').val(),
			user_type: $('input[name=user_type]:checked').val(),
			profile: $('#profile').val()
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				 if(data.status == 'email_exist'){
					 $('#submit').removeAttr("disabled");
					 $('#pwd').val('');
					 $('#loading_data').html('Email already exist');
				 }else{
					 window.location.href = "full_info.php?id="+$('#record').val();
				 }
			 }else{
				$('#form_crud')[0].reset();
				$('#form_option')[0].reset();
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('Successful Insert Process');				 
			 }
		 }else if(data.status == 'username_exist'){
			 $('#submit').removeAttr("disabled");
			 $('#pwd').val('');
			 $('#loading_data').html('Username already exist');
		 }else if(data.status == 'email_exist'){
			 $('#submit').removeAttr("disabled");
			 $('#pwd').val('');
			 $('#loading_data').html('Email already exist');
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');
		 }else{
			 $('#submit').removeAttr("disabled");
			 $('#pwd').val('');
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})