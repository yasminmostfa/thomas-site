$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var id = $('#id').val();
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			title_en: $('#title_en').val(), 
			title_ar: $('#title_ar').val(), 
			type_format:$('#format').val()
			
		};
		
	$.ajax({
	 type: type,
	 url: url+'?id='+id,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		if(data.status == 'work'){
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('Successful Add Process');
				window.location.href = 'view.php';
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');			
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 });

 $('.confirm_delete').click(function(){
	
	var task = $("#task_type").val();
	var del_id = $(this).attr('id');
	var data = {
		 task : task ,
		 id : del_id
		};
	var url = "data_model/delete.php";
	var type = "GET";
	$.ajax({
		url : url,
		type : type,
		data : data,
		success : function(data){
			window.location.href = 'view.php';
			// $('#mg_'+del_id).remove();

		}
		
		
		});
	});    
});