$(document).ready(function (){
	//hidden button 
	$(".recalculate").hide();
	//if quantity changed show recalculate button 
	$(".quantity_input").change(function() {
		$(".recalculate").show();
	});	
	//if price changed show recalculate button 
	$(".price_input").change(function() {
		$(".recalculate").show();
	});	
	//if price changed show recalculate button 
	$("#tax").change(function() {
		$(".recalculate").show();
	});		
	//if button clicked sum price column
	$(".recalculate").click(function () {
		var total_price = 0;
		// iterate through each td based on class and add the values
		$('.orders').each(function() {
			var price = $(this).find('input.price_input').val();
			var quantity = $(this).find('input.quantity_input').val();
			if(price != 0 || quantity != 0){
				$(this).find('.total').html(price*quantity);
				var multiplying_pricing_quantity =  price*quantity;
				total_price += multiplying_pricing_quantity;	
			}
		});
		$(".sub_total").html(total_price);
		$(".final_total").html(total_price+((total_price/100)*$("#tax").val()));
	})
	//final orders
	var orders = [];	
	var shipping_info = [];	
	var billing_info = [];	
	$("#form_crud").submit(function (){
		$('#submit').attr("disabled", "true");
		var type = 'POST';
		var url = $('#form_crud').attr('action');	
		//get billing address
		$('.shipping_address').each(function() {
			shipping_info.push($(this).val());
		})		
		//get shipping address info		
		$('.billing_address').each(function() {
			billing_info.push($(this).val());
		})
		//get order price/quantity
		$('.orders').each(function() { 
			orders[$(this).attr("id")] = $(this).find('input.price_input').val()+','+$(this).find('input.quantity_input').val();
		})	
		 //send json data
		 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			status: $('#status').val(),
			payment: $('#payment').val(),
			tax: $('#tax').val(),
			shipper: $('#shipper').val(),
			billing_info:billing_info,
			shipping_info:shipping_info,
			orders:orders
		};	
		$.ajax({
		 type: type,
		 url: url,
		 data: data,
		 beforeSend: function(){
			//show laoding 
			$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
		 },
		 success: function(data){
			 if(data.status == 'work'){
				 window.location.href = "full_info.php?id="+$('#record').val();
			 }else{
				$('#loading_data').html('Error In Process');
			 }
		 }
		})	
	 	//do not go to any where
	  	return false;     
	 })    
})