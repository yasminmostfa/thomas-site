<?php

require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
require_once('../../../../classes/vufindFormat.php');
require_once('../../../../classes/vufind_title.php');

require_once('../../../../classes/Localization.php');
//check  session user  log in 

if ($session->is_logged() == false) {
    redirect_to("../../../index.php");
}
header('Content-Type: application/json');
// get user profile   
$user_data = Users::find_by_id($session->user_id);
// get user profile data 
$user_profile = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block 

if ($user_profile->profile_block == "yes") {

    redirect_to("../../../index.php");
}
$languages = Localization::find_all('label', 'asc');
//send json data 
header('Content-Type: application/json');
// save posted data on ram 
$title_en = $_POST['title_en'];
$title_ar = $_POST['title_ar'];
$type_format = $_POST['type_format'];

if(!empty($title_en) && !empty($title_ar) && !empty($type_format)){

if (!empty($_POST["task"]) && $_POST["task"] == "insert") {
    $check_required_fields = [];
    if (count($check_required_fields) == 0) {
         $add = new vufindFormat();
            $add->inserted_by = $session->user_id;
            $add->type_format = $_POST['type_format'];
            $add->insert();
      
         foreach ($languages as $language) {
            $title = new vufindTitle();
            $title->title = $_POST['title_' . $language->label];
            $title->lang_id = $language->id;
            $title->format_id = $add->id;
            $title->insert();
        }

       
        $data  = array("status"=>"work"); 
                    echo json_encode($data); 
       
    } else {
        //validation error 
        $comma_separated = implode("<br>", $check_required_fields);
        $data = array("status" => "valid_error", "fileds" => $comma_separated);
        echo json_encode($data);
    }
}
}else{
      $data = array("status" => "valid_error", "fileds" => 'All flields are  required ');
        echo json_encode($data);
}
//close connection 
if (isset($database)) {
    $database->close_connection();
}
?>