<?php

require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
require_once('../../../../classes/vufindFormat.php');
require_once('../../../../classes/vufind_title.php');

require_once('../../../../classes/Localization.php');
//check  session user  log in 
// $data = new vufindTitle();
// $result =$data->getAll_by_format_id($_GET['id'],1);
// var_dump($result);
// die();
if ($session->is_logged() == false) {
    redirect_to("../../../index.php");
}
header('Content-Type: application/json');
// get user profile   
$user_data = Users::find_by_id($session->user_id);
// get user profile data 
$user_profile = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block 

if ($user_profile->profile_block == "yes") {

    redirect_to("../../../index.php");
}

//send json data 
// header('Content-Type: application/json');
if (!empty($_POST["task"]) && $_POST["task"] == "update") {
    $check_required_fields = [];
    if (count($check_required_fields) == 0) {

           //retrieve all available languages 
        $languages = Localization::find_all('label', 'asc');
      $format =  vufindFormat::find_by_id($_GET['id']);
      $format->type_format = $_POST['type_format'];
      $format->update();
        //insert url title and description and it's alias content  
        foreach ($languages as $language) {
            $vufind_title =  vufindTitle::getAll_by_format_id($_GET['id'],$language->id);
            $vufind_title->title = $_POST['title_' . $language->label];

            $vufind_title->update();
        }
      
        $data  = array("status"=>"work"); 
                    echo json_encode($data); 

       
    } else {
        //validation error 
        $comma_separated = implode("<br>", $check_required_fields);
        $data = array("status" => "valid_error", "fileds" => $comma_separated);
        echo json_encode($data);
    }
}
//close connection 
if (isset($database)) {
    $database->close_connection();
}
?>