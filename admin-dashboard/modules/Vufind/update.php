<?php  
require_once("../layout/initialize.php"); 
$languages = Localization::find_all(); 
$data = new vufindFormat();
$data->enable_relation();
$id = $_GET['id'];


//get all category 

require_once("../layout/header.php");	 
include("../../assets/texteditor4/head.php");  
?>
<!-- <script type="text/javascript" src="../../js-crud/gallery.js"></script>  
<script type="text/javascript" src="../../js-crud/nodes.js"></script> 
<script type="text/javascript" src="../../js-crud/auto_complete.js"></script> 
 --><!--header end--> 
   <script type="text/javascript" src="../../js-crud/crud_vufind.js"></script>  

<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
<section class="wrapper site-min-height"> 
<h4>Nodes Module</h4> 
<div class="row"> 
  <aside class="col-lg-8"> 
	<section> 
	  <div class="panel"> 
		<div class="panel-heading"> Add Formats</div> 
		<div class="panel-body"> 
		  <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"  > 
			<input type="hidden" id="process_type" value="update"> 
			<section class="panel "> 
			  <header class="panel-heading tab-bg-dark-navy-blue"> 
				<ul class="nav nav-tabs"> 
				  <li class=" center-block active" > <a data-toggle="tab" href="#main_option" class="text-center"><strong> Main Info</strong></a></li> 
				 <!--  <li> <a data-toggle="tab" href="#model_images" class="text-center"><strong>Gallery &  Images</strong> </a> </li> 
				  <li> <a data-toggle="tab" href="#events_details" class="text-center"><strong>Nodes Details </strong> </a> </li> 
				  <li> <a data-toggle="tab" href="#taxonomies" class="text-center"><strong> Taxonomies</strong> </a> </li>  -->
				</ul> 
			  </header> 
			  <div class="panel-body"> 
				<div class="tab-content"> 
				  <div id="main_option" class="tab-pane active "> 
					 <section class="panel col-lg-9"> 
                        <header class="panel-heading tab-bg-dark-navy-blue "> 
                          <ul class="nav nav-tabs"> 
                            <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
								<strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                          </ul> 
                        </header> 
                        <div class="panel-body"> 
                          <div class="tab-content"> 
                            <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language){
                            $result =  $data->get_format_data_by_id($id,$language->id);
 
								echo " 
								<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'> 
										<div class='form-group'> 
										<label  class='col-lg-2'>Title:</label> 
										<div class='col-lg-9'> 
										  <input type='text' class='form-control main_content' 
										  value= '{$result->title}'  id='title_$language->label'  autocomplete='off'> 
										</div> 
									  </div> 
									  
									 
									   
									  
									  
									 
								</div>"; 
								// $type_format= $result->type_format;
                                $serial_tabs_content++; 
                            } 
                          ?> 

                          <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                            <div class='form-group'> 
                    <label  class='col-lg-2'>Format:</label> 
                    <div class='col-lg-9'> 
                      <input type='text' class='form-control main_content' id='format' 
                      value="<?php echo $result->type_format?>" autocomplete='off'> 
                    </div> 
                    </div> 
                          </div> 
                        </div> 

				</section>   
				  </div> 
				
			<div class="form-group"> 
			  <div class="col-lg-offset-2 col-lg-4"> 
				<button type="submit" class="btn btn-info" id="submit">Save</button> 
				<button type="reset" class="btn btn-default">Reset</button>
				<div id="loading_data"></div> 
			  </div> 
			</div> 
		  </form> 
		</div> 
	  </div> 
	</section> 
  </aside> 
 
          </form> 
          </div> 
        </section> 
      </div>
</div> 
<!-- page end-->  
</section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
