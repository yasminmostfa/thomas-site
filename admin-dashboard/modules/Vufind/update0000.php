 
<?php  
	require_once("../layout/initialize.php"); 
	
	$data = vufindFormat::find_by_id($_GET['id']);
 
	
	//check id access    
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_vufind.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  



 <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4> General Settings Module</h4> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Update Format</div> 
              <div class="panel-body">  
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php?id=<?php echo $data->id;?>"> 
                  <input type="hidden" id="process_type" value="update"> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Title:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="title" placeholder="title"  value="<?php echo $data->title;?>"autocomplete="off"> 
                    </div> 
                  </div> 
                   <div class="form-group"> 
                    <label  class="col-lg-3 ">format:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="format" value="<?php echo $data->type_format;?>" placeholder="format" autocomplete="off"> 
                    </div> 
                  </div> 
                 
                  
                  
                  
                 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
    <?php require_once("../layout/footer.php");?>