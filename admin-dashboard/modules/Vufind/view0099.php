<?php  
	require_once("../layout/initialize.php"); 
	// define user class 
	$all_data = new vufindFormat();
	$all_data = $all_data->find_all();
	
 
	// delete record check 
    if(isset($_GET["id"]) && $_GET["task"]=="delete"){ 
			$id = $_GET['id']; 
			// array of all tables in databese related to user 
			$all_related_database_tables = array( 
			'posts ,page,event,product'=>'nodes','profile'=>'profile','modules access'=>'profile_modules_access','pages access'=>'profile_pages_access', 
			'menu qroup'=>'structure_menu_group','menu link'=>'structure_menu_link','categories ,tages or authors'=>'taxonomies'); 
			// array to add the tables that relation with user want delete 
			$relation_tables = array(); 
			foreach($all_related_database_tables as $key=>$table_name){ 
				$sql = " SElECT *  from {$table_name}  where inserted_by='$id' "; 
				$result = $database->query($sql); 
				$result_number = $database->mysql_num_rows($result); 
				  if($result_number != 0){ 
					array_push($relation_tables ,$key); 
				  } 
			} 
			$count_relation_tables = count($relation_tables); 
			if($count_relation_tables  == 0){  
				redirect_to("data_model/delete.php?task=delete&id=$id");	 
			} 
		} 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Vufind Module</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading"> View Formats </header> 
      <br> 
      <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"
      <li class="icon-plus-sign"></li>Add Format </button> 
     <br> 
      <div class="panel-body"> 
       
        <div class="adv-table editable-table ">  
          <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
          <div class="space15"></div> 
          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th>title</th> 
                <th>format</th> 
                <th>action</th>
                
              </tr> 
            </thead> 
            <tbody> 
              <?php   
				  $serialize = 1; 
				  foreach($all_data as $record){ 
					  echo "<tr> 
					  <td>{$serialize}</td> 
					  <td>{$record->title}</td> 
					  <td>{$record->type_format}</td> 
					  ";
					$opened_module_view_menu_link= 'Vufind/view'; 
					$opened_module_add_menu_link = 'Vufind/insert'; 
						//full info 
					
					
					 
					
					echo "						   
					<td> <a href='update.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
			data-original-title='Edit' >  <i class='icon-edit'></i></a>  <a href='data_model/delete.php?task=delete&id={$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips' data-placement='top' data-original-title='Delete'> 
			 <i class='icon-remove'></i></a>"; 
					
			 echo "</td>		 
					   
			 </tr> 
			     <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
								  <div class='modal-dialog'> 
									  <div class='modal-content'> 
										  <div class='modal-header'> 
											  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
											  <h4 class='modal-title'>Delete</h4> 
										  </div> 
										  <div class='modal-body'> 
										   <p> Are you sure you want delete  $record->title?</p> 
										  </div> 
										  <div class='modal-footer'> 
											  <button class='btn btn-warning' type='button'  
											  onClick=\"window.location.href = 'view.php?task=delete&id={$record->id}'\"/>Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
										  </div> 
									  </div> 
								  </div> 
							  </div>"; 
  				$serialize++; 
				  } 
				  ?> 
            </tbody> 
          </table> 
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 
<div style="display:none"> 
  <div class="" id="filter_form"  style=" width:auto; height:500px; "> 
    <aside class="col-lg-12"> 
      <section> 
        <div class="panel"> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_crud" method="get" action="view.php"> 
              <div class="form-group"> 
                <label  class="col-lg-3">Profile:</label> 
                <div class="col-lg-7"> 
                  <select class="form-control" name="profile"> 
                    <option value="" > Select Profile </option> 
                    <?php 
                      	$profiles = Profile::find_all(); 
					 	 foreach($profiles as $profile_filter){ 
							 echo "<option value='$profile_filter->id'"; 
							if(isset($_GET["profile"])){ 
								if($profile_filter->id == $profile){ 
									echo "selected"; 
								}    
							 } 
							echo " > $profile_filter->title</option>"; 
						 }?> 
                  </select> 
                </div> 
              </div> 
              <div class="form-group"> 
                  <label class=" col-lg-3">Insert Date</label> 
                  <div class="col-lg-8"> 
                     <span class="list-group-item-text">&nbsp;&nbsp;<strong>From</strong></span> 
                      <input type="text" class="form-control " name="from"  id="start" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>"> 
                      <br> 
                      <br> 
                      <span class="list-group-item-text">&nbsp;&nbsp;<strong>To</strong></span> 
                      <input type="text" class="form-control" name="to" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>" id="end"> 
                     
                    <span class="help-block">Select date range</span> </div> 
                </div> 
              <div class="form-group"> 
                <label  class="col-lg-2">Created By:</label> 
                <div class="col-lg-3"> 
                  <?php  $users = Users::find_all();?> 
                  <select multiple="multiple" class="multi-select s" id="my_multi_select1" name="inserted_by[]"> 
                    <?php  
						  foreach ($users as $user) :  
							if(in_array($user->id , $inserted_by)){   
								echo "<option value={$user->id} selected>{$user->user_name}</option>"; 
							}else{ 
								echo "<option value={$user->id} >{$user->user_name}</option>"; 
							} 
					  	 ?> 
                    </option> 
                    <?php endforeach;?> 
                  </select> 
                </div> 
              </div> 
              <div class="form-group"> 
                <div class="col-lg-offset-2 col-lg-10"> 
                  <button type="submit" class="btn btn-info" id="submit">Execut</button> 
                  <button type="reset" class="btn btn-default" onClick="window.location.href = 'view.php'">Clear</button> 
                </div> 
              </div> 
            </form> 
          </div> 
        </div> 
      </section> 
    </aside> 
  </div> 
</div> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>