<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Advertisements.php');
require_once('../../../../classes/AdvertisementContent.php');
require_once('../../../../classes/Localization.php');

//check log in 
if($session->is_logged() == false){
	redirect_to("../../index.php");
}
//send notifiction by json 
header('Content-Type: application/json');
//retrieve all available languages 
$languages = Localization::find_all('label','asc'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){
	//validite required required
   $required_fields = array('status'=>"- Insert status");
   $check_required_fields = check_required_fields($required_fields);
	 if(count($check_required_fields) == 0){
	  $add = new Advertisements();
	  $add->node_id = $_POST['path'];
	 if(!empty($_POST['imageVal'])){
		  $current_file = $_POST['imageVal'];
		  $parts = explode('/',$current_file);
		  $image_cover = $parts[count($parts)-1];
		  $add->image_cover = $image_cover;
	   }
	   $add->status = $_POST["status"];
	    $insert = $add->insert();
		$inserted_id = $add->id;
		 
		  if($insert){
		    foreach($languages as $language){ 
				  $add_adv_content = new AdvertisementContent(); 
				  $add_adv_content->adv_id = $inserted_id; 
				  $add_adv_content->title = $_POST['main_content']['title_'.$language->label]; 
				  $add_adv_content->content = $_POST['main_content']['content_'.$language->label];
				  $add_adv_content->lang_id = $language->id;
				  $insert_adv_content = $add_adv_content->insert(); 
		     } 
			  $data  = array("status"=>"work");
			  echo json_encode($data);
		  }else{
			  $data  = array("status"=>"error");
			  echo json_encode($data);
		  }
	 }else{
		//validation error
		$comma_separated = implode("<br>", $check_required_fields);
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated);
		echo json_encode($data);
	}		
}
//close connection
if(isset($database)){
	$database->close_connection();
}

?>