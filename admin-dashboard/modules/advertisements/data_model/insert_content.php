<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/AdvertisementContent.php');
if(!empty($_POST["task"]) && $_POST["task"] == "insert_info"){
	$define_class = new AdvertisementContent();
	$id = $_POST["record"];
	$lang = $_POST["lang"];
	$adv_id = $_POST["adv_id"];
	$adv_info = $define_class->get_adv_content($adv_id,$lang);
	if(!empty($adv_info)){
		$define_class->id  = $id;
		$define_class->adv_id = $adv_id;
		$define_class->title_one = $_POST["title_one"];
		$define_class->title_two = $_POST["title_two"];
		$define_class->lang_id = $lang;
		$update_info = $define_class->update();
		if($update_info){
			$data  = array("status"=>"work");
			echo json_encode($data);
		}else{
			$data  = array("status"=>"error");
			echo json_encode($data);
		}
	}else{
		$define_class->adv_id = $adv_id;
		$define_class->title_one = $_POST["title_one"];
		$define_class->title_two = $_POST["title_two"];
		$define_class->lang_id = $lang;
		$insert_info = $define_class->insert();
		if($insert_info){
			$data  = array("status"=>"work");
			echo json_encode($data);
		}else{
			$data  = array("status"=>"error");
			echo json_encode($data);
		}
		
	}
	
	
}




//close connection
if(isset($database)){
	$database->close_connection();
}

?>