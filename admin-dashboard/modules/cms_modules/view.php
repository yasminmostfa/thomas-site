<?php  
	require_once("../layout/initialize.php");	 
	$modules = CMSModules::find_all_by_custom_filed('sid', 0, 'sorting', 'ASC'); 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_module.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Modules Builder</h4> 
      <!-- page start--> 
      <section class="panel"> 
        <header class="panel-heading">View Modules </header> 
        <br> 
         <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"> 
        <li class="icon-plus-sign"></li> Add New Modules Or Page</button> 
        <br> 
        <br> 
        <table class="table table-striped table-advance table-hover"> 
          <thead> 
            <tr> 
              <th>#</th> 
              <th>Name</th> 
              <th>Type</th> 
              <th>Action</th> 
            </tr> 
          </thead> 
          <tbody> 
            <?php   
				  $serialize = 1; 
				  foreach($modules as $module){ 
					  echo "<tr> 
					  <td style='width:10px'>{$serialize}</td> 
					  <td><span style='font-weight:bold'>"; echo ucwords($module->title)."<span></td> 
					  <td>{$module->type}</td> 
					   <td>"; 
						$opened_module_update = 'cms_modules/update'; 
						$opened_module_delete = 'cms_modules/delete'; 
						//update 
						if(!in_array($opened_module_update, $user_allowed_page_array)){ 
							 echo " <a href='' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
							   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
						}else{ 
							if($user_profile->global_edit == 'all_records' || $record->inserted_by == $user_data->user_name){ 
								echo " <a href='update.php?id={$module->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
								data-original-title='Edit' >  <i class='icon-edit'></i></a>"; 
							}else { 
							   echo "<a href='' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
							   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
							}	 
						} 
					//delete	 
					if(!in_array($opened_module_delete, $user_allowed_page_array)){ 
						 echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
						 data-original-title='Delete' disabled ><i class='icon-remove'></i></a>"; 
					}else{ 
						if($user_profile->global_delete == 'all_records' || $record->inserted_by == $user_data->user_name){ 
							 echo " <a href='#my{$module->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips' data-placement='top' 
							  data-original-title='Delete'> <i class='icon-remove'></i></a>"; 
						}else{ 
							 echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='Delete' 
							  disabled > <i class='icon-remove'></i></a>"; 
						}		 
					} 
					  echo "</td> 
				  </tr>"; 
				  echo   "<div class='modal fade' id='my{$module->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
									  <div class='modal-dialog'> 
										  <div class='modal-content'> 
											  <div class='modal-header'> 
												  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
												  <h4 class='modal-title'>Delete</h4> 
											  </div> 
											  <div class='modal-body'> 
											   <p> Are you sure you want delete  $module->title??</p> 
											  </div> 
											  <div class='modal-footer'> 
												  <button class='btn btn-warning' type='button'  
												  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$module->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
											  </div> 
										  </div> 
									  </div> 
								  </div>"; 
					  $pages = CMSModules::find_all_by_custom_filed('sid', $module->id, 'sorting', 'ASC'); 
					  foreach($pages as $page){ 
						  echo "<tr> 
						  <td style='width:auto'></td> 
						  <td><span>&nbsp;&nbsp;&lfloor;"; echo ucwords($page->title)."</span></td> 
						  <td>{$page->type}</td> 
						  <td>"; 
						  //update 
						 if(!in_array($opened_module_update, $user_allowed_page_array)){ 
							 echo " <a href='' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
							   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
						}else{ 
							if($user_profile->global_edit == 'all_records' || $page->inserted_by == $user_data->id){ 
								echo "<a href='update.php?id={$page->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
							data-original-title='Edit' >  <i class='icon-edit'></i></a>"; 
							}else { 
								echo "<a href='update.php?id={$page->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
								data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
							} 
						} 
						//delete	 
						if(!in_array($opened_module_delete, $user_allowed_page_array)){ 
							echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='Delete' 
							 disabled ><i class='icon-remove'></i></a>";							 
						}else{ 
							if($user_profile->global_delete == 'all_records' || $page->inserted_by == $user_data->id){ 
								echo " <a href='#my{$page->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips'  
								data-placement='top' data-original-title='Delete'  >  <i class='icon-remove'></i></a>"; 
							}else{ 
								echo " <a href= '#myModal'  class='btn btn-primary btn-xs tooltips'  
								data-placement='top' data-toggle='tooltip' data-original-title='Delete'  disabled >  <i class='icon-remove'></i></a>"; 
							} 
						} 
					  echo "</td> 
				  </tr>"; 
				  //delete dialoge  
				  echo   "<div class='modal fade' id='my{$page->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
									  <div class='modal-dialog'> 
										  <div class='modal-content'> 
											  <div class='modal-header'> 
												  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
												  <h4 class='modal-title'>Delete</h4> 
											  </div> 
											  <div class='modal-body'> 
											   <p> Are you sure you want delete  $page->title??</p> 
											  </div> 
											  <div class='modal-footer'> 
												  <button class='btn btn-warning' type='button'  
												  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$page->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
											  </div> 
										  </div> 
									  </div> 
								  </div>"; 
					  } 
				  $serialize++; 
				  } 
				?> 
          </tbody> 
        </table> 
      </section> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>