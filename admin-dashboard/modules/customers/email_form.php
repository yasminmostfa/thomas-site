<!DOCTYPE html>
<html lang="en">
  <head>
    <?php header('Content-Type: text/html; charset=utf-8'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="../../img/favicon.png">

    <title>Language Switch Bar</title>    
     <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<!--table css-->
    <link rel="stylesheet" href="../../assets/data-tables/DT_bootstrap.css" />
    <!-- Custom styles for this template -->
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../css/style-responsive.css" rel="stylesheet" />
    <link href="../../../../assets/dropzone/css/dropzone.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-datetimepicker/css/datetimepicker.css" />  
    <link rel="stylesheet" type="text/css" href="../../assets/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="jquery-te-1.4.0.css" />  
    <script src="../../js/jquery.js"></script>
     <script type="text/javascript" src="../../js-crud/crud_send_mail.js"></script>
     <?php include("../../assets/texteditor4/head.php"); ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="../../js/html5shiv.js"></script>
      <script src="../../js/respond.min.js"></script>
    <![endif]-->
    
    
   
    
    
  </head>
  <body>
  
      


<!--sidebar start-->



<!--sidebar end--> 

<!--main content start-->


  <section class="wrapper site-min-height" style="margin-top:0px">
    <h4>Social Email </h4>
    
    <!-- page start-->
    
    <div class="row">
      <aside class="col-md-8">
        <section>
          <div class="panel">
            <div class="panel-heading"> Email Form </div>
            <div class="panel-body">
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/send_mail.php">
                <input type="hidden" id="process_type" value="send">
              
                
                    <input type="hidden" class="form-control" id="form" value="thewhatnews@thewhatnews.net">
                  
               
                <div class="form-group">
                  <label  class="col-lg-2">Subject:</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" id="subject"  >
                  </div>
                </div>
                
               
                <div class="form-group">
                  <label class="col-lg-2">Body:</label>
                  <div class="col-md-8">
                    <textarea class="form-control" name="editor1" rows="6" id="editor1" ></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info" id="submit">Save</button>
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <div id="loading_data"></div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </aside>
      
      
      
    </div>
    
    <!-- page end--> 
    
  </section>


<!--main content end--> 

<!--footer start--> 


	

<!-- js placed at the end of the document so the pages load faster -->

 
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.scrollTo.min.js"></script>
    <script src="../../js/jquery.nicescroll.js" type="text/javascript"></script>

    <script src="../../js/jquery-ui-1.9.2.custom.min.js"></script>
    <script class="include" type="text/javascript" src="../../js/jquery.dcjqaccordion.2.7.js"></script>

  <!--custom switch-->
  <script src="../../js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="../../js/jquery.tagsinput.js"></script>
  <!--custom checkbox & radio-->
  <script type="text/javascript" src="../../js/ga.js"></script>

  <script type="text/javascript" src="../../assets/bootstrap-datepicker/../../js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="../../assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="../../assets/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="../../assets/bootstrap-colorpicker/../../js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="../../assets/ckeditor/ckeditor.js"></script>

  <script type="text/javascript" src="../../assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script src="../../js/respond.min.js" ></script>


  <!--common script for all pages-->
    <script src="../../js/common-scripts.js"></script>

  <!--script for this page-->
  <script src="../../js/form-component.js"></script>
    <!--<script src="../../js/jquery.js"></script>-->

  </body>

</html>
