<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$define_class = new CustomerInfo(); 
		$record_info = $define_class->customer_data($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		}
	}else{ 
		redirect_to("view.php");	 
	} 
	//get customer points 
	
	
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Registerd Users Module</h4> 
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading">User : <?php echo $record_info->full_name() ;  ?></div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <section class="panel"> 
                  <header class="panel-heading tab-bg-dark-navy-blue "> 
                    <ul class="nav nav-tabs"> 
                      <li class="active"> <a data-toggle="tab" href="#main_info"><strong>User Main Info</strong></a> </li> 
                      
                     </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_info" class="tab-pane active"> 
                         <div class="form-group"> 
                          <label  class="col-lg-2 " > First Name:</label> 
                          <div class="col-lg-8"> 
                           <?php echo $record_info->first_name ?>
                          </div> 
                        </div>
                        <div class="form-group"> 
                          <label  class="col-lg-2 " > Last Name:</label> 
                          <div class="col-lg-8"> 
                            <?php echo $record_info->last_name ?>
                          </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Email:</label> 
                          <div class="col-lg-8"> 
                            <?php echo $record_info->email ?>
                          </div> 
                        </div> 
                        
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">City :</label> 
                          <div class="col-lg-8"> 
                           <?php echo $record_info->city ?>
                          </div> 
                          </div> 
                          <div class="form-group"> 
                          <label  class="col-lg-2 ">Area :</label> 
                          <div class="col-lg-8"> 
                           <?php echo $record_info->area ?>
                          </div> 
                          </div>

                           <div class="form-group"> 
                          <label  class="col-lg-2 ">Street :</label> 
                          <div class="col-lg-8"> 
                           <?php echo $record_info->street ?>
                          </div> 
                          </div>

                          <div class="form-group"> 
                          <label  class="col-lg-2 ">Birthday :</label> 
                          <div class="col-lg-8"> 
                           <?php echo $record_info->birth_date ?>
                          </div> 
                          </div>



                                 
                      </div>
                       
                           
                  </div> 
                </section> 
                <div class="form-group"> 
          <div class="col-lg-offset-2 col-lg-10"> 
            <button type="button" class="btn btn-info" onClick="window.location.href = 'update.php?id=<?php echo $record_id?>'" ><i class="icon-edit-sign"></i> Update </button> 
          </div> 
        </div>
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading tab-bg-dark-navy-blue"> 
            <ul class="nav nav-tabs"> 
            <li class=" center-block active" ><a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"> </i> <strong> Account Options</strong> </a> </li> 
              <li class=" center-block " ><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"> </i> <strong> Entry Info</strong> </a> </li> 
               
             
            </ul> 
          </header> 
          <div class="panel-body"> 
            <div id="list_info"> 
              <ul> 
                <div class="tab-content"> 
                  <div id="op1" class="tab-pane  "> 
                    <li><span style="color:#428bca">>  Registration Date:</span> <?php echo $record_info->registeration_date?></li> 
                    </div> 
                    <div id="op2" class="tab-pane active  "> 
                    <li><span style="color:#428bca">> Account Status:</span> <?php echo $record_info->account_status?></li> 
                    
                    </div>
                   
                </div> 
              </ul> 
            </div> 
          </div> 
        </section> 
         
      </div>
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
