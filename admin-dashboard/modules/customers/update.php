<?php  
require_once("../layout/initialize.php"); 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	$record_info = CustomerInfo::find_by_id($record_id);
	
	
    
	
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php");	 
	} 
	if($user_profile->global_edit != 'all_records'){ 
	  redirect_to('view.php');	 
	}
}else{ 
	redirect_to("view.php");	 
} 
require_once("../layout/header.php"); 
require_once("../../assets/texteditor4/head.php"); 	 
?> 
<script type="text/javascript" src="../../js-crud/customer.js"></script> 
<!-- <script type="text/javascript" src="../../js-crud/customer_address.js"></script> --> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
<section class="wrapper site-min-height"> 
<h4>Registered Users Module</h4> 
<!-- page start--> 
<div class="row"> 
  <aside class="col-lg-8"> 
	<section> 
	  <div class="panel"> 
		<div class="panel-heading"> Edit User : <?php echo  $record_info->full_name() ; ?> </div> 
		<div class="panel-body"> 
		  <section class="panel"> 
			<header class="panel-heading tab-bg-dark-navy-blue "> 
			  <ul class="nav nav-tabs"> 
				<li class="active"> <a data-toggle="tab" href="#main_info"><strong>User Main Info</strong></a> </li> 
				
               </ul> 
			</header> 
			<div class="panel-body"> 
			  <div class="tab-content"> 
				<div id="main_info" class="tab-pane active"> 
                 <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
					<input type="hidden" id="process_type" value="update"> 
					<input type="hidden" id="record" value="<?php echo $record_info->id?>"> 
                    <div class="form-group"> 
                          <label  class="col-lg-2 " > First Name:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="first_name"  autocomplete="off" value="<?php echo $record_info->first_name ?>"> 
                          </div> 
                        </div>
                        <div class="form-group"> 
                          <label  class="col-lg-2 " > Last Name:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="last_name" placeholder=" " autocomplete="off"  value="<?php echo $record_info->last_name ?>"> 
                          </div> 
                        </div> 
                         
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Email:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="email" placeholder=" " autocomplete="off"  value="<?php echo $record_info->email ?>"> 
                          </div> 
                        </div> 

                          <div class="form-group"> 
                          <label  class="col-lg-2 ">Password:</label> 
                          <div class="col-lg-8"> 
                            <input type="password" class="form-control" id="password" placeholder=" " autocomplete="off" value=""> 
                          </div> 
                          </div>
                          
                          

                         <div class="form-group"> 
                          <label  class="col-lg-2 " > City :</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control start" id="city" placeholder=" " autocomplete="off" value="<?php echo $record_info->city ?>"> 
                          </div> 
                        </div>

                         <div class="form-group"> 
                          <label  class="col-lg-2 " > Area :</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control start" id="area" placeholder=" " autocomplete="off" value="<?php echo $record_info->area ?>"> 
                          </div> 
                        </div>


                         <div class="form-group"> 
                          <label  class="col-lg-2 " > Street :</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control start" id="street" placeholder=" " autocomplete="off" value="<?php echo $record_info->street ?>"> 
                          </div> 
                        </div> 


                         <div class="form-group"> 
                          <label  class="col-lg-2"> Gender :</label> 
                          <div class="col-lg-8"> 
                            <select name="" id="gender" class="form-control">
                              
                              <option name = "gn" value="Male" <?php if($record_info->gender == 'Male'){echo 'selected' ; } ?>>Male</option>
                              <option name = "gn" value="Female" <?php if($record_info->gender == 'Female'){echo 'selected' ; } ?>>Female</option>
                            </select> 
                          </div> 
                        </div> 



                        <div class="form-group"> 
                          <label  class="col-lg-2 " > Birthday:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control start" id="birthday" placeholder=" " autocomplete="off" value="<?php echo $record_info->birth_date ?>"> 
                          </div> 
                        </div>
                        
                         
                       
                       
                       <div class="form-group"></div>
                      <div class="form-group"> 
                        <div class="col-lg-offset-2 col-lg-8"> 
                          <button type="submit" id="submit" class="btn btn-info">Save</button> 
                          <button type="button" class=" btn btn-info "   
                          onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                         
                        </div> 
                </div>
                 <div id="loading_data"></div> 
                  </form>
                 
			  </div> 
			</div> 
		  </section> 
		</div> 
	  </div> 
	</section> 
  </aside>
  <div class="col-lg-4"> 
      <section class="panel panel-primary"> 
          <header class="panel-heading"> Account Options </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form"> 
              <div class="form-group"> 
                <label class="col-lg-5">Account Status</label> 
                <div class="col-lg-6">
                   <select class="form-control" id="account_status">
                     <option>Select Status</option>
                     <option value="verified"  <?php if($record_info->account_status == 'verified'){echo "selected";} ?>>Verified</option>
                     <option value="not_verified" <?php if($record_info->account_status == 'not_verified'){echo "selected";} ?>>Not Verified</option>
                      <option value="blocked"  <?php if($record_info->account_status == 'blocked'){echo "selected";} ?>>Blocked</option>
                   </select>
                </div> 
              </div> 
              
                  
            </form> 
          </div> 
        </section>
        </div> 
</div> 
<!-- page end-->  
</section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>


// <script>
  
// var password = document.getElementById('password') ; 
// var password_compare = document.getElementById('password_compare') ;


// function ComparePasswords(){

//   if(password.value != password_compare.value ){
//      password_compare.setCustomValidity("Password confirmation  is incorrect ");
//   }else{
//      password_compare.setCustomValidity("");
//   }


//   function ValidatePassword (){

//     if(password == ''){
//       password.setCustomValidity("Password Cannot Be Empty");
//     }else{
//       password.setCustomValidity("");
//     }
//   }


//     password.onfocus = ValidatePassword ; 
//     password.onchange = ValidatePassword ; 
//     password.onkeyup = ComparePasswords ;
//     password_compare.onchange = ComparePasswords ; 

// } 


 </script>