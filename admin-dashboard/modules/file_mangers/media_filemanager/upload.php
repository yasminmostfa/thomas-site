<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <link rel="shortcut icon" href="../../img/favicon.png"> 
    
    <!-- Bootstrap core CSS --> 
    <link href="../../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
     <link rel="stylesheet" type="text/css" href="../../../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 
    <link href="../../../assets/dropzone/css/dropzone.css" rel="stylesheet"/> 
      <!-- Custom styles for this template --> 
    <link href="../../../css/style.css" rel="stylesheet"> 
    <link href="../../../css/style-responsive.css" rel="stylesheet" /> 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries --> 
    <!--[if lt IE 9]> 
      <script src="js/html5shiv.js"></script> 
      <script src="js/respond.min.js"></script> 
    <![endif]--> 
  </head> 
   <?php  
   $directory_title= $_GET['title']; 
	$path = "../../../../media-library/"; 
	$records = array_diff(scandir($path.$directory_title), array('..', '.'));  
	?> 
    <!-- Custom styles for this template --> 
     
 </head> 
 <body style="background:#fff"> 
  
   
          <section class="panel"> 
              <header class="panel-heading"> 
                  Upload File to <?php echo $directory_title;?> 
              </header> 
              <br /> 
              <a href="view_media_files.php?title=<?php echo $directory_title;?>" class="btn btn-info"sstyle="margin-left:15px" >Back</a> 
              
              <div class="panel-body"> 
                  <form action="data_model/upload.php?title=<?php echo $directory_title?>" class="dropzone" id="my-awesome-dropzone"></form> 
              </div> 
               
          </section> 
      
 </body> 
</html> 
 <script src="../../../js/jquery.js"></script> 
    <script src="../../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../../assets/dropzone/dropzone.js"></script> 
    <script src="../../../js/respond.min.js" ></script> 
    <script type="text/javascript" src="../../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
      <script type="text/javascript" src="../../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
      <script> 
	<!-- for update model--> 
$(document).ready(function() { 
	$("#back").fancybox({ 
		        'width'		: '80%', 
				'height'	   : '80%', 
				'autoScale'	: true, 
				'transitionIn' : 'none', 
				'transitionOut': 'none', 
				'type'		 : 'iframe' 
		 
		 
	}); 
}); 
</script> 
    <!--common script for all pages--> 
    <script src="../../../js/common-scripts.js"></script> 
