<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <!-- Bootstrap core CSS --> 
    <link href="../../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link rel="stylesheet" type="text/css" href="../../../assets/gritter/css/jquery.gritter.css" /> 
    <link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
     <link href="../../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" /> 
    <link href="../../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" href="../../../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 
     <link href="../../../css/style.css" rel="stylesheet"> 
    <link href="../../../css/style-responsive.css" rel="stylesheet" /> 
   <?php  
    $path = "../../../../media-library/"; 
	$records = array_diff(scandir($path), array('..', '.'));  
     
    
	?> 
    <!-- Custom styles for this template --> 
    <style> 
	.panel ul li { 
		display:inline-block; 
		padding:0 25px; 
		text-align:center;	 
		-webkit-transition:all .3s linear; 
		transition:all .3s linear; 
	} 
	 
	.panel ul li:hover a img { 
		border:1px solid #87b4df; 
		} 
	 
	</style> 
 </head> 
 <body style="background:#fff"> 
  <section class="panel"> 
   <header class="panel-heading">Media Directories </header> 
    
<div class="panel-body"> 
<ul> 
<?php  
$ser = 1; 
foreach($records as $record){ 
	if($record == 'slider'){ 
	 
	 
	?> 
<li><a href="view_media_files.php?title=<?php echo $record;?>" id="folder_img<?php echo $ser;?>"><img src="../../../img/folder.png"  style="width: 90px;height: 90px;"/> 
<br><?php echo $record?></a></li> 
<script> 
	<!-- for update model--> 
$(document).ready(function() { 
	$("folder_img<?php echo $ser;?>").fancybox({ 
		        'width'		: '80%', 
				'height'	   : '80%', 
				'autoScale'	: true, 
				'transitionIn' : 'none', 
				'transitionOut': 'none', 
				'type'		 : 'iframe' 
		 
		 
	}); 
}); 
</script> 
<?php  
$ser++; 
}}?> 
 </ul> 
      </div> 
     <!-- END JAVASCRIPTS --> 
      <script src="../../../js/jquery.js"></script> 
    <script src="../../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../../js/respond.min.js" ></script> 
     <script type="text/javascript" src="../../../assets/gritter/js/jquery.gritter.js"></script> 
    <script type="text/javascript" language="javascript" src="../../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
    <script type="text/javascript" src="../../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
      <script type="text/javascript" src="../../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
    <!--common script for all pages--> 
    <script src="../../../js/common-scripts.js"></script> 
    <script src="../../../js/gritter.js" type="text/javascript"></script> 
    
    </section> 
 </body> 
</html> 
