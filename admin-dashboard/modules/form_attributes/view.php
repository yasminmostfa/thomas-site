<?php
	require_once("../layout/initialize.php");
	if(isset($_GET['form_id'])){
	$form_id = $_GET['form_id'];
	$form_name = Forms::find_by_id($form_id);
   }else{
	 redirect_to('../forms/view.php');	
   }  
	$define_class= new FormAttributes();
	$define_class->enable_relation();
	$records = $define_class->form_attribute_data('sorting', 'ASC',null,$form_id);
	require_once("../layout/header.php");
?>  <!--header end-->
      <!--sidebar start-->
      
       <?php require_once("../layout/navigation.php");?>
       
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
         <section class="wrapper site-min-height">
          <h4>Forms</h4> 
              <section class="panel">
                 <header class="panel-heading">
                      View <?php echo $form_name->label;?> Attributes
                  </header>
                  <div class="panel-body">
                       <div class="space15"></div>
                            <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php?form_id='+<?php echo $form_id?>" 
                            <?php
							  $module_name = $opened_url_parts[count($opened_url_parts) - 2];
							  $opened_module_page_insert = $module_name.'/insert';
							  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){
								echo "disabled";
							  }
							  ?>>
                              
               <i class="icon-plus"></i>   Add New Form  Attributes  </button>
               <button type="button" class="btn btn-info" style="margin-left:15px" onClick="window.location.href = 'view_table.php?form_id='+<?php echo $form_id?>" ><i class="icon-list"></i>   View In Table</button>
                               <br/>
                               <br/>
                               
                               <div class="panel-body">
                           <form class="form-horizontal tasi-form" role="form" action=""  id="form_<?php echo $form_id?>">
                               <?php 
							   $input_array = array(1,2,3,8,9);
							   foreach($records as $record){
								    $att_values = $record->attribute_values;
								   if(!empty($att_values)){
									    $values = explode(",",$att_values);
									 }
								?>
                                   <div class="form-group">
                                     <label for="" class="col-lg-2 "><?php echo $record->attribute_label?>:</label>
                                      <div class="col-lg-8">
                                      <?php  if(in_array($record->attribute_id,$input_array)){?>
                                        <input type="<?php echo $record->attribute_type?>" class="form-control" 
                                        id="<?php echo $record->attribute_type.$record->id;?>" 
                                         name="<?php echo $record->attribute_type.$record->id;?>">
                                         
                                        <?php }elseif($record->attribute_id == 4){?>
                                         <textarea class="form-control"  id="<?php echo $record->attribute_type.$record->id;?>" 
                                         name="<?php echo $record->attribute_type.$record->id;?>"></textarea>
                                         
                                         <?php }else if($record->attribute_id == 7 ){
											 echo "<select class='form-control'id='$record->attribute_type$record->id'
											  name='$record->attribute_type$record->id'>";
											 foreach($values as $value){
										     echo "<option value='$value' >$value</option>";
											  }?>
										 </select>
                                         
                                         <?php }else if($record->attribute_id == 6){?> 
                                         <div class="checkboxes">
                                         <?php
										  foreach($values as $value){
								            echo "<label class='label_check'><input type='checkbox' value='$value' name='$value'>
								            $value</label>";
										  } ?>
                                         </div>
                                         
                                         <?php }else if($record->attribute_id == 5){
											  foreach($values as $value){?>
                                         <label class="checkbox-inline">
                                            <input type="radio" name="<?php echo $record->attribute_type.$record->id;?>" class="radio"
                                             value="<?php echo $value?>" />
                                            <?php echo $value;?></label>
										<?php  }} ?>
                                       </div>
                                     </div>
                                   <?php }?>
                                   
								 
                               </form>
                               </div>
                         
                     
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
     <?php require_once("../layout/footer.php");?>
