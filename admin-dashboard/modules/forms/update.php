<?php
	require_once("../layout/initialize.php");
	if(isset($_GET['id']) && is_numeric($_GET['id'])){
		$record_id = $_GET['id'];
		$record_info = Forms::find_by_id($record_id);
		//check id access
		if(empty($record_info->id)){
			redirect_to("view.php");	
		}
		if($user_profile->globel_edit != 'all_records' && $record_info->inserted_by != $session->user_id ){
		  redirect_to('view.php');	
	    }
	}else{
		redirect_to("view.php");	
	}
	require_once("../layout/header.php");
	include("../../assets/texteditor4/head.php"); 
?>
<script type="text/javascript" src="../../js-crud/crud_form.js"></script>
  <!--header end--> 
  <!--sidebar start-->
  <?php require_once("../layout/navigation.php");?>
  <!--sidebar end--> 
  <!--main content start-->
  <section id="main-content">
    <section class="wrapper site-min-height">
      <h4>Forms</h4> 
      <!-- page start-->
  <div class="row">
    <aside class="profile-info col-lg-8">
      <section>
        <div class="panel">
          <div class="panel-heading"> Edit  Form</div>
          <div class="panel-body">
            <form class="form-horizontal tasi-form" role="form" action="data_model/update.php"  id="form_crud">
                  <input type="hidden" id="process_type" value="update">
                   <input type="hidden" id="record" value="<?php echo $record_id?>">
                   <div class="form-group">
                     <label for="" class="col-lg-2 ">Name:</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" id="name" value="<?php echo $record_info->name?>">
                       </div>
                     </div>
                     <div class="form-group">
                     <label for="" class="col-lg-2 ">Label:</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" id="label"value="<?php echo $record_info->label?>" disabled="disabled">
                       </div>
                     </div>
                     <div class="form-group">
                     <label for="" class="col-lg-2 ">Email:</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" id="email" value="<?php echo $record_info->email_to?>">
                       </div>
                     </div>
                     <div class="form-group">
                        <label class="col-lg-2">Enable:</label>
                        <div class="col-lg-8">
                          <label class="checkbox-inline">
                            <input type="radio" name="enable" class="radio" value="yes" <?php if($record_info->enable=="yes") echo 'checked'?>>
                            Yes</label>
                          <label class="checkbox-inline">
                            <input type="radio" name="enable" class="radio" value="no" <?php if($record_info->enable=="no") echo 'checked'?>>
                            No</label>
                        </div>
                      </div>
                    
                        
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-6">
                            <button type="submit" id="submit" class="btn btn-info">Save</button>
                             <button type="submit" class="btn btn-default">Cancel</button>
                             <div id="loading_data"></div
                        ></div>
                    </div>
            </form>
          </div>
          </div>
          </section>
          </aside>
          
          </div>
      
      <!-- page end--> 
    </section>
  </section>
  <!--main content end--> 
  <!--footer start-->
  
  <?php require_once("../layout/footer.php");?>