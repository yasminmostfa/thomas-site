<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/GeneralSettings.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
// data json 
//header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	 
	//validite required required 
	$required_fields = array('title'=>"- Insert Title", 'email'=>'- Insert email', 'translate_lang'=>'- Insert  translate langage','front_lang'=>"- Insert front langage"); 
	$check_required_fields = check_required_fields($required_fields); 
    if(count($check_required_fields) == 0){ 
		  //update	 
		  $edit = GeneralSettings::find_by_id(1); 
		  $edit->title = $_POST["title"]; 
		  $edit->meta_key = $_POST["meta_key"]; 
		  $edit->site_url = $_POST["site_url"]; 
		  $edit->email = $_POST["email"]; 
		  $edit->description = $_POST["description"]; 
		  // $edit->enable_store = $_POST["enable_store"];
		  $edit->enable_website = $_POST["enable_website"]; 
		  $edit->offline_messages = $_POST["offline_messages"];
		  $edit->google_analitic = $_POST["google_analitic"]; 
		  $edit->time_zone_id = $_POST["time_zone"]; 
		  $edit->translate_lang_id = $_POST["translate_lang"]; 
		  $edit->front_lang_id = $_POST["front_lang"]; 
		  $edit->update_by = $session->user_id; 
		  $edit->last_update = date_now();	
		  $edit->vufind_main_url = $_POST['vufind_main'];
		  $edit->vufind_advanced_search = $_POST['vufind_advanced']; 
		  $update = $edit->update(); 
		  header('Content-Type: application/json'); 
		  if($update){ 
			  $data  = array("status"=>"work"); 
			  echo json_encode($data); 
		  }else{ 
			  $data  = array("status"=>"error"); 
			  echo json_encode($data); 
		  } 
 }else{ 
		//validation error 
		$comma_separated = implode("<br>", $check_required_fields); 
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		echo json_encode($data); 
	}	 
		 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>