<?php

require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
require_once('../../../../classes/HomePageLayout.php');
require_once('../../../../classes/parsingResult.php');

//check  session user  log in 
if ($session->is_logged() == false) {
    redirect_to("../../../index.php");
}

function insert_data(){
    //send notifiction by json 
if (!empty($_POST["task"]) && $_POST["task"] == "insert") {
    //validite required required 

    $required_fields = array('postion' => '- Select Postion', "plugin" => "- Select Plugin ");
    $check_required_fields = check_required_fields($required_fields);
    if (count($check_required_fields) == 0) {
        //insert data 
        $add = new HomePageLayout();
        $add->position = $_POST["postion"];
        $add->status = $_POST["status"];
        $add->title = $_POST["title"];
        $add->plugin = $_POST["plugin"];
        $add->header_title = $_POST["header_title"];
        $add->plugin_value = $_POST["plugin_value"];
        $add->link_en = $_POST['link_en'];
        $add->link_ar = $_POST['link_ar'];
        $add->link_limit = $_POST['limit'];
        $insert = $add->insert();
        $inserted_id = $add->id;
        
        if ($insert) {
//             $books = simplexml_load_file($trimmed_url . '&view=rss&skip_rss_sort=1');
//            $items = array();
//            for ($i = 0; $i < count($books->channel->item); $i++) {
//                $items[$i] = $books->channel->item[$i];
//                $dataParsing = new parsingResult();
//                $dataParsing->parsed_title = $items[$i]->title;
//                $dataParsing->publish_date = $items[$i]->pubDate;
//                $dataParsing->url = $items[$i]->link;
//                $dataParsing->link_id = $inserted_id;
//            $dataParsing->insert();}
            //return json  
            $data = array("status" => "work");
            echo json_encode($data);
           
            
        } else {
            $data = array("status" => "error");
            echo json_encode($data);
        }
    } else {
        //validation error 
        $comma_separated = implode("<br>", $check_required_fields);
        $data = array("status" => "valid_error", "fileds" => $comma_separated);
        echo json_encode($data);
    }
}
}

// get user profile   
$user_data = Users::find_by_id($session->user_id);
// get user profile data 
$user_profile = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block 
if ($user_profile->profile_block == "yes") {
    redirect_to("../../../index.php");
}
header('Content-Type: application/json');
// save url on ram 
$url_ar = $_POST['link_ar'];
$url_en = $_POST['link_en'];
$limit = $_POST['limit'];
$id = $_POST['id'];

// validate url  before saving 
if ($id  == 38 || $id == 39){
    if (!filter_var($url_ar, FILTER_VALIDATE_URL) === false  &&  !filter_var($url_en, FILTER_VALIDATE_URL) === false ) {
if ($limit != ''){
    insert_data();

}else{
     $data = array("status" => 'valid_error', 'fileds'=> "Limit number shouldnot be empty");
            echo json_encode($data);
}
} else {
    $data = array("status" => 'valid_error', 'fileds'=> "Url is not valid, Please check inserted url");
            echo json_encode($data);
}
    
}else{
    insert_data();
}


//close connection 
if (isset($database)) {
    $database->close_connection();
}
?>