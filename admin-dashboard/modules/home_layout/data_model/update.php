<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/HomePageLayout.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//send notifiction by json 
header('Content-Type: application/json');	 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//validite required required 
	$required_fields = array('postion'=>'- Select Postion',"plugin"=>"- Select Plugin ");  
    $check_required_fields = check_required_fields($required_fields); 
    if(count($check_required_fields) == 0){ 
			 
			$id = $_POST['record']; 
			$edit = HomePageLayout::find_by_id($id);
			$edit->position = $_POST["postion"] ; 
			$edit->status = $_POST["status"]; 
		    $edit->title = $_POST["title"]; 
			$edit->plugin = $_POST["plugin"]; 
			$edit->header_title = $_POST["header_title"];
			$edit->plugin_value = $_POST["plugin_value"];
          	$edit->link_en = $_POST['link_en'];
          	$edit->link_ar = $_POST['link_ar'];
            $edit->link_limit = $_POST['limit'];
			$update = $edit->update();
		
		
                        
			if($update){ 
				//return json  
				$data  = array("status"=>"work"); 
				echo json_encode($data); 
			}else{ 
				$data  = array("status"=>"error"); 
				echo json_encode($data); 
			} 
 	 }else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  } 
 }
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>