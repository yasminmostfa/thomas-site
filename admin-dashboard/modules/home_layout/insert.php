<?php 
 	require_once("../layout/initialize.php"); 
 	require_once("../layout/header.php"); 
	$home_page_plugin = Plugins::find_all_by_custom_filed("enable_index","yes");
 ?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start-->  
  <script src="../../js-crud/index_layout.js"></script> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Home Page Layout</h4> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel "> 
              <div class="panel-heading">Add New Postion </div> 
              <div class="panel-body"> 
                  <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php" method="post"> 
                      <input type="hidden" id="process_type" value="insert" name="task"> 
                 <div class="form-group"> 
                    <label  class="col-lg-2">Title:</label> 
                    <div class="col-lg-8"> 
                     <input type="text" class="form-control" id="title" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Postion:</label> 
                    <div class="col-lg-8"> 
                       <select class="form-control" id="postion"> 
                       <option value=""> Select Postion</option> 
                     <?php  
						for($i=1;$i<12;$i++){ 
							echo "<option value='$i'>$i";
							
							if($i>= 7){
								echo " - Left";
						    }else{
								echo "- Right";
							}
							echo"</option>"; 
							
							
						} 
						 
						?> 
                      </select> 
                     </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Status:</label> 
                    <div class="col-lg-8"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="status" class="radio " value="publish"  checked="checked" /> 
                        Publish</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio"  name="status" class="radio " value="draft" > 
                        Draft
                        </label> 
                    </div> 
                  </div> 
                  <div class="form-group  " > 
                    <label  class="col-lg-2">Plugin:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="plugin"> 
                      <option value="">Select Plugin</option>
                      <?php
					  foreach($home_page_plugin as $pulgin){
						  echo "<option value='$pulgin->id'>$pulgin->name</option>";
					  }
					  
					   ?>
                       
                      </select> 
                      
                    </div> 
                  </div> 
                   <div class="form-group"> 
                      <label  class="col-lg-2"> Header Title:</label> 
                      <div class="col-lg-8"> 
                          <input type="text" class="form-control" name="title" id="header_title" placeholder=" " autocomplete="off"> 
                      </div> 
                    </div>
                    <div class="form-group"> 
                      <label  class="col-lg-2">Plugin value:</label> 
                      <div class="col-lg-8"> 
                          <input type="text" class="form-control" name="plugin_value" id="plugin_value" placeholder=" " autocomplete="off"> 
                      </div> 
                    </div>
                <div class="form-group"> 
                      <label  class="col-lg-2">Vu-find URL :</label> 
                      <div class="col-lg-8"> 
                          <label>yes</label>
                          <input type="radio"  id="yes" name='add' value="yes"> 
                          
                        <label>no</label>
                          <input type="radio"  id="no" name='add' value="no" checked>
                      </div>
                      </div>
                      <div id="add_input1"></div>
                <div id="add_input"></div>
                      
                  </div> 
                   <div class="form-group"> 
                    <div class="col-lg-offs	et-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </section> 
        </aside> 
        <section class='col-lg-4'> 
        <img  src="index-layout.jpg" style="width: 350px;height: 481px;"/> 
         
         
        </section> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?> 
 
