<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Localization.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
$file_name=$_POST["file_name"]; 
$content=$_POST["content"]; 
$id=$_POST["record"]; 
$record_info=Localization::find_by_id($id); 
$path="../../../../localization/".$record_info->label."/"; 
if (file_exists($path.$file_name.".php")){ 
	  $file = fopen($path.$file_name.".php","w"); 
	  fwrite($file,$content); 
	  fclose($file); 
	  $data  = array("status"=>"work"); 
	  echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"notExist"); 
	    echo json_encode($data); 
     
	    
		 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>