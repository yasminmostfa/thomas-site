<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Localization.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	$path="../../../../localization/"; 
	//find record	 
	$find_lang = Localization::find_by_id($id); 
	//if there is record perform delete 
	//if there is no record go back to view 
	if($find_lang){ 
		$label=$find_lang->label; 
		$delete = $find_lang->delete(); 
		if($delete){ 
			//delete language folder 
			unlink($path.$label."/labels.php"); 
			unlink($path.$label."/messages.php"); 
			rmdir($path.$label); 
			//delete all realted  posts, events and pages 
			//delete  language posts  
			$sql_delete_posts = "DELETE FROM node WHERE lang_id = '{$id}'"; 
			$preform_delete_posts = $database->query($sql_delete_posts); 
			//delete  language events 
			$sql_delete_events = "DELETE FROM events WHERE lang_id = '{$id}'"; 
			$preform_delete_events = $database->query($sql_delete_events); 
			//delete  language pages 
			$sql_delete_pages = "DELETE FROM pages WHERE lang_id = '{$id}'"; 
			$preform_delete_pages = $database->query($sql_delete_pages); 
			 
	         
			 
			 
			redirect_to("../view.php"); 
		}else{ 
			redirect_to("../view.php"); 
		}	 
		//if there is no record go back to view 
	}else{ 
		redirect_to("../view.php");	 
	}  
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>