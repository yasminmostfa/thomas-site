<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$record_info = Localization::find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_localization.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
   <section id="main-content"> 
     <section class="wrapper site-min-height">  
       <h4>Localization Module </h4> 
        <!-- page start--> 
       <div class="row"> 
          <aside class="profile-info col-lg-8"> 
            <section> 
              <div class="panel"> 
                <div class="panel-heading"> Edit Languages</div> 
                <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php">
                 <input type="hidden" id="process_type" value="update"> 
                 <input type="hidden" id="record" value="<?php  echo $record_id;?>"> 
                         <div class="form-group"> 
                  <label  class="col-lg-2">Name:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="name" placeholder=" " value="<?php echo $record_info->name; ?>" autocomplete="off"/> 
                  </div> 
                </div> 
                  <div class="form-group"> 
                  <label  class="col-lg-2">Label:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="label" placeholder=" "  value="<?php echo $record_info->label; ?>" autocomplete="off"/> 
                  </div> 
                </div> 
                  <div class="form-group"> 
                  <label  class="col-lg-2">Sorting:</label> 
                  	<div class="col-lg-8"> 
                    	<input type="text" class="form-control" id="sorting" placeholder=" "  value="<?php echo $record_info->sorting; ?>" autocomplete="off" style="width:50px;"/> 
                  	</div> 
                </div> 
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info" id="submit">Save</button>
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <div id="loading_data"></div>
                  </div>
                </div>
                
               </form> 
              </div> 
             </div> 
           </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Publish Options: </header>  
            <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form"> 
                 <div class="form-group"> 
              <label class="col-lg-5">Status:</label> 
              <div class="col-lg-6"> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="shadow" class="radio" value="disable"<?php if($record_info->status == 'disable'){echo 'checked';}?>> 
                  disable</label> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="shadow" class="radio" value="active" <?php if($record_info->status == 'active'){echo 'checked';}?> > 
                  active</label> 
              </div> 
            </div> 
               </form> 
              </div> 
          </section> 
        </div>
       </div> 
       <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br> 
  <?php require_once("../layout/footer.php");?>