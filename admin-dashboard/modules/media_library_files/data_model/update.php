<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/MediaLibraryFiles.php'); 
require_once('../../../../classes/MediaLibraryDirectories.php'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//send json data 
	header('Content-Type: application/json');	 
	 
	//directory id 
	$dir_id  = $_POST['dir_id']; 
	$dir_info = MediaLibraryDirectories::find_by_id($dir_id); 
	//file id 
	$id = $_POST['record']; 
	 
	//check if file name changed 
	$file_name = $_POST["file_name"]; 
	if($file_name != $edit->name){ 
		//check file name change check if name already taken  
		$check_file_name = MediaLibraryFiles::find_all_by_custom_filed('name', $file_name); 
		if(count($check_file_name) > 0){ 
			$data  = array("status"=>"exist"); 
			echo json_encode($data); 
		}else{ 
			$path = "../../../../media-library/{$dir_info->title}/"; 
			$old_file_name = $path.$edit->name.'.'.$edit->type;			 
			$new_file_name = $path.$file_name.'.'.$edit->type;		 
			//change file name 
			rename($old_file_name, $new_file_name); 
			$edit->name = $file_name; 
		} 
	} 
	//perform update  
	$update = $edit->update(); 
	if($update){ 
		$data  = array("status"=>"work"); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	} 
	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>