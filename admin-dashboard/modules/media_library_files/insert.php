<?php 
	require_once("../layout/initialize.php"); 
	//get directory data 
	$directory_title = $_GET['title']; 
	require_once("../layout/header.php"); 
?> 
  <!--header end--> 
  <!--sidebar start--> 
         <?php require_once("../layout/navigation.php"); ?> 
  <!--sidebar end--> 
  <!--main content start--> 
  <section id="main-content"> 
<section class="wrapper site-min-height"> 
  <h4>Media Library Module</h4> 
          <!-- page start--> 
          <section class="panel"> 
              <header class="panel-heading"> 
                  Upload File to <?php echo $directory_title;?> 
              </header> 
              <div class="panel-body"> 
                  <form action="data_model/insert.php?title=<?php echo $directory_title?>" class="dropzone" id="my-awesome-dropzone"></form> 
              </div> 
          </section> 
          <!-- page end--> 
      </section> 
  </section> 
  <!--main content end--> 
  <!--footer start--> 
<?php require_once("../layout/footer.php"); ?>