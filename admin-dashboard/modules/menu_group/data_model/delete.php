<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/MenuGroup.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
// user log in profile details to chech authority 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_menu = MenuGroup::find_by_id($id); 
	//check global edit and delete authorization 
	if($user_profile->global_delete == 'all_records' || $find_post->inserted_by == $session->user_id){ 
		//if there is record perform delete 
		//if there is no record go back to view 
		if($find_menu){ 
			$delete = $find_menu->delete(); 
			if($delete){ 
				//delete all realted  menu links 
				//delete  menu links 
				$sql_delete = "DELETE structure_menu_link_content,structure_menu_link 
                               FROM structure_menu_link  
                               LEFT JOIN structure_menu_link_content ON structure_menu_link_content.link_id = structure_menu_link.id 
                               WHERE structure_menu_link.group_id= {$id}"; 
				$preform_delete = $database->query($sql_delete); 
				 
				redirect_to("../view.php"); 
			}else{ 
				redirect_to("../view.php"); 
			}	 
			//if there is no record go back to view 
		}else{ 
			redirect_to("../view.php");	 
		}  
	 }else{ 
			  redirect_to("../view.php");	 
	} 
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>