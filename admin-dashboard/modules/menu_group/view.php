<?php 
	require_once("../layout/initialize.php"); 
	$define_class = new MenuGroup(); 
	$define_class->enable_relation(); 
	$records =$define_class->menu_group_data('inserted_date', 'DESC'); 
	//localization  
	//get lang  
	$languages = Localization::find_all(); 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<script src="../../js-crud/menu_group.js"></script>
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4> Structure Menu Group  Module</h4> 
    <div class="row"> 
      <div class="col-lg-12"> 
        <section class="panel"> 
          <header class="panel-heading">Show Menu Groups</header> 
          <br/> 
          <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'" <?php 
		  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
		  $opened_module_page_insert = $module_name.'/insert'; 
		  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
			echo "disabled"; 
		  } 
		  ?>> 
          <li class="icon-plus-sign"></li> 
          Add New Menu </button> 
          <br> 
          <br/> 
            <div class="panel-body"><div class="adv-table editable-table ">
            <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th><i class=""></i>Title</th> 
                <th><i class=""></i>Alias</th> 
                <th><i class=""></i>created Date</th> 
                <th><i class=""></i>created by</th> 
                 <th>Link Option</th> 
                <th>Action</th> 
              </tr> 
            </thead> 
            <tbody  id="myTable"> 
              <?php  
			  $serialize = 1;  
			  foreach($records as $record){ 
				echo "<tr id=\"mg_{$record->id}\"> 
				   <td>{$serialize}</td> 
				   <td><a href='full_info.php?id={$record->id}'>{$record->title}</a></td> 
				   <td>{$record->alias}</td> 
				  <td>{$record->inserted_date}</td> 
				   <td>{$record->inserted_by}</td> 
					<td>"; 
					$opened_module_view_menu_link= 'menu_link/view'; 
					$opened_module_add_menu_link = 'menu_link/insert'; 
						//full info 
					if(!in_array($opened_module_view_menu_link, $user_allowed_page_array)){ 
						echo " <a href=''  class='btn btn-success btn-xs tooltips'  
						data-placement='top' data-toggle='tooltip' data-original-title='View menu link' disabled> <i class='icon-link'></i> </a> ";	 
					}else{ 
						echo " <a href='../menu_link/view.php?group_id={$record->id}'  class='btn btn-success btn-xs tooltips'  
						data-placement='top' data-toggle='tooltip' data-original-title='View menu link' > <i class='icon-link'></i> </a> ";	 
					} 
					if(!in_array($opened_module_add_menu_link, $user_allowed_page_array)){ 
						echo "<a href='' class='btn btn-success btn-xs tooltips'  
						data-placement='top' data-toggle='tooltip' data-original-title='Add Menu Link' disabled> <i class=' icon-plus'></i></a>&nbsp;";	 
					}else{ 
						echo "<a href='../menu_link/insert.php?group_id={$record->id}' class='btn btn-success btn-xs tooltips'  
						data-placement='top' data-toggle='tooltip' data-original-title='Add Menu Link' > <i class=' icon-plus'></i></a>&nbsp;";	 
					} 
					echo "</td>						   
					<td>"; 
					 $opened_module_view_menu_link= 'menu_link/view'; 
					 $opened_module_add_menu_link = 'menu_link/insert'; 
					 $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
					  include('../layout/btn_control.php'); 
			 echo "</td>		 
				  </tr> 
				  <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
                                  <div class='modal-dialog'> 
                                      <div class='modal-content'> 
                                          <div class='modal-header'> 
                                              <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                                              <h4 class='modal-title'>Delete</h4> 
                                          </div> 
                                          <div class='modal-body'> 
                                           <p> Are you sure you want delete  $record->title ??</p> 
                                          </div> 
                                          <div class='modal-footer'>
										    <input type='hidden' id='task_type' value='delete'> 
                                              <button class='btn btn-warning confirm_delete' id='{$record->id}' type='button'  data-dismiss='modal' /> Confirm</button> 
											   <button data-dismiss='modal' class='btn btn-default' type='button'>cancel</button> 
                                          </div> 
                                      </div> 
                                  </div> 
                              </div>"; 
			   $serialize++; 
             }?> 
              </tbody> 
             
          </table></div></div> 
        </section> 
      </div> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>