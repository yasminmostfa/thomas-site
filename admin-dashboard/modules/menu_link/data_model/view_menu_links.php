<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once("../../../../classes/MenuLink.php"); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
$menu_id = $_GET["m_id"]; 
$lang_id = $_GET["lang_id"]; 
$define_class = new MenuLink(); 
$define_class->enable_relation(); 
$define_class->view_inserted_menu_links(0,$menu_id,$lang_id);  
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>