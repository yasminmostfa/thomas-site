<?php 
require_once("../layout/initialize.php"); 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	//get menu link info 
	$define_class = new MenuLink(); 
	$record_info =  $define_class->find_by_id($record_id);  
   //Menu Group Info 
	$menu_data = MenuGroup::find_by_id($record_info->group_id); 
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php"); 
	}else{ 
		//get all lanaguges 
		$languages = Localization::find_all(); 
		 
		//define user class to get inserted/created by 
		$define_user_class = new users(); 
		$created_by = $define_user_class->find_by_id($record_info->inserted_by); 
		$updated_by = $define_user_class->find_by_id($record_info->update_by); 
	} 
}else{ 
	redirect_to("view.php"); 
} 
require_once("../layout/header.php");	 
?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
   <section class="wrapper site-min-height">  
    <h4>Menu link Module </h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
            <div class="panel-heading"> Add Menu link to <?php echo '"'.$menu_data->title.'"' ?> </div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <section class="panel"> 
                  <header class="panel-heading tab-bg-dark-navy-blue"> 
                    <ul class="nav nav-tabs"> 
                      <li class=" center-block active" > <a data-toggle="tab" href="#main_info" class="text-center"><strong>Main Info</strong></a></li> 
                      <li class=" center-block"> <a data-toggle="tab" href="#link_option" class="text-center"><strong>Link Option</strong></a></li> 
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_info" class="tab-pane active "> 
                        <section class="panel col-lg-9"> 
                          <header class="panel-heading tab-bg-dark-navy-blue "> 
                            <ul class="nav nav-tabs"> 
                              <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                            </ul> 
                          </header> 
                          <div class="panel-body"> 
                            <div class="tab-content"> 
                              <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language){ 
								//get data by language 
								$main_content = MenuLinkContent::get_link_content($record_id, $language->id); 
								echo " 
								<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'> 
									<div class='form-group'> 
										<label  class='col-lg-2'>Title:</label> 
										<div class='col-lg-9'>$main_content->title</div> 
									 </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Description:</label> 
										<div class='col-lg-9'>$main_content->description</div> 
									  </div> 
								</div>"; 
							$serial_tabs_content++; 
                            } 
                          ?> 
                            </div> 
                          </div> 
                        </section> 
                      </div> 
                      <div id="link_option" class="tab-pane "> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Sort:</label> 
                          <div class="col-lg-8" > <?php echo $record_info->sorting?> </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Paren Link:</label> 
                          <div class="col-lg-8" > 
                            <?php  
								if($record_info->parent_id == 0){ 
									echo "Root"; 
								}else{ 
									  $perant = MenuLinkContent::find_by_custom_filed('link_id',$record_info->parent_id); 
									  echo $perant->title; 
								}  
							?> 
                          </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Path Type:</label> 
                          <div class="col-lg-8" > <?php echo $record_info->path_type?> </div> 
                        </div> 
                        <?php  
						if($record_info->path_type == 'post' || $record_info->path_type == 'page' || $record_info->path_type == 'event' ){ 
							//get nodes path data 
		                    $link_path_data = Nodes::get_node_content($record_info->path,$general_setting_info->translate_lang_id); 
							echo "<div class='form-group'> 
                          <label  class='col-lg-2 '> Path:</label> 
                          <div class='col-lg-8' >$link_path_data->title</div> 
                        </div>"; 
						}else if($record_info->path_type == 'category'){ 
							 $link_path_data = Taxonomies::get_taxonomy_content($record_info->path,$general_setting_info->translate_lang_id); 
							echo "<div class='form-group'> 
                          <label  class='col-lg-2 '> Path:</label> 
                          <div class='col-lg-8' >$link_path_data->name</div> 
                        </div>"; 
							 
						}else{ 
							echo "<div class='form-group'> 
                          <label  class='col-lg-2 '> External path:</label> 
                          <div class='col-lg-8' >$record_info->external_path</div> 
                        </div>"; 
							 
						}	 
						 
						?> 
                         
                      </div> 
                    </div> 
                  </div> 
                </section> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > 
                    <li class="icon-edit-sign"></li> 
                    Update </button> 
                    <button type="button" class="btn btn-info" 
                     onClick="window.location.href = 'view.php?menu_id='+<?php echo $record_info->group_id?>" > <i class="icon-edit-sign"></i> View Links </button> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading tab-bg-dark-navy-blue"> 
              <ul class="nav nav-tabs"> 
                <li class=" center-block active" ><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"> </i> <strong> Entry Info</strong> </a> </li> 
                <li class=" center-block " ><a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"> </i> <strong> Publish Option</strong> </a> </li> 
                 
              </ul> 
            </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <div class="tab-content"> 
                    <div id="op1" class="tab-pane active "> 
                      <li><span style="color:#428bca">> Created By:</span> <?php echo $created_by->user_name?></li> 
                      <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                      <li><span style="color:#428bca">> Last Update By:</span> 
                        <?php if($updated_by){ 
					             echo $updated_by->user_name; 
							  }else{ 
								  echo "--";} 
							  ?> 
                      </li> 
                      <li><span style="color:#428bca">> Last Update Date:</span> 
                        <?php if($record_info->last_update!=""){ 
								  echo $record_info->last_update; 
							  }else{ 
									echo "--"; 
							   } 
					  ?> 
                      </li> 
                    </div> 
                          <div id="op2" class="tab-pane  "> 
                      <li><span style="color:#428bca">> Status:</span> <?php echo $record_info->status?></li> 
                    </div> 
                     
                  </div> 
                </ul> 
              </div> 
            </div> 
          </section> 
           
        </div> 
        <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">  Included Images & Icons :</header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_image"> 
            <div class="form-group"> 
                    <label  class="col-lg-2 ">Icon:</label> 
                    <div class="col-lg-8" ><?php echo $record_info->icon?> 
                    </div> 
                  </div>  
                <div class="form-group"> 
                  <label  class="col-lg-4 ">Cover Image:</label> 
                  <div class="col-lg-6" >  
                  <div <?php if(empty($record_info->image)) {echo "style='display:none'";} ?> > 
                   <img src="../../../media-library/<?php echo $record_info->image?>" id="imageSrc" style="width:80px; height:80px;"></div> 
                 </div> 
                </div> 
              
            </form> 
          </div> 
        </section> 
      </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>