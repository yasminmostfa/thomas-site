<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$record_info = Users::find_by_id($record_id); 
		$profiles = Profile::find_all();	 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to('view.php');	 
		} 
		if($user_profile->global_edit != 'all_records' && $record_info->inserted_by != $session->user_id ){ 
		  redirect_to('view.php');	 
	    } 
	}else{ 
		redirect_to('view.php');	 
	} 
	require_once("../layout/header.php");	 
?> 
<script type="text/javascript" src="../../js-crud/crud_user.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>User Module</h4>     
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Edit User</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                <input type="hidden" id="process_type" value="update"> 
                <input type="hidden" id="record" value="<?php echo $record_info->id?>"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">User Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="user_name" disabled value="<?php echo $record_info->user_name?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">First Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="first_name" value="<?php echo $record_info->first_name?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Last Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="last_name" value="<?php echo $record_info->last_name?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Email:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="email" value="<?php echo $record_info->email?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Password</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="pwd" value="" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="button" class=" btn btn-info "   
                    onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Rules Options: </header> 
            <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_option"> 
                  <div class="form-group"> 
                    <label  class="col-lg-5 control-label">Profile:</label> 
                    <div class="col-lg-7"> 
                      <select class="form-control" id="profile"> 
                       <option value=""> Select Profile </option> 
                      <?php 
					 	 foreach($profiles as $profile){ 
							 echo "<option value='$profile->id' "; 
							 if($record_info->user_profile == $profile->id){ 
								 echo "selected"; 
							 } 
							 echo " >$profile->title</option>"; 
						 }?> 
                      </select> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                <label class="col-lg-4">User Type</label>
                <div class="col-lg-8">
                  <label class="checkbox-inline"> 
                    <input type="radio" name="user_type" class="radio" value="admin" <?php  if($record_info->user_type == 'admin') echo "checked";?>> 
                    Admin</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="user_type" class="radio" value="admin_store" <?php  if($record_info->user_type == 'admin_store') echo "checked";?> > 
                    Admin Store </label> 
                </div> 
              </div>
                   
                 
                </form> 
              </div> 
          </section> 
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>