<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/Nodes.php'); 
require_once('../../../../classes/NodesContent.php'); 
require_once('../../../../classes/NodesSelectedTaxonomies.php'); 
require_once('../../../../classes/Localization.php'); 
require_once('../../../../classes/NodesImageGallery.php');
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/TaxonomiesContent.php');
require_once('../../../../classes/EventDetails.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//send json data 
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	$node_id = $_POST['record']; 
	$edit =  Nodes::find_by_id($node_id);
	$old_type = $edit->node_type;
	$new_type = $_POST["type"];	 
	//check global edit and update authorization 
	if($user_profile->global_edit != 'all_records' && $edit->inserted_by != $session->user_id ){ 
		redirect_to("../view.php"); 
	}else{ 
		 //validite required required 
		$required_fields = array('model'=>'- Insert Model');
		  $check_required_fields = check_required_fields($required_fields); 
		 if(count($check_required_fields) == 0){ 
			//update	 
			$edit->status = $_POST["shadow"]; 
			$edit->enable_comments=$_POST["enable_comments"]; 
			$edit->enable_summary=$_POST["enable_summary"];	 
			$edit->update_by=$session->user_id; 
			$edit->last_update = date_now(); 
			$edit->front_page=$_POST["show_in_front_page"]; 
			$edit->slide_show=$_POST["show_in_slide_show"];	 
			$edit->end_date=$_POST["end_date"];
			$edit->node_type = $new_type; 
			$edit->start_date=$_POST["start_date"]; 
			$edit->start_publishing = $_POST["start_time"]; 
			$edit->end_publishing = $_POST["end_date"]; 
			$edit->side_bar = $_POST["side_bar"];
			$edit->model = $_POST["model"]; 
			 if(!empty($_POST['imageVal'])){ 
				$current_file = $_POST['imageVal']; 
				$parts = explode('/',$current_file); 
				$image_cover = $parts[count($parts)-1]; 
				$folder = $parts[count($parts)-2]; 
				$path = $folder."/".$image_cover; 
				$edit->cover_image = $path; 
			}else{
				$edit->cover_image = ""; 
			} 
			if($_POST['imageVal_slider']){ 
				$current = $_POST['imageVal_slider']; 
				$parts_slider = explode('/',$current); 
				$image_covers = $parts_slider[count($parts_slider)-1]; 
				$edit->slider_cover = $image_covers; 
	        }else{
				$edit->slider_cover = ""; 
			} 
			$update = $edit->update(); 
			if($update){ 
			   //event details 
			   if($_POST["type"] == "event"){
				$edit_event_details = EventDetails::find_by_custom_filed('event_id',$node_id);
				if($edit_event_details){
			      $edit_event_details->place  = $_POST["place"];
				  $edit_event_details->enable_course = $_POST["enable_course"];
				  $edit_event_details->start_date = $_POST["start_date"];
				  $edit_event_details->subject_plus_url = $_POST["url"];
				  $edit_event_details->end_date = $_POST["end_date"];
				  $edit_event_details->instructor = $_POST["instructor"];
				  $update_event_details =  $edit_event_details->update();
			   }else{
				   $edit_event_details = new EventDetails();
				   $edit_event_details->place  = $_POST["place"];
				   $edit_event_details->enable_course = $_POST["enable_course"];
				   $edit_event_details->subject_plus_url = $_POST["url"];
				   $edit_event_details->start_date = $_POST["start_date"];
				   $edit_event_details->instructor = $_POST["instructor"];
				   $edit_event_details->end_date = $_POST["end_date"];
				   $edit_event_details->event_id = $node_id;
				   $insert_event_details = $edit_event_details->insert();
				}
			   }
				 //retrieve all available languages 
				$languages = Localization::find_all('label','asc');		 
			//update page content  
			foreach($languages as $language){ 
				$add_update_content = new NodesContent(); 
				$add_update_content->node_id = $node_id; 
				$add_update_content->title = $_POST['main_content']['title_'.$language->label]; 
				$add_update_content->alias = $_POST['main_content']['alias_'.$language->label];	 
				$add_update_content->body = $_POST['main_content']['full_content_'.$language->label]; 
				$add_update_content->summary = $_POST['main_content']['summary_'.$language->label]; 
				$add_update_content->lang_id = $language->id; 
				$add_update_content->meta_description = $_POST['main_content']['meta_description_'.$language->label]; 
				$add_update_content->meta_keys = $_POST['main_content']['meta_keys_'.$language->label]; 
				//check content id exist or not 
				//if exist update if not insert new record 
				$check_content_exist = $add_update_content->find_by_id($_POST['main_content']['content_id_'.$language->label]); 
				if($check_content_exist){ 
					$add_update_content->id = $_POST['main_content']['content_id_'.$language->label]; 
					$add_update_content->update();				 
				}else{ 
					$add_page_content->insert(); 
				}  
					
			} 
				//update gallery
				if(!empty($_POST["selected_image_gallery"])){
				 $selected_image_gallery  = array_unique($_POST["selected_image_gallery"]);
				 $images_array =  array();
				  foreach($selected_image_gallery as $key => $value){ 
					if($key != 0){ 
						 $explode_value = explode(',', $value); 
						 if(!empty($explode_value[0])){ 
						  $images_array[] = file_folder_src($explode_value[0]); 
						 }
					}
				  }
				 
				 $implded_image_galeery = implode(",",$images_array);		
				  $sql_delete_categories_not_in_array = "DELETE FROM nodes_image_gallery WHERE related_id = '$node_id'  AND image NOT IN ('$implded_image_galeery')";
				  $preform_categories_tags = $database->query($sql_delete_categories_not_in_array);
				  //check first this categories does not exist for this page
				 
				  foreach($selected_image_gallery as $key => $value){ 
					if($key != 0){ 
						 $explode_value = explode(',', $value); 
						 if(!empty($explode_value[0])){ 
						 $check_image = NodesImageGallery::get_node_images_gallery(file_folder_src($explode_value[0]),$node_id);
						 if(empty($check_image)){
							 $add_update_value = new NodesImageGallery(); 
							 $add_update_value->image = file_folder_src($explode_value[0]); 
							 $add_update_value->sort = $explode_value[1]; 
							 $add_update_value->related_id = $node_id;
							 $add_update_value->caption = $explode_value[2];
							 $add_update_value->insert(); 
						 }
						
						}
					}
				  }
						
			  }else{
				  $sql_delete_imahes = "DELETE FROM  nodes_image_gallery WHERE related_id = '$node_id'  ";
				  $preform_delete_images = $database->query($sql_delete_imahes);  
			  }
				//update selected categories 
				if(!empty($_POST["categories"])){ 
					$selected_categories  = array_unique($_POST["categories"]); 
					//delete all categories that exist in EventCategories tbl and doesn exist in new selections 
					$implode_categories = implode(",", $selected_categories); 
					$sql_delete_categories_not_in_array = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id' 
					 AND taxonomy_id NOT IN ($implode_categories) 
					 AND node_type = '$old_type' AND taxonomy_type = 'category' "; 
					$preform_categories_tags = $database->query($sql_delete_categories_not_in_array); 
					//check first this categories does not exist for this event 
					foreach($selected_categories as $category){ 
						  $check_category_exist = NodesSelectedTaxonomies::check_taxonomy_exist($node_id,$category,$old_type,'category'); 
						  if(empty($check_category_exist)){ 
							  $add_category = new NodesSelectedTaxonomies(); 
							  $add_category->node_id = $node_id; 
							  $add_category->taxonomy_type = 'category'; 
							  $add_category->taxonomy_id = $category; 
							  $add_category->node_type = $new_type; 
							  $add_category->insert(); 
						  } 
					    } 
				}else{ 
					$sql_delete_categories = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id' 
					AND node_type = '$old_type' AND taxonomy_type = 'category'"; 
					$preform_categories = $database->query($sql_delete_categories);							 
			}
			//update selected categories 
				if(!empty($_POST["selected_courses"])){ 
					$selected_categories  = array_unique($_POST["selected_courses"]); 
					//delete all categories that exist in EventCategories tbl and doesn exist in new selections 
					$implode_categories = implode(",", $selected_categories); 
					$sql_delete_categories_not_in_array = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id' 
					 AND taxonomy_id NOT IN ($implode_categories) 
					 AND node_type = '$old_type' AND taxonomy_type = 'courses' "; 
					$preform_categories_tags = $database->query($sql_delete_categories_not_in_array); 
					//check first this categories does not exist for this event 
					foreach($selected_categories as $category){ 
						  $check_category_exist = NodesSelectedTaxonomies::check_taxonomy_exist($node_id,$category,$old_type,'courses'); 
						  if(empty($check_category_exist)){ 
							  $add_category = new NodesSelectedTaxonomies(); 
							  $add_category->node_id = $node_id; 
							  $add_category->taxonomy_type = 'courses'; 
							  $add_category->taxonomy_id = $category; 
							  $add_category->node_type = $new_type; 
							  $add_category->insert(); 
						  } 
					    } 
				}else{ 
					$sql_delete_categories = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id' 
					AND node_type = '$old_type' AND taxonomy_type = 'courses'"; 
					$preform_categories = $database->query($sql_delete_categories);							 
			}
			//update tags 
			if(!empty($_POST["tags"])){ 
				  $tags =  $_POST["tags"]; 
				  //array hold ids of selected tags 
				  $selected_tags_id = array(); 
				  //to hold new inserted tag 
				  $new_tags_ids = array(); 
				  foreach($tags as $tag){ 
					 $tag_id = Taxonomies::get_tag_id($tag); 
					 if($tag_id){ 
						  $selected_tags_id[] = $tag_id->id; 
					  }else{ 
						  //insert new tag on fly 
						  $insert_new_tag = new Taxonomies(); 
						 // $insert_new_tag->parent_id = 0; 
						  $insert_new_tag->status = "publish"; 
						  $insert_new_tag->taxonomy_type = 'tag'; 
						  $insert_new_tag->inserted_date = date_now(); 
						  $insert_new_tag->inserted_by = $session->user_id; 
						  $insert_new_tag->insert(); 
						  $tag_new_id = $insert_new_tag->id; 
						//insert new tag  content 
						  $insert_new_tag_content = new TaxonomiesContent(); 
						  $insert_new_tag_content->taxonomy_id = $tag_new_id; 
						  $insert_new_tag_content->name = $tag; 
						  $insert_new_tag_content->alias = preg_replace('/\s+/', '_',$tag); 
						  $insert_new_tag_content->lang_id = 0; 
						  $add_new_tag_content = $insert_new_tag_content->insert(); 
						  $new_tags_ids[] = $tag_new_id; 
					  } 
				   } 
					  //delete all tags in PostTags tbl where not in selected_tags_id array 
					  if(!empty($selected_tags_id)){ 
						$implode_tags = $database->escape_values(implode(',', $selected_tags_id)); 
						$sql_delete_tags_not_in_array = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id' 
					    AND taxonomy_id NOT IN ($implode_tags) 
						AND node_type = '$new_type'  
						AND taxonomy_type = 'tag' "; 
						$preform_delete_tags = $database->query($sql_delete_tags_not_in_array); 
						//insert tags  
						//check if tag exist do not insert 
						foreach($selected_tags_id as $selected_tag){ 
						  //check first 
						  //if return 0 insert 
						  $check_tag_exist = NodesSelectedTaxonomies::check_taxonomy_exist($node_id,$selected_tag,'event','tag'); 
						  $count_retrieved_tags = count($check_tag_exist); 
						  if($count_retrieved_tags == 0){ 
							  $add_tag = new NodesSelectedTaxonomies(); 
							  $add_tag->node_id = $node_id; 
							  $add_tag->taxonomy_id = $selected_tag; 
							  $add_tag->node_type = $new_type; 
							  $add_tag->taxonomy_type = 'tag'; 
							  $add_tag->insert(); 
						  } 
						} 
						//insert new tags  
						foreach($new_tags_ids as $new_tag){ 
							  $add_tag = new NodesSelectedTaxonomies(); 
							  $add_tag->node_id = $node_id; 
							  $add_tag->taxonomy_id = $new_tag; 
							  $add_tag->node_type = $new_type; 
							  $add_tag->taxonomy_type = 'tag'; 
							  $add_tag->insert(); 
						}					 
					  } 
				  }else{ 
					  $delete_all_tags = "DELETE FROM nodes_selected_taxonomies WHERE node_id = '$node_id' 
					  AND node_type = '$new_type' 
					  AND taxonomy_type = 'tag' "; 
					  $preform_delete_tags = $database->query($delete_all_tags); 
				  }  
		  $data  = array("status"=>"work"); 
		  echo json_encode($data); 
		}else{ 
		  $data  = array("status"=>"error"); 
		  echo json_encode($data); 
		} 
  }else{ 
		//validation error 
		$comma_separated = implode("<br>", $check_required_fields); 
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		echo json_encode($data); 
	}	 
} 
} 
//close connection 
if(isset($database)){ 
$database->close_connection(); 
} 
?>