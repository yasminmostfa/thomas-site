<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Plugins.php'); 
require_once('../../../../classes/Posts.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
//send notifiction by json  
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	//validite required required 
   $required_fields = array('name'=>"- Insert Name", 'post_alias'=>'- Insert Post Alias', 'description'=>"- Insert Description"); 
   $check_required_fields = check_required_fields($required_fields); 
	 if(count($check_required_fields) == 0){ 
		 //get post id by alias 
		  $post_alias = $_POST["post_alias"]; 
		  $get_post_alias = Posts::find_by_custom_filed('alias', $post_alias); 
		  if($get_post_alias){ 
			  $add = new Plugins(); 
			  $add->name = $_POST["name"]; 
			  $add->source = $get_post_alias->id; 
			  $add->description = $_POST["description"]; 
			  $add->type = "video"; 
			  $add->version = "1.0"; 
			  $add->author = "diva"; 
			  $add->uploaded_by = $session->user_id; 
			  $add->uploaded_date = date_now(); 
			  $insert = $add->insert(); 
			  $inserted_menu_group_id = $add->id; 
			  if($insert){			   
				  $data  = array("status"=>"work"); 
				  echo json_encode($data); 
			  }else{ 
				  $data  = array("status"=>"error"); 
				  echo json_encode($data); 
			  } 
		  }else{ 
			  	$data  = array("status"=>"alias_error"); 
				echo json_encode($data); 
		  } 
	 }else{ 
		//validation error 
		$comma_separated = implode("<br>", $check_required_fields); 
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		echo json_encode($data); 
	}		 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>