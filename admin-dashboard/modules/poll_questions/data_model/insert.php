<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/PollQuestions.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	$add = new PollQuestions(); 
	$add->poll = $_POST["poll"]; 
	$add->status = $_POST["status"]; 
	$add->inserted_by = $session->user_id; 
	$add->inserted_date = date_now(); 
	$insert = $add->insert(); 
 	header('Content-Type: application/json'); 
	if($insert){ 
		$data  = array("status"=>"work"); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	}	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>