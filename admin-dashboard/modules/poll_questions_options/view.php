<?php  
	require_once("../layout/initialize.php");	 
	$poll_id = $_GET["poll_id"]; 
	$options = PollQuestionsOptions::find_all_by_custom_filed('poll_id',$poll_id); 
	$define_class = new PollQuestionsOptions(); 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
   <h4>Poll Options  Module</h4> 
    <div class="row"> 
      <div class="col-lg-12"> 
        <section class="panel"> 
          <header class="panel-heading"> Show Poll Options</header> 
          <br> 
        <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php?poll_id='+<?php echo $poll_id;?>" 		 
		> 
          <li class="icon-plus-sign"></li> 
          Add New Option </button> 
          <br> 
          <br> 
          <table class="table table-striped table-advance table-hover"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th><i class=""></i>Poll Options</th> 
                <th><i class=""></i> Option Counter</th> 
                <th><i class=""></i> Option Counter Percentage</th> 
                <th><i class=""></i> created Date</th> 
                <th><i class=""></i> created by</th> 
                <th>Action</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php  
				  $serialize = 1;  
				  $total_number = 0; 
				   foreach($options as $option){  
				   $total_number = $option->option_counter+$total_number; 
				  }; 
				  foreach($options as $option){ 
					  $record = $define_class->options_data("inserted_date","DESC",$option->id); 
					  if($total_number > 0){ 
					  $percentage  = round($option->option_counter/$total_number*100); 
					  }else { 
						 $percentage = 0;  
					  } 
					echo "<tr> 
					   <td>{$serialize}</td> 
					   <td><a href='full_info.php?id={$record->id}'>{$record->poll_option}</a></td> 
						<td>{$record->option_counter}</td> 
					   <td>{$percentage}%</td> 
					   <td>{$record->inserted_date}</td> 
					   <td>{$record->inserted_by}</td> 
					  <td>"; 
					  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
									include('../layout/btn_control.php'); 
					  echo "</td>		 
						  </tr>"; 
						  //delete dialoge    
					 echo   "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
															  <div class='modal-dialog'> 
																  <div class='modal-content'> 
																	  <div class='modal-header'> 
																		  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
																		  <h4 class='modal-title'>Delete</h4> 
																	  </div> 
																	  <div class='modal-body'> 
							 
																	   <p> Are you sure you want delete  $record->poll_option ??</p> 
							 
																	  </div> 
																	  <div class='modal-footer'> 
																		   
																		  <button class='btn btn-warning' type='button'  
																		  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
																	  </div> 
																  </div> 
															  </div> 
														  </div>"; 
										   $serialize++; 
										 }?> 
				 
            </tbody> 
          </table> 
        </section> 
      </div> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>