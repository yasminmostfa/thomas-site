<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Profile.php'); 
require_once("../../../../classes/CMSModules.php"); 
require_once("../../../../classes/ProfileModulesAccess.php"); 
require_once("../../../../classes/ProfilePagesAccess.php"); 
require_once('../../../../classes/Users.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//get profile and selected module 
//check if this module insert before in profile module page  
//if not exist insert it then insert all module pages to profile pages tbl 
//if exist update only module with access no to be yes 
//note update only module with access no becouse we should not chnage any users choices  
//finlay change all module and pagess that not sleteced to be with access no 
//updateprofile 
$profile_id = $_POST["record"]; 
$edit = Profile::find_by_id($profile_id); 
//send json data 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//check global edit and update authorization 
		if($user_profile->global_edit != 'all_records' && $edit->inserted_by != $session->user_id ){ 
			redirect_to("../view.php"); 
		}else{ 
			 header('Content-Type: application/json'); 
			 $required_fields = array('title'=>"- Insert title",'modules'=>'- Insert modules'); 
			$check_required_fields = check_required_fields($required_fields); 
			if(count($check_required_fields) == 0){ 
					//get selected modules  
				$selected_modules = $_POST['modules']; 
				$edit->title = $_POST["title"]; 
				$edit->last_update = date_now(); 
				$edit->global_edit = $_POST["global_edit"]; 
				$edit->global_delete = $_POST["global_delete"]; 
				$edit->profile_block = $_POST["profile_turn_off"]; 
				$edit->developer_mode = $_POST["developer_mode"]; 
				$edit->page_publishing = $_POST["page_publishing"]; 
				$edit->post_publishing = $_POST["post_publishing"]; 
				$edit->event_publishing = $_POST["event_publishing"]; 
				$update = $edit->update(); 
				if($update){ 
					foreach($selected_modules as $module){ 
					//check if those modules exist in profile modules 
					$check_module = ProfileModulesAccess::check_module_availabilty($profile_id, $module); 
					if(!$check_module){ 
						//if module does not exist insert  
						$add_modules = new ProfileModulesAccess(); 
						$add_modules->profile_id = $profile_id; 
						$add_modules->module_id = $module;  
						$add_modules->access = 'yes';  
						$add_modules->inserted_by = $session->user_id;  
						$add_modules->inserted_date = date_now();  
						$insert_module = $add_modules->insert(); 
						unset($insert_module); 
						//if module insert  
						//get all pages under this module then insert those pages with profile in module pages access 
							if($insert_module){ 
								$module_pages = CMSModules::find_all_by_custom_filed('sid', $module, 'sorting', 'ASC'); 
								foreach($module_pages as $page){ 
									//insert all pages under each module 
									 $add_page = new ProfilePagesAccess(); 
									 $add_page->profile_id = $profile_id; 
									 $add_page->module_id = $module; 
									 $add_page->Page_id = $page->id; 
									 $add_page->access = 'yes'; 
									 $add_page->inserted_date = date_now(); 
									 $add_page->inserted_by = $session->user_id;  
									 $add_page->insert(); 
									 unset($add_page); 
								} 
							} 
						}else{ 
							//if module exist 
							//update only modules with access no to be yes 
							//do not update modules with access yes to keep users choices 
							if($check_module->access == "no"){ 
								$sql_update_module_access = "UPDATE profile_modules_access SET access = 'yes' WHERE profile_id = '{$profile_id}' AND module_id = '{$module}'"; 
								$perform_query_update_module_access = $database->query($sql_update_module_access); 
								if($perform_query_update_module_access){ 
									//update pages 
									$sql_update_pages_access = "UPDATE profile_pages_access SET access = 'yes' WHERE profile_id = '{$profile_id}' AND  module_id = '{$module}'"; 
									$database->query($sql_update_pages_access); 
								} 
							} 
							 
						} 
					} 
						//after foreach finished  
						//update access status for all unselcted modules to be no 
						$implode_modules = $database->escape_values(implode(',', $selected_modules));   
						$sql_update_modules_access = " 
						UPDATE profile_modules_access SET access = 'no' WHERE profile_id = '{$profile_id}' AND module_id NOT IN ($implode_modules)"; 
						$perform_query_module_access = $database->query($sql_update_modules_access); 
						if($perform_query_module_access){ 
							//update pages access 
							$sql_update_pages_access = " 
							UPDATE profile_pages_access SET access = 'no' WHERE profile_id = '{$profile_id}' AND module_id NOT IN ($implode_modules)"; 
							$perform_query_pages_access = $database->query($sql_update_pages_access);	 
						} 
					$data  = array("status"=>"work"); 
					echo json_encode($data);	 
				}else{ 
					$data  = array("status"=>"error"); 
					echo json_encode($data); 
				} 
			}else{ 
				  //validation error 
				  $comma_separated = implode("<br>", $check_required_fields); 
				  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
				  echo json_encode($data); 
			  }			 
		} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>