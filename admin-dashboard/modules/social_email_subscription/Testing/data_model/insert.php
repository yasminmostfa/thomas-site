<?php

require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
require_once('../../../../classes/urlBeforeParse.php');
require_once('../../../../classes/urlLink.php');
require_once('../../../../classes/parsingResult.php');
require_once('../../../../classes/Localization.php');
//check  session user  log in 

if ($session->is_logged() == false) {
    redirect_to("../../../index.php");
}
//header('Content-Type: application/json');
// get user profile   
$user_data = Users::find_by_id($session->user_id);
// get user profile data 
$user_profile = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block 

if ($user_profile->profile_block == "yes") {

    redirect_to("../../../index.php");
}

//send json data 
//header('Content-Type: application/json');
if (!empty($_POST["process_type"]) && $_POST["process_type"] == "insert") {
    $check_required_fields = [];
    if (count($check_required_fields) == 0) {
         $trimmed_url = trim($_POST['url']);
        //retrieve all available languages 
        $languages = Localization::find_all('label', 'asc');
        $link = new urlLink();
        $link->link = $_POST['url'];
        $link->insert();
        $link_id = $link->id;
        //insert url title and description and it's alias content  
        foreach ($languages as $language) {
            $url = new urlBeforeParse();
            $url->inserted_date = date_now();
            $url->inserted_by = $session->user_id;
            $url->title = $_POST['title_' . $language->label];
            $url->alias = $_POST['alias_' . $language->label];
            $url->description = $_POST['description_' . $language->label];
            $url->lang_id = $language->id;
            $url->link = $link_id;

            $url->insert();
        }
        /// parsing xml file and store in database
        $books = simplexml_load_file($trimmed_url.'&view=rss&skip_rss_sort=1');
        $items= array();
        for ($i = 0; $i < count($books->channel->item); $i++) {
            $items[$i] = $books->channel->item[$i];
            $dataParsing = new parsingResult();
            $dataParsing->parsed_title = $items[$i]->title;
            $dataParsing->publish_date = $items[$i]->pubDate;
            $dataParsing->url = $items[$i]->link;
            $dataParsing->link_id = $link_id;
            $dataParsing->insert();
        }
        

        echo 'done';




        /*
         * 
         * 
         */
    } else {
        //validation error 
        $comma_separated = implode("<br>", $check_required_fields);
        $data = array("status" => "valid_error", "fileds" => $comma_separated);
        echo json_encode($data);
    }
}
//close connection 
if (isset($database)) {
    $database->close_connection();
}
?>