<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/TaxonomiesContent.php'); 
require_once('../../../../classes/Localization.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
////send notifiction by json 
header('Content-Type: application/json');	 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	//retrieve all available languages 
	$languages = Localization::find_all('label','asc');	 
	//validite required required 
	$required_fields = array(); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
			//insert data 
			$add = new Taxonomies(); 
			$add->taxonomy_type = $_POST['taxonomy']; 
			$add->parent_id = $_POST["parent_id"]; 
			$add->url = $_POST["url"];
			$add->status = $_POST["shadow"]; 
			$add->main_menu = $_POST['main_menu'];
			$add->sorting = $_POST["sorting"];
			$add->show_image = $_POST["display_image"]; 
			$add->inserted_date = date_now(); 
			$add->inserted_by = $session->user_id; 
		   if(!empty($_POST['cover'])){ 
			  $current_file = $_POST['cover']; 
			  $parts = explode('/',$current_file); 
			  $image_cover = $parts[count($parts)-1]; 
			  $folder = $parts[count($parts)-2]; 
			  $path = $folder."/".$image_cover; 
			  $add->cover = $path; 
	        }  
			$insert = $add->insert(); 
			$inserted_id = $add->id; 
			if($insert){ 
				//insert category content 
				foreach($languages as $language){ 
					$add_content = new TaxonomiesContent(); 
					$add_content->taxonomy_id = $inserted_id; 
					$add_content->name = $_POST['main_content']['name_'.$language->label]; 
					$add_content->alias = $_POST['main_content']['alias_'.$language->label];	 
					$add_content->description = $_POST['main_content']['description_'.$language->label]; 
					$add_content->lang_id = $language->id; 
					$add_content->insert(); 
				}	 
			   //return json  
				$data  = array("status"=>"work","id"=>$inserted_id); 
				echo json_encode($data); 
			}else{ 
				$data  = array("status"=>"error"); 
				echo json_encode($data); 
			} 
 	 }else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  } 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>