<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/TaxonomiesContent.php'); 
require_once('../../../../classes/Localization.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//get data 
	 $id = $_POST["record"]; 
	 $edit = Taxonomies::find_by_id($id); 
	if($user_profile->global_edit != 'all_records' && $edit->inserted_by != $session->user_id  ){	 
		redirect_to("../view.php"); 
	}else{ 
		header('Content-Type: application/json'); 
		//retrieve all available languages 
		$languages = Localization::find_all('label','asc');	 
		//validite required required 
		$required_fields = array(); 
		$check_required_fields = check_required_fields($required_fields); 
		if(count($check_required_fields) == 0){ 
			//update	 
			  $edit->status = $_POST["shadow"]; 
			  $edit->url = $_POST["url"];
			  $edit->parent_id = $_POST["parent_id"]; 
			  $edit->taxonomy_type = $_POST['taxonomy'] ;
			  $edit->sorting = $_POST["sorting"];
			  $edit->update_by=$session->user_id; 
			  $edit->last_update = date_now(); 
			  if(!empty($_POST['cover'])){ 
				  $current_file = $_POST['cover']; 
				  $parts = explode('/',$current_file); 
				  $image_cover = $parts[count($parts)-1]; 
				  $folder = $parts[count($parts)-2]; 
				  $path = $folder."/".$image_cover; 
				  $edit->cover = $path; 
				} 
				
				
				
			  $update = $edit->update(); 
			  if($update){ 
					//update category content  
					foreach($languages as $language){ 
						//check content id exist or not 
						//if exist update if not insert new record 
						$edit_content = TaxonomiesContent::find_by_id($_POST['main_content']['content_id_'.$language->label]); 
						if($edit_content){ 
							$edit_content->name = $_POST['main_content']['name_'.$language->label]; 
							$edit_content->alias = $_POST['main_content']['alias_'.$language->label];	 
							$edit_content->description = $_POST['main_content']['description_'.$language->label]; 
							$edit_content->update();				 
						}else{ 
							$add_content = new TaxonomiesContent(); 
							$add_content->taxonomy_id = $id; 
							$add_content->name = $_POST['main_content']['name_'.$language->label]; 
							$add_content->alias = $_POST['main_content']['alias_'.$language->label];	 
							$add_content->description = $_POST['main_content']['description_'.$language->label]; 
							$add_content->lang_id = $language->id; 
							$add_content->insert(); 
						} 
					}					 
					$data  = array("status"=>"work"); 
					echo json_encode($data); 
			  }else{ 
				  $data  = array("status"=>"error"); 
				  echo json_encode($data); 
			  } 
		}else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
		} 
 	 }	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>