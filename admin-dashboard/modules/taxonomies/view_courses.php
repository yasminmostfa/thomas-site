<?php 
	require_once("../layout/initialize.php"); 
	$define_class= new Taxonomies(); 
	$define_class->enable_relation(); 
	$records = $define_class->taxonomies_data('courses',null,null,'inserted_date', 'DESC',null,'many',$general_setting_info->translate_lang_id); 
	//get texonomy content class 
    $define_taxonomy_content = new TaxonomiesContent(); 
	
		  
	require_once("../layout/header.php"); 
?>  <!--header end--> 
      <!--sidebar start--> 
      <script src="../../js-crud/crud_taxonomy_category.js"></script> 
       <?php require_once("../layout/navigation.php");?> 
        
      <!--sidebar end--> 
      <!--main content start--> 
      <section id="main-content"> 
         <section class="wrapper site-min-height"> 
          <h4>Taxonomy Courses Module</h4>  
              <section class="panel"> 
                 <header class="panel-heading"> 
                      View Courses 
                  </header> 
                  <div class="panel-body"> 
                      <div class="adv-table editable-table "> 
                          <div class="space15"></div> 
                             
                          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
                              <thead> 
                               <tr> 
                                 
                                 <th>Name</th> 
                                 <th>Created Date</th> 
                                 <th>Action</th> 
                              </tr> 
                              </thead> 
                              <tbody> 
                               <?php  


										$define_class->enable_relation(); 

										$courses = $define_class->get_type(0,0,$main_lang_id, 'courses');

										if(!empty($courses)){ 

										foreach ($courses as $key=>$value) { 

											$record = Taxonomies::find_by_id($key); 

											echo "<tr> 

											 <td><a href='full_info.php?id={$record->id}'> {$value}</a></td> 

											 <td>{$record->inserted_date}</td> 

												<td>"; 

												 $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 

												 include('../layout/btn_control.php'); 

												 "</td> 

											  </tr> 

											  <div class='modal fade' id='my{$key}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 

																  <div class='modal-dialog'> 

																	  <div class='modal-content'> 

																		  <div class='modal-header'> 

																			  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 

																			  <h4 class='modal-title'>Delete</h4> 

																		  </div> 

																		  <div class='modal-body'> 

																		   <p> Are you sure you want delete  $record->name ??</p> 

																		  </div> 

																		  <div class='modal-footer'> 

																			  <button class='btn btn-warning' type='button'  

																			  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 

																		  </div> 

																	  </div> 

																  </div> 

															  </div>"; 

											 

										}

										}

				?>  
							 
           </tbody> 
        </table> 
                      </div> 
                  </div> 
              </section> 
              <!-- page end--> 
          </section> 
      </section> 
      <!--main content end--> 
      <!--footer start--> 
     <?php require_once("../layout/footer.php");?> 
