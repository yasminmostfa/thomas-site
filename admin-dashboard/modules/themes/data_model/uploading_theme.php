<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/UploadFile.php'); 
//send notfy by json 
header('Content-Type: application/json'); 
//catch file  
$file =  $_FILES['file']; 
if(!empty($file)){ 
	//root 
	$root = "../../../../theme_temp/"; 
	//initialize class, insert rrot , allow ext, size 
	$upload_file = new UploadFile($root , array('zip'), 31457280); 
	//uploading file 
	$upload_file->attach_file($file); 
	//if file uploaded resize 
	if($upload_file->succeeded == 'yes'){ 
		//file name 
		$file_name = $upload_file->new_file_name;	 
		$data  = array("status"=>"work", "name"=>$file_name); 
		echo json_encode($data); 
	}else{ 
		$error = join("<br>", $upload_file->errors); 
		$data  = array("status"=>"error", "message"=>$error); 
		echo json_encode($data); 
	} 
} 
?>