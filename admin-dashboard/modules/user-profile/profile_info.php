<?php  
	require_once("../layout/initialize.php"); 
	// get user if from session 
	$id = $session->user_id; 
	$define_class = new Users(); 
	$define_class->enable_relation(); 
	$record_info = $define_class->user_data(null,null,$id); 
	require_once("../layout/header.php");	 
?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Profile Info</h4>  
      <!-- page start--> 
      <div class="row"> 
        <aside class="profile-info col-lg-8"> 
          <section> 
            <div class="panel panel-primary"> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">User Name:</label> 
                    <div class="col-lg-8"> 
                    <?php echo $record_info->user_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">First Name:</label> 
                    <div class="col-lg-8"> 
                    <?php echo $record_info->first_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Last Name:</label> 
                    <div class="col-lg-8"> 
                    <?php echo $record_info->last_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Email:</label> 
                    <div class="col-lg-8"> 
                    <?php echo $record_info->email?> 
                    </div> 
                  </div> 
                  </form> 
                 <br/> 
                 <div class="pull-left"> 
                 <button type="button" class="btn btn-info" id="edit_info" onclick="window.location='update_info.php'"> <li class="icon-pencil"></li> Update Profile Info</button> 
                  <button type="button" class="btn btn-info" id="edit_pass" onclick="window.location='update_password.php'"> <li class="icon-edit"></li> Update Password </button> 
                 </div> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
        <section class="panel"> 
          <header class="panel-heading tab-bg-dark-navy-blue"> 
              <ul class="nav nav-tabs"> 
                <li class="center-block active"><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"></i><strong> Entry Info</strong></a></li> 
                <li > <a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"></i> <strong>  Rules Options</strong> </a> </li> 
                
              </ul> 
            </header> 
             
            <div class="panel-body"> 
                <div id="list_info"> 
                <ul> 
                 <div class="tab-content"> 
                 <div id="op1" class="tab-pane active "> <br /> 
                  <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update By:</span> 
                    <?php  
						if($record_info->update_by!=""){ 
						  echo $record_info->update_by; 
						}else{ 
							  echo "--"; 
						} 
					  ?> 
                  </li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> 
                    <?php  
						if($record_info->last_update != "0000-00-00 00:00:00"){ 
					 	 	echo $record_info->last_update; 
				      	}else{ 
						  echo "--"; 
					  	} 
					  ?> 
                  </li> 
                    </div> 
                    <div id="op2" class="tab-pane  "> <br /> 
                   <li><span style="color:#428bca">> Profile Name:</span>  
                   <a id="profile_info" href="../users/profile_info.php?id=<?php echo $record_info->user_profile_id?>"><?php echo $record_info->user_profile?></a></li> 
                   </div> 
                   </div> 
                    
                </ul> 
              </div> 
              </div> 
          </section> 
           
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  
  <?php require_once("../layout/footer.php");?>