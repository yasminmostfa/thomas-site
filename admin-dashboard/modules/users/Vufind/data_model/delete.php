<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/vufindFormat.php');
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
// user log in profile details to chech authority 
// get user profile   
// get user profile data 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_data = vufindFormat::find_by_id($id); 
	if($find_data){ 
		 $delete = $find_data->delete(); 
			 redirect_to("../view.php");
		
	
	} }
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>