<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check log in  
if($session->is_logged() == false){ 
redirect_to("../../index.php"); 
} 
// user log in profile details to chech authority 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
//get data 
$id = $_POST['record']; 
$edit = Users::find_by_id($id); 
//check global edit and update authorization 
	if($user_profile->global_edit != 'all_records' && $edit->inserted_by != $session->user_id ){ 
		redirect_to("../view.php"); 
	}else{ 
		header('Content-Type: application/json'); 
		//check rqquierd fields 
		$required_fields = array('user_name'=>"- Insert user name",'first_name'=>'- Insert first name' , 'last_name'=>' - Insert last name' , 'email'=>'- Insert email', 'profile'=>'- Insert profile',  
		'password'=>'- Insert password'); 
		$check_required_fields = check_required_fields($required_fields); 
		if(count($check_required_fields) == 0){		 
			  //update	 
			  $edit->first_name = $_POST["first_name"]; 
			  $edit->last_name = $_POST["last_name"]; 
			  $edit->update_by = $session->user_id; 
			  $edit->user_profile = $_POST["profile"];
			  $edit->user_type = $_POST["user_type"]; 
			  $edit->last_update = date_now();	 
			  //if email changed 
			  if($edit->email != $_POST['email']){ 
				  $email = $_POST['email']; 
				  //check if email already taken 
				  $check_email = Users::find_by_custom_filed('email', $email); 
				  if(!$check_email){ 
					  $edit->email = $_POST['email']; 
					  //check if password changed 
					  if(!empty($_POST["password"])){ 
						  $edit->password = sha1(md5($_POST["password"]));			 
					  } 
					  //perform update 
					  $update = $edit->update(); 
					  if($update){ 
						  $data  = array("status"=>"work"); 
						  echo json_encode($data); 
					  }else{ 
						  $data  = array("status"=>"error"); 
						  echo json_encode($data); 
					  } 
				  }else{ 
					  $data  = array("status"=>"email_exist"); 
					  echo json_encode($data);					 
				  } 
			  }else{ 
				  //if email not changed preform update 
				  //check if password changed 
				  if(!empty($_POST["password"])){ 
					  $edit->password = sha1(md5($_POST["password"]));			 
				  } 
				  $update = $edit->update(); 
				  if($update){ 
					  $data  = array("status"=>"work"); 
					  echo json_encode($data); 
				  }else{ 
					  $data  = array("status"=>"error"); 
					  echo json_encode($data); 
				  } 
			  } 
		 }else{ 
			//validation error 
			$comma_separated = implode("<br>", $check_required_fields); 
			$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
			echo json_encode($data); 
		}		 
	} 
} 
//close connection 
if(isset($database)){ 
$database->close_connection(); 
} 
?> 
