<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <!-- Bootstrap core CSS --> 
    <link href="../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link rel="stylesheet" type="text/css" href="../../assets/gritter/css/jquery.gritter.css" /> 
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
     <link href="../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" /> 
    <link href="../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" /> 
     <link href="../../css/style.css" rel="stylesheet"> 
    <link href="../../css/style-responsive.css" rel="stylesheet" /> 
   <?php  
  require_once("../../../classes/Profile.php"); 
  require_once("../../../classes/ProfilePagesAccess.php"); 
  require_once("../../../classes/ProfileModulesAccess.php"); 
  $profile_id = $_GET['id']; 
  $modules = new ProfileModulesAccess(); 
  $modules->enable_relation(); 
   $pages = new ProfilePagesAccess(); 
   $pages->enable_relation(); 
  $records = $modules->profile_modules_data($profile_id, 'yes', 'module_sorting', 'ASC');	 
	 
	// get profile id 
	 
    
	?> 
    <!-- Custom styles for this template --> 
     
 </head> 
 <body style="background:#fff"> 
  <section class="panel"> 
  <div class="panel-body"> 
        <header class=" panel-heading  tab-bg-dark-navy-blue"> 
              <ul class="nav nav-tabs"> 
               <?php foreach($records as $record):?> 
             <li class="center-block <?php if($record->module_id== 6) echo "active"?>" style="width:auto;"><a data-toggle="tab" href="#op<?php echo $record->module_id?>" class="text-center"> <i class=" icon-check" ></i><strong>      <?php echo $record->module_title;?></strong></a></li> 
                 <?php endforeach;?> 
                  
              </ul> 
            </header> 
            <section class="panel"> 
            <div class="tab-content"> 
              <?php foreach($records as $record):?> 
               <div id="op<?php echo $record->module_id?>" class="tab-pane <?php if($record->module_id== 6) echo "active"?> "> <br /> 
               <div class="panel-body"> 
                
               <form action="#" method="get" accept-charset="utf-8"> 
                <div class="checkboxes"> 
                  <?php 
				  
				  $page_records = $pages->profile_pages_data($record->module_id, $profile_id, 'page_sorting', 'ASC'); 
				  if(count($page_records) > 0){ 
					foreach($page_records as $page) { 
						echo "<label  style='font-weight:bold' ><input type='checkbox' id='checkbox-{$page->Page_id}' value='{$page->Page_id}' "; 
						if($page->access == 'yes'){ 
							echo "checked";	 
						} 
						echo " disabled />"; echo ucwords($page->page_title)."</label> ";  
					}				   
				  } 
				 ?> 
                </div> 
                 
              </form> 
              </div> 
               </div> 
             
                 <?php endforeach;?> 
                 </div> 
                </section> 
                  
        
      </div> 
    
     <!-- END JAVASCRIPTS --> 
      <script src="../../js/jquery.js"></script> 
    <script src="../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../js/respond.min.js" ></script> 
     <script type="text/javascript" src="../../assets/gritter/js/jquery.gritter.js"></script> 
    <script type="text/javascript" language="javascript" src="../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
    <!--common script for all pages--> 
    <script src="../../js/common-scripts.js"></script> 
    <script src="../../js/gritter.js" type="text/javascript"></script> 
    <script type="text/javascript" charset="utf-8"> 
          $(document).ready(function() { 
              $('#example2').dataTable( { 
                  "aaSorting": [[ 4, "desc" ]] 
              } ); 
			   $('#example1').dataTable( { 
                  "aaSorting": [[ 4, "desc" ]] 
              } ); 
			   $('#example').dataTable( { 
                  "aaSorting": [[ 4, "desc" ]] 
              } ); 
          } ); 
      </script> 
    </section> 
 </body> 
</html> 
