<?php
require_once 'CRUD.php';
class AdvertisementContent extends CRUD{
   //calss attributes
   public $id;
   public $adv_id;   
   public $lang_id;
   public $title;
   public $content;
  
   //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields , '');
   }   
	//define table name and fields
	protected static $table_name = 'advertisement_content';
	protected static $primary_fields = array('id', 'adv_id', 'title','content','lang_id');
	
	//get adv content
	public function get_adv_content($adv_id = null,$lang = null){
		$sql = " SELECT * FROM advertisement_content WHERE `adv_id` = $adv_id AND lang_id = $lang " ;
		$result_array = static::find_by_sql($sql);
	    return !empty($result_array)? array_shift($result_array) : false;
		
	}
	
	
	
}
?>