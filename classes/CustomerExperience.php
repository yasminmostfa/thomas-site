<?php 

require_once 'CRUD.php';

	 

class CustomerExperience extends CRUD{ 

	//calss attributes 

	public $id; 

	public $customer_id ; 

	public $job_title;

	public $job_role;

	public $experience_type; 

	public $career_level; 

	public $company_name; 

	public $from_date; 

	public $to_date ; 


	protected static $table_name = 'customer_experience'; 

	protected static $primary_fields = array('id','customer_id','job_title','job_role','experience_type','career_level','company_name','from_date', 'to_date'); 
 

	public function customer_experience( $customer_id = null){ 

			$sql = "SELECT  Distinct customer_experience.id AS id ,  customer_experience.job_title AS job_title ,
			        customer_experience.job_role AS job_role, customer_experience.experience_type AS experience_type ,  customer_experience.career_level AS career_level ,  customer_experience.company_name AS company_name , 
			            customer_experience.from_date AS from_date ,  customer_experience.to_date AS to_date
 
				FROM customer_experience
				WHERE 1
				 "; 

		if(!empty($customer_id)){		 

			 $sql .= " AND  customer_experience.customer_id = $customer_id ";

			 $result_array = static::find_by_sql($sql); 

			 return !empty($result_array)? array_shift($result_array) : false; 

		}else{ 

			return self::find_by_sql($sql);   

		}	


				 

	} 

	 // return all activities related to this user 
		public function get_experiences($customer_id){

			$sql  = "SELECT * FROM customer_experience WHERE customer_id = '{$customer_id}' "; 
			return self::find_by_sql($sql);

		}	

	

	 

}?> 