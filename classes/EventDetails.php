<?php 
require_once 'CRUD.php'; 
class EventDetails extends CRUD{ 
   //calss attributes 
   public $id; 
   public $event_id; 
   public $place;    
   public $start_date; 
   public $instructor;
   public $end_date;  
   public $enable_course; 
   //relation table attribute 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields, ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'events_details'; 
	protected static $primary_fields = array('id', 'event_id', 'place', 'start_date','end_date','instructor','enable_course');	 
} 
?>