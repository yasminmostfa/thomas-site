<?php
 require_once 'CRUD.php';
 class ForgetPassword extends CRUD{
    //calss attributes
    public $id;
    public $user_id;
    public $code;
   
    //relation table attribute
    
     //push attributes for relational tables
    public function enable_relation(){
 		array_push(static::$primary_fields , '');
    }   
    //define table name and fields
 	protected static $table_name = 'forget_password';
 	protected static $primary_fields = array('id','user_id','code');
	
}
 ?>
 