<?php 
require_once 'CRUD.php'; 
class GeneralSettings extends CRUD{ 
   //calss attributes 
   public $id; 
   public $meta_key; 
   public $title; 
   public $site_url; 
   public $email; 
   public $time_zone_id; 
   public $front_lang_id; 
   public $translate_lang_id; 
   public $enable_website; 
   public $offline_messages; 
   public $description;
   public $main_order_statues; 
   public $enable_store; 
   public $google_analitic;
   public $vufind_main_url;
   public $vufind_advanced_search;
   public $update_by; 
   public $last_update; 
   //relation table attribute 
   public $time_zone_name; 
   public $translate_lang_name; 
   public $front_lang_name; 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'time_zone_name', 'translate_lang_name', 'front_lang_name'); 
   }    
   //define table name and fields 
	protected static $table_name = 'general_setting'; 
	protected static $primary_fields = array('id','title','site_url','meta_key','email','time_zone_id','front_lang_id','translate_lang_id','enable_website','offline_messages','main_order_statues','enable_store','description','google_analitic','update_by', 'last_update','vufind_main_url','vufind_advanced_search'); 
   // get general settings data 
    public function general_settings_data(){ 
		  $sql = "SELECT general_setting.id AS id, general_setting.meta_key AS meta_key , general_setting.title AS title, 
		          general_setting.email AS email,general_setting.offline_messages AS offline_messages, 
				  general_setting.description AS description,general_setting.enable_website AS enable_website, 
				  users.user_name AS update_by, general_setting.last_update AS last_update, 
				  time_zones.name AS time_zone_name, localization_trans_lang.id AS translate_lang_id,  
				  general_setting.main_order_statues AS main_order_statues,general_setting.enable_store AS enable_store,
				  localization_trans_lang.name AS translate_lang_name,general_setting.site_url AS site_url, 
				  localization_front_lang.id AS front_lang_id, localization_front_lang.name AS front_lang_name,
				  general_setting.google_analitic AS google_analitic
				  FROM general_setting 
				  LEFT JOIN localization AS localization_trans_lang ON general_setting.translate_lang_id = localization_trans_lang.id 
				  LEFT JOIN localization AS localization_front_lang ON general_setting.front_lang_id = localization_front_lang.id 
				  LEFT JOIN users ON general_setting.update_by = users.id 
				  LEFT JOIN time_zones ON general_setting.time_zone_id = time_zones.id"; 
			 $result_array = static::find_by_sql($sql); 
			 return !empty($result_array)? array_shift($result_array) : false;			 
	} 
} 
?>