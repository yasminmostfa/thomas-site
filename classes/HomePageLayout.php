<?php 
require_once 'CRUD.php'; 
class HomePageLayout extends CRUD{ 
   //calss attributes 
   public $id; 
   public $title;
   public $position; 
   public $status;    
   public $plugin; 
   public $header_title;
   public $plugin_value;
   public $link_ar ;
   public $link_en ;
   public $link_limit;
   
   // relation columns 
    public $parsed_title;
    public $url;
    public $publish_date;
    public $link_id;
   //push attributes for relational tables 
   public function enable_relation(){ 
		 array_push(static::$primary_fields('parsed_title','url','publish_date','link_id')); 
   }    
   //define table name and fields 
	protected static $table_name = 'home_page_layout'; 
	protected static $primary_fields = array('id', 'title','position','status','plugin','header_title','plugin_value','link_ar','link_en','link_limit'); 
	public static function get_position($position){
		$sql = "SELECT * FROM home_page_layout WHERE `position`='$position' AND status = 'publish'";
		 $result_array = static::find_by_sql($sql); 
         return !empty($result_array)? array_shift($result_array) : false; 
	}
        
        public static function get_url($position){
		$sql = "SELECT * FROM home_page_layout WHERE `position`='$position' AND status = 'publish' And 'link' IS NOT NULL order by id asc ";
		 $result_array = static::find_by_sql($sql); 
         return !empty($result_array)? array_shift($result_array) : false; 
        
         
	}

  public function get_by_plugin($pluginId,$position){

      $sql = "SELECT * FROM home_page_layout WHERE `plugin`='$pluginId' AND `position`='$position' AND status = 'publish' And 'link' IS NOT NULL order by id asc ";
     $result_array = static::find_by_sql($sql); 
         return !empty($result_array)? array_shift($result_array) : false; 
  }
	 
	   
} 
?> 
