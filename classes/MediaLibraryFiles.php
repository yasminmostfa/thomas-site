<?php 
require_once 'CRUD.php'; 
class MediaLibraryFiles extends CRUD{ 
	//calss attributes 
	public $id; 
	public $name; 
	public $size; 
	public $type; 
	public $dir_id; 
	public $alternative_text; 
	public $description; 
	public $inserted_by; 
	public $inserted_date; 
	public $last_update;          
	public $updated_by; 
	//relation table attribute 
	 
	//push attributes for relational tables 
	public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
	}    
	//define table name and fields 
	protected static $table_name = 'media_library_files'; 
	protected static $primary_fields = array('id', 'name', 'size', 'type', 'dir_id', 'alternative_text', 'description', 'inserted_by', 'inserted_date', 'last_update', 
	 'updated_by'); 
	public function media_library_files_data($sort_filed = null, $order_by = null, $id = null){ 
		$sql = "SELECT media_library_files.id AS id, media_library_files.name AS name, media_library_files.type AS type, media_library_files.size AS size, 
				media_library_files.alternative_text AS alternative_text, media_library_files.description AS description, users.user_name AS inserted_by,  
				media_library_files.inserted_date AS inserted_date, user2.user_name AS updated_by, media_library_files.last_update AS last_update 
				FROM media_library_files 
				LEFT JOIN users ON media_library_files.inserted_by = users.id 
				LEFT JOIN users AS user2 ON media_library_files.updated_by = user2.id"; 
		if(!empty($id)){		 
			 $sql .= " WHERE media_library_files.id = $id "; 
			 $result_array = static::find_by_sql($sql); 
			 return !empty($result_array)? array_shift($result_array) : false; 
		}else{		 
			if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
			 } 
			return self::find_by_sql($sql);   
		}				 
	} 
} 
?>