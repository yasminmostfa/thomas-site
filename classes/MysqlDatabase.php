<?php
require_once ('ConnectionConfig.php');
class MysqlDatabase {
    public $connection;
    public $select_db;
    private $sql_command;
    //call open connection function when instantiation  class
    public function __construct() {
        $this->open_connection();
    }
    //open connection and select database
     private function open_connection(){
        $this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS,DB_NAME);
        if(!$this->connection){
            die("Connection Failed".  mysqli_error($this->connection));
        }else{
			  //mysqli_query($this->connection,"set character_set_server='utf8'"); 
		      //mysqli_query($this->connection,"set names 'utf8'");
			  mysqli_query($this->connection,"SET OPTION SQL_BIG_SELECTS = 1");
		}
    }
    //close connection
    public function close_connection(){
        if(isset($this->connection)){
            mysqli_close($this->connection);
            unset($this->connection);
        }
    }
    //excute sql statement query
    public function query($sql){
        $this->sql_command = $sql;
        $result_set = mysqli_query($this->connection,$sql);
        $this->check_query($result_set);
        return $result_set;
    }
    //check if there is sql error
    private function check_query($query){
        if(!$query){
            $error = 'Sql Command Error: '.$this->sql_command."<br />";
            $error .= 'sql Statement error: '.  mysqli_error($this->connection);
            die($error);
        }
    }
    //fetch array sql query
    public function fetch_array($result_set){
        return mysqli_fetch_array($result_set);
    }
    //num rows
    public function mysql_num_rows($result_set){
        return mysqli_num_rows($result_set);
    }
    //get inserted id
    public function inserted_id(){
        return mysqli_insert_id($this->connection);
    }
    //reutrn true if any process excute in db
    public function mysql_affected_rows(){
        return mysqli_affected_rows($this->connection);
    }
    //prevent xss & sql injection
    public function escape_values($val){
        $trim_val = trim($val);
        $xss_val = html_entity_decode($val);
        return mysqli_real_escape_string($this->connection,$xss_val);
    }
}
$database = new MysqlDatabase();
?>