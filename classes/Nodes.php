<?php 
require_once 'CRUD.php'; 
class Nodes extends CRUD{ 
   //calss attributes 
   public $id; 
   public $node_type; 
   public $status; 
   public $inserted_by; 
   public $inserted_date; 
   public $enable_comments; 
   public $enable_summary; 
   public $front_page;
   public $last_update; 
   public $update_by; 
   public $slide_show; 
   public $side_bar;
   public $start_publishing; 
   public $end_publishing; 
   public $hit_counter; 
   public $cover_image; 
   public $slider_cover; 
   public $model; 
   public $shortcut_link; 
   //relation table attribute 
   public $content_id; 
   public $title; 
   public $alias;  
   public $summary; 
   public $lang_name; 
   public $body;  
   public $lang_id;  
   public $last_name;  
   public $first_name; 
   public $category_title; 
   public $category_alias; 
   public $meta_keys; 
   public $meta_description; 
   public $layout_name; 
   public $model_name; 
   public $theme_name; 
    
    //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields ,'lang_name','$category_title','category_alias','content_id','title','alias','summary','body','lang_id', 
		'first_name','last_name','meta_keys','meta_description','model_name','layout_name','theme_name'); 
   }    
   //define table name and fields 
	protected static $table_name = 'nodes'; 
	protected static $primary_fields = array('id','status','enable_summary','inserted_by','inserted_date','last_update','front_page','slide_show','side_bar','update_by','enable_comments','start_publishing','end_publishing','url_redirect','cover_image','slider_cover','node_type','model','shortcut_link');     
	 
	//get by nodes data 
	public function node_data($type,$id = null,$status=null,$cat_id=null,$date_from=null,$date_to=null,$lang=null,$inserted_by=null,$sort_filed=null,$order_by=null, $limit=null,$offset=null){
		$sql = "SELECT DISTINCT nodes.id AS id, nodes_content.id AS content_id, nodes.status AS status,nodes_content.title as title,
		        nodes.enable_summary AS enable_summary,nodes.node_type AS node_type,
		  		nodes.inserted_date AS inserted_date,nodes.last_update AS last_update, users.user_name AS inserted_by, user2.user_name AS update_by,
				nodes.cover_image AS cover_image ,nodes.slider_cover AS slider_cover, nodes.enable_comments AS enable_comments,
				nodes_content.lang_id AS lang_id,nodes.start_publishing AS start_publishing, nodes.end_publishing AS end_publishing, nodes.slide_show AS slide_show,
				nodes.front_page AS front_page, localization.name AS lang_name,localization.id AS lang_id,nodes_content.meta_keys AS meta_keys, 
				nodes_content.meta_description AS meta_description,nodes.shortcut_link AS shortcut_link ,nodes.side_bar AS side_bar
				FROM nodes  
				LEFT JOIN users ON nodes.inserted_by = users.id 
				LEFT JOIN nodes_content ON nodes.id = nodes_content.node_id 
				LEFT JOIN users AS user2 ON nodes.update_by = user2.id 
				LEFT JOIN nodes_selected_taxonomies ON nodes.id = nodes_selected_taxonomies.node_id 
				LEFT JOIN localization ON nodes_content.lang_id = localization.id 
				WHERE 1 "; 
			    if(!empty($lang)){ 
				$sql .= " AND nodes_content.lang_id = '$lang' ";	 
			    } 
		if(!empty($id)){		 
			 $sql .= "  AND nodes.id = $id "; 
			 $result_array = static::find_by_sql($sql); 
			 return !empty($result_array)? array_shift($result_array) : false; 
		}else{
			$sql .=" AND nodes.node_type = '$type'   "; 
			
			if(!empty($cat_id)){ 
				$categories = implode(",",$cat_id); 
				$sql .= " AND nodes_selected_taxonomies.taxonomy_id in ($categories) 
				          AND nodes_selected_taxonomies.node_type = '$type'  
						  AND nodes_selected_taxonomies.taxonomy_type = 'category'";	 
			} 
			if(!empty($date_from)){ 
				$sql .=" AND CAST(nodes.inserted_date as DATE)  BETWEEN '$date_from' AND '$date_to' ";	 
			} 
			if(!empty($status)){ 
				$sql .= " AND nodes.status = '$status' ";	 
			}	 
			if(!empty($inserted_by)){ 
				$inserted_by_string = implode(",",$inserted_by); 
				$sql .= " AND  nodes.inserted_by  in  ($inserted_by_string) ";	 
			}		 
			if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= "  ORDER BY ".$sort_filed." ".$order_by;  
			} 
			if(!empty($limit)){ 
			    $sql .= " LIMIT {$limit}";
				if(!empty($offset)){
				   $sql .= " OFFSET {$offset}";	 
				}
			}
			return self::find_by_sql($sql);   
		}				 
	} 
	//for front nodes 
	public static function front_node_data($id=null, $type = null,$alias = null,$alias_exclusion = null,$lang = null,$front_page = null,$slide_show = null,$side_bar = null,$cat_id = null,$tag_id = null,$date  = null, $search = null, $retrieve = null,$limit = null,$page_offset = null,
	$enable_course = null,$courses = null){ 
		global $database; 
	    $lang = $database->escape_values($lang); 
		$alias = $database->escape_values($alias); 
		$alias_exclusion = $database->escape_values($alias_exclusion); 
		$sql = "SELECT DISTINCT nodes.id AS id, nodes_content.title AS title, nodes.status AS status, nodes_content.summary AS summary,  
				nodes.enable_summary AS enable_summary, nodes_content.body AS body, nodes_content.alias AS alias,nodes.enable_comments AS enable_comments, 
				nodes.start_publishing AS start_publishing, nodes.cover_image  AS cover_image ,nodes.node_type as  node_type, nodes.side_bar AS side_bar,
				nodes.slider_cover AS slider_cover,users.first_name AS first_name ,users.last_name AS last_name, nodes_content.meta_description AS meta_description
				FROM nodes  
				LEFT JOIN nodes_content ON nodes.id = nodes_content.node_id 
				LEFT JOIN nodes_selected_taxonomies ON nodes.id = nodes_selected_taxonomies.node_id 
				LEFT JOIN users ON nodes.inserted_by = users.id ";
				if($type == "event"){
					$sql .= " LEFT JOIN events_details ON nodes.id = events_details.event_id ";
				}
				$sql .= " WHERE nodes.status = 'publish'  
				AND nodes_content.lang_id = '$lang' 
				"; 
				 if(!empty($type)){ 
			           $sql .=" AND nodes.node_type = '$type'"; 
			     } 
				 if(!empty($id)){
				 	$sql .=" AND nodes.id = '$id'";
				 }
				if(!empty($alias)){ 
					$sql .=" AND nodes_content.alias = '$alias' "; 
				} 
				if(!empty($search)){ 
					$sql .=" AND nodes_content.body like '%$search%'  
					        OR nodes_content.title like '%$search%' "; 
				} 
				if(!empty($alias_exclusion)){ 
					$sql .=" AND nodes_content.alias != '$alias_exclusion' "; 
				} 
				if(!empty($date)){
				   $sql .=" AND nodes.start_publishing <= '$date' AND (nodes.end_publishing >= '$date' OR nodes.end_publishing = '0000-00-00 00:00:00') ";	
				}
				
				if(!empty($front_page)){ 
					$sql .=" AND nodes.front_page = '$front_page' "; 
				} 
				if(!empty($side_bar)){ 
					$sql .=" AND nodes.side_bar = '$side_bar' "; 
				} 
				if(!empty($slide_show)){ 
					$sql .=" AND nodes.slide_show = '$slide_show' "; 
				}				 
				if(!empty($cat_id)){ 
					$categories = $database->escape_values($cat_id); 
					$sql .= " AND nodes_selected_taxonomies.taxonomy_id in ($categories) 
					          AND nodes_selected_taxonomies.taxonomy_type = 'category'  
							  AND nodes_selected_taxonomies.node_type = '$type' ";	 
				} 
				if(!empty($tag_id)){ 
					$sql .= " AND nodes_selected_taxonomies.taxonomy_id in ($tag_id) 
					         AND nodes_selected_taxonomies.taxonomy_type = 'tag' 
							 AND nodes_selected_taxonomies.node_type = '$type' "; 
				} 
				if($type == "event"){
				 if(!empty($enable_course)){ 
					$sql .= " AND  events_details.enable_course = '$enable_course' ";	 
				  } 
				}
				if($type == "event"){
				  if(!empty($courses)){ 
					$sql .= " AND nodes_selected_taxonomies.taxonomy_id in ($courses) 
					         AND nodes_selected_taxonomies.taxonomy_type = 'courses' 
							 AND nodes_selected_taxonomies.node_type = 'event' 
							
							 
							 "; 
				 }
				}

	 		    $sql .= " ORDER BY nodes.start_publishing DESC";	 
				if(!empty($limit)){ 
					$sql .= " LIMIT ".$limit; 
				} 
				if(!empty($page_offset)){ 
				   $sql .= " OFFSET $page_offset";	 
				 } 
				if($retrieve == 'one'){ 
				   $result_array = static::find_by_sql($sql); 
				   return !empty($result_array)? array_shift($result_array) : false;				 
				}else{ 
				   return self::find_by_sql($sql);  
				}	 
		} 
	//get node content  
	public static function get_node_content($node_id,$lang = null){ 
		$sql = "SELECT nodes_content.id AS id, nodes_content.lang_id AS lang_id, nodes_content.title AS title, nodes_content.alias AS alias,
				nodes_content.summary AS summary, nodes_content.body AS body, nodes_content.meta_keys AS meta_keys, nodes_content.meta_description AS meta_description
				FROM nodes_content 
				WHERE node_id = '$node_id'";
		if(!empty($lang)){		
			$sql .= "AND lang_id = '$lang'" ; 
			$result_array = static::find_by_sql($sql); 
	    	return !empty($result_array)? array_shift($result_array) : false; 			
		}else{
			return static::find_by_sql($sql); 
		}
	} 			 
	//return rest languages
	public function return_other_languages($alias, $lang){ 
		global $database; 
	    $alias = $database->escape_values($alias);	 
		$lang = $database->escape_values($lang);		 
		$sql = "SELECT nodes_content.alias AS alias 
				FROM nodes, nodes_content, localization 
				WHERE nodes.id = nodes_content.node_id AND nodes_content.node_id 
				IN (SELECT node_id FROM nodes_content WHERE alias = '{$alias}') 
				AND nodes_content.lang_id = localization.id  
				AND localization.label = '{$lang}'";  
		$result_array = static::find_by_sql($sql);
		return !empty($result_array)? array_shift($result_array) : false;	 
	} 
	//get node model,theme and layout 
	public static function get_model($node_id = null) { 
		$sql = "SELECT  nodes.model AS model, theme_layout_model.name AS model_name, themes.name AS theme_name 
				FROM nodes, theme_layout_model, themes_layouts, themes 
				WHERE nodes.model = theme_layout_model.id AND theme_layout_model.layout_id = themes_layouts.id  
				AND nodes.id = $node_id 
			    AND themes_layouts.theme_id = themes.id"; 
       $result_array = static::find_by_sql($sql); 
       return !empty($result_array)? array_shift($result_array) : false; 
	}  
	//get front node model,theme and layout 
	public static function front_get_model($node_id = null) { 
		$sql = "SELECT themes_layouts.layout_name AS layout_name, nodes.model AS model  
				FROM nodes,theme_layout_model,themes_layouts 
				WHERE nodes.model = theme_layout_model.id  
				AND theme_layout_model.layout_id = themes_layouts.id  
				AND nodes.id = '$node_id' "; 
		$result_array = static::find_by_sql($sql); 
		return !empty($result_array)? array_shift($result_array) : false; 
	} 
	//for front products 
	public static function front_product_data($alias = null,$alias_exclusion = null,$lang = null,$date = null,$front_page = null,$slide_show = null,$cat_id = null,$tag_id = null,$retrieve = null,$limit = null,$page_offset = null,$store_id = null,$search = null){ 
		global $database; 
	    $lang = $database->escape_values($lang); 
		$alias = $database->escape_values($alias); 
		$alias_exclusion = $database->escape_values($alias_exclusion); 
		$sql = "SELECT DISTINCT nodes.id AS id, nodes_content.title AS title, nodes.status AS status, nodes_content.summary AS summary,  
				nodes.enable_summary AS enable_summary, nodes_content.body AS body, nodes_content.alias AS alias,nodes.enable_comments AS enable_comments, 
				nodes.start_publishing AS start_publishing, nodes.cover_image  AS cover_image , 
				nodes.slider_cover AS slider_cover,users.first_name AS first_name ,users.last_name AS last_name  
				FROM nodes  
				LEFT JOIN nodes_content ON nodes.id = nodes_content.node_id 
				LEFT JOIN nodes_selected_taxonomies ON nodes.id = nodes_selected_taxonomies.node_id 
				LEFT JOIN ecom_products_selected_stores ON nodes.id = ecom_products_selected_stores.product_id
				LEFT JOIN users ON nodes.inserted_by = users.id 
				WHERE nodes.status = 'publish'  
				AND nodes_content.lang_id = '$lang' 
				AND nodes.node_type = 'product' "; 
				if(!empty($alias)){ 
					$sql .=" AND nodes_content.alias = '$alias' "; 
				} 
				if(!empty($search)){ 
					$sql .=" AND nodes_content.body like '%$search%'  
					        OR nodes_content.title like '%$search%' "; 
				} 
				if(!empty($alias_exclusion)){ 
					$sql .=" AND nodes_content.alias != '$alias_exclusion' "; 
				} 
				if(!empty($date)){ 
				   $sql .=" AND nodes.start_publishing <= '$date' AND (nodes.end_publishing >= '$date' OR nodes.end_publishing = '0000-00-00 00:00:00') ";	 
				} 
				if(!empty($front_page)){ 
					$sql .=" AND nodes.front_page = '$front_page' "; 
				} 
				if(!empty($slide_show)){ 
					$sql .=" AND nodes.slide_show = '$slide_show' "; 
				}				 
				if(!empty($cat_id)){ 
					$categories = $database->escape_values($cat_id); 
					$sql .= " AND nodes_selected_taxonomies.taxonomy_id in ($categories) 
					          AND nodes_selected_taxonomies.taxonomy_type = 'category'  
							  AND nodes_selected_taxonomies.node_type = 'product' ";	 
				} 
				if(!empty($store_id)){ 
					 
					$sql .= " AND ecom_products_selected_stores.branch_id = '$store_id'";	 
				} 
				if(!empty($tag_id)){ 
					$sql .="  AND nodes_selected_taxonomies.taxonomy_id in ($tag_id) 
					          AND nodes_selected_taxonomies.taxonomy_type = 'tag' 
							  AND nodes_selected_taxonomies.node_type = 'product' "; 
				} 
	 		$sql .= " ORDER BY nodes.start_publishing DESC";	 
				if(!empty($limit)){ 
					$sql .= " LIMIT ".$limit; 
				} 
				if(!empty($page_offset)){ 
				   $sql .= " OFFSET $page_offset";	 
				 } 
				if($retrieve == 'one'){ 
				   $result_array = static::find_by_sql($sql); 
				   return !empty($result_array)? array_shift($result_array) : false;				 
				}else{ 
				   return self::find_by_sql($sql);  
				}	 
		}	 		 
} 
?>