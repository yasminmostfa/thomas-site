<?php 
require_once 'CRUD.php'; 
class NodesImageGallery extends CRUD{ 
   //calss attributes 
   public $id; 
   public $related_id; 
   public $image;    
   public $sort; 
   public $caption;   
   //relation table attribute 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields, ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'nodes_image_gallery'; 
	protected static $primary_fields = array('id', 'related_id', 'image', 'sort','caption');	
	
	public static function get_node_images_gallery($image,$node_id){
			$sql = "SELECT DISTINCT  * from nodes_image_gallery WHERE  related_id = '{$node_id}' AND  image = '{$image}' ORDER BY sort ASC ";
			$result_array = static::find_by_sql($sql);
            return  $result_array;
	}
	 
} 
?>