<?php
require_once 'CRUD.php';
class PollQuestions extends CRUD{
   //calss attributes
   public $id;
   public $poll;
   public $status;
   public $inserted_by;
   public $inserted_date;
   public $update_by;
   public $last_update;
 
   //relation table attribute
   public $page_alias;
   //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields , '');
   }   
   //define table name and fields
	protected static $table_name = 'poll_questions';
	protected static $primary_fields = array('id','poll','status','inserted_by','inserted_date','update_by','last_update');
	// get poll  questions data
	public function poll_questions_data($sort_filed = null, $order_by = null, $id = null, $menu_group_id = null){
		$sql = "SELECT poll_questions.id AS id,poll_questions.poll AS poll , users.user_name AS inserted_by,
		        poll_questions.inserted_date AS inserted_date,poll_questions.status AS status,
				users2.user_name AS update_by ,poll_questions.last_update AS last_update
				FROM poll_questions
				LEFT JOIN users ON poll_questions.inserted_by = users.id
				LEFT JOIN users AS users2 ON poll_questions.update_by = users2.id";
		if(!empty($id)){		
			 $sql .= " WHERE poll_questions.id = $id ";
			 $result_array = static::find_by_sql($sql);
			 return !empty($result_array)? array_shift($result_array) : false;
		}else{	
			if(!empty($sort_filed) && !empty($order_by)){
				$sql .= " ORDER BY ".$sort_filed." ".$order_by; 
			 }
			return self::find_by_sql($sql);  
		}				
	}
	
	
	
	
	
	
	
	
	
}
?>