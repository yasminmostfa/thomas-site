<?php 
require_once 'CRUD.php'; 
class ProfilePagesAccess extends CRUD{ 
   //calss attributes 
   public $id; 
   public $profile_id; 
   public $module_id; 
   public $Page_id; 
   public $access; 
   public $inserted_by; 
   public $inserted_date; 
   //relation table attribute 
   public $page_title; 
   public $page_sorting; 
   public $source; 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields, 'page_title', 'page_sorting', 'source'); 
   }    
   //define table name and fields 
	protected static $table_name = 'profile_pages_access'; 
	protected static $primary_fields = array('id', 'profile_id', 'module_id', 'Page_id', 'access', 'inserted_by','inserted_date'); 
	//get pages name 
	public function profile_pages_data($module_id, $profile_id, $sort_filed = null, $order_by = null){ 
		$sql = "SELECT profile_pages_access.Page_id AS Page_id, cms_module_access.title AS page_title, profile_pages_access.access AS access, 
				cms_module_access.sorting AS page_sorting 
				FROM cms_module_access, profile_pages_access 
				WHERE cms_module_access.id = profile_pages_access.Page_id AND profile_pages_access.module_id = '{$module_id}'  
				AND profile_pages_access.profile_id = '{$profile_id}'";	 
		if(!empty($sort_filed) && !empty($order_by)){ 
		   $sql .= " ORDER BY ".$sort_filed." ".$order_by; 
	   	}		 
		return static::find_by_sql($sql);  		 
	}	 
	 
	//get modules for login user  
	public function user_profile_pages_access($profile_id, $module_id){ 
		$sql = "SELECT cms_module_access.title AS page_title, cms_module_access.file_source AS source 
				FROM profile_pages_access, cms_module_access 
				WHERE profile_pages_access.page_id = cms_module_access.id AND  profile_pages_access.profile_id = '{$profile_id}' 
				AND profile_pages_access.access =  'yes'"; 
		if(!empty($module_id)){		 
		  $sql .=" AND cms_module_access.shadow =  'no' AND profile_pages_access.module_id = '{$module_id}'"; 
		} 
		$sql .=" ORDER BY cms_module_access.sorting ASC  "; 
		return self::find_by_sql($sql);  		 
	}	 
} 
?>
