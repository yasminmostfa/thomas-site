<?php 
require_once 'CRUD.php'; 
class SocialEmailSubscription extends CRUD{ 
   //calss attributes 
   public $id; 
   public $user_name; 
   public $email; 
   public $inserted_date; 
   //relation table attribute 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'social_email_subscription'; 
	protected static $primary_fields = array('id','user_name','email'); 

} 
?>
