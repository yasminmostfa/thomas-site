<?php
require_once 'CRUD.php';
class Taxonomies  extends CRUD{
   //calss attributes
   public $id;
   public $parent_id ;
   public $taxonomy_type;
   public $sorting;
   public $url;
   public $status;
   public $main_menu;
   public $show_image;
   public $cover;
   public $inserted_by;
   public $inserted_date;
   public $update_by;
   public $last_update;
   public $user_allowed_page_array;
   //relation table attribute
   public $content_id;
   public $name;
   public $alias; 
   public $description;
   public $lang_name;
   public $lang_id; 
   
    //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields ,'content_id','name','alias','description','lang_id','lang_name');
   }   
   
   //define table name and fields
	protected static $table_name = 'taxonomies';
	protected static $primary_fields = array('id','parent_id','taxonomy_type','sorting','show_image','status','main_menu','inserted_by','cover', 'inserted_date','update_by', 'last_update',"url");
	
      // get taxonomy category data
    public  static function taxonomies_data($taxonomy_type, $id = null ,$parent_id = null,$sort_filed = null, $order_by = null,$name = null,
	$retrieve = 'one',$lang = null ,$main_menu = null,$show_image = null,$limit = null,$page_offset = null){
		$sql = "SELECT Distinct taxonomies.id AS id, taxonomies_content.id AS content_id, taxonomies_content.name AS name, taxonomies.status AS status,
		        taxonomies_content.description AS description , users.user_name AS inserted_by,taxonomies_content.alias as alias,
				taxonomies_content.lang_id AS lang_id,taxonomies.inserted_date AS inserted_date,user2.user_name AS update_by,
				taxonomies.last_update AS last_update,taxonomies.cover AS cover,taxonomies.parent_id  AS parent_id,taxonomies.url AS url
				FROM taxonomies 
				LEFT JOIN  taxonomies_content  ON taxonomies.id = taxonomies_content.taxonomy_id
				LEFT JOIN localization ON taxonomies_content.lang_id = localization.id
				LEFT JOIN users ON taxonomies.inserted_by = users.id
				LEFT JOIN users AS user2 ON taxonomies.update_by = user2.id
				WHERE  taxonomies.taxonomy_type = '$taxonomy_type'  ";
				if(!empty($lang)){
				$sql .= " AND taxonomies_content.lang_id = '$lang' ";	
			    }
		if(!empty($id)){		
			 $sql .= "  AND  taxonomies.id = '$id' ";
			 $result_array = static::find_by_sql($sql);
			 return !empty($result_array)? array_shift($result_array) : false;
		}else{
			if(!empty($parent_id)){
				 $sql .= "  AND  taxonomies.parent_id = '$parent_id' ";
			 }
			 if(!empty($main_menu)){
				 $sql .= "  AND  taxonomies.main_menu = '$main_menu' ";
			 }
			 if(!empty($show_image)){
				 $sql .= "  AND  taxonomies.show_image = '$show_image' ";
			 }
			 if(!empty($name)){
				 $sql .= "  AND  taxonomies_content.name LIKE '%$name%' ";
			 }
			if(!empty($sort_filed) && !empty($order_by)){
				$sql .= " ORDER BY  taxonomies. ".$sort_filed." ".$order_by; 
			  if(!empty($limit)){
					$sql .= " LIMIT ".$limit;
				}
				if(!empty($page_offset)){
				   $sql .= " OFFSET $page_offset";	
				 }
			 }
			 
			if($retrieve == 'one'){
				   $result_array = static::find_by_sql($sql);
				   return !empty($result_array)? array_shift($result_array) : false;				
				}else{
				   return self::find_by_sql($sql); 
				}	
		}				
	}
	
	public static function get_tag_id($name){
       $sql = "SELECT taxonomies.id AS id
	   		   FROM taxonomies, taxonomies_content
			   WHERE taxonomies.id = taxonomies_content.taxonomy_id AND  taxonomy_type = 'tag' AND  taxonomies_content.name = '{$name}' ";
       $result_array = static::find_by_sql($sql);
       return !empty($result_array)? array_shift($result_array) : false;
	}
	public  function get_taxonomy_content($taxonomy_id = null,$lang = null){ 
		$sql = " SELECT * FROM taxonomies_content WHERE taxonomy_id = '$taxonomy_id'  
		AND lang_id = '$lang'" ; 
		$result_array = static::find_by_sql($sql); 
	    return !empty($result_array)? array_shift($result_array) : false; 
	} 	
 
	//get all taxonomies in front pages 
	public  function front_taxonomies_data($id = null,$taxonomy_type,$alias = null,$sort_filed = null, $order_by = null,
	                 $retrieve = 'one',$lang = null,$perant = null,$show_main_menu = null, $search = null,$limit = null){
		$sql = "SELECT Distinct taxonomies.id AS id, taxonomies_content.id AS content_id, taxonomies_content.name AS name,
		        taxonomies_content.alias as alias, taxonomies_content.description AS description, taxonomies_content.lang_id AS lang_id
				FROM taxonomies 
				JOIN taxonomies_content ON taxonomies.id = taxonomies_content.taxonomy_id
				JOIN localization ON taxonomies_content.lang_id = localization.id
				WHERE taxonomies.taxonomy_type = '$taxonomy_type' AND taxonomies.status = 'publish'";
			    if(!empty($lang)){
				   $sql .= " AND taxonomies_content.lang_id = '$lang' ";	
			    }
				
		if(!empty($id)){		
			 $sql .= "  AND  taxonomies.id = '$id' ";
			 $result_array = static::find_by_sql($sql);
			 return !empty($result_array)? array_shift($result_array) : false;
		}else{
			if(!empty($perant)){
				   $sql .= " AND taxonomies.parent_id = '$perant' ";	
			 }
			 if(!empty($alias)){
				   $sql .= " AND taxonomies_content.alias = '$alias' ";	
			 }
			  if(!empty($show_main_menu)){
				   $sql .= " AND taxonomies.main_menu = '$show_main_menu' ";	
			 }
		
			if(!empty($sort_filed) && !empty($order_by)){
				$sql .= " ORDER BY  taxonomies. ".$sort_filed." ".$order_by; 
				 if(!empty($limit)){
					 $sql .= " LIMIT ".$limit;
				 }
			 }
			if($retrieve == 'one'){
				   $result_array = static::find_by_sql($sql);
				   return !empty($result_array)? array_shift($result_array) : false;				
				}else{
				   return self::find_by_sql($sql); 
				}	
		}						
	}	
	//return second langauge  page by alias 
	public function change_alias_language($alias, $lang){
		global $database;
	    $alias = $database->escape_values($alias);	
		$lang = $database->escape_values($lang);		
		$sql = "SELECT taxonomies_content.alias AS alias
				FROM taxonomies, taxonomies_content, localization
				WHERE taxonomies.id = taxonomies_content.taxonomy_id AND taxonomies_content.taxonomy_id
				IN (SELECT taxonomy_id FROM taxonomies_content WHERE alias = '{$alias}')
				AND taxonomies_content.lang_id = localization.id 
				AND localization.label = '{$lang}'"; 
		$result_array = static::find_by_sql($sql);
		return !empty($result_array)? array_shift($result_array) : false;		
	}
	//get categories and subcat for admin area	 
	public function get_type($current_cat_id,$count,$lang , $type) {
		  static $option_results;
		   $indent_flag = "";
		  // if there is no current category id set, start off at the top level (zero)
		  if (!isset($current_cat_id)) {
		  $current_cat_id =0;
		  }
		  // increment the counter by 1
		  $count = $count+1;
		  // query the database for the sub-categories of whatever the parent category is
		  $sql =  "SELECT taxonomies.id AS id, taxonomies_content.name AS name
						  FROM taxonomies  
						  LEFT JOIN taxonomies_content ON taxonomies.id = taxonomies_content.taxonomy_id
						  WHERE taxonomies.parent_id = '$current_cat_id'
						  AND taxonomies_content.lang_id = '$lang'
						  AND  taxonomies.taxonomy_type = '$type'";
		  $records = self::find_by_sql($sql); 
		  // our category is apparently valid, so go ahead €¦
		  if ($records) {
			foreach($records as $record){
			  if ($current_cat_id!=0) {
				$indent_flag =  '&nbsp;&nbsp;';
				for ($x =2; $x<=$count; $x++) {
				$indent_flag .=  '<strong >></strong>';
				}
			}
			$cat_name = $indent_flag.$record->name;
			$option_results[$record->id] = $cat_name;
			// now call the function again, to recurse through the child categories
			$this->get_type($record->id, $count,$lang , $type);
			}
		   }
		  return $option_results;
	}
	//get parent and child 
	//unlimite loop
	//used it on pageload to get all parent and child
	public function front_get_parent_sub_categories($current_cat_id, $count, $lang_id, $node_type) {
		  static $option_results;
		   $indent_flag = "";
		  // if there is no current category id set, start off at the top level (zero)
		  if (!isset($current_cat_id)) {
		  $current_cat_id =0;
		  }
		  // increment the counter by 1
		  $count = $count+1;
		  // query the database for the sub-categories of whatever the parent category is
		  $sql =  "SELECT taxonomies.id AS id, taxonomies_content.name AS name
				   FROM taxonomies
				   JOIN taxonomies_content ON taxonomies.id = taxonomies_content.taxonomy_id AND taxonomies.taxonomy_type = 'category' 
				   JOIN nodes_selected_taxonomies ON  nodes_selected_taxonomies.taxonomy_id = taxonomies.id AND nodes_selected_taxonomies.node_type = '$node_type'  
				   WHERE taxonomies.parent_id = '$current_cat_id' AND taxonomies_content.lang_id = '$lang_id'  ";
		 $records = self::find_by_sql($sql); 
		  // our category is apparently valid, so go ahead €¦
		  if ($records) {
			foreach($records as $record){
			  if ($current_cat_id!=0) {
				$indent_flag =  '&nbsp;&nbsp;';
				for ($x =2; $x<=$count; $x++) {
				$indent_flag .=  '<strong >></strong>';
				}
			}
			$cat_name = $indent_flag.$record->name;
			$option_results[$record->id] = $cat_name;
			// now call the function again, to recurse through the child categories
			$this->front_get_parent_sub_categories($record->id, $count, $lang_id, $node_type);
			}
		   }
		  return $option_results;
	}
	
	public function front_categories_for_filtration($categories, $features){
		$sql = "
			SELECT DISTINCT taxonomy_id AS id
			FROM nodes_selected_taxonomies ";
		if(!empty($features)){	
			$features = explode("/",$features);
			foreach($features as $i=>$n){
				if(!empty($n))
				$sql .= " JOIN ecom_products_selected_features_values AS b$i ON nodes_selected_taxonomies.node_id = b$i.product_id AND b$i.feature_value_id in ($n) ";
			}
		}			
		$sql .= " WHERE taxonomy_id IN ($categories) ";
		  return self::find_by_sql($sql); 
	}
	
}
?>
