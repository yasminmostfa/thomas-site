<?php 
require_once 'CRUD.php'; 
class ThemeIndex extends CRUD{ 
	//calss attributes 
	public $id; 
	public $theme_id; 
	public $plugin_place_number; 
	public $plugin;    
	//relation table attribute 
	//push attributes for relational tables 
	public function enable_relation(){ 
		array_push(static::$primary_fields , 'user_name'); 
	}    
	//define table name and fields 
	protected static $table_name = 'theme_index'; 
	protected static $primary_fields = array('id', 'theme_id', 'plugin_place_number', 'plugin'); 
	 
	public static function theme_index_plugin_space($theme_name = null){ 
		$sql = "SELECT theme_index.id AS id, theme_index.plugin_place_number AS plugin_place_number, plugins.source AS plugin 
				FROM themes, theme_index, plugins 
				WHERE themes.id = theme_index.theme_id AND themes.name = '{$theme_name}' AND plugins.id = theme_index.plugin 
				ORDER BY plugin_place_number ASC"; 
		 return static::find_by_sql($sql);   
	}	 
} 
?>