<?php 
require_once 'CRUD.php'; 
class UsersFeeds extends CRUD{ 
   //calss attributes 
   public $id;
   public $custome_info_id; 
   public $resource_type; 
   public $resource_id;    
   public $feeds_topic_id; 
   public $feed_title;
   public $inserted_date;   
   //relation table attribute 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields, ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'users_feeds'; 
	protected static $primary_fields = array('id', 'resource_type', 'resource_id', 'feeds_topic_id','inserted_date','custome_info_id','feed_title');	
	
	// get feed function
	public function get_feeds($resource_type = null,$custome_info_id = null,$resource_id = null,$sort_filed = null,$order_by = null,$limit = null,$offset = null){
	    $sql = "SELECT  * From users_feeds WHERE  1 ";
		
		 if($resource_type){
			 $sql .= " AND  resource_type  = '$resource_type' ";
		 }
		 if($custome_info_id){
			 $sql .= " AND  custome_info_id  = '$custome_info_id' ";
		 }
		  if($resource_id){
			 $sql .= " AND  resource_id  = '$resource_id' ";
		 }
		 if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= "   ORDER BY ".$sort_filed." ".$order_by;  
	    }
		if(!empty($limit)){ 
			    $sql .= " LIMIT {$limit}";
				if(!empty($offset)){
				   $sql .= " OFFSET {$offset}";	 
				}
	   }
	    return self::find_by_sql($sql);   
	 
     } 
} 
  
?>