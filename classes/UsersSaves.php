<?php 
require_once 'CRUD.php'; 
class UsersSaves extends CRUD{ 
   //calss attributes 
   public $id;
   public $custome_info_id; 
   public $resource_id;    
   public $url; 
   public $title;
   public $inserted_date;   
   //relation table attribute 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields, ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'users_saves'; 
	protected static $primary_fields = array('id', 'resource_id', 'url','inserted_date','custome_info_id','title');	
	
	// get feed function
	public function get_feeds($custome_info_id = null,$resource_id = null,$sort_filed = null,$order_by = null,$limit = null,$offset = null){
	    $sql = "SELECT  * From users_saves WHERE  1 ";
		
		 
		 if($custome_info_id){
			 $sql .= " AND  custome_info_id  = '$custome_info_id' ";
		 }
		  if($resource_id){
			 $sql .= " AND  resource_id  = '$resource_id' ";
		 }
		 if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= "   ORDER BY ".$sort_filed." ".$order_by;  
	    }
		if(!empty($limit)){ 
			    $sql .= " LIMIT {$limit}";
				if(!empty($offset)){
				   $sql .= " OFFSET {$offset}";	 
				}
	   }
	    return self::find_by_sql($sql);   
	 
     } 
} 
  
?>