<?php 
require_once 'CRUD.php'; 
class Views extends CRUD{ 
   //calss attributes 
   public $id;
   public $resource_id;    
   public $number_views;
   public $type; 
  
   //relation table attribute 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields, ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'views'; 
	protected static $primary_fields = array('id', 'resource_id', 'number_views','type');	
	
	// get feed function
	public function get_views($type = null,$resource_id = null){
	    $sql = "SELECT  * From views WHERE  1 ";
		  if($type){
			 $sql .= " AND  type  = '$type' ";
		 }
		  if($resource_id){
			 $sql .= " AND  resource_id  = '$resource_id' ";
		 }
		
	   $result_array = static::find_by_sql($sql); 
       return !empty($result_array)? array_shift($result_array) : false; 
	 
     } 
} 
  
?>