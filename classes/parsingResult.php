<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of parsingResult
 *
 * @author Anemam
 */
require_once 'CRUD.php'; 

class parsingResult extends CRUD {
    //put your code here
    public $parsed_title;
    public $url;
    public $publish_date;
    public $link_id;
    public $id;
    
    
     //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
	//define table name and fields 
	protected static $table_name = 'parsing_results'; 
	protected static $primary_fields = array('id', 'link_id','parsed_title','publish_date','url'); 
	
       
    
}
