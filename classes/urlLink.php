<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'CRUD.php'; 
/**
 * Description of urlLink
 *
 * @author Anemam
 */
class urlLink extends CRUD{
    //put your code here
    public $id; 
  public $link; 
  public $title;
  public $description;
  public $inserted_date;
  public $inserted_by;
  public $url;
  public $publish_date;
  public $parsed_title;
  
 
    
     //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'title','description','inserted_date','inserted_by','url','publish_date','parsed_title'); 
   }    
	//define table name and fields 
	protected static $table_name = 'url'; 
	protected static $primary_fields = array('id', 'link'); 
	
	 //for front nodes 
	public static function ParsingUrl($alias = null,$lang = null){ 
		global $database; 
	    $lang = $database->escape_values($lang); 
		$alias = $database->escape_values($alias); 
		$sql = "SELECT  url.id AS id, url.link AS link , url_before_parse.title AS title, url_before_parse.description AS description, url_before_parse.inserted_date
                        , url_before_parse.inserted_by, parsing_results.url,parsing_results.publish_date,parsing_results.parsed_title AS parsed_title
				FROM url 
				LEFT JOIN url_before_parse ON url.id = url_before_parse.link_id 
				LEFT JOIN parsing_results ON url.id = parsing_results.link_id 
				WHERE url_before_parse.lang_id = '$lang' "; 
				
				
				if(!empty($alias)){ 
					$sql .=" AND url_before_parse.alias = '$alias' "; 
				} 
				 
				
				
				   return self::find_by_sql($sql);  
					 
		} 
}
