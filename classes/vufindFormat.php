<?php 
require_once 'CRUD.php'; 
class vufindFormat extends CRUD{ 
   //calss attributes 
   public $id; 
   public $type_format; 
   public $inserted_by;
   // relation attributes
    public $title; 
   public $lang_id;
   public $name;
   
    
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'title','lang_id','name'); 
   }    
   //define table name and fields 
	protected static $table_name = 'vufind_format'; 
	protected static $primary_fields = array('id','type_format','inserted_by'); 
   // get general settings data 
   

   public  function get_format_data(){ 
      $sql = "SELECT vufind_format.id AS id, vufind_title.title AS title, vufind_format.type_format AS type_format, users.user_name as inserted_by,vufind_title.lang_id AS lang_id
            FROM vufind_format 
            LEFT JOIN vufind_title ON vufind_title.format_id= vufind_format.id
            LEFT join users on users.id = vufind_format.inserted_by";
           
         return self::find_by_sql($sql);   
   }   

   //  public  function get_format_data(){ 
   //    $sql = "SELECT  vufind_format.type_format AS type_format FROM vufind_format";
           
   //      $result_array = self::find_by_sql($sql); 
   //        return !empty($result_array)? array_shift($result_array) : false;   
   // }   
   
    public static function get_format_data_by_id($id, $lang){ 
      $sql = "SELECT vufind_format.id AS id, vufind_title.title AS title, vufind_format.type_format AS type_format, users.user_name as inserted_by, localization.name as name
            FROM vufind_format 
            LEFT JOIN vufind_title ON  vufind_format.id = vufind_title.format_id
            LEFT JOIN localization ON localization.id  = vufind_title.lang_id
            LEFT join users on users.id = vufind_format.inserted_by WHERE vufind_title.lang_id = {$lang} AND vufind_format.id = {$id}";
           
         $result_array = self::find_by_sql($sql); 
          return !empty($result_array)? array_shift($result_array) : false;   
   }  


    public static function get_formatData($lang){ 
      $sql = "SELECT vufind_format.id AS id, vufind_title.title AS title, vufind_format.type_format AS type_format, users.user_name as inserted_by, localization.name as name, localization.label as label
            FROM vufind_format 
            LEFT JOIN vufind_title ON  vufind_format.id = vufind_title.format_id
            LEFT JOIN localization ON localization.id  = vufind_title.lang_id
            LEFT join users on users.id = vufind_format.inserted_by WHERE localization.label = '{$lang}'";
           
           return self::find_by_sql($sql);
         // $result_array = self::find_by_sql($sql); 
          // return !empty($result_array)? array_shift($result_array) : false;   
   }  
} 
?>