<?php
require_once 'CRUD.php';
class vufindTitle extends CRUD{
   //calss attributes
   public $id;
   public $title; 
   public $lang_id;
   public $format_id;
  // relation attribute 
  
  
	
   //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields , '');
   }   
   //define table name and fields
	protected static $table_name = 'vufind_title';
	protected static $primary_fields = array('id','title','lang_id','format_id');
   
   public static function getAll_by_format_id($id, $lang){

          $sql = "SELECT * from " .static::$table_name ." WHERE format_id = {$id} AND lang_id = {$lang}";
           
         $result_array= self::find_by_sql($sql);   

          return !empty($result_array)? array_shift($result_array) : false;
   }
   
   
}
?>
