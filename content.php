<?php 
require_once('initialize_classes_files.php');
require_once('main/layout_parts/header.php');
require_once('main/layout_parts/nav.php');
	if(empty($node_alias)){
		redirect_to('index.php');
	}
	//retrive layout 
	$get_page_content = $define_node->front_node_data(null,"page",$node_alias,null,$lang_info->id,null,null,null,null, null,null,null,"one");
	if(!empty($get_page_content)){
		$page_layout_info = $define_node->front_get_model($get_page_content->id);
		$node_type = 'page';
		//layout
		require_once("main/layout_templates/".$page_layout_info->layout_name.".php");
	}else{
		echo "<h1>Page not found</h1>
			<p>We don't think you came to the right page. Please use the website navigation to reach what you want.</p>";	
	}
require_once('main/layout_parts/footer.php');
?>