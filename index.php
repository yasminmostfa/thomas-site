<?php 
require_once('initialize_classes_files.php');
require_once('main/layout_parts/header.php');
require_once('main/layout_parts/slider.php');
require_once('main/layout_parts/nav.php');?>
<div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-8 main-content"><!--start of main content-->
				  <?php
                  for($i = 1;$i<=6;$i++){
					  $section_data = HomePageLayout::get_position($i);
					  if($section_data){
						  $plugin_data = Plugins::find_by_id($section_data->plugin);
						  if($plugin_data){
							  $plugin_value = $section_data->plugin_value;
							  $header_title = $section_data->header_title;
							  require("plugins/$plugin_data->source/viewer.php");
						  }						  
					  }
                    
                  
                  }
                
                  ?>
		      </div>
               <aside class="col-md-4 main-sidebar"><!--start of sidebar-->
               <?php
                  for($i = 7;$i<=12;$i++){
					  $section_data = HomePageLayout::get_position($i);
					  if($section_data){
						  $plugin_data = Plugins::find_by_id($section_data->plugin);
						  if($plugin_data){
							  $plugin_value = $section_data->plugin_value;
							  $header_title = $section_data->header_title;
							  require("plugins/$plugin_data->source/viewer.php");
						  }						  
					  }
                    
                  
                  }
                
                  ?>
               </aside>
        </div>
  </div>
  <?php 
 require_once('main/layout_parts/footer.php');
 ?>