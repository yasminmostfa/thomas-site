<?php
require_once("classes/Session.php");
require_once("classes/FrontSession.php");
require_once("classes/Users.php");
require_once("classes/Functions.php");
require_once("classes/MysqlDatabase.php");
require_once("classes/Localization.php");
require_once("classes/GeneralSettings.php");
require_once("classes/MenuLink.php");
require_once("classes/Advertisements.php");
require_once("classes/Nodes.php");
require_once("classes/NodesContent.php");
require_once("classes/Taxonomies.php");
require_once("classes/TaxonomiesContent.php");
require_once("classes/Plugins.php");
require_once("classes/NodesPluginsValues.php");
require_once("classes/ThemeLayoutModel.php");
require_once("classes/ThemeLayoutModelPlugin.php");
require_once("classes/MenuGroup.php");
require_once("classes/Forms.php");
require_once("classes/NodesSelectedTaxonomies.php");
require_once("classes/FormAttributes.php");
require_once("classes/FormInsertedData.php");
require_once("classes/Pagination.php");
require_once("classes/SocialComments.php");
require_once("classes/NodesImageGallery.php");
require_once("classes/CustomerInfo.php");
require_once("classes/HomePageLayout.php");
require_once("classes/EventDetails.php");
require_once("classes/CustomerActivites.php");
require_once("classes/CustomerExperience.php");
require_once("classes/CustomerEducation.php");
require_once("classes/UsersFeeds.php");
require_once("classes/parsingResult.php");
require_once("classes/urlBeforeParse.php");
require_once("classes/urlLink.php");
require_once('classes/vufindFormat.php');
require_once('classes/UsersSaves.php');
require_once('classes/Views.php');


ob_start();
//define classes
if(isset($_GET["lang"])){
	$lang = $_GET["lang"]; 
	require_once('localization/'.$_GET["lang"].'/form_label.php');
	require_once('localization/'.$_GET["lang"].'/validation_label.php');
    $date_now_method = date_now();
	//catch url and convert to utf8
	$catch_url = urldecode(basename($_SERVER['REQUEST_URI']));
	//get type of opened node content/event/post
	$current_opnened_url_file = $_SERVER["PHP_SELF"];
	$explode_opened_url_file = Explode('/', $current_opnened_url_file);
	$opened_url_file = $explode_opened_url_file[count($explode_opened_url_file)-1];   //give u file
	//check lang exist in db
	$lang_info = Localization::find_by_custom_filed('label',$lang);
	//define general setting
	$define_general_setting = GeneralSettings::find_by_id(1);
	$new_website_title = $define_general_setting->title;
	//define taxonomy class
	$define_taxonomy_class = new Taxonomies();
	$define_taxonomy_class->enable_relation();
	//define taxonomy content class
	$define_taxonomy_content_class = new TaxonomiesContent();
	$define_taxonomy_content_class->enable_relation();
    //define Node class
	$define_node = new Nodes();
	$define_node->enable_relation();
	//define post class
	$define_node_tax = new NodesSelectedTaxonomies();
	$define_node_tax->enable_relation();
	//define Node content  class
	$define_node_content = new NodesContent();
	$define_node_content->enable_relation();
	//define menu link class
	$define_menu_link = new MenuLink();
	$define_menu_link->enable_relation();
	//define advertisements class
	$define_adv = new Advertisements();
	$define_adv->enable_relation();
	//define userfeeds 
	$define_feeds = new UsersFeeds();
	//get the main website title
	//define view 
	$define_view = new Views();
	$website_new_title = "";
    $website_title = $define_general_setting->title;
	$website_home_title = "";
	$separate_website_title = explode("|",$website_title);
    if($lang == "ar"){
       $website_home_title 	= $separate_website_title[1];
    }else{
	   $website_home_title =  $separate_website_title[0];
	}
	 $website_meta_tages = "";
	 $website_meta = $define_general_setting->meta_key;
	 $separate_website_meta = explode("||",$website_meta);
	  if($lang == "ar"){
       $website_meta_tages 	= $separate_website_meta[1];
    }else{
	   $website_meta_tages =  $separate_website_meta[0];
	}
	
	
if(!$lang_info){
		redirect_to('index.php?lang=en');
	}
	if($lang == 'en'){
		if(isset($_GET['alias'])){
		    $node_alias = urldecode($_GET['alias']);
			//if website in en mode search in ar alias 
			//if alias exist replace eng alias with ar alias
			//if ar not found give current url  //english
			/*******************************************/
			if($opened_url_file == "content.php"){
				$translate_alias_ar =  $define_node->return_other_languages($node_alias,'ar');
			}elseif($opened_url_file == "post_details.php"){
				$translate_alias_ar =  $define_node->return_other_languages($node_alias,'ar');
			}elseif($opened_url_file == "event_details.php"){
				$translate_alias_ar =  $define_node->return_other_languages($node_alias,'en');
			}
			if($translate_alias_ar){
				$url_replace_alias_ar = str_replace($node_alias,$translate_alias_ar->alias,$catch_url);	
				$url_ar = str_replace('lang=en','lang=ar',$url_replace_alias_ar);	
			}else{
				$url_ar = str_replace('lang=en','lang=ar',$catch_url);	
			}
		}else{
			$url_ar = str_replace('lang=en','lang=ar',$catch_url);	
		}
	}else{
		if(isset($_GET['alias'])){
			 $node_alias = urldecode($_GET['alias']);
			//if website in ar mode search in en alias 
			//if alias exist replace ar alias with en alias
			//if en not found give current url  //arabic
			if($opened_url_file == "content.php"){
				$translate_alias_en =  $define_node->return_other_languages($node_alias,'en');
			}elseif($opened_url_file == "post_details.php"){
				$translate_alias_en =  $define_node->return_other_languages($node_alias,'en');
			}elseif($opened_url_file == "event_details.php"){
				$translate_alias_ar = $define_node->return_other_languages($node_alias,'en');
			}
			if($translate_alias_en){
				$url_replace_alias_en = str_replace($node_alias,$translate_alias_en->alias,$catch_url);	
				$url_en = str_replace('lang=ar','lang=en',$url_replace_alias_en);
			}else{
				$url_en = str_replace('lang=ar','lang=en',$catch_url);	
			}
		}else{
			$url_en = str_replace('lang=ar','lang=en',$catch_url);	
		}
		$ar_month =  $ar_month =  array("Jan"=>"يناير","Feb"=>"فبراير","Mar"=>"مارس","Apr"=>"ابريل","May"=>"مايو","Jun"=>"يونيو","Jul"=>"يوليو","Aug"=>"اغسطس", 
					"Sep"=>"سبتمبر","Oct"=>"اكتوبر","Nov"=>"نوفمبر","Dec"=>"ديسمبر"); 
	}

	

}else{
	redirect_to('index.php?lang=en');
}
?>