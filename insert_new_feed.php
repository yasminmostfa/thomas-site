<?php require_once("initialize_classes_files.php");
require_once("main/layout_parts/header.php");
require_once("main/layout_parts/nav.php");

?>
 <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="index.php?lang=<?php  echo $lang?>"><?php echo $home; ?></a></li>
              <li class="active"><?php echo $insert_new_feed; ?></li>
            </ol>
          </div>
 </section><!--end of internal-cover-->


     <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-8 main-content"><!--start of main content-->
            <section class="col-m-12 main-content-section"><!--start of main content section-->

               
                  <h2 class="col-md-12 give-darkRed-c give-boldFamily"><?php echo $insert_new_feed ;  ?></h2>
                   <form action="" id="user_feed" class="row clearfix primary-form">
                        <input type="hidden" id="lang" value="<?php echo $lang ?>" />
                        <div class="form-group col-md-12">
                          <input type="text" id="title" class="form-control" 
                          oninvalid="this.setCustomValidity('<?php echo $vtitle ; ?>')" onchange="this.setCustomValidity('')"
                           placeholder="<?php echo $title ; ?>" required>
                        </div>
                         <div class="form-group col-md-12">
                          <textarea id="summary" class="form-control" oninvalid="this.setCustomValidity('<?php echo $vsummary ; ?>')" onchange="this.setCustomValidity('')"
                           placeholder="<?php echo $summary ; ?>" required rows="10" cols="20"></textarea>
                        </div>
                         <div class="form-group col-md-12">
                          <textarea id="body" class="form-control" oninvalid="this.setCustomValidity('<?php echo $vbody ; ?>')" onchange="this.setCustomValidity('')"
                           placeholder="<?php echo $body ; ?>" required rows="20" cols="20"></textarea>
                        </div>
                         <div class="form-group col-md-12">
                          <input type="file" id="cover_image"  name="file" class="form-control" placeholder="<?php echo $cover_image ; ?>"  required="required">
                        </div>
                        
                        <div class="col-md-all form-group">
                          <input type="submit" id="submit" class="btn give-red-bg" value="<?php echo $submit ; ?>">
                          <div id="validation_message_feed"></div>
                        </div>
                        <br>
                        
                      </form>
            

            
              
            </section><!--end of main content section-->
           
          </div><!--end of main content-->

         
        </div><!--end of row-->
      </div><!--end of main content-->
    <!--==============content Goes here==============================-->

<?php require_once("main/layout_parts/footer.php");?>



