<?php
//footer 
$footer_header_three = "روابط مهمة1";
$footer_header_two = "2روابط مهمة";
$footer_header_one = "روابط مهمة";
$subscribe = "الاشتراك";
$subscribe_message = "أبجد هوز هو مجرد دمية النص من طباعة وتنضيدt";
$Copyright = "النسخة © 2016  نيوموترز , جميع الحقوق محفوظة" ;
$powered_by = "التشغيل بواسطة:";
$read_more = "قراءة المزيد";
$view_all_events = "عرض جميع الأحداث";
$showing = "عرض";
$of = "من";
$main_menu_title = "القائمة الرئيسية";
$view_all = "عرض جميع";
$traniner = "المدرب";
$place = "المكان ";
$duration = "المدة ";
//===============================================================================
//header
$home= "الرئسية";
$login = "تسجيل الدخول / تسجيل";
$forget_password = "هل نسيت كلمة المرور ؟";
$new_user = "مستخدم جديد ؟";
$reset_password = "تعيين كلمه المرور";
$login = "Login";
$shared ="Shared";
$share = "Share"; 
$like = "Like";
$insert_new_feed = "Insert New Feed";
$view_all_feeds = "View all feeds";
$advanced = "متقدمه";
$search = "بحث";
$select_format = "اختار التنسيق";



//========================================================================
$forget_password_process = "استعاده كلمه المرور"; 
$forget_password_text = "من فضلك ادخل بريدك الاليكترونى بالاسفل "; 
$send = "ارسال"; 
$reset_password_text = "من فضلك قم بملى الحقول الاتيه" ; 
//=======================================================================
$registration = "التسجيل";
$fname = "الاسم الاول";
$lname = "الاسم الاخير";
$email = "البريد الالكتروني";
$r_email = "اعد كتابه البريد الالكترونى"; 
$password = "كلمة المرور";
$r_password = "اعد كتابه كلمه المرور"; 
$city = "المدينة"; 
$area = "المنطقة" ; 
$street = "الشارع"; 
$gender = "الجنس"; 
$select_gender = "اختر النوع"; 
$male = "زكر" ; 
$female = "انثى" ; 
$birthday = "تاريخ الميلاد";
$submit = "التسجيل";
$mobile = "رقم الهاتف";
$mobile_other = "رقم هاتف اخر"; 
//======================================================================================================
// text in mail : 
$text_link_mail = "اضغط هنا لتقوم بتفعيل حسابك"; 
$text_link_password = "اضغط هنا لتقوم باعده تعيين كلمه المرور"; 
//======================================================================================================
// user-profile : 
$user_profile = "الملف الشخصى";
$first_tab = "معلومات عامه"; 
$second_tab = "الخبره"; 
$third_tab = "التعليم";
$fourth_tab = "المهارات";
$personal_info = "المعلومات الشخصية";
$contact_info = "معلومات الاتصال"; 
$activities = "النشاطات"; 
$your_activities = "نشاطك الخاص"; 
$add_activity = "اضافه نشاط";
$your_activities_2 = "النشاطات الخاصه بك"; 
$yours_expereinces = "الخبرات الخاصه بك"; 
$yours_education = "المراحل التعليميه الخاصه بك"; 
//=======================================activity form labels ========================================
$from = "من" ;
$to = "الى" ;
$activity_name = "اسم النشاط"; 
$activity_type = "نوع النشاط";
$from_date = "بدايه من"; 
$to_date = "انتهاءا الى ";
$role = "الدور" ; 
$description = "وصف النشاط"; 
$company_name = "اسم الشركه"; 
$job_title_holder = "المسمى الوظيفى";
$select_degree   = "اختر الدرجه العلميه";  
// ====================================================================================================
// the fucken countries dropdown list  : 
$select_nationality = "اختر الجنسيه"; 
$afghan = "افغانى"; 
$albanian = "البانى";
$algerian = "جزائري"; 
$american = "اميركى"; 
$andorran = "انورانى"; 
$angolan = "انجولانى";  
$antiguans = "أنتيغوانى"; 
$argentinean = "ارجنتينى"; 
$armenian = "ارمينى"; 
$australian = "استرالى"; 
$austrian = "مجرى"; 
$azerbaijani = "اذربيجانى";
$bahamian = "باهامى"; 
$bahraini = "بحرينى"; 
$bangladeshi = "بنجلاديشى"; 
$batswana = "بتسوانى"; 
$belgian = "بلجيكى"; 
$bolivian = "بوليفيانى"; 
$brazilian = "برازيلى"; 
$british = "بريطانى"; 
$bulgarian = "بلغارى"; 
$burundian = "بوروندى"; 
$cambodian = "كمبودى"; 
$cameroonian = "كاميرونى"; 
$canadian = "كندى"; 
$capeverdean = "كاب فيردان"; 
$centralafrican = "افريقيا الوسطى"; 
$chilean = "تشيلى"; 
$chinese = "صينى"; 
$colombian = "كولومبى"; 
$costarican = "كوستاريكى"; 
$croatian = "كرواتى"; 
$cuban = "كوبى"; 
$czech = "تشيكى"; 

$dutch = "هولندى"; 
$ecuadorean = "اكوادورى"; 
$egyptian = "مصرى"; 
$emirian = "اماراتى"; 
$estonian = "استونى"; 
$ethiopian = "اثيوبى"; 
$filipino = "فليبينى"; 
$french = "فرنسى"; 
$gabonese = "جابونى"; 
$georgian = "جورجى"; 
$german = "المانى"; 
$ghanaian = "غانى"; 
$greek = "يونانى"; 
$guinean = "غينى"; 
$honduran = "هندورنى"; 
$hungarian = "بلغارى"; 
$icelander = "ايسلندى"; 
$indian = "هندى"; 
$indonesian = "اندونيسى"; 
$iranian = "ايرانى"; 
$iraqi = "عراقى"; 
$israeli = "اسرائيلى"; 
$italian = "ايطالى"; 
$ivorian = "ايفوارى"; 
$jamaican = "جامايكى"; 
$japanese = "يابانى"; 
$jordanian = "اردنى"; 
$kazakhstani = "كازاخستانى"; 
$kenyan = "كيني"; 
$kuwaiti = "كويتي"; 
$laotian = "ليتوانى"; 
$latvian = "لاتفى"; 
$lebanese = "لبنانى"; 
$liberian = "ليبيرى"; 
$libyan = "ليبي"; 
$Lithuanian = "ليتوانى"; 
$malawian = "مالى"; 
$malaysian = "ماليزى"; 
$maldivan = "مالديفى"; 
$mauritanian = "موريتانى"; 
$mexican = "مكسيكى"; 
$moroccan = "مغربى"; 
$mozambican = "موزمبيقى"; 
$namibian = "ناميبى"; 
$nigerien = "نيجيرى"; 
$north_korean = "كوريا الشماليه"; 
$norwegian = "نرويجى"; 
$omani =   "عمانى"; 
$pakistani = "باكستانى"; 
$panamanian = "بنمى"; 
$paraguayan = "باراجواى"; 
$portuguese = "برتغالى"; 
$qatari = "قطرى"; 
$romanian = "رومانى"; 
$russian = "روسى"; 
$rwandan = "رواندى"; 
$salvadoran = "سلفادورى"; 
$saudi = "سعودى"; 
$scottish = "سكتلندى"; 
$senegalese = "سنغالى"; 
$serbian = "صيربى"; 
$singaporean = "سنغافورى"; 
$slovakian = "سلوفاكى"; 
$slovenian = "سلوفانى"; 
$somali = "صومالى"; 
$southafrican = "جنوب افريقى"; 
$southkorean = "كوريا الجنوبيه";
$spanish = "اسبانى"; 
$srilankan = "سيريلانكى"; 
$sudanese = "سودانى"; 
$swedish = "سويدى"; 
$swiss = "سويسرى"; 
$syrian = "سورى"; 
$taiwanese = "تيوانى"; 
$tanzanian = "تنزانى" ;
$togolese = "توجولى"; 
$tunisian = "تونسى"; 
$turkish = "تركى"; 
$ugandan = "اوغندى"; 
$ukrainian = "اوكرانى"; 
$uruguayan = "اوروجوانى"; 
$uzbekistani = "اوزباكستانى"; 
$venezuelan = "فينزولى "; 
$vietnamese = "فيتنامى"; 
$yemenite = "يمنى"; 
$zambian = "زامبى"; 
$zimbabwean = "زيمبابوى"; 
//======================================================================================================
$select_colleage = "الكليه او القسم"; 
$dept1 = "القسم الاول" ; 
$dept2 = "القسم الثانى"; 
$dept3 = "القسم الثالث"; 
$dept4 = "القسم الرابغ"; 
$dept5 = "القسم الخامس"; 
$dept6 = "القسم الثانى";
//=======================================================================================================
$save = "حفظ"; 
$edit = "تعديل"; 
$next = "التالى"; 
//=======================================================================================================
$experience_title = "الهيدر الرئيسى"; 
$job_title = "المسمى الوظيفى";
$job_role = "الدور الوظيفى"; 
$date_from = "بدايه من " ;  
$date_to = "انتهاءا الى" ;  
$career = "السيره"; 
$ex_type = "مستوى الخبره"; 
$role1 = "مسمى 1"; 
$role2 = "مسمى 2"; 
$role3 = "مسمى 3"; 
$role4 = "مسمى 4"; 
$level1 = "مستوى 1"; 
$level2 = "مستوى 2"; 
$level3 = "مستوى 3"; 
$level4 = "مستوى 4";
$type1 = "نوع 1"; 
$type2 = "نوع 2"; 
$type3 = "نوع 3"; 
$type4 = "نوع 4";
$add_new_experince  = "اضافه خبره جديده "; 
//=========================================================================================================
$education_title = "الهيدر الرئيسى"; 
$edu_type = "مستوى التعليم"; 
$degree_type1 = "المستوى الاول"; 
$degree_type2 = "المستوى الثانى"; 
$degree_type3 = "المستوى  الثالث"; 
$degree_type4 = "المستوى المستوى الرابع"; 
$add_new_education  = "اضافه تعليم جديد"; 
// ===============================================
$update_activity = "تحديث النشاط"; 
$delete_activity = "مسح النشاط"; 
$delete_text = "هل تريد حذف هذا النشاط بالفعل ؟"; 
$delete_yes = "حذف"; 
$delete_no = "لا"; 
$update_experience = "تحديث الخبرات"; 
$delete_experience = "حذف الخبره" ; 
$delete_experience_text = "هل تريد حذف هذه الخبره بالفعل ؟"; 
$expereinces = "الخبرات"; 
$education = "التعليم" ; 
$update_education = "تحديث التعليم" ; 
$delete_education = "حذف التعليم" ; 
$delete_education_text = "هل تريد حذف هذا التعليم بالفعل ؟"; 
//==============================================
// BreadCrumb : 
$home = "الرئيسيه" ; 
$user_profile_bread = "الملف الشخصى" ; 
$user_register_bread = "التسجيل"; 
$update_activity_bread = "تحديث النشاط";
//======================================================================================================
//education place holder labels : 
$university_name = "اسم الكليه او المعهد"; 
$degree_level  = "مستوى التعليم"; 
$fields_of_study = "مجالات الدراسه (مثل \ كيمياء , فيزياء , الخ)"; 
$education_date_from = "تاريخ بدايه المرحله التعليميه"; 
$education_date_to = "تاريخ نهايه المرحله التعليميه"; 
//=======================================================
$cpass = "قبول كلمة المرور";
$message = "الرسالة";
$name = "الاسم";
$company = "اسم الشركة";
$login_btn = "تسجيل الدخول";
$fup_link = "نسيت كلمه المرور ؟";
$go_back = "رجوع";
$login_facebook="قم بالتسجيل من خلال الفيس بوك";
$create_account="قم بإنشاء حساب";
$forget_password_submit_btn="تسجيل";
$please_enter_email="من فضلك ادخل البريد الالكترونى";
$please_enter_password="من فضلك ادخل كلمه المرور";
$view_catalogue = "عرض كتالوج";
$resat = "إعادة ضبط";
$save = "حفظ";
$register = "تسجيل";
$welcome_user= "مرحبا";
$profile = "ملفك الشخصي";
$view_orders = "عرض الطلبات";
$log_out = "تسجيل الخروج" ;
$register = "تسجيل";
$login = "تسجيل الدخول ";
$language = "اللغة";
$items = "العنصر/ العناصر" ;
// footer.php
$payment = "طرق الدفع" ;
$links = "الروابط المفيدة" ;
$Copyright = "النسخة © 2016  نيوموترز , جميع الحقوق محفوظة" ;
$powered_by = "التشغيل بواسطة:";
// pages_header.php
$home = "الرئيسية";
$search_by = "البحث  ";
$profile = "الملف الشخصي";
//user_profile :
$update_password = "تحديث كلمة المرور";
$history = "سجل";
$favourite = "المفضلة";
$credit = "الرصيد ";
$account_summary = "ملخص الحساب" ;
$first_name_in_user_info = "الاسم الاول" ;
$last_name_in_user_info = "الاسم الاخير" ;
$email_in_user_info = "البريد الالكترونى"  ;
$mobile_in_user_info = "الموبايل" ;
$birthday_in_user_info = "تاريخ الميلاد";
$occupationvalue = "المهنه الأولى " ;
$gender_in_user_info = "الجنس"  ;
$male_in_user_info = "ذكر"  ;
$female_in_user_info = "أنثى";
$pass_confirm_in_user_info = "قبول كلمة المرور" ;
$edit = "تعديل";
$save_in_user_info = "حفظ" ;
$wishlist_empty = "قائمة رغباتك فارغة";
$add_wishlist = "أضف إلى المفضلة";
$ask_for_quotation = "اسأل عن عرض أسعار";
?>
