<?php
//footer 
$footer_header_three = "First Links";
$footer_header_two = "Second Links";
$footer_header_one = "Third Links";
$subscribe = "Subscribe";
$subscribe_message = "Lorem Ipsum is simply dummy text of the printing and typesett";
$Copyright = "Copyright © 2016 NewMotores. All rights reserved";
$powered_by = "Powered By : ";
$read_more = "Read More";
$view_all_events = "View All Events";
$showing = "Showing";
$of = "Of";
$main_menu_title = "Main Menu";
$view_all = " View All";
$traniner = "Traniner";
$place = "Place "; 
$duration = "Duration";
//=========================================================================د
//Header
$home= "Home";
$login_title = "Login / Register";
$forget_password = "Forgot Password ?";
$new_user = "New user ?";
$login = "Login";
$shared ="Shared";
$share = "Share"; 
$like = "Like";
$insert_new_feed = "Insert New Feed";
$view_all_feeds = "View all feeds";
$advanced = 'advanced';
$search = 'search';
$select_format = "Select Format";


//============================================================
$forget_password_process = "Forget Password Process"; 
$forget_password_text = "Please enter your email down below "; 
$text_link_password = "Please click here to reset you account password"; 
$send = "Send"; 
$reset_password_text = "Please Fill in the fields below" ; 
$reset_password = "Reset Password"; 

//==========================================================================
// registeration page : 
$registration = "Register";
$fname = "First name";
$lname = "Last name";
$email = "E-Mail";
$r_email = "Re-enter email"; 
$password = "Password";
$r_password = "Re-enter password" ; 
$city = "City"; 
$area = "Area" ; 
$street = "Street"; 
$gender = "Gender" ; 
$select_gender = "Select Gender"; 
$male = "Male" ; 
$female = "Female" ; 
$birthday = "Birthday";
$submit = "Submit";
$mobile = "Mobile";
$mobile_other = "Other Mobile"; 

$yours_expereinces = "Your Expereinces"; 
$yours_education = "Your Education Stages"; 

//========================================================================================================
// textin mail :

$text_link_mail = "Click Here To Activate Your account"; 


//=======================================================================================================
// user-profile : 
$user_profile = "Personal Profile";
$first_tab = "General Information"; 
$second_tab = "Experience"; 
$third_tab = "Education";
$fourth_tab = "Skills";
$personal_info = "Personal Information";
$contact_info = "Contact Information"; 
$activities = "Activities";
$your_activities = "Your activity";
$add_activity = "Add Activity";
$your_activities_2 = "Your Activity";  
$company_name = "Company Name"; 
$job_title_holder = "Job Title";
$select_degree   = "Select Degree"; 

//=======================================activity form labels ========================================
$from = "From" ;
$to = "To" ;
$activity_name = "Activity name"; 
$activity_type = "Activity Type";
$from_date = "From Date"; 
$to_date = "To Date";
$role = "Role" ; 
$description = "Description"; 

// ========================================================================================================

// the fucken countries dropdown list  : 

$select_nationality = "Select your Nationality"; 
$afghan = "Afghanian"; 
$albanian = "Albanian";
$algerian = "Algerian"; 
$american = "American"; 
$andorran = "Andorran"; 
$angolan = "Angolan";  
$antiguans = "Antiguans"; 
$argentinean = "Argentinean"; 
$armenian = "Armenian"; 
$australian = "Australian"; 
$austrian = "Austrian"; 
$azerbaijani = "Azerbaijani";
$bahamian = "Bahamian"; 
$bahraini = "Bahraini"; 
$bangladeshi = "Bangladeshi";  
$batswana = "Batswana"; 
$belgian = "Belgian"; 
$bolivian = "Bolivian"; 
$brazilian = "Brazilian"; 
$british = "British"; 
$bulgarian = "Bulgarian"; 
$burundian = "Burundian"; 
$cambodian = "Cambodian"; 
$cameroonian = "Cameroonian"; 
$canadian = "Canadian"; 
$capeverdean = "Capeverdean"; 
$centralafrican = "Centralafrican"; 
$chilean = "Chilean"; 
$chinese = "Chinese"; 
$colombian = "Colombian"; 
$costarican = "Costarican"; 
$croatian = "Croatian"; 
$cuban = "Cuban"; 
$czech = "Czech"; 
$dutch = "Dutch"; 
$ecuadorean = "Ecuadorean"; 
$egyptian = "Egyptian"; 
$emirian = "Emirian"; 
$estonian = "Estonian"; 
$ethiopian = "Ethiopian"; 
$filipino = "Filipino"; 
$french = "French"; 
$gabonese = "Gabonese"; 
$georgian = "Georgian"; 
$german = "German"; 
$ghanaian = "Ghanaian"; 
$greek = "Greek"; 
$guinean = "Guinean"; 
$honduran = "Honduran"; 
$hungarian = "Hungarian"; 
$icelander = "Icelander"; 
$indian = "Indian"; 
$indonesian = "Indonesian"; 
$iranian = "Iranian"; 
$iraqi = "Iraqi"; 
$israeli = "Israeli"; 
$italian = "Italian"; 
$ivorian = "Ivorian"; 
$jamaican = "Jamaican"; 
$japanese = "Japanese"; 
$jordanian = "Jordanian"; 
$kazakhstani = "Kazakhstani"; 
$kenyan = "Kenyan"; 
$kuwaiti = "Kuwaiti"; 
$laotian = "Laotian"; 
$latvian = "Latvian"; 
$lebanese = "Lebanese"; 
$liberian = "Liberian"; 
$libyan = "Libyan"; 
$Lithuanian = "Lithuanian"; 
$malawian = "Malawian"; 
$malaysian = "Malaysian"; 
$maldivan = "Maldivan"; 
$mauritanian = "Mauritanian"; 
$mexican = "Mexican"; 
$moroccan = "Moroccan"; 
$mozambican = "Mozambican"; 
$namibian = "Namibian"; 
$nigerien = "Nigerien"; 
$north_korean = "North korean"; 
$norwegian = "Norwegian"; 
$omani =   "Omani"; 
$pakistani = "Pakistani"; 
$panamanian = "Panamanian"; 
$paraguayan = "Paraguayan"; 
$portuguese = "Portuguese"; 
$qatari = "Qatari"; 
$romanian = "Romanian"; 
$russian = "Russian"; 
$rwandan = "Rwandan"; 
$salvadoran = "Salvadoran"; 
$saudi = "Saudi"; 
$scottish = "Scottish"; 
$senegalese = "Senegalese"; 
$serbian = "Serbian"; 
$singaporean = "Singaporean"; 
$slovakian = "Slovakian"; 
$slovenian = "Slovenian"; 
$somali = "Somali"; 
$southafrican = "South African"; 
$southkorean = "South Korean";
$spanish = "Spanish"; 
$srilankan = "Srilankan"; 
$sudanese = "Sudanese"; 
$swedish = "Swedish"; 
$swiss = "Swiss"; 
$syrian = "Syrian"; 
$taiwanese = "Taiwanese"; 
$tanzanian = "Tanzanian" ;
$togolese = "Togolese"; 
$tunisian = "Tunisian"; 
$turkish = "Turkish"; 
$ugandan = "Ugandan"; 
$ukrainian = "Ukrainian"; 
$uruguayan = "Uruguayan"; 
$uzbekistani = "Uzbekistani"; 
$venezuelan = "Venezuelan"; 
$vietnamese = "Vietnamese"; 
$yemenite = "Yemenite"; 
$zambian = "Zambian"; 
$zimbabwean = "Zimbabwean"; 


//======================================================================================================
$select_colleage = "Colleage/Department"; 
$dept1 = "Dept 1" ; 
$dept2 = "Dept 2"; 
$dept3 = "Dept 3"; 
$dept4 = "Dept 4"; 
$dept5 = "Dept 5"; 
$dept6 = "Dept 6";

//==========================================================================================================
$save = "Save"; 
$edit = "Update"; 
$next = "Next"; 

//===========================================================================================================

$experience_title = "Main Header"; 
$job_title = "Job Title";
$job_role = "Job Role"; 
$date_from = "Date From" ;  
$date_to = "Date To " ;
$career = "Career";
$ex_type = "Experience Type";  
$role1 = "job role 1"; 
$role2 = "job role 2"; 
$role3 = "job role 3"; 
$role4 = "job role 4";

$level1 = "level 1"; 
$level2 = "level 2"; 
$level3 = "level 3"; 
$level4 = "level 4";

$type1 = "type 1"; 
$type2 = "type 2"; 
$type3 = "type 3"; 
$type4 = "type 4";

$add_new_experince = "Add New Experince"; 



//========================================================================================================

$education_title = "Main Header Edu"; 
$edu_type = "Degree Level"; 
$degree_type1 = "type1"; 
$degree_type2 = "type2"; 
$degree_type3 = "type3"; 
$degree_type4 = "type4";
$add_new_education = "Add New Education";

//==============================================================================

$update_activity = "Update Activity"; 
$delete_activity = "Delete Activity";
$delete_text = "Are You Sure To Delete This Activity ?"; 
$delete_yes = "Yes"; 
$delete_no = "No";
$update_experience = "Update Experience"; 
$delete_experience = "Delete Experience" ; 
$delete_experience_text = "Are You Sure To Delete This Experience ?"; 
$expereinces = "Experiences"; 
$education = "Education" ; 
$update_education = "Update Education" ; 
$delete_education = "Delete Education" ; 
$delete_education_text = "Are You Sure To Delete This Education ?"; 
//================================================
//breadcrumb : 

$home = "index" ; 
$user_profile_bread = "User Profile" ; 
$user_register_bread = "Register"; 
$update_activity_bread = "Update Activity"; 



//=================================================================

//======================================================================================================
//education place holder labels : 
$university_name = "University / Institue name"; 
$degree_level  = "Education / Degree Level"; 
$fields_of_study = "Fields Of Study (ex / math , physics , etc )"; 
$education_date_from = "Education Date From"; 
$education_date_to = "Education Date To "; 
//=======================================================






$cpass = "Password Confirmation";



$message = "Message";

$gender = "Gender";

$name = "Name";

$company = "Company";

$company = "Company Name";

$login_btn = "Login";

$fup_link = "Forget Your password?";

$go_back = "Go Back!";

$login_facebook="Log in with Facebook";

$create_account="Sign Up!";

$forget_password_submit_btn="Submit";



$please_enter_email="Please enter your Email";

$please_enter_password="Please Enter Your Password";

$view_catalogue = "View Catalogue";

//

$save = "Save";



$register = "Register";

$resat = "Reset";



$welcome_user = "Welcome " ;



$view_orders = "View Orders";

$log_out  = "Logout";

$register = "Register";

$login = "Login";

$language = "Language";

$items = "Item/s";



// footer.php



$Copyright = "Copyright © 2016 NewMotores. All rights reserved";

$powered_by = "Powered By : ";





// pages_header.php

$home = "Home";

$search_by = "Search " ;

$profile = "User Profile";





// user_profile :

$update_password = "Update Password" ;

$history = "History";

$favourite = "Wishlist" ;

$credit = "Credit";

$account_summary = "Account summary" ;

$first_name_in_user_info = "First Name" ;

$last_name_in_user_info = "Last Name" ;

$email_in_user_info = "Email" ;

$mobile_in_user_info = "Mobile" ;

$birthday_in_user_info  = "Birthday" ;

$occupationvalue = "occupation one ";

$gender_in_user_info = "Gender";

$male_in_user_info = "Male";

$female_in_user_info = "Female" ;

$pass_confirm_in_user_info = "Password Confirmation"  ;

$wishlist_empty = "Your wishlist is empty";

$add_wishlist = "ADD To Wishlist";

$ask_for_quotation = "Ask For Quotation";







$save_in_user_info = "Save";
/**********************************************************************************************/
//insert new feed
$title = "Title";
$body = "Body";
$summary = "Summary";
$cover_image = "Cover image";





?>

