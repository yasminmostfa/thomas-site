<?php
require_once('../../classes/Session.php');
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/SocialEmailSubscription.php');
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
//header('Content-Type: application/json'); 
//get data
	$required_fields = array('first_name'=>$vfname,'email'=>$vemail);

	$check_required_fields = check_required_fields($required_fields);  
	if(count($check_required_fields) != 0){  
	     $comma_separated = implode("<br>", $check_required_fields);  
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated);  
		  echo json_encode($data);  
}else{
	 $email = $_POST["email"];
  if (filter_var($email, FILTER_VALIDATE_EMAIL)) { 
     $check_exist_email =  SocialEmailSubscription::find_by_custom_filed('email',$email);
	 if(empty($check_exist_email)){
	  $add = new SocialEmailSubscription();
	  $add->email=$_POST["email"];
	   $add->inserted_date=date_now();
	  $insert = $add->insert();
	  if($insert){
	   $data  = array("status"=>"work");
		  echo json_encode($data);
	   
		}else{
			$data  = array("status"=>"error");
			 echo json_encode($data);
		   
		}
	 }else{
		 $data  = array("status"=>"Wrong_email_exist");
			 echo json_encode($data);
	 }
  }else{
	  $data  = array("status"=>"Wrong_email");
		  echo json_encode($data);
	  
  }
   }	
	  
	  
//close connection
if(isset($database)){
	$database->close_connection();
}
?>