<?php
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/FrontSession.php');
require_once('../../classes/Views.php');
require_once("../../classes/GeneralSettings.php");
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
require_once('../../localization/'.$_POST["lang"].'/form_label.php');
//header('Content-Type: application/json');
$define_general_setting =  new GeneralSettings();
$define_general_setting->enable_relation();
$general_setting_info = $define_general_setting->general_settings_data();
$lang = $_POST["lang"];
 
$add = new Views();
$check_view = $add->get_views($_POST["source_type"],$_POST["source_id"]);
if($check_view){
	$check_view->number_views = $check_view->number_views +1;
	$work = $check_view->update();
	$number_view = $check_view->number_views;
	
}else{
	$add->resource_id = $_POST["source_id"];
	$add->number_views = 1;
	$add->type = $_POST["source_type"];
	$work = $add->insert(); 
	$number_view = $add->number_views;
}

// 
if($work ){
	$data = array("status"=>"work","number"=>$number_view);
	 echo json_encode($data);
}else{
	  $data = array("status"=>"not_work","message"=>$verror_from_system);
	  echo json_encode($data);
  }
						
?>
