<?php
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/FrontSession.php');
require_once('../../classes/UsersSaves.php');
require_once("../../classes/GeneralSettings.php");
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
require_once('../../localization/'.$_POST["lang"].'/form_label.php');
//header('Content-Type: application/json');
$define_general_setting =  new GeneralSettings();
$define_general_setting->enable_relation();
$general_setting_info = $define_general_setting->general_settings_data();
$lang = $_POST["lang"];
$add = new UsersSaves();
$add->resource_id = $_POST["source_id"];
$add->url = $_POST["source_type"];
$add->title = $_POST["title"];
$add->inserted_date = date_now();
$add->custome_info_id = $front_session->user_id;
$insert = $add->insert();
// 
if($insert ){
	$data = array("status"=>"work","message"=>$vmessage_share);
	 echo json_encode($data);
}else{
	  $data = array("status"=>"not_work","message"=>$verror_from_system);
	  echo json_encode($data);
  }
						
?>
