jQuery(document).ready(function($) {

	// process the form

	$('#education_form').submit(function() {

		 $('#add_new_education').attr('disabled', 'disabled'); 

		var formData = {

		
			lang: $('#lang').val(), 

			customer_id : $('#customer_id').val(),  

			university : $('#university').val(),  

			education_level : $('#education_level').val(),  

			field_of_study : $('#field_of_study').val(), 

			from_date : $('#datetimepicker6').val() , 

			to_date : $('#datetimepicker7').val() 


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/add_education.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 
				 
				if(data.status == 'work'){

					$("#notify_new_education").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					window.setTimeout(function(){location.reload();},220);
					//$('#general_info')[0].reset();

				}else if (data.status == 'not_work'){
					$("#notify_new_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#add_new_education').removeAttr('disabled'); 
				}else if(data.status == 'email_exists'){
					$("#notify_new_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#add_new_education').removeAttr('disabled'); 
				}else if(data.status == 'pass_length'){
					$("#notify_new_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#add_new_education').removeAttr('disabled');
				}else{

					$("#notify_new_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");

				}

 
			

				

			}

		});

	return false;

	});

	

	

		

});