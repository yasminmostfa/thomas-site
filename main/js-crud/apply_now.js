
jQuery(document).ready(function($) { 
	
	// process the form
	$('#apply_now').submit(function(event) {
	
		
	
		var formData = {
					name:$('#username').val(),
					email:$('#email').val(),
					program:$('#program').val(),
					tel:$('#tel').val(),
					message:$('#message').val()
					
					
					
		};
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/apply_now.php', // the url where we want to POST
			data 		: formData, // our data object
			dataType 	: 'json', // what type of data do we expect back from the server
             encode          : true
		})
			// using the done promise callback
			.done(function(data) {
                 if(data.status == 'work'){
			       
			       $("#fromEmailHolderMessage").html('<div class="alert alert-success alert-block fade in"><span style ="font-size:16px"> Your message has been sent  </span></div>');
				  
				   $('#apply_now')[0].reset();
                    setTimeout(function() { $('#fromEmailHolderMessage').hide();}, 5000);
				   
					
				    $('#submit').removeAttr('disabled');
				}else if(data.status == 'Wrong_email'){
					$("#fromEmailHolderMessage").show();
			        $("#fromEmailHolderMessage").html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px"> E-mail is incorrect </span></div>');
					 $('#appaly_now').removeAttr('disabled');
					
				}else if(data.status == 'valid_error'){
					 $("#fromEmailHolderMessage").show();
				 $('#fromEmailHolderMessage').html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">'+data.fileds+'</span></div>');
				 $('#appaly_now').removeAttr('disabled');
				 
				}else{ 
			      
			     $("#fromEmailHolderMessage").html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">Please enter your data</span></div>');
			     $("#fromEmailHolderMessage").show();
				 $('#appaly_now')[0].reset();
			     }
			});
		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	});
	
});