jQuery(document).ready(function($) {
	// process the form
	$('#forget_pass_new_pass').submit(function(){
 		var formData = {		
			lang:$('#lang').val(),
			code:$('#code').val(),
			new_pwd:$('#new_pwd').val(),
			conf_pwd:$('#conf_pwd').val(),
		};
		// process the form
		$.ajax({
			type 		: 'POST', 
			url 		: 'main/data_model/forget_pass_new_pass.php', 
			data 		: formData, 
			beforeSend: function(){
			//show laoding 
			$('#validation_message_new_pass').html('<img src="main/images/loading.gif"/>');
			},
			success : function(data) {
				$("#validation_message_new_pass").html("");
				$("#validation_message_new_pass").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
				$('#forget_pass_new_pass')[0].reset();
				if(data.status == 'work'){
					location.href = "index.php?lang="+$("#lang").val();
				} 				
				
			}
		});
	return false;
	});
});