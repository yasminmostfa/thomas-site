jQuery(document).ready(function($) {

	// process the form

	$('#activity_form_update').submit(function() {

		$('#update_activity').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 

			customer_id : $('#customer_id').val(),  

			activity_name:$('#activity_name').val(), 

			activity_type:$('#activity_type').val(), 

			from_date : $('#datetimepicker2').val() , 
			
			to_date:$('#datetimepicker3').val() , 

			role:$('#role').val() , 

			description:$('#description').val()


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/update_activity.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				
				 
				  
				if(data.status == 'work'){

					$("#updating_current_activity").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					 window.location.href="user-profile.php?lang="+$('#lang').val(); 
				}else if (data.status == 'not_work'){
					$("#updating_current_activity").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#update_activity').removeAttr('disabled'); 
				}else{

					$("#updating_current_activity").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");

				}

 		 

				

			}

		});

	return false;

	});

	

	

		

});