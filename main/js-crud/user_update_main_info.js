jQuery(document).ready(function($) {
	// process the form
	$('#main_data').submit(function(){
		
		$('#submit_info').attr("disabled", "true");
		var formData = {
			lang: $('#lang').val(), 
			first_name: $('#fname').val(), 
			last_name: $('#lname').val(),
			email:  $('#email').val(),
			mobile: $('#mobile').val(), 
			birthday: $('#datetimepicker').val(),
			company:$("#company").val()
		};
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/user_update_main_info.php', // the url where we want to POST
			data 		: formData, // our data object
			beforeSend: function(){
			//show laoding 
			$('#validation_message_update').html('<img src="main/images/loading.gif"/>');
			},
			success : function(data) {
				if(data.status == 'work'){
					//alert(data);
					$("#validation_message_update").html('');
					
					$("#validation_message_update").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					$("#main_data input").prop('disabled', true);
					$("#main_data select").prop('disabled', true);
				     $("#submit_info").addClass("hide");
					 $(".primary-form-edit").removeClass("hide");
					   setTimeout(function() { $('#validation_message_update').hide();}, 5000);
					 
				}else{
					$("#validation_message_update").html('');
					$(".user-profile-save").removeAttr("disabled");
					$("#validation_message_update").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
				}
			}
		});
	return false;
	});
});