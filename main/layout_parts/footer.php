<footer class="footer"><!--start-of footer-->
        <div class="container">
          <div class="row clearfix">
            <div class="col-md-3">
              <a href="" class="col-md-12 footer-logo"><img src="main/images/footer-logo.png" alt=""></a>
              <ul class="col-md-12 footer-social">
              <?php 
			  $social_links = $define_menu_link->menu_submenu_front_data('sorting','ASC','social_menu',0,$lang_info->id);
					if(count($social_links)){
						foreach($social_links as $link){
							echo " <li><a href='$link->external_path' target='_blank' class='fa fa-$link->icon-square'></a></li>";
						}
					}
			  
			  ?>
                
              </ul>
            </div>
            <div class="col-md-3">
              <h3 class="col-md-12 give-white-c give-boldFamily"><?php echo $footer_header_one ?></h3>
              <ul class="col-md-12 footer-list">
                <?php
			   $footer_menu_one = $define_menu_link->menu_submenu_front_data('sorting','ASC','footer_menu_one',0,$lang_info->id);
			        if(count($footer_menu_one)){
							foreach($footer_menu_one as $link){
								$path_link_data = $define_node->get_node_content($link->path,$lang_info->id);
									$path = "";
									if($link->path_type == "page"){
										$path = "content.php";
									}else if($link->path_type == "post"){
										$path = "post_details.php";
									}else if($link->path_type == "event"){
										$path = "event_details.php";
									}
									if($link->path_type == "external"){
										echo "<li><a href='$link->external_path' target='_blank '>$link->title</a></li>";
									}else{
									 echo "<li><a href='$path?lang=$lang&alias=$path_link_data->alias'>$link->title</a></li>";
									}
							}
					}
			
			
			 ?>
              </ul>
            </div>
             <div class="col-md-3">
              <h3 class="col-md-12 give-white-c give-boldFamily"><?php echo $footer_header_two ?></h3>
              <ul class="col-md-12 footer-list">
                <?php
			   $footer_menu_two = $define_menu_link->menu_submenu_front_data('sorting','ASC','footer_menu_two',0,$lang_info->id);
			        if(count($footer_menu_two)){
							foreach($footer_menu_two as $link){
								$path_link_data = $define_node->get_node_content($link->path,$lang_info->id);
									$path = "";
									if($link->path_type == "page"){
										$path = "content.php";
									}else if($link->path_type == "post"){
										$path = "post_details.php";
									}else if($link->path_type == "event"){
										$path = "event_details.php";
									}
									if($link->path_type == "external"){
										echo "<li><a href='$link->external_path' target='_blank '>$link->title</a></li>";
									}else{
									 echo "<li><a href='$path?lang=$lang&lang=$lang&alias=$path_link_data->alias'>$link->title</a></li>";
									}
							}
					}
			
			
			 ?>
              </ul>
            </div>
             <div class="col-md-3">
              <h3 class="col-md-12 give-white-c give-boldFamily"><?php echo $footer_header_three ?></h3>
              <ul class="col-md-12 footer-list">
                <?php
			   $footer_menu_three = $define_menu_link->menu_submenu_front_data('sorting','ASC','footer_menu_three',0,$lang_info->id);
			        if(count($footer_menu_three)){
							foreach($footer_menu_three as $link){
								$path_link_data = $define_node->get_node_content($link->path,$lang_info->id);
									$path = "";
									if($link->path_type == "page"){
										$path = "content.php";
									}else if($link->path_type == "post"){
										$path = "post_details.php";
									}else if($link->path_type == "event"){
										$path = "event_details.php";
									}
									if($link->path_type == "external"){
										echo "<li><a href='$link->external_path' target='_blank '>$link->title</a></li>";
									}else{
									 echo "<li><a href='$path?lang=$lang&alias=$path_link_data->alias'>$link->title</a></li>";
									}
							}
					}
			
			
			 ?>
              </ul>
            </div>
          </div>
          <form class="col-md-12 footer-subscribe">
            <h3 class="col-md-12 give-white-c give-boldFamily">  <?php  echo $subscribe ?></h3>
            <p class="col-md-12 give-white-c"> <?php  echo $subscribe_message ?> </p>
            <div class="clearfix form-group">
              <input type="text" class="form-control" placeholder="Your Email">
              <button type="submit" class="btn give-red-bg">
                <span class="fa fa-arrow-right"></span>
              </button>
            </div>
          </form> 
        </div>
        <div class="col-md-12 copyright">
          <div class="container">
            <p><?php echo $Copyright ;  ?>. <?php echo $powered_by ;  ?><a href="http://diva-lab.com" class="col-md-12 give-boldFamily">Diva-lab</a></p>
          </div>
        </div>
    </footer><!--end-of footer-->
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="main/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="main/js/bootstrap.min.js"></script>
    
     <!--mobile friendly-->
     <script src="main/js/jquery.mobile.custom.min.js"></script>
  <script src="main/js/jquery.datetimepicker.js"></script>
       <script src="main/js-crud/user_register.js"></script> 
    <!--custom-->
     <script src="main/js/script.js"></script>
     <script>
     $(".datetimepicker_class").datetimepicker({timepicker:!1,format:"Y-m-d"});
     
     </script>
     <script src="main/js-crud/insert_general_info.js"></script> 
     <script src="main/js-crud/insert_experience.js"></script> 
     <script src="main/js-crud/insert_education.js"></script> 
     <script src="main/js-crud/add_activity.js"></script> 
     <script src="main/js-crud/add_experience.js"></script> 
     <script src="main/js-crud/add_education.js"></script> 
     <script src="main/js-crud/remove_activity.js"></script> 
     <script src="main/js-crud/update_activity.js"></script> 
     <script src="main/js-crud/delete_activity.js"></script> 
     <script src="main/js-crud/update_experience.js"></script> 
     <script src="main/js-crud/delete_experience.js"></script> 
     <script src="main/js-crud/update_education.js"></script> 
     <script src="main/js-crud/delete_education.js"></script>
     <script src="main/js-crud/forget_password.js"></script>
     <script src="main/js-crud/reset_password_tm.js"></script>
     <script src="main/js-crud/user_login.js"></script>
     <script src="main/js-crud/share_artical.js"></script>
     <script src="main/js-crud/save_artical.js"></script>
     <script src="main/js-crud/user_feed.js"></script>
     <script src="main/js-crud/add_view.js"></script>
     <script type="text/javascript">

   $(document).ready(function () {
    var selected ='';
     $("#data li").click(function() {
      
   
    selected = $(this).attr('data-format'); // jQuery's .attr() method, same but more verbose
   
 
     
});
    
$('#search-form').submit(function(){
      var keyword=$('#search_key').val();
      var main_url = $('#main_url').val();
     var common ="Search/Results?"; 
     var normal_search = "lookfor="+keyword;
     var format_search ='join=AND&lookfor0[]='+keyword+'&type0[]=AllFields&lookfor0[]=&type0[]=AllFields&lookfor0[]=&type0[]=AllFields&bool0[]=AND&filter[]=~format:"'+selected+'"&illustration=-1&daterange[]=publishDate&publishDatefrom=&publishDateto='
      
     if (selected) {
      window.open(main_url+common+format_search,'_blank');
    }else{
      window.open(main_url+common+normal_search,'_blank');
    }


return false;
});
   


}); 


     </script>
  </body>
</html>