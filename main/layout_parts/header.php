<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="Classification" content="" />
    <meta name="RESOURCE-TYPE" content="DOCUMENT" />
    <meta name="DISTRIBUTION" content="GLOBAL" />
    <meta name="robots" content="all" />
    <meta name="revisit-after" content="5 days" />
    <meta name="rating" content="general" />
    <meta http-equiv="Content-Language" content="" />
    <meta name="author" content=""/>
  <?php
	 $main = "";
	if(isset($_GET["alias"])){
		$node_alias = $_GET["alias"];
		if($opened_url_file == "post_details.php"){
			   $post_info = $define_node->front_node_data(null,"post",$node_alias,null,$lang_info->id,null,null,null,null, null,null,null,"one");
			   if(!empty($post_info)){
				   $main = $post_info;
				   if(!empty($post_info->cover_image)){
				       $image = $post_info->cover_image;
					   $parts = explode('/',$image);
					   $image_cover = $parts[count($parts)-1];
					   $folder = $parts[count($parts)-2];
				   }else{
						 $folder = "";
						 $image_cover = "";
					 }
				   $website_new_title = $post_info->title;
					echo "  <meta property='og:url' content='{$define_general_setting->site_url}post_details.php?lang=$lang;&alias=$post_info->alias' />
					<meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
					<meta property='og:description' content='".mb_substr(strip_tags($post_info->summary),0,110,'utf-8')."'/>
					<meta property='og:title' content='$post_info->title' />";
				}
	        }else if($opened_url_file == "content.php"){
				$page_info =  $define_node->front_node_data(null,"page",$node_alias,null,$lang_info->id,null,null,null,null, null,null,null,"one");
				 if(!empty($page_info)){
					 $main = $page_info;
					 if($page_info->cover_image){
					   $image = $page_info->cover_image;
					   $parts = explode('/',$image);
					   $image_cover = $parts[count($parts)-1];
					   $folder = $parts[count($parts)-2];
					 }else{
						 $folder = "";
						 $image_cover = "";
					 }
					   $website_new_title = $page_info->title;
					echo "  <meta property='og:url' content='{$define_general_setting->site_url}post_details.php?lang=$lang;&alias=$page_info->alias' />
						<meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
						<meta property='og:description' content='".mb_substr(strip_tags($page_info->summary),0,110,'utf-8')."'/>
						<meta property='og:title' content='$page_info->title' />";
				 }
			}
	}else{
		$website_new_title = $website_title;
		?>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo $website_new_title;?>" />
        <meta property="og:description" content="<?php echo $define_general_setting->description;?>" />
        <meta property="og:url" content="<?php echo $define_general_setting->site_url;?>" />
        <meta property="og:site_name" content="<?php  echo $website_new_title?>" />
	<?php }?>
<title> <?php echo $website_new_title;  ?> </title>
    <!-- Bootstrap -->
  <!-- Bootstrap -->
    <link href="main/css/bootstrap.min.css" rel="stylesheet">

    <!--font-awesome-->
    <link href="main/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="main/css/jquery.datetimepicker.css">

    <!--Custom-->
    <link href="main/css/style-<?php echo $lang ?>.css" rel="stylesheet">
    <link href="main/css/responsive-<?php echo $lang ?>.css" rel="stylesheet">


     </head>
  <body>
    
    <header class="header"><!--start of header-->
      <div class="header-announcements hide"><!--start of header annoucements-->
        <a href=""><img src="main/images/header-speaker.png">Public Library will be closed in Kuwait on Monday, February 25 to celebrate National Day</a>
         <ul class="col-md-12 header-social">
          <li><a href="" class="fa fa-facebook-square" target="_blank"></a></li>
          <li><a href="" class="fa fa-twitter-square" target="_blank"></a></li>
          <li><a href="" class="fa fa-youtube-square" target="_blank"></a></li>
          <li><a href="" class="fa fa-google-plus-square" target="_blank"></a></li>
        </ul>
      </div><!--end of header annoucements-->

      <div class="container-fluid header-main"><!--start of header main-->

        <div class="header-main-left"><!--start of header main right-->
            <button class="col-md-12 header-nav-btn">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <a href="index.php?lang=<?php echo $lang ?>" class="col-md-12 logo"><img src="main/images/logo.png" alt=""></a>
        </div><!--end of header main right-->

        <div class="header-main-right"><!--start of header main right-->
        <?php  if($front_session->is_logged() == false){?>
            <li class="dropdown">
             <a href="" class="col-md-12 header-login dropdown-toggle" data-toggle="dropdown"><?php echo  $login ?> <span class="caret"></span></a>
             <form class="dropdown-menu give-border-top header-login-form" id="login">
             <input type="hidden" id="lang" value="<?php  echo $lang?>">
                <h4 class="col-md-12 give-darkRed-c give-boldFamily"> <?php echo  $login_title ?></h4>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder=" <?php echo  $email ?> " id="login_email">
                </div>

                 <div class="form-group">
                  <input type="password" class="form-control" placeholder=" <?php echo  $password ?>" id="login_pass">
                </div>

                <div class=" form-group">
                  <a href="forget-password.php?lang=<?php echo $lang ?>"><?php echo  $forget_password ?></a>
                </div>

                <div class="clearfix form-group">
                  <input type="submit" class="btn give-red-bg" class="form-control" placeholder="<?php echo  $submit; ?>">
                  <a href="user-register.php?lang=<?php echo $lang ?>" class="pull-right"> <?php echo  $new_user; ?></a>
                </div>
                <div id="validation_message"></div>
             </form>
             
          </li>
          <?php }else{ 
		  $user_data = CustomerInfo::find_by_id($front_session->user_id);
		  if($user_data){
		  
		  
		  ?>
          <ul class="col-dm-12 header-afterLogin">
             <li class="dropdown header-user">
               <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $welcome_user. " ".$user_data->full_name(); ?>  <span class="caret"></span></a>
               <ul class="dropdown-menu give-border-top">
                 <li><a href="user-profile.php?lang=<?php echo $lang; ?>"><span class="fa fa-user give-red-c"></span> <?php  echo $profile ?></a></li>
                  <li><a href="insert_new_feed.php?lang=<?php echo $lang; ?>"><span class="fa fa-share-square give-red-c "></span>  <?php  echo $insert_new_feed ?></a></li>
                 <li><a href="logout.php?lang=<?php echo $lang; ?>"><span class="fa fa-sign-out give-red-c"></span>  <?php  echo $log_out ?></a></li>
                
               </ul>
             </li>

              <li class="dropdown header-notify hasnew " onclick="
              if ($(this).hasClass('hasnew')){
                $(this).removeClass('hasnew')
                $(this).find('.header-notify-quan').css('display','none')
                }">
                <?php  $get_feeds = $define_feeds->get_feeds(null,null,null,"inserted_date","DESC",3);
				   $feed_info =  $define_node->front_node_data(16,null,$node_alias,null,$lang_info->id,null,null,null,null, null,null,null,"one");
				   $feed_count = count($get_feeds);
				   
				   ?>
               <a href="" class="dropdown-toggle give-gray-bg give-border" data-toggle="dropdown"> <span class="fa fa-bell"></span> <p class="header-notify-quan give-red-c"><?php  echo $feed_count?></p></a>
               <ul class="dropdown-menu give-border-top">
               <?php
			     
				  if($get_feeds){
					  foreach($get_feeds as $feed){
						  if($feed->resource_type =="divacms"){
							  $feed_all_data = $define_node->front_node_data($feed->resource_id,null,null,null,$lang_info->id,null,null,null,null, null,null,null,"one");
							  
							   if($feed_all_data->node_type == "post"){
								   $path = "post_details.php?lang=$lang&alias=$feed_all_data->alias";
							   }else if($feed_all_data->node_type == "page"){
								   $path = "content.php?lang=$lang&alias=$feed_all_data->alias";
							   }else{
								   $path = "event_details.php?lang=$lang&alias=$feed_all_data->alias";
							   }
						  }else{
							  $feed_all_data = $define_node->front_node_data($feed->resource_id,null,null,null,$lang_info->id,null,null,null,null, null,null,null,"one");
							  $path = "content.php?lang=$lang&alias=$feed_info->alias";
							  
						  }
						  if($lang == "en"){
						       $date = english_date($feed_all_data->start_publishing);
						   }else{
								$date = arabic_date($feed_all_data->start_publishing);
						   }
						  echo "<li>
                    <article class='col-md-12 header-notify-post'>
                        <a href='$path' class='fa fa-book header-notify-icon give-red-c'></a>
                        
                        <div class='col-md-12 header-notify-text'> 
                          <h4 >
                              <a href='$path' >$feed_all_data->title....</a>
                           </h4>
                           <ul class='col-md-12 post-meta give-flex'>
                            <li>
                                <p class='fa fa-clock-o give-red-c'></p>  ".date('h:i:s A',strtotime($feed_all_data->start_publishing))."
                            </li>
                             <li>
                                <p class='fa fa-calendar give-red-c'></p> $date
                            </li>
                          </ul>
                        </div>
                    </article>
                 </li>";
					}
				  }
			   
			    ?>
                  <li class="col-md-12 give-red-c give-align-right">
                   <a href="content.php?lang=<?php echo $lang."&alias=$feed_info->alias" ?>" > <span class="fa fa-long-arrow-right"><?php echo $view_all_feeds;  ?></span></a>
                 </li>
               </ul>
             </li>

              <li class="dropdown header-notify hide">
               <a href="" class="dropdown-toggle give-gray-bg give-border" data-toggle="dropdown"> <span class="fa fa-envelope"></span> <p class="header-notify-quan give-red-c">+999</p></a>
               <ul class="dropdown-menu give-border-top">
                <li>
                    <article class="col-md-12 header-notify-post">
                        <a href="" class="fa fa-book header-notify-icon give-red-c"></a>
                        
                        <div class="col-md-12 header-notify-text"> 
                          <h4 >
                              <a href="" >Notification1 written here notification1 written notification1 written here....</a>
                           </h4>
                           <ul class="col-md-12 post-meta">
                            <li>
                                <p class="fa fa-clock-o give-red-c"></p> 2 Hrs ago.
                            </li>
                             <li>
                                <p class="fa fa-calendar give-red-c"></p> 12/1/2014.
                            </li>
                          </ul>
                        </div>
                    </article>
                 </li>
                 <li>
                    <article class="col-md-12 header-notify-post">
                        <a href="" class="fa fa-book header-notify-icon give-red-c"></a>
                        
                        <div class="col-md-12 header-notify-text"> 
                          <h4 >
                              <a href="" >Notification1 written here notification1 written notification1 written here....</a>
                           </h4>
                           <ul class="col-md-12 post-meta">
                            <li>
                                <p class="fa fa-clock-o give-red-c"></p> 2 Hrs ago.
                            </li>
                             <li>
                                <p class="fa fa-calendar give-red-c"></p> 12/1/2014.
                            </li>
                          </ul>
                        </div>
                    </article>
                 </li>
                 <li>
                    <article class="col-md-12 header-notify-post">
                        <a href="" class="fa fa-book header-notify-icon give-red-c"></a>
                        
                        <div class="col-md-12 header-notify-text"> 
                          <h4 >
                              <a href="" >Notification1 written here notification1 written notification1 written here....</a>
                           </h4>
                           <ul class="col-md-12 post-meta">
                            <li>
                                <p class="fa fa-clock-o give-red-c"></p> 2 Hrs ago.
                            </li>
                             <li>
                                <p class="fa fa-calendar give-red-c"></p> 12/1/2014.
                            </li>
                          </ul>
                        </div>
                    </article>
                 </li>
                 <?php
				 $feed_info =  $define_node->front_node_data(16,null,$node_alias,null,$lang_info->id,null,null,null,null, null,null,null,"one");
				 
				 
				  ?>
                 <li class="col-md-12 give-red-c give-align-right">
                   <a href="content.php?lang=<?php echo $lang."&alias=$feed_info->alias" ?>" > <span class="fa fa-long-arrow-right"><?php echo $view_all_feeds;  ?></span></a>
                 </li>
               </ul>
             </li>

           </ul>
          <?php }
		  }?>
           <?php
			if($lang == 'en'){
				echo "<a href='$url_ar' class='btn give-red-bg give-white-c header-language'>عربى</a>";
			}else{
				echo "<a href='$url_en' class='btn give-red-bg give-white-c header-language'>English</a>";
			}
	      ?>

            
        </div><!--end of header main right-->

        <?php 
            $data = new vufindFormat();
            $data->enable_relation();
            $data = $data->get_formatData($_GET['lang']);
            
            $setting = GeneralSettings::find_by_id(1);

                   ?>

        <form class="col-md-12 header-search" id="search-form" action=""  target="_blank">
        <!--start of header search-->
          <div class="col-md-12 header-search-select-wrap"><!--start of header search select-->
            <div class="col-md-12 header-search-select">
              <p class="col-md-12 header-search-selected"><?php echo $select_format?></p>
              <span class="caret"></span>
            </div>
            <ul class="col-md-12 header-search-list give-border-top" id="data">
            <?php
              foreach ($data as $info){?>
                   <li data-format="<?php echo $info->type_format;?>"><?php echo $info->title;?></li>
              
            <?php  }
            ?>
             
            </ul>
          </div><!--end of header search select-->
          
            <input type="hidden" id="main_url"  value="<?php echo  $setting->vufind_main_url;?>">
          <div class="form-group  header-search-input">
            <input type="text" class="form-control" placeholder="" id="search_key">
          </div>

           <div class="form-group  header-search-button">
            <input type="submit" value="<?php echo $search?>" name="" type="submit" class="btn give-red-bg header-search-submit fa fa-search" >
            <a href="<?php echo  $setting->vufind_advanced_search;?>"  class="btn give-red-bg header-search-advanced" target='_blank'><?php echo $advanced ?></a>
          </div>
         
        </form><!--end of header search-->

      </div><!--end of header main-->
    </header><!--end of header-->

