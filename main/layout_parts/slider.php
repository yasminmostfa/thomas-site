<section class="carousel slide metro-slider" id="main-slider"><!--start of metro-slider-->
 <div class="carousel-inner"><!--start of carousel-inner-->
 <?php
 $slider = $define_node->front_node_data(null,null,null,null,$lang_info->id,null,"yes",null, null, null,null,null,"many");
 if($slider){
	 
	 $count_slides = count($slider);
	 
	 $number_of_slides = ceil($count_slides/5);
     $offset = 0 ;
	 for($i = 0;$i<$number_of_slides;$i++){
		
		 if($i == 0){
			 $class = "active";
		  }else{
			  $class = "";
		  }
		 
		 echo " <div class='item $class clearfix'>";
		 $sec_metro_first_slid =  $define_node->front_node_data(null,null,null,null,$lang_info->id,null,"yes",null, null, null,null,null,'one',1,$offset+1); 
		 $sec_metro=  $define_node->front_node_data(null,null,null,null,$lang_info->id,null,"yes",null, null, null,null,null,'many',2,$offset+2);
		 $first_artical = $define_node->front_node_data(null,null,null,null,$lang_info->id,null,"yes",null, null, null,null,null,'one',1,$offset);
		 $third_metro=  $define_node->front_node_data(null,null,null,null,$lang_info->id,null,"yes",null, null, null,null,null,'many',2,$offset+4);
		 if($first_artical){ 
		  if($first_artical->node_type =="page"){
			  $path = "content.php";
			  
		  }else if($first_artical->node_type == "post"){
			   $path = "post_details.php";
		  }else{
			   $path = "event_details.php";
		  }
		 echo "<div class='col-md-3 metro-quarter metro-item ' style='background-image: url(media-library/slider/$first_artical->slider_cover)'><!--metro-item-->
                <h3 class='col-md-12 metro-caption give-boldFamily'>
                  <a href='$path?lang=$lang&alias=$first_artical->alias'  class='add_view' id='$first_artical->id' = name = 'divacms'>
				  $first_artical->title
                  </a>
                </h3>
              </div>";
		 }
		echo "<div class='col-md-6 metro-half metro-item'><!--metro-item-->
		       <div class='col-md-12 metro-height-half'>";
			   if($sec_metro){
				   foreach($sec_metro as $post){
					   if($post->node_type =="page"){
							$path = "content.php";
							
						}else if($post->node_type == "post"){
							 $path = "post_details.php";
						}else{
							 $path = "event_details.php";
						}
					   echo "<div class='col-md-6 metro-item'  style='background-image: url(media-library/slider/$post->slider_cover)'>
							  <h3 class='col-md-12 metro-caption give-boldFamily'>
								<a href='post_details.php?lang=$lang&alias=$post->alias' class='add_view' id='$post->id' = name = 'divacms'>
								 $post->title
								</a>
							  </h3>
							</div>";
				   }
			   }
		
		
		echo "</div>";
		if($sec_metro_first_slid){
			if($sec_metro_first_slid->node_type =="page"){
				  $path = "content.php";
				  
			  }else if($sec_metro_first_slid->node_type == "post"){
				   $path = "post_details.php";
			  }else{
				   $path = "event_details.php";
			  }
			echo "<div class='col-md-12 metro-height-half' style='background-image: url(media-library/slider/$sec_metro_first_slid->slider_cover)'>
					  <h3 class='col-md-12 metro-caption give-boldFamily'>
						  <a href='$path?lang=$lang&alias=$sec_metro_first_slid->alias' class='add_view' id='$sec_metro_first_slid->id' = name = 'divacms'>$sec_metro_first_slid->title
						  </a>
					  </h3>
					</div>";
			echo "</div>";
		}
        echo "<div class='col-md-3 metro-quarter metro-item'>";
		if($third_metro){
			foreach($third_metro as $post){
				if($post->node_type =="page"){
							$path = "content.php";
							
				}else if($post->node_type == "post"){
							 $path = "post_details.php";
				}else{
							 $path = "event_details.php";
				}
				echo "<div class='col-md-12 metro-height-half'>
                  <div class='col-md-12 metro-item'  style='background-image: url(media-library/slider/$post->slider_cover)'>
                    <h3 class='col-md-12 metro-caption give-boldFamily'>
                      <a href='$path?lang=$lang&alias=$post->alias' class='add_view' id='$sec_metro_first_slid->id' = name = 'divacms'>$post->title
                      </a>
                    </h3>
                  </div>
                </div>";
			}
		}
		
		echo "</div>";			  
		 
		 echo "</div>";
		 $offset = $offset +5;
		 
	}
}

?>

     </div>
</section>