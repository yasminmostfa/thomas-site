<?php
    $title = "";
	$summary = "";
	if($node_type == "page"){
		$node_id = $get_page_content->id;
		$node_layout = $page_layout_info->model;
		$title =  $get_page_content->title;
		
	 }else if($node_type == "post"){
		$node_id = $get_post_content->id;
		$node_layout = $post_layout_info->model;
		$title =  $get_post_content->title;
		//$category = $define_node_tax->return_node_taxonomy($get_post_content->id,'post','category','one',$lang_info->id);
		//$page_data = $define_node_tax->return_taxonomy_nodes($category->id,"page","category",'one',$lang_info->id);
	}else{
		$node_id = $get_event_content->id;
		$node_layout = $event_layout_info->model;
		$title =  $get_event_content->title;
		 
	}
	
?> 
  <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="index.php?lang=<?php echo $lang ?>"><?php echo $home ?></a></li>
              <li class="active"><?php echo $title; ?></li>
            </ol>
          </div>
      </section><!--end of internal-cover-->
        <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
            <div class="col-md-8 main-content">
		 <?php 
         //get left models by model ids 
                        $define_theme_left_plugins = new ThemeLayoutModelPlugin();
                         $define_theme_left_plugins->enable_relation();
                         $left_plugins = $define_theme_left_plugins->front_get_model_plugin($node_layout, 'left');
                        foreach($left_plugins as $plugin){
                            require_once("plugins/".$plugin->plugin_source."/viewer.php");
                            
                        }
                        
        ?>
        </div>
         <aside class="col-md-4 main-sidebar">
         <?php 
         //get left models by model ids 
                        $define_theme_left_plugins = new ThemeLayoutModelPlugin();
                         $define_theme_left_plugins->enable_relation();
                         $left_plugins = $define_theme_left_plugins->front_get_model_plugin($node_layout, 'right');
                        foreach($left_plugins as $plugin){
                            require_once("plugins/".$plugin->plugin_source."/viewer.php");
                            
                        }
                        
        ?>
         </aside>
</div>
</div>
 