-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 07, 2016 at 11:19 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `new_diva_cms`
--
CREATE DATABASE IF NOT EXISTS `new_diva_cms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `new_diva_cms`;

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE IF NOT EXISTS `advertisements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_cover` varchar(500) NOT NULL,
  `path` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `type` enum('post','page','event','external') NOT NULL,
  `external` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_content`
--

CREATE TABLE IF NOT EXISTS `advertisement_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NOT NULL,
  `adv_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(500) CHARACTER SET latin1 NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `type`) VALUES
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password');

-- --------------------------------------------------------

--
-- Table structure for table `cms_module_access`
--

CREATE TABLE IF NOT EXISTS `cms_module_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `sorting` int(11) NOT NULL,
  `type` enum('module','page') NOT NULL,
  `shadow` enum('yes','no') NOT NULL,
  `file_source` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=283 ;

--
-- Dumping data for table `cms_module_access`
--

INSERT INTO `cms_module_access` (`id`, `sid`, `title`, `icon`, `sorting`, `type`, `shadow`, `file_source`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(5, 0, 'Media Library', 'icon-inbox', 2, 'module', '', 'media_library_directories', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(6, 0, 'Users', 'icon-user', 1, 'module', 'no', 'users', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(7, 0, 'Menus Group', ' icon-link', 3, 'module', '', 'menu_group,menu_link', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(10, 0, 'Nodes', 'icon-file-text-alt', 4, 'module', 'no', 'nodes', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(11, 0, 'Taxonomies', 'icon-tags', 8, 'module', 'no', 'taxonomies', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(12, 0, 'Setting', 'icon-cogs', 10, 'module', 'no', 'profiles,profile_pages,localization,cms_modules,general_settings,ecom_customers_groups,ecom_taxes,ecom_payment_methods,home_layout', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(13, 5, 'Media Directories', '', 1, 'page', 'no', 'media_library_directories/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(16, 6, 'show all', '', 1, 'page', 'no', 'users/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(17, 6, 'add new', '', 2, 'page', 'no', 'users/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(22, 10, 'Posts', '', 1, 'page', 'no', 'nodes/view_posts', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(23, 10, 'add new', '', 4, 'page', 'no', 'nodes/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(25, 11, 'tags', '', 6, 'page', 'no', 'taxonomies/view_tags', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(26, 12, 'profiles', '', 2, 'page', 'no', 'profiles/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(27, 12, 'Localization', '', 8, 'page', 'no', 'localization/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(28, 12, 'CPanel Menus Structure', '', 7, 'page', 'no', 'cms_modules/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(29, 7, 'show all ', '', 1, 'page', 'no', 'menu_group/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(30, 7, 'add new ', '', 2, 'page', 'no', 'menu_group/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(31, 11, 'update', '', 11, 'page', 'no', 'taxonomies/update', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(33, 0, 'Social Activity', ' icon-comments-alt', 7, 'module', 'no', 'social_suggestion_topics,social_comments,social_email_subscription,poll_questions_options,poll_questions,form_attributes,forms,advertisements', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(34, 33, 'Comments ', '', 1, 'page', 'no', 'social_comments/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(35, 33, 'Polls ', '', 5, 'page', 'no', 'poll_questions/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(36, 33, 'Poll Options  View', '', 10, 'page', 'yes', 'poll_questions_options/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(37, 33, 'Email Subscription ', '', 15, 'page', 'no', 'social_email_subscription/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(38, 33, 'suggestion topics ', '', 16, 'page', 'no', 'social_suggestion_topics/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(39, 12, 'General Settings', '', 1, 'page', 'no', 'general_settings/update', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(40, 275, 'Plugins', '', 1, 'page', 'no', 'plugins/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(42, 275, 'Themes & Layouts', '', 2, 'page', 'no', 'themes/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(43, 12, 'Profile Insert', '', 0, 'page', 'yes', 'profiles/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(44, 12, 'Profile Update', '', 2, 'page', 'yes', 'profiles/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(45, 12, 'Profile Delete', '', 2, 'page', 'yes', 'profiles/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(47, 12, 'CPanel Menus Structure Insert', '', 3, 'page', 'yes', 'cms_modules/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(48, 12, 'CPanel Menus Structure Update', '', 3, 'page', 'yes', 'cms_modules/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(50, 12, 'Localization  Insert ', '', 8, 'page', 'yes', 'localization/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(52, 12, 'Localization update', '', 8, 'page', 'yes', 'localization/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(53, 12, 'Localization Delete', '', 8, 'page', 'yes', 'localization/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(54, 12, 'CPanel Menus Structure Delete', '', 8, 'page', 'yes', 'cms_modules/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(56, 11, 'Categories', '', 1, 'page', 'no', 'taxonomies/view_categories', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(58, 11, 'Courses', '', 5, 'page', 'no', 'taxonomies/view_courses', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(64, 10, 'update', '', 3, 'page', 'yes', 'nodes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(65, 10, 'delete', '', 5, 'page', 'yes', 'nodes/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(70, 6, 'update', '', 3, 'page', 'yes', 'users/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(71, 6, 'delete', '', 5, 'page', 'yes', 'users/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(72, 7, 'group update ', '', 3, 'page', 'yes', 'menu_group/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(73, 7, 'group delete ', '', 5, 'page', 'yes', 'menu_group/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(74, 33, 'Comments update ', '', 2, 'page', 'yes', 'social_comments/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(77, 33, ' Poll Insert', '', 6, 'page', 'yes', 'poll_questions/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(78, 33, 'Poll Update ', '', 7, 'page', 'yes', 'poll_questions/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(79, 33, 'Poll full info', '', 8, 'page', 'yes', 'poll_questions/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(80, 33, ' Poll delete', '', 9, 'page', 'yes', 'poll_questions/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(81, 33, 'Poll Options Insert', '', 11, 'page', 'yes', 'poll_questions_options/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(83, 33, 'Poll Options update', '', 12, 'page', 'yes', 'poll_questions_options/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(84, 33, 'Poll Option full info', '', 13, 'page', 'yes', 'poll_questions_options/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(86, 33, 'Poll Options delete', '', 14, 'page', 'yes', 'poll_questions_options/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(87, 33, 'Suggestion topics delete ', '', 17, 'page', 'yes', ' social_suggestion_topics /delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(91, 6, 'full info', '', 4, 'page', 'yes', 'users/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(92, 7, 'group full info ', '', 4, 'page', 'yes', 'menu_group/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(93, 10, 'full info', '', 4, 'page', 'yes', 'nodes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(94, 11, 'Topics', '', 4, 'page', 'no', 'taxonomies/view_topics', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(95, 11, 'Add New ', '', 7, 'page', 'no', 'taxonomies/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(96, 11, 'Full info', '', 14, 'page', 'yes', 'taxonomies/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(97, 12, 'profile access updates', '', 2, 'page', 'yes', 'profile_pages/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(98, 7, 'menu show all', '', 6, 'page', 'yes', 'menu_link/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(99, 7, 'menu add new', '', 7, 'page', 'yes', 'menu_link/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(101, 7, 'menu update', '', 7, 'page', 'yes', 'menu_link/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(102, 7, 'menu delete', '', 9, 'page', 'yes', 'menu_link/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(103, 7, 'menu full info', '', 8, 'page', 'yes', 'menu_link/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(107, 12, 'localization label&message', '', 17, 'page', 'yes', 'localization/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(108, 12, 'Profile Full Info', '', 2, 'page', 'yes', 'profiles/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(109, 33, 'suggestion topics full info', '', 16, 'page', 'yes', ' social_suggestion_topics /full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(110, 33, 'comments full info', '', 3, 'page', 'yes', 'social_comments/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(111, 33, 'Email Subscription  delete', '', 15, 'page', 'yes', 'social_email_subscription/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(112, 33, 'Comments delete', '', 4, 'page', 'yes', 'social_comments/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(113, 11, 'Authors', '', 5, 'page', 'no', 'taxonomies/view_authors', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(114, 11, 'delete', '', 10, 'page', 'yes', 'taxonomies/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(116, 12, 'insert theme', '', 7, 'page', 'yes', 'themes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(117, 12, 'view layouts', '', 7, 'page', 'yes', 'themes/view_layouts', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(118, 12, 'plugins insert', '', 6, 'page', 'yes', 'plugins/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(119, 5, 'media library Insert', '', 2, 'page', 'yes', 'media_library_directories/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(120, 5, 'media library edit', '', 3, 'page', 'yes', 'media_library_directories/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(121, 5, 'media File Insert', '', 4, 'page', 'yes', 'media_library_files/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(122, 12, 'customize layout plugin', '', 14, 'page', 'yes', 'themes/customize_layout_plugin', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(127, 10, 'Pages', '', 3, 'page', 'no', 'nodes/view_pages', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(129, 7, 'Insert menu link content', '', 11, 'page', 'yes', 'menu_link/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(131, 10, ' Plugin Option', '', 7, 'page', 'yes', 'nodes/plugin_option', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(132, 33, 'Forms', '', 2, 'page', 'no', 'forms/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(133, 33, 'form insert ', '', 21, 'page', 'yes', 'forms/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(134, 33, 'forms update', '', 22, 'page', 'yes', 'forms/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(135, 33, 'forms full info', '', 23, 'page', 'yes', 'forms/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(136, 33, 'forms delete', '', 24, 'page', 'yes', 'forms/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(137, 33, 'form attributes view', '', 25, 'page', 'yes', 'form_attributes/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(138, 33, 'form attributes Insert', '', 26, 'page', 'yes', 'form_attributes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(139, 33, 'form attributes  update', '', 27, 'page', 'yes', 'form_attributes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(140, 33, 'form attributes  full info', '', 28, 'page', 'yes', 'form_attributes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(141, 33, 'form attributes delete', '', 29, 'page', 'yes', 'form_attributes/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(142, 33, 'advertisements ', '', 30, 'page', 'no', 'advertisements/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(143, 33, 'advertisements insert', '', 31, 'page', 'yes', 'advertisements/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(144, 33, 'advertisements update', '', 32, 'page', 'yes', 'advertisements/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(145, 33, 'advertisements full info', '', 33, 'page', 'yes', 'advertisements/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(146, 33, 'advertisements delete', '', 34, 'page', 'yes', 'advertisements/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(149, 33, 'form content', '', 34, 'page', 'yes', 'forms/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(150, 33, 'advertisements content', '', 36, 'page', 'yes', 'advertisements/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(151, 33, 'view form table', '', 38, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(152, 33, 'form view table', '', 4, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(153, 33, 'form inserted data', '', 5, 'page', 'yes', 'form_attributes/inserted_data', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(154, 5, 'view files', '', 2, 'page', 'yes', 'media_library_files/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(229, 12, 'taxes full info', '', 2, 'page', 'yes', 'ecom_taxes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(235, 12, 'payment method insert', '', 3, 'page', 'yes', 'ecom_payment_methods/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(237, 12, 'payment methods update', '', 3, 'page', 'yes', 'ecom_payment_methods/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(238, 12, ' payment methods info', '', 3, 'page', 'yes', 'ecom_payment_methods/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(254, 12, 'Home layout ', '', 40, 'page', 'no', 'home_layout/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(255, 12, 'Home layout Insert', '', 41, 'page', 'yes', 'home_layout/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(256, 12, 'home layout update', '', 41, 'page', 'yes', 'home_layout/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(257, 12, 'home layout full_info', '', 41, 'page', 'yes', 'index_layout/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(258, 12, 'home layout delete', '', 41, 'page', 'yes', 'home_layout/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(267, 33, 'Contact US', '', 39, 'page', 'no', 'contact_us/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(268, 33, 'Contact us delete', '', 39, 'page', 'yes', 'contact_us/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(275, 0, 'utilities ', ' icon-wrench', 11, 'module', 'no', 'plugins,themes,cities,options,ecom_order_statuses', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(282, 10, 'Events', '', 3, 'page', 'no', 'nodes/view_events', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(265) CHARACTER SET utf16 COLLATE utf16_esperanto_ci NOT NULL,
  `company` varchar(100) NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE IF NOT EXISTS `customer_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `fbid` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `company` varchar(250) NOT NULL,
  `birthday` date DEFAULT '0000-00-00',
  `registration_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `account_status` enum('verified','not_verified','blocked') NOT NULL,
  `activate_code` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_selections`
--

CREATE TABLE IF NOT EXISTS `customer_selections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events_details`
--

CREATE TABLE IF NOT EXISTS `events_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `place` varchar(2580) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `events_details`
--

INSERT INTO `events_details` (`id`, `event_id`, `place`, `start_date`, `end_date`) VALUES
(1, 1, 'uytuty', '2016-06-02 11:58:00', '2016-06-02 11:58:00'),
(2, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 10, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 11, '', '2016-06-07 05:17:00', '2016-06-18 05:17:00'),
(12, 12, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 13, '', '2016-06-07 11:44:00', '2016-06-07 11:44:00'),
(14, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 15, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `forget_password`
--

CREATE TABLE IF NOT EXISTS `forget_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `code` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE IF NOT EXISTS `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `label` varchar(250) NOT NULL,
  `enable` enum('yes','no') NOT NULL,
  `email_to` varchar(500) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `name`, `label`, `enable`, `email_to`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'dfgdfg', 'tret', 'no', 'ttrt', '2015-05-24 16:09:05', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_attributes`
--

CREATE TABLE IF NOT EXISTS `form_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_label` varchar(250) NOT NULL,
  `sorting` int(11) NOT NULL,
  `required` enum('yes','no') NOT NULL,
  `form_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_values` text NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` date NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `form_attributes`
--

INSERT INTO `form_attributes` (`id`, `attribute_label`, `sorting`, `required`, `form_id`, `attribute_id`, `attribute_values`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'Name', 1, 'no', 1, 1, '', '2015-05-24 16:10:14', 1, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_inserted_data`
--

CREATE TABLE IF NOT EXISTS `form_inserted_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `label` varchar(25) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `general_setting`
--

CREATE TABLE IF NOT EXISTS `general_setting` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `site_url` varchar(500) NOT NULL,
  `meta_key` text NOT NULL,
  `email` varchar(256) NOT NULL,
  `time_zone_id` int(11) NOT NULL,
  `front_lang_id` int(11) NOT NULL,
  `translate_lang_id` int(11) NOT NULL,
  `enable_website` enum('yes','no') NOT NULL,
  `offline_messages` longtext NOT NULL,
  `description` longtext NOT NULL,
  `google_analitic` longtext CHARACTER SET utf16 NOT NULL,
  `main_order_statues` int(11) NOT NULL,
  `enable_store` enum('yes','no') NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_setting`
--

INSERT INTO `general_setting` (`id`, `title`, `site_url`, `meta_key`, `email`, `time_zone_id`, `front_lang_id`, `translate_lang_id`, `enable_website`, `offline_messages`, `description`, `google_analitic`, `main_order_statues`, `enable_store`, `update_by`, `last_update`) VALUES
(1, 'Welcome To New Motors ||Ù…Ø±Ø­Ø¨Ø§ Ø¨ÙƒÙ…   Ù†ÙŠÙˆ Ù…ÙˆØªÙˆØ±Ø² ', 'www.diva-lab.com/projects/jow3an', 'website design in Egypt, web development in egypt,Mobile solutions, web applications, web based applications, web marketing, web services, seo, search engine optimization||', 'info@diva-lab.com', 35, 1, 1, 'yes', '7567', 'Diva company provides the latest technologies in web design and web development in Egypt & ME providing responsive and good user experience interface||', '767', 8, '', 1, '2016-05-12 12:02:10');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_layout`
--

CREATE TABLE IF NOT EXISTS `home_page_layout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `position` int(11) NOT NULL,
  `plugin` int(11) NOT NULL,
  `header_title` varchar(500) NOT NULL,
  `plugin_value` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `home_page_layout`
--

INSERT INTO `home_page_layout` (`id`, `title`, `status`, `position`, `plugin`, `header_title`, `plugin_value`) VALUES
(1, 'position one ', 'publish', 1, 38, '', ''),
(2, 'postion2', 'publish', 3, 39, '', ''),
(3, '3', 'publish', 2, 40, '', ''),
(4, '7', 'publish', 9, 41, 'Latest News||Ø§Ø®Ø± Ø§Ù„Ø§Ø®Ø¨Ø§Ø±', '2'),
(5, '9', 'publish', 7, 42, 'Important links||Ø±ÙˆØ§Ø¨Ø· Ù…Ù‡Ù…Ø©', 'main_menu'),
(6, '8', 'publish', 8, 43, 'Upcoming Event ||Ø§Ø®Ø± Ø§Ù„Ø§Ø­Ø¯Ø§Ø«', '12');

-- --------------------------------------------------------

--
-- Table structure for table `localization`
--

CREATE TABLE IF NOT EXISTS `localization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `label` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `localization`
--

INSERT INTO `localization` (`id`, `name`, `sorting`, `status`, `label`) VALUES
(1, 'English', 1, 'active', 'en'),
(2, 'Arabic', 2, 'active', 'ar');

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE IF NOT EXISTS `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('draft','publish') NOT NULL,
  `enable_summary` enum('yes','no') NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `start_publishing` datetime NOT NULL,
  `end_publishing` datetime NOT NULL,
  `enable_comments` enum('yes','no') NOT NULL,
  `front_page` enum('yes','no') NOT NULL,
  `slide_show` enum('yes','no') NOT NULL,
  `side_bar` enum('yes','no') NOT NULL,
  `cover_image` varchar(250) NOT NULL,
  `slider_cover` varchar(250) NOT NULL,
  `node_type` enum('post','page','event') NOT NULL,
  `place` varchar(256) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `model` int(11) NOT NULL,
  `shortcut_link` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `status`, `enable_summary`, `inserted_date`, `inserted_by`, `last_update`, `update_by`, `start_publishing`, `end_publishing`, `enable_comments`, `front_page`, `slide_show`, `side_bar`, `cover_image`, `slider_cover`, `node_type`, `place`, `start_date`, `end_date`, `model`, `shortcut_link`) VALUES
(2, 'publish', 'no', '2016-06-05 15:54:36', 1, '2016-06-07 10:42:49', 1, '2016-06-05 15:54:36', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/cover.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465142076'),
(3, 'publish', 'no', '2016-06-07 02:46:45', 1, '2016-06-07 03:47:27', 1, '2016-06-05 03:42:00', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'yes', '', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465267605'),
(4, 'publish', 'no', '2016-06-07 03:48:07', 1, '2016-06-07 04:50:09', 1, '2016-06-07 03:48:07', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'yes', 'images/cover.jpg  ', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465271287'),
(5, 'publish', 'no', '2016-06-07 03:49:58', 1, '0000-00-00 00:00:00', 0, '2016-06-07 03:49:58', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'no', '', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465271398'),
(6, 'publish', 'no', '2016-06-07 03:50:50', 1, '0000-00-00 00:00:00', 0, '2016-06-07 03:50:50', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'no', '', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465271450'),
(7, 'publish', 'no', '2016-06-07 03:52:34', 1, '0000-00-00 00:00:00', 0, '2016-06-07 03:52:34', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'no', '', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465271554'),
(8, 'publish', 'no', '2016-06-07 03:53:20', 1, '2016-06-07 03:58:49', 1, '2016-06-07 03:53:20', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'no', '', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465271600'),
(9, 'publish', 'no', '2016-06-07 03:53:59', 1, '2016-06-07 03:54:28', 1, '2016-06-07 03:53:59', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'no', '', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465271639'),
(10, 'publish', 'no', '2016-06-07 03:55:06', 1, '2016-06-07 11:12:32', 1, '2016-06-07 03:55:06', '0000-00-00 00:00:00', 'no', 'no', 'yes', 'yes', 'images/cover.jpg   ', 'dummy-image.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1465271706'),
(11, 'publish', 'no', '2016-06-07 05:17:23', 1, '2016-06-07 11:36:53', 1, '2016-06-07 05:17:23', '2016-06-18 05:17:00', 'no', 'no', 'no', 'yes', 'images/dummy-image.jpg', '', 'event', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '1465276643'),
(12, 'publish', 'no', '2016-06-07 05:33:37', 1, '2016-06-07 12:13:38', 1, '2016-06-07 05:33:37', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, '1465277617'),
(13, 'publish', 'no', '2016-06-07 11:45:18', 1, '0000-00-00 00:00:00', 0, '2016-06-07 11:45:18', '2016-06-07 11:44:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, '1465299918'),
(14, 'publish', 'no', '2016-06-07 12:20:09', 1, '2016-06-07 12:20:55', 1, '2016-06-07 12:20:09', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1465302009'),
(15, 'publish', 'no', '2016-06-07 12:25:59', 1, '2016-06-07 12:27:49', 1, '2016-06-07 12:25:59', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1465302359');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_content`
--

CREATE TABLE IF NOT EXISTS `nodes_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `alias` varchar(250) NOT NULL,
  `summary` text NOT NULL,
  `body` longtext NOT NULL,
  `meta_keys` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `nodes_content`
--

INSERT INTO `nodes_content` (`id`, `node_id`, `lang_id`, `title`, `alias`, `summary`, `body`, `meta_keys`, `meta_description`) VALUES
(3, 2, 2, 'News2', 'news2', '', '<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13</p>', '', ''),
(4, 2, 1, 'News', 'news', '', '<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13</p>', '', ''),
(5, 3, 2, 'New collection of African-', 'new_collection_of_african-', '', '', '', ''),
(6, 3, 1, 'New collection of African- American voice recordings enhances holdings New collection of African- American voice recordings enhances holdings', 'new_collection_of_african-_american_voice_recordings_enhances_holdings_new_collection_of_african-_american_voice_recordings_enhances_holdings', '', '', '', ''),
(7, 4, 2, 'New collection of African- American voice recordings enhances holdings New collection of African- American ', 'new_collection_of_african-_american_voice_recordings_enhances_holdings_new_collection_of_african-_american_', '', '', '', ''),
(8, 4, 1, 'New collection of ', 'new_collection_of_', '', '', '', ''),
(9, 5, 2, 'New Post', 'new_post', '', '', '', ''),
(10, 5, 1, 'New Post', 'new_post', '', '', '', ''),
(11, 6, 2, 'News Post ', 'news_post_', '', '', '', ''),
(12, 6, 1, 'News Post ', 'news_post_', '', '', '', ''),
(13, 7, 2, 'latest News', 'latest_news', '', '', '', ''),
(14, 7, 1, 'latest News', 'latest_news', '', '', '', ''),
(15, 8, 2, 'New Artificial Title', 'new_artificial_title', '', '', '', ''),
(16, 8, 1, 'New Artificial Title', 'new_artificial_title', '', '', '', ''),
(17, 9, 2, 'New Post title', 'new_post_title', '', '', '', ''),
(18, 9, 1, 'New Post title', 'new_post_title', '', '', '', ''),
(19, 10, 2, 'New', 'new', '<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking</p>', '<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking</p>', '', ''),
(20, 10, 1, 'New', 'new', '', '', '', ''),
(21, 11, 2, 'A New Yorker doesn', 'a_new_yorker_doesn_t_necessarily_come', '', '', '', ''),
(22, 11, 1, 'A New Yorker doesn', 'a_new_yorker_doesn_t_necessarily_come', '', '', '', ''),
(23, 12, 2, 'Ø§Ù„Ø§Ø­Ø¯Ø§Ø«', 'Ø§Ù„Ø§Ø­Ø¯Ø§Ø«', '', '', '', ''),
(24, 12, 1, 'Events', 'events', '', '', '', ''),
(25, 13, 2, 'Ø§Ù„Ø§Ø®Ø¨Ø§Ø±', 'Ø§Ù„Ø§Ø®Ø¨Ø§Ø±', '', '', '', ''),
(26, 13, 1, 'News', 'news', '', '', '', ''),
(27, 14, 2, 'Ù…Ù† Ù†Ø­Ù†', 'Ù…Ù†_Ù†Ø­Ù†', '', '<section class="col-m-12 main-content-section"><!--start of main content section-->\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Our Vision</h2>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library\\ , where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Our Mission</h2>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library\\ , where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Contact Us</h2>\n<div class="col-md-12 map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d844.4912976216501!2d47.974819859680295!3d29.371838037419856!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3fcf84e9f2bfa193%3A0xa32b700108a07831!2sExpropriation+for+Public+Benifit+Department!5e0!3m2!1sen!2seg!4v1461508561574" width="100%" height="400" frameborder="0" allowfullscreen="allowfullscreen"></iframe> <img class="col-md-12 qr" src="images/qr.svg" alt="" /></div>\n<div class="row clearfix">\n<div class="col-md-6 contact-list">\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Contact Information</h2>\n<ul class="col-md-12 contact-list-inner">\n<li>\n<p class="fa fa-location-arrow give-red-c">Â </p>\n<p>O) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at t</p>\n</li>\n<li>\n<p class="fa fa-phone give-red-c">Â </p>\n<div>\n<p>(02) 0100 56 23653</p>\n<p>(02) 0100 56 23653</p>\n</div>\n</li>\n<li>\n<p class="fa fa-envelope give-red-c">Â </p>\n<p>company@info.com</p>\n</li>\n</ul>\n</div>\n<div class="col-md-6 contact-list">\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Newsletter</h2>\n<form class="col-md-12 primary-form" action="">\n<div class="form-group"><input class="form-control" type="text" placeholder="Name" /></div>\n<div class="form-group"><input class="form-control" type="text" placeholder="Email" /></div>\n<div class="form-group"><input class="btn give-red-bg" type="submit" value="Submit" /></div>\n</form></div>\n</div>\n</section>', '', ''),
(28, 14, 1, 'About Us', 'about_us', '', '<section class="col-m-12 main-content-section"><!--start of main content section-->\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Our Vision</h2>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library\\ , where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Our Mission</h2>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library\\ , where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Contact Us</h2>\n<div class="col-md-12 map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d844.4912976216501!2d47.974819859680295!3d29.371838037419856!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3fcf84e9f2bfa193%3A0xa32b700108a07831!2sExpropriation+for+Public+Benifit+Department!5e0!3m2!1sen!2seg!4v1461508561574" width="100%" height="400" frameborder="0" allowfullscreen="allowfullscreen"></iframe> <img class="col-md-12 qr" src="images/qr.svg" alt="" /></div>\n<div class="row clearfix">\n<div class="col-md-6 contact-list">\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Contact Information</h2>\n<ul class="col-md-12 contact-list-inner">\n<li>\n<p class="fa fa-location-arrow give-red-c">Â </p>\n<p>O) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at t</p>\n</li>\n<li>\n<p class="fa fa-phone give-red-c">Â </p>\n<div>\n<p>(02) 0100 56 23653</p>\n<p>(02) 0100 56 23653</p>\n</div>\n</li>\n<li>\n<p class="fa fa-envelope give-red-c">Â </p>\n<p>company@info.com</p>\n</li>\n</ul>\n</div>\n<div class="col-md-6 contact-list">\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Newsletter</h2>\n<form class="col-md-12 primary-form" action="">\n<div class="form-group"><input class="form-control" type="text" placeholder="Name" /></div>\n<div class="form-group"><input class="form-control" type="text" placeholder="Email" /></div>\n<div class="form-group"><input class="btn give-red-bg" type="submit" value="Submit" /></div>\n</form></div>\n</div>\n</section>', '', ''),
(29, 15, 2, 'Elizabeth Pickard', 'elizabeth_pickard', '', '<article class="col-md-12 internal-post"><!--start of internal-post-->\n<h2 class="col-md-12 give-boldFamily give-red-c">Elizabeth Pickard</h2>\n<div class=" clearfix">\n<div class="col-md-6 secondary-image give-mb-med" style="background-image: url(''main/images/dummy-image.jpg'');">Â </div>\n<div class="col-md-6 secondary-text give-mb-med">\n<h2 class="col-md-12 give-boldFamily give-darkRed-c give-mb-no">Contact</h2>\n<a class="col-md-12 give-inlineblock give-mb-sm">epickard@pdx.edu</a>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c give-mb-no">Subjects</h2>\n<p>Anthropology, Geography, Geology</p>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Ask Librarian</h2>\n<ul class="col-md-12 secondary-text-list">\n<li><a target="_blank"><img src="main/images/ask1.png" alt="" /></a></li>\n<li><a target="_blank"><img src="main/images/ask2.png" alt="" /></a></li>\n<li><a target="_blank"><img src="main/images/ask3.png" alt="" /></a></li>\n<li><a target="_blank"><img src="main/images/ask4.png" alt="" /></a></li>\n</ul>\n</div>\n</div>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n\\</article>', '', ''),
(30, 15, 1, 'Elizabeth Pickard', 'elizabeth_pickard', '', '<article class="col-md-12 internal-post"><!--start of internal-post-->\n<h2 class="col-md-12 give-boldFamily give-red-c">Elizabeth Pickard</h2>\n<div class=" clearfix">\n<div class="col-md-6 secondary-image give-mb-med" style="background-image: url(''main/images/dummy-image.jpg'');">Â </div>\n<div class="col-md-6 secondary-text give-mb-med">\n<h2 class="col-md-12 give-boldFamily give-darkRed-c give-mb-no">Contact</h2>\n<a class="col-md-12 give-inlineblock give-mb-sm">epickard@pdx.edu</a>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c give-mb-no">Subjects</h2>\n<p>Anthropology, Geography, Geology</p>\n<h2 class="col-md-12 give-boldFamily give-darkRed-c">Ask Librarian</h2>\n<ul class="col-md-12 secondary-text-list">\n<li><a target="_blank"><img src="main/images/ask1.png" alt="" /></a></li>\n<li><a target="_blank"><img src="main/images/ask2.png" alt="" /></a></li>\n<li><a target="_blank"><img src="main/images/ask3.png" alt="" /></a></li>\n<li><a target="_blank"><img src="main/images/ask4.png" alt="" /></a></li>\n</ul>\n</div>\n</div>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n<p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking Dynamic Sun solar feed project on Thursday, February 11 at 7:00 p.m. at the Commons Wall in the James B. Hunt Jr. Library, where the high-definition, near-real time video image of the sun will be exhibited through March 13.</p>\n\\</article>', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_image_gallery`
--

CREATE TABLE IF NOT EXISTS `nodes_image_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('node','propertie') NOT NULL,
  `related_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `sort` varchar(5) NOT NULL,
  `caption` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `nodes_plugins_values`
--

CREATE TABLE IF NOT EXISTS `nodes_plugins_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('post','page','event') NOT NULL,
  `title` varchar(250) NOT NULL,
  `node_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `nodes_plugins_values`
--

INSERT INTO `nodes_plugins_values` (`id`, `type`, `title`, `node_id`, `plugin_id`, `lang_id`, `content`) VALUES
(1, 'post', 'test', 10, 44, 1, '5'),
(2, 'post', 'test', 10, 44, 2, '4'),
(3, 'post', 'Latest News', 10, 46, 1, '2'),
(4, 'post', 'Ø£Ø­Ø¯Ø« Ø§Ù„Ø£Ø®Ø¨Ø§Ø±', 10, 46, 2, '2'),
(5, 'post', '', 10, 45, 1, ''),
(6, 'post', '', 10, 45, 2, ''),
(7, 'post', 'test', 2, 44, 1, '1'),
(8, 'post', '', 2, 44, 2, '0'),
(9, 'post', 'rtyret', 2, 46, 1, '2'),
(10, 'post', 'tete', 2, 46, 2, '2'),
(11, 'post', '', 2, 45, 1, ''),
(12, 'post', '', 2, 45, 2, ''),
(13, 'post', 'Upcoming Events', 2, 47, 1, '12'),
(14, 'post', 'Ø§Ù„Ø£Ø­Ø¯Ø§Ø« Ø§Ù„Ù‚Ø§Ø¯Ù…Ø©', 2, 47, 2, '12'),
(15, 'event', 'Social Media', 11, 44, 1, '5'),
(16, 'event', '', 11, 44, 2, '0'),
(17, 'event', 'Upcoming Events', 11, 47, 1, '12'),
(18, 'event', '', 11, 47, 2, '0'),
(19, 'page', 'upcoming events ', 13, 47, 1, '12'),
(20, 'page', '', 13, 47, 2, '0'),
(21, 'page', 'Latest News', 13, 46, 1, '2'),
(22, 'page', '', 13, 46, 2, '0'),
(23, 'page', 'Important lINKS ', 13, 44, 1, '1'),
(24, 'page', '', 13, 44, 2, '0');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_selected_taxonomies`
--

CREATE TABLE IF NOT EXISTS `nodes_selected_taxonomies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxonomy_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `node_type` enum('post','page','event','faq','product') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `nodes_selected_taxonomies`
--

INSERT INTO `nodes_selected_taxonomies` (`id`, `taxonomy_id`, `node_id`, `taxonomy_type`, `node_type`) VALUES
(1, 2, 10, 'category', 'post'),
(2, 2, 4, 'category', 'post'),
(3, 2, 13, 'category', 'page');

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `version` varchar(50) NOT NULL,
  `author` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `source` varchar(150) NOT NULL,
  `enable_options` enum('yes','no') NOT NULL,
  `enable_translate` enum('yes','no') NOT NULL,
  `enable_event` enum('no','yes') NOT NULL,
  `enable_post` enum('no','yes') NOT NULL,
  `enable_page` enum('no','yes') NOT NULL,
  `enable_index` enum('no','yes') NOT NULL,
  `position` enum('left','right','no_position') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `description`, `version`, `author`, `uploaded_date`, `uploaded_by`, `source`, `enable_options`, `enable_translate`, `enable_event`, `enable_post`, `enable_page`, `enable_index`, `position`) VALUES
(1, 'Show Page details', 'this plug to Show Page details', '1.0', 'diva', '2015-07-29 20:21:57', 1, 'show_page_details', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(3, 'Show contact us page details', 'this plug to Show contact us Page details', '1.0', 'diva', '2015-07-31 13:16:26', 1, 'contact_us_page', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(7, 'Show post details', 'this plug to list to Show post details', '1.0', 'diva', '2015-08-12 15:24:19', 1, 'show_post_details', '', 'yes', 'no', 'yes', 'no', 'no', 'left'),
(34, 'Show event details', 'this plug to list to Show event details', '1.0', 'diva', '2016-01-24 17:07:36', 1, 'show_event_details', '', 'yes', 'yes', 'no', 'no', 'no', 'left'),
(35, 'posts list for page', 'posts_list_for_page', '1.0', 'diva', '2016-05-18 00:00:00', 1, 'posts_list_for_page', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(36, 'list events in brands page', 'this plug to list all brands in page', '1.0', 'diva', '2016-05-19 13:50:56', 1, 'page_list_brands', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(38, 'Home page latest database', 'this plug to list all database in home page', '1.0', 'diva', '2016-06-05 13:07:27', 1, 'home_page_latest_data_base', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(39, 'Home page latest database resourses', 'this plug to list all database  resourses in home page', '1.0', 'diva', '2016-06-05 13:07:33', 1, 'home_page_latest_data_base_resourses', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(40, 'Home page latest Courses', 'this plug to list all Courses in home page', '1.0', 'diva', '2016-06-05 13:07:41', 1, 'home_page_upcoming_courses', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(41, 'Sidebare latest news', 'this plug to latest news in side bar', '1.0', 'diva', '2016-06-05 13:07:55', 1, 'side_bar_latest_news', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(42, 'Sidebare menu links', 'this plug to sidebar menu links in sidebar', '1.0', 'diva', '2016-06-05 13:08:02', 1, 'side_bar_menu_links', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(43, 'sidebar  upcoming events', 'this plug to list all sidebar  upcoming events in side bar', '1.0', 'diva', '2016-06-05 13:08:11', 1, 'side_bar_upcoming_events', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(44, 'sidebar menu link in internal page', 'this plugin to view menu link in sidebar bin internal page', '1.0', 'diva', '2016-06-07 09:17:43', 1, 'sidebar_menu_link_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(45, 'sidebar text editor', 'this plug-in used to add free text in sidebar', '1.0', 'diva', '2016-06-07 09:50:35', 1, 'sidebar_texteditor', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(46, 'sidebar menu link in post by category page', 'this plugin to view menu link in sidebar bin internal post by category page', '1.0', 'diva', '2016-06-07 09:58:56', 1, 'side_bar_latest_posts_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(47, 'sidebar Upcoming events', 'this plugin to view Upcoming events', '1.0', 'diva', '2016-06-07 11:00:00', 1, 'side_bar_upcoming_events_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(48, 'list view events', 'this plug to list to view all events', '1.0', 'diva', '2016-06-07 12:12:33', 1, 'events_list_for_page', '', 'yes', 'no', 'no', 'yes', 'no', 'left');

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions`
--

CREATE TABLE IF NOT EXISTS `poll_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll` varchar(250) NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions_options`
--

CREATE TABLE IF NOT EXISTS `poll_questions_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `poll_option` varchar(250) NOT NULL,
  `option_counter` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL,
  `description` varchar(150) NOT NULL,
  `global_edit` enum('all_records','awn_record') NOT NULL,
  `global_delete` enum('all_records','awn_record') NOT NULL,
  `developer_mode` enum('yes','no') NOT NULL,
  `profile_block` enum('yes','no') NOT NULL,
  `post_publishing` enum('yes','no') NOT NULL,
  `page_publishing` enum('yes','no') NOT NULL,
  `event_publishing` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `title`, `description`, `global_edit`, `global_delete`, `developer_mode`, `profile_block`, `post_publishing`, `page_publishing`, `event_publishing`, `inserted_by`, `inserted_date`, `last_update`) VALUES
(1, 'access all', '', 'all_records', 'all_records', 'yes', 'no', 'yes', 'yes', 'yes', 1, '2014-05-04 10:42:08', '2016-05-11 17:02:32'),
(9, 'new admin', '', 'all_records', 'awn_record', 'no', 'yes', 'yes', 'yes', 'yes', 1, '2016-02-03 18:40:32', '2016-02-09 10:49:35'),
(10, 'admin', '', 'all_records', 'all_records', 'no', 'no', 'yes', 'yes', 'yes', 1, '2016-02-24 08:34:23', '0000-00-00 00:00:00'),
(11, 'Super Admin', '', 'awn_record', 'awn_record', 'no', 'no', 'no', 'no', 'no', 1, '2016-02-29 11:40:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profile_modules_access`
--

CREATE TABLE IF NOT EXISTS `profile_modules_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `profile_id` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=137 ;

--
-- Dumping data for table `profile_modules_access`
--

INSERT INTO `profile_modules_access` (`id`, `module_id`, `access`, `profile_id`, `inserted_by`, `inserted_date`) VALUES
(1, 5, 'yes', 1, 1, '2014-05-04 10:42:08'),
(2, 6, 'yes', 1, 1, '2014-05-04 10:42:08'),
(3, 7, 'yes', 1, 1, '2014-05-04 10:42:09'),
(6, 10, 'yes', 1, 1, '2014-05-04 10:42:09'),
(7, 11, 'yes', 1, 1, '2014-05-04 10:42:09'),
(8, 12, 'yes', 1, 1, '2014-05-04 10:42:09'),
(9, 33, 'yes', 1, 1, '2014-05-10 08:51:36'),
(33, 275, 'yes', 1, 1, '2015-07-01 05:16:22'),
(107, 6, 'yes', 9, 1, '2016-02-03 18:40:32'),
(108, 5, 'yes', 9, 1, '2016-02-03 18:40:32'),
(109, 7, 'yes', 9, 1, '2016-02-03 18:40:32'),
(112, 10, 'yes', 9, 1, '2016-02-03 18:40:32'),
(113, 33, 'no', 9, 1, '2016-02-03 18:40:32'),
(114, 11, 'no', 9, 1, '2016-02-03 18:40:32'),
(115, 12, 'no', 9, 1, '2016-02-03 18:40:32'),
(116, 275, 'no', 9, 1, '2016-02-03 18:40:32'),
(117, 6, 'yes', 10, 1, '2016-02-24 08:34:23'),
(118, 5, 'yes', 10, 1, '2016-02-24 08:34:23'),
(119, 7, 'yes', 10, 1, '2016-02-24 08:34:23'),
(122, 10, 'yes', 10, 1, '2016-02-24 08:34:23'),
(123, 33, 'no', 10, 1, '2016-02-24 08:34:23'),
(124, 11, 'yes', 10, 1, '2016-02-24 08:34:23'),
(125, 12, 'no', 10, 1, '2016-02-24 08:34:23'),
(126, 275, 'no', 10, 1, '2016-02-24 08:34:23'),
(127, 6, 'yes', 11, 1, '2016-02-29 11:40:27'),
(128, 5, 'yes', 11, 1, '2016-02-29 11:40:27'),
(129, 7, 'yes', 11, 1, '2016-02-29 11:40:27'),
(132, 10, 'yes', 11, 1, '2016-02-29 11:40:27'),
(133, 33, 'yes', 11, 1, '2016-02-29 11:40:27'),
(134, 11, 'yes', 11, 1, '2016-02-29 11:40:27'),
(135, 12, 'yes', 11, 1, '2016-02-29 11:40:27'),
(136, 275, 'yes', 11, 1, '2016-02-29 11:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pages_access`
--

CREATE TABLE IF NOT EXISTS `profile_pages_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2842 ;

--
-- Dumping data for table `profile_pages_access`
--

INSERT INTO `profile_pages_access` (`id`, `profile_id`, `module_id`, `page_id`, `access`, `inserted_by`, `inserted_date`) VALUES
(1, 1, 5, 13, 'yes', 1, '2014-05-04 10:42:09'),
(3, 1, 6, 16, 'yes', 1, '2014-05-04 10:42:09'),
(4, 1, 6, 17, 'yes', 1, '2014-05-04 10:42:09'),
(5, 1, 7, 29, 'yes', 1, '2014-05-04 10:42:09'),
(6, 1, 7, 30, 'yes', 1, '2014-05-04 10:42:09'),
(11, 1, 10, 22, 'yes', 1, '2014-05-04 10:42:09'),
(12, 1, 10, 23, 'yes', 1, '2014-05-04 10:42:09'),
(14, 1, 11, 25, 'yes', 1, '2014-05-04 10:42:09'),
(15, 1, 12, 26, 'yes', 1, '2014-05-04 10:42:09'),
(16, 1, 12, 27, 'yes', 1, '2014-05-04 10:42:09'),
(17, 1, 12, 28, 'yes', 1, '2014-05-04 10:42:09'),
(18, 1, 11, 31, 'yes', 1, '2014-05-04 10:42:09'),
(20, 1, 33, 34, 'yes', 1, '2014-05-10 08:56:40'),
(21, 1, 33, 35, 'yes', 1, '2014-05-10 08:57:56'),
(22, 1, 33, 36, 'yes', 1, '2014-05-10 08:58:46'),
(23, 1, 33, 37, 'yes', 1, '2014-05-10 02:54:51'),
(24, 1, 33, 38, 'no', 1, '2014-05-11 08:38:53'),
(49, 1, 12, 39, 'yes', 1, '2014-05-31 09:45:31'),
(51, 1, 275, 40, 'yes', 1, '2014-06-23 07:44:38'),
(53, 1, 275, 42, 'yes', 1, '2014-06-23 08:04:35'),
(237, 1, 12, 43, 'yes', 1, '2014-07-19 15:06:57'),
(238, 1, 12, 44, 'yes', 1, '2014-07-19 15:24:29'),
(239, 1, 12, 45, 'yes', 1, '2014-07-19 15:27:29'),
(241, 1, 12, 47, 'yes', 1, '2014-07-19 15:43:14'),
(242, 1, 12, 48, 'yes', 1, '2014-07-19 15:46:43'),
(244, 1, 12, 50, 'yes', 1, '2014-07-19 15:55:50'),
(246, 1, 12, 52, 'yes', 1, '2014-07-19 15:59:55'),
(247, 1, 12, 53, 'yes', 1, '2014-07-19 16:03:18'),
(248, 1, 12, 54, 'yes', 1, '2014-07-19 16:07:07'),
(250, 1, 11, 56, 'yes', 1, '2014-07-19 16:12:43'),
(252, 1, 11, 58, 'yes', 1, '2014-07-19 16:15:54'),
(258, 1, 10, 64, 'yes', 1, '2014-07-19 16:24:05'),
(259, 1, 10, 65, 'yes', 1, '2014-07-19 16:25:03'),
(264, 1, 6, 70, 'no', 1, '2014-07-19 16:31:33'),
(265, 1, 6, 71, 'yes', 1, '2014-07-19 16:32:05'),
(266, 1, 7, 72, 'yes', 1, '2014-07-19 16:33:37'),
(267, 1, 7, 73, 'yes', 1, '2014-07-19 16:33:54'),
(268, 1, 33, 74, 'yes', 1, '2014-07-19 16:36:05'),
(271, 1, 33, 77, 'yes', 1, '2014-07-19 16:47:13'),
(272, 1, 33, 78, 'yes', 1, '2014-07-19 16:47:49'),
(273, 1, 33, 79, 'yes', 1, '2014-07-19 16:48:28'),
(274, 1, 33, 80, 'yes', 1, '2014-07-19 16:51:47'),
(275, 1, 33, 81, 'yes', 1, '2014-07-19 16:53:27'),
(277, 1, 33, 83, 'yes', 1, '2014-07-19 16:56:50'),
(278, 1, 33, 84, 'yes', 1, '2014-07-19 16:58:58'),
(280, 1, 33, 86, 'yes', 1, '2014-07-19 17:09:19'),
(281, 1, 33, 87, 'no', 1, '2014-07-19 17:12:40'),
(285, 1, 6, 91, 'yes', 1, '2014-07-19 17:22:38'),
(286, 1, 7, 92, 'yes', 1, '2014-07-19 17:23:39'),
(287, 1, 10, 93, 'yes', 1, '2014-07-19 17:24:46'),
(288, 1, 11, 94, 'yes', 1, '2014-07-19 17:26:35'),
(289, 1, 11, 95, 'yes', 1, '2014-07-19 17:28:12'),
(290, 1, 11, 96, 'yes', 1, '2014-07-19 17:29:24'),
(291, 1, 12, 97, 'yes', 1, '2014-07-20 13:23:07'),
(292, 1, 7, 98, 'yes', 1, '2014-07-20 14:07:32'),
(293, 1, 7, 99, 'yes', 1, '2014-07-20 14:08:28'),
(295, 1, 7, 101, 'yes', 1, '2014-07-20 14:12:07'),
(296, 1, 7, 102, 'yes', 1, '2014-07-20 14:13:13'),
(297, 1, 7, 103, 'yes', 1, '2014-07-20 14:18:52'),
(301, 1, 12, 107, 'yes', 1, '2014-07-23 14:43:37'),
(302, 1, 12, 108, 'yes', 1, '2014-07-24 02:03:06'),
(303, 1, 33, 109, 'no', 1, '2014-07-27 19:28:05'),
(304, 1, 33, 110, 'yes', 1, '2014-07-27 19:43:43'),
(305, 1, 33, 111, 'yes', 1, '2014-07-27 20:19:35'),
(306, 1, 33, 112, 'yes', 1, '2014-07-31 10:45:13'),
(307, 1, 11, 113, 'yes', 1, '2014-07-31 10:47:42'),
(308, 1, 11, 114, 'yes', 1, '2014-07-31 10:49:27'),
(310, 1, 12, 116, 'no', 1, '2014-08-02 14:27:33'),
(311, 1, 12, 117, 'yes', 1, '2014-08-02 14:28:00'),
(312, 1, 12, 118, 'yes', 1, '2014-08-02 16:53:09'),
(313, 1, 5, 119, 'yes', 1, '2014-08-07 07:25:56'),
(314, 1, 5, 120, 'yes', 1, '2014-08-07 07:52:13'),
(315, 1, 5, 121, 'yes', 1, '2014-08-07 10:25:41'),
(316, 1, 12, 122, 'yes', 1, '2014-08-10 07:55:26'),
(321, 1, 10, 127, 'yes', 1, '2014-08-17 10:31:22'),
(323, 1, 7, 129, 'yes', 1, '2014-08-18 11:46:56'),
(325, 1, 10, 131, 'yes', 1, '2014-08-23 16:33:24'),
(326, 1, 33, 132, 'yes', 1, '2014-09-17 11:04:43'),
(327, 1, 33, 133, 'yes', 1, '2014-09-17 11:06:57'),
(328, 1, 33, 134, 'yes', 1, '2014-09-17 11:07:19'),
(329, 1, 33, 135, 'yes', 1, '2014-09-17 11:07:50'),
(330, 1, 33, 136, 'yes', 1, '2014-09-17 11:08:18'),
(331, 1, 33, 137, 'yes', 1, '2014-09-17 11:10:05'),
(332, 1, 33, 138, 'yes', 1, '2014-09-17 11:10:25'),
(333, 1, 33, 139, 'yes', 1, '2014-09-17 11:10:55'),
(334, 1, 33, 140, 'yes', 1, '2014-09-17 11:11:19'),
(335, 1, 33, 141, 'yes', 1, '2014-09-17 11:11:46'),
(336, 1, 33, 142, 'yes', 1, '2014-09-17 11:46:09'),
(337, 1, 33, 143, 'yes', 1, '2014-09-17 11:46:49'),
(338, 1, 33, 144, 'yes', 1, '2014-09-17 11:47:16'),
(339, 1, 33, 145, 'yes', 1, '2014-09-17 11:47:41'),
(340, 1, 33, 146, 'yes', 1, '2014-09-17 11:47:59'),
(341, 1, 33, 149, 'yes', 1, '2014-09-20 02:09:15'),
(342, 1, 33, 150, 'yes', 1, '2014-09-20 02:10:19'),
(343, 1, 33, 152, 'yes', 1, '2014-09-20 11:12:36'),
(344, 1, 33, 153, 'yes', 1, '2014-09-20 17:12:38'),
(345, 1, 5, 154, 'yes', 1, '2014-09-21 00:01:05'),
(540, 1, 12, 229, 'yes', 1, '2015-03-30 08:00:31'),
(546, 1, 12, 235, 'yes', 1, '2015-03-31 14:02:26'),
(548, 1, 12, 237, 'yes', 1, '2015-03-31 14:04:06'),
(549, 1, 12, 238, 'yes', 1, '2015-03-31 14:06:46'),
(565, 1, 12, 254, 'yes', 1, '2015-04-06 18:30:02'),
(566, 1, 12, 255, 'yes', 1, '2015-04-06 18:30:32'),
(567, 1, 12, 256, 'yes', 1, '2015-04-06 18:30:57'),
(568, 1, 12, 257, 'yes', 1, '2015-04-06 18:31:36'),
(569, 1, 12, 258, 'yes', 1, '2015-04-06 18:32:04'),
(784, 1, 33, 267, 'yes', 1, '2015-05-24 16:14:17'),
(786, 1, 33, 268, 'yes', 1, '2015-05-24 16:14:45'),
(2401, 9, 6, 16, 'yes', 1, '2016-02-03 18:40:32'),
(2402, 9, 6, 17, 'yes', 1, '2016-02-03 18:40:32'),
(2403, 9, 6, 70, 'yes', 1, '2016-02-03 18:40:32'),
(2404, 9, 6, 91, 'yes', 1, '2016-02-03 18:40:32'),
(2405, 9, 6, 71, 'yes', 1, '2016-02-03 18:40:32'),
(2406, 9, 5, 13, 'yes', 1, '2016-02-03 18:40:32'),
(2407, 9, 5, 119, 'yes', 1, '2016-02-03 18:40:32'),
(2408, 9, 5, 154, 'yes', 1, '2016-02-03 18:40:32'),
(2409, 9, 5, 120, 'yes', 1, '2016-02-03 18:40:32'),
(2410, 9, 5, 121, 'yes', 1, '2016-02-03 18:40:32'),
(2411, 9, 7, 29, 'yes', 1, '2016-02-03 18:40:32'),
(2412, 9, 7, 30, 'yes', 1, '2016-02-03 18:40:32'),
(2413, 9, 7, 72, 'yes', 1, '2016-02-03 18:40:32'),
(2414, 9, 7, 92, 'yes', 1, '2016-02-03 18:40:32'),
(2415, 9, 7, 73, 'yes', 1, '2016-02-03 18:40:32'),
(2416, 9, 7, 98, 'yes', 1, '2016-02-03 18:40:32'),
(2417, 9, 7, 99, 'yes', 1, '2016-02-03 18:40:32'),
(2418, 9, 7, 101, 'yes', 1, '2016-02-03 18:40:32'),
(2419, 9, 7, 103, 'yes', 1, '2016-02-03 18:40:32'),
(2420, 9, 7, 102, 'yes', 1, '2016-02-03 18:40:32'),
(2421, 9, 7, 129, 'yes', 1, '2016-02-03 18:40:32'),
(2436, 9, 10, 22, 'yes', 1, '2016-02-03 18:40:32'),
(2437, 9, 10, 23, 'yes', 1, '2016-02-03 18:40:32'),
(2438, 9, 10, 64, 'yes', 1, '2016-02-03 18:40:32'),
(2439, 9, 10, 127, 'yes', 1, '2016-02-03 18:40:32'),
(2440, 9, 10, 93, 'yes', 1, '2016-02-03 18:40:32'),
(2441, 9, 10, 65, 'yes', 1, '2016-02-03 18:40:32'),
(2442, 9, 10, 131, 'yes', 1, '2016-02-03 18:40:32'),
(2443, 9, 33, 34, 'no', 1, '2016-02-03 18:40:32'),
(2444, 9, 33, 74, 'no', 1, '2016-02-03 18:40:32'),
(2445, 9, 33, 132, 'no', 1, '2016-02-03 18:40:32'),
(2446, 9, 33, 110, 'no', 1, '2016-02-03 18:40:32'),
(2447, 9, 33, 112, 'no', 1, '2016-02-03 18:40:32'),
(2448, 9, 33, 152, 'no', 1, '2016-02-03 18:40:32'),
(2449, 9, 33, 35, 'no', 1, '2016-02-03 18:40:32'),
(2450, 9, 33, 153, 'no', 1, '2016-02-03 18:40:32'),
(2451, 9, 33, 77, 'no', 1, '2016-02-03 18:40:32'),
(2452, 9, 33, 78, 'no', 1, '2016-02-03 18:40:32'),
(2453, 9, 33, 79, 'no', 1, '2016-02-03 18:40:32'),
(2454, 9, 33, 80, 'no', 1, '2016-02-03 18:40:32'),
(2455, 9, 33, 36, 'no', 1, '2016-02-03 18:40:32'),
(2456, 9, 33, 81, 'no', 1, '2016-02-03 18:40:32'),
(2457, 9, 33, 83, 'no', 1, '2016-02-03 18:40:32'),
(2458, 9, 33, 84, 'no', 1, '2016-02-03 18:40:32'),
(2459, 9, 33, 86, 'no', 1, '2016-02-03 18:40:32'),
(2460, 9, 33, 37, 'no', 1, '2016-02-03 18:40:32'),
(2461, 9, 33, 111, 'no', 1, '2016-02-03 18:40:32'),
(2462, 9, 33, 38, 'no', 1, '2016-02-03 18:40:32'),
(2463, 9, 33, 109, 'no', 1, '2016-02-03 18:40:32'),
(2464, 9, 33, 87, 'no', 1, '2016-02-03 18:40:32'),
(2465, 9, 33, 133, 'no', 1, '2016-02-03 18:40:32'),
(2466, 9, 33, 134, 'no', 1, '2016-02-03 18:40:32'),
(2467, 9, 33, 135, 'no', 1, '2016-02-03 18:40:32'),
(2468, 9, 33, 136, 'no', 1, '2016-02-03 18:40:32'),
(2469, 9, 33, 137, 'no', 1, '2016-02-03 18:40:32'),
(2470, 9, 33, 138, 'no', 1, '2016-02-03 18:40:32'),
(2471, 9, 33, 139, 'no', 1, '2016-02-03 18:40:32'),
(2472, 9, 33, 140, 'no', 1, '2016-02-03 18:40:32'),
(2473, 9, 33, 141, 'no', 1, '2016-02-03 18:40:32'),
(2474, 9, 33, 142, 'no', 1, '2016-02-03 18:40:32'),
(2475, 9, 33, 143, 'no', 1, '2016-02-03 18:40:32'),
(2476, 9, 33, 144, 'no', 1, '2016-02-03 18:40:32'),
(2477, 9, 33, 145, 'no', 1, '2016-02-03 18:40:32'),
(2478, 9, 33, 146, 'no', 1, '2016-02-03 18:40:32'),
(2479, 9, 33, 149, 'no', 1, '2016-02-03 18:40:32'),
(2480, 9, 33, 150, 'no', 1, '2016-02-03 18:40:32'),
(2481, 9, 33, 151, 'no', 1, '2016-02-03 18:40:32'),
(2482, 9, 33, 267, 'no', 1, '2016-02-03 18:40:32'),
(2483, 9, 33, 268, 'no', 1, '2016-02-03 18:40:32'),
(2486, 9, 11, 56, 'no', 1, '2016-02-03 18:40:32'),
(2487, 9, 11, 94, 'no', 1, '2016-02-03 18:40:32'),
(2488, 9, 11, 113, 'no', 1, '2016-02-03 18:40:32'),
(2489, 9, 11, 25, 'no', 1, '2016-02-03 18:40:32'),
(2490, 9, 11, 58, 'no', 1, '2016-02-03 18:40:32'),
(2492, 9, 11, 95, 'no', 1, '2016-02-03 18:40:32'),
(2493, 9, 11, 114, 'no', 1, '2016-02-03 18:40:32'),
(2494, 9, 11, 31, 'no', 1, '2016-02-03 18:40:32'),
(2497, 9, 11, 96, 'no', 1, '2016-02-03 18:40:32'),
(2503, 9, 12, 43, 'no', 1, '2016-02-03 18:40:32'),
(2504, 9, 12, 39, 'no', 1, '2016-02-03 18:40:32'),
(2505, 9, 12, 26, 'no', 1, '2016-02-03 18:40:32'),
(2506, 9, 12, 44, 'no', 1, '2016-02-03 18:40:32'),
(2507, 9, 12, 45, 'no', 1, '2016-02-03 18:40:32'),
(2508, 9, 12, 97, 'no', 1, '2016-02-03 18:40:32'),
(2509, 9, 12, 108, 'no', 1, '2016-02-03 18:40:32'),
(2513, 9, 12, 229, 'no', 1, '2016-02-03 18:40:32'),
(2514, 9, 12, 47, 'no', 1, '2016-02-03 18:40:32'),
(2515, 9, 12, 48, 'no', 1, '2016-02-03 18:40:32'),
(2517, 9, 12, 235, 'no', 1, '2016-02-03 18:40:32'),
(2518, 9, 12, 237, 'no', 1, '2016-02-03 18:40:32'),
(2519, 9, 12, 238, 'no', 1, '2016-02-03 18:40:32'),
(2520, 9, 12, 118, 'no', 1, '2016-02-03 18:40:32'),
(2521, 9, 12, 28, 'no', 1, '2016-02-03 18:40:32'),
(2522, 9, 12, 116, 'no', 1, '2016-02-03 18:40:32'),
(2523, 9, 12, 117, 'no', 1, '2016-02-03 18:40:32'),
(2524, 9, 12, 27, 'no', 1, '2016-02-03 18:40:32'),
(2525, 9, 12, 50, 'no', 1, '2016-02-03 18:40:32'),
(2526, 9, 12, 52, 'no', 1, '2016-02-03 18:40:32'),
(2527, 9, 12, 53, 'no', 1, '2016-02-03 18:40:32'),
(2528, 9, 12, 54, 'no', 1, '2016-02-03 18:40:32'),
(2529, 9, 12, 122, 'no', 1, '2016-02-03 18:40:32'),
(2530, 9, 12, 107, 'no', 1, '2016-02-03 18:40:32'),
(2533, 9, 12, 254, 'no', 1, '2016-02-03 18:40:32'),
(2534, 9, 12, 256, 'no', 1, '2016-02-03 18:40:32'),
(2535, 9, 12, 257, 'no', 1, '2016-02-03 18:40:32'),
(2536, 9, 12, 258, 'no', 1, '2016-02-03 18:40:32'),
(2537, 9, 12, 255, 'no', 1, '2016-02-03 18:40:32'),
(2538, 9, 275, 40, 'no', 1, '2016-02-03 18:40:32'),
(2539, 9, 275, 42, 'no', 1, '2016-02-03 18:40:32'),
(2540, 10, 6, 16, 'yes', 1, '2016-02-24 08:34:23'),
(2541, 10, 6, 17, 'yes', 1, '2016-02-24 08:34:23'),
(2542, 10, 6, 70, 'yes', 1, '2016-02-24 08:34:23'),
(2543, 10, 6, 91, 'yes', 1, '2016-02-24 08:34:23'),
(2544, 10, 6, 71, 'yes', 1, '2016-02-24 08:34:23'),
(2545, 10, 5, 13, 'yes', 1, '2016-02-24 08:34:23'),
(2546, 10, 5, 119, 'yes', 1, '2016-02-24 08:34:23'),
(2547, 10, 5, 154, 'yes', 1, '2016-02-24 08:34:23'),
(2548, 10, 5, 120, 'yes', 1, '2016-02-24 08:34:23'),
(2549, 10, 5, 121, 'yes', 1, '2016-02-24 08:34:23'),
(2550, 10, 7, 29, 'yes', 1, '2016-02-24 08:34:23'),
(2551, 10, 7, 30, 'yes', 1, '2016-02-24 08:34:23'),
(2552, 10, 7, 72, 'yes', 1, '2016-02-24 08:34:23'),
(2553, 10, 7, 92, 'yes', 1, '2016-02-24 08:34:23'),
(2554, 10, 7, 73, 'yes', 1, '2016-02-24 08:34:23'),
(2555, 10, 7, 98, 'yes', 1, '2016-02-24 08:34:23'),
(2556, 10, 7, 99, 'yes', 1, '2016-02-24 08:34:23'),
(2557, 10, 7, 101, 'yes', 1, '2016-02-24 08:34:23'),
(2558, 10, 7, 103, 'yes', 1, '2016-02-24 08:34:23'),
(2559, 10, 7, 102, 'yes', 1, '2016-02-24 08:34:23'),
(2560, 10, 7, 129, 'yes', 1, '2016-02-24 08:34:23'),
(2575, 10, 10, 22, 'yes', 1, '2016-02-24 08:34:23'),
(2576, 10, 10, 23, 'yes', 1, '2016-02-24 08:34:23'),
(2577, 10, 10, 64, 'yes', 1, '2016-02-24 08:34:23'),
(2578, 10, 10, 127, 'yes', 1, '2016-02-24 08:34:23'),
(2579, 10, 10, 93, 'yes', 1, '2016-02-24 08:34:23'),
(2580, 10, 10, 65, 'yes', 1, '2016-02-24 08:34:23'),
(2581, 10, 10, 131, 'yes', 1, '2016-02-24 08:34:23'),
(2582, 10, 33, 34, 'no', 1, '2016-02-24 08:34:23'),
(2583, 10, 33, 74, 'no', 1, '2016-02-24 08:34:23'),
(2584, 10, 33, 132, 'no', 1, '2016-02-24 08:34:23'),
(2585, 10, 33, 110, 'no', 1, '2016-02-24 08:34:23'),
(2586, 10, 33, 112, 'no', 1, '2016-02-24 08:34:23'),
(2587, 10, 33, 152, 'no', 1, '2016-02-24 08:34:23'),
(2588, 10, 33, 35, 'no', 1, '2016-02-24 08:34:23'),
(2589, 10, 33, 153, 'no', 1, '2016-02-24 08:34:23'),
(2590, 10, 33, 77, 'no', 1, '2016-02-24 08:34:23'),
(2591, 10, 33, 78, 'no', 1, '2016-02-24 08:34:23'),
(2592, 10, 33, 79, 'no', 1, '2016-02-24 08:34:23'),
(2593, 10, 33, 80, 'no', 1, '2016-02-24 08:34:23'),
(2594, 10, 33, 36, 'no', 1, '2016-02-24 08:34:23'),
(2595, 10, 33, 81, 'no', 1, '2016-02-24 08:34:23'),
(2596, 10, 33, 83, 'no', 1, '2016-02-24 08:34:23'),
(2597, 10, 33, 84, 'no', 1, '2016-02-24 08:34:23'),
(2598, 10, 33, 86, 'no', 1, '2016-02-24 08:34:23'),
(2599, 10, 33, 37, 'no', 1, '2016-02-24 08:34:23'),
(2600, 10, 33, 111, 'no', 1, '2016-02-24 08:34:23'),
(2601, 10, 33, 38, 'no', 1, '2016-02-24 08:34:23'),
(2602, 10, 33, 109, 'no', 1, '2016-02-24 08:34:23'),
(2603, 10, 33, 87, 'no', 1, '2016-02-24 08:34:23'),
(2604, 10, 33, 133, 'no', 1, '2016-02-24 08:34:23'),
(2605, 10, 33, 134, 'no', 1, '2016-02-24 08:34:23'),
(2606, 10, 33, 135, 'no', 1, '2016-02-24 08:34:23'),
(2607, 10, 33, 136, 'no', 1, '2016-02-24 08:34:23'),
(2608, 10, 33, 137, 'no', 1, '2016-02-24 08:34:23'),
(2609, 10, 33, 138, 'no', 1, '2016-02-24 08:34:23'),
(2610, 10, 33, 139, 'no', 1, '2016-02-24 08:34:23'),
(2611, 10, 33, 140, 'no', 1, '2016-02-24 08:34:23'),
(2612, 10, 33, 141, 'no', 1, '2016-02-24 08:34:23'),
(2613, 10, 33, 142, 'no', 1, '2016-02-24 08:34:23'),
(2614, 10, 33, 143, 'no', 1, '2016-02-24 08:34:23'),
(2615, 10, 33, 144, 'no', 1, '2016-02-24 08:34:23'),
(2616, 10, 33, 145, 'no', 1, '2016-02-24 08:34:23'),
(2617, 10, 33, 146, 'no', 1, '2016-02-24 08:34:23'),
(2618, 10, 33, 149, 'no', 1, '2016-02-24 08:34:23'),
(2619, 10, 33, 150, 'no', 1, '2016-02-24 08:34:23'),
(2620, 10, 33, 151, 'no', 1, '2016-02-24 08:34:23'),
(2621, 10, 33, 267, 'no', 1, '2016-02-24 08:34:23'),
(2622, 10, 33, 268, 'no', 1, '2016-02-24 08:34:23'),
(2625, 10, 11, 56, 'yes', 1, '2016-02-24 08:34:23'),
(2626, 10, 11, 94, 'yes', 1, '2016-02-24 08:34:23'),
(2627, 10, 11, 113, 'yes', 1, '2016-02-24 08:34:23'),
(2628, 10, 11, 25, 'yes', 1, '2016-02-24 08:34:23'),
(2629, 10, 11, 58, 'yes', 1, '2016-02-24 08:34:23'),
(2631, 10, 11, 95, 'yes', 1, '2016-02-24 08:34:23'),
(2632, 10, 11, 114, 'yes', 1, '2016-02-24 08:34:23'),
(2633, 10, 11, 31, 'yes', 1, '2016-02-24 08:34:23'),
(2636, 10, 11, 96, 'yes', 1, '2016-02-24 08:34:23'),
(2642, 10, 12, 43, 'no', 1, '2016-02-24 08:34:23'),
(2643, 10, 12, 39, 'no', 1, '2016-02-24 08:34:23'),
(2644, 10, 12, 26, 'no', 1, '2016-02-24 08:34:23'),
(2645, 10, 12, 44, 'no', 1, '2016-02-24 08:34:23'),
(2646, 10, 12, 45, 'no', 1, '2016-02-24 08:34:23'),
(2647, 10, 12, 97, 'no', 1, '2016-02-24 08:34:23'),
(2648, 10, 12, 108, 'no', 1, '2016-02-24 08:34:23'),
(2652, 10, 12, 229, 'no', 1, '2016-02-24 08:34:23'),
(2653, 10, 12, 47, 'no', 1, '2016-02-24 08:34:23'),
(2654, 10, 12, 48, 'no', 1, '2016-02-24 08:34:23'),
(2656, 10, 12, 235, 'no', 1, '2016-02-24 08:34:23'),
(2657, 10, 12, 237, 'no', 1, '2016-02-24 08:34:23'),
(2658, 10, 12, 238, 'no', 1, '2016-02-24 08:34:23'),
(2659, 10, 12, 118, 'no', 1, '2016-02-24 08:34:23'),
(2660, 10, 12, 28, 'no', 1, '2016-02-24 08:34:23'),
(2661, 10, 12, 116, 'no', 1, '2016-02-24 08:34:23'),
(2662, 10, 12, 117, 'no', 1, '2016-02-24 08:34:23'),
(2663, 10, 12, 27, 'no', 1, '2016-02-24 08:34:23'),
(2664, 10, 12, 50, 'no', 1, '2016-02-24 08:34:23'),
(2665, 10, 12, 52, 'no', 1, '2016-02-24 08:34:23'),
(2666, 10, 12, 53, 'no', 1, '2016-02-24 08:34:23'),
(2667, 10, 12, 54, 'no', 1, '2016-02-24 08:34:23'),
(2668, 10, 12, 122, 'no', 1, '2016-02-24 08:34:23'),
(2669, 10, 12, 107, 'no', 1, '2016-02-24 08:34:23'),
(2672, 10, 12, 254, 'no', 1, '2016-02-24 08:34:23'),
(2673, 10, 12, 256, 'no', 1, '2016-02-24 08:34:23'),
(2674, 10, 12, 257, 'no', 1, '2016-02-24 08:34:23'),
(2675, 10, 12, 258, 'no', 1, '2016-02-24 08:34:23'),
(2676, 10, 12, 255, 'no', 1, '2016-02-24 08:34:23'),
(2677, 10, 275, 40, 'no', 1, '2016-02-24 08:34:23'),
(2678, 10, 275, 42, 'no', 1, '2016-02-24 08:34:23'),
(2679, 11, 6, 16, 'yes', 1, '2016-02-29 11:40:27'),
(2680, 11, 6, 17, 'yes', 1, '2016-02-29 11:40:27'),
(2681, 11, 6, 70, 'yes', 1, '2016-02-29 11:40:27'),
(2682, 11, 6, 91, 'yes', 1, '2016-02-29 11:40:27'),
(2683, 11, 6, 71, 'yes', 1, '2016-02-29 11:40:27'),
(2684, 11, 5, 13, 'yes', 1, '2016-02-29 11:40:27'),
(2685, 11, 5, 119, 'yes', 1, '2016-02-29 11:40:27'),
(2686, 11, 5, 154, 'yes', 1, '2016-02-29 11:40:27'),
(2687, 11, 5, 120, 'yes', 1, '2016-02-29 11:40:27'),
(2688, 11, 5, 121, 'yes', 1, '2016-02-29 11:40:27'),
(2689, 11, 7, 29, 'yes', 1, '2016-02-29 11:40:27'),
(2690, 11, 7, 30, 'yes', 1, '2016-02-29 11:40:27'),
(2691, 11, 7, 72, 'yes', 1, '2016-02-29 11:40:27'),
(2692, 11, 7, 92, 'yes', 1, '2016-02-29 11:40:27'),
(2693, 11, 7, 73, 'yes', 1, '2016-02-29 11:40:27'),
(2694, 11, 7, 98, 'yes', 1, '2016-02-29 11:40:27'),
(2695, 11, 7, 99, 'yes', 1, '2016-02-29 11:40:27'),
(2696, 11, 7, 101, 'yes', 1, '2016-02-29 11:40:27'),
(2697, 11, 7, 103, 'yes', 1, '2016-02-29 11:40:27'),
(2698, 11, 7, 102, 'yes', 1, '2016-02-29 11:40:27'),
(2699, 11, 7, 129, 'yes', 1, '2016-02-29 11:40:27'),
(2714, 11, 10, 22, 'yes', 1, '2016-02-29 11:40:27'),
(2715, 11, 10, 23, 'yes', 1, '2016-02-29 11:40:27'),
(2716, 11, 10, 64, 'yes', 1, '2016-02-29 11:40:27'),
(2717, 11, 10, 127, 'yes', 1, '2016-02-29 11:40:27'),
(2718, 11, 10, 93, 'yes', 1, '2016-02-29 11:40:27'),
(2719, 11, 10, 65, 'yes', 1, '2016-02-29 11:40:27'),
(2720, 11, 10, 131, 'yes', 1, '2016-02-29 11:40:27'),
(2721, 11, 33, 34, 'yes', 1, '2016-02-29 11:40:27'),
(2722, 11, 33, 74, 'yes', 1, '2016-02-29 11:40:27'),
(2723, 11, 33, 132, 'yes', 1, '2016-02-29 11:40:27'),
(2724, 11, 33, 110, 'yes', 1, '2016-02-29 11:40:27'),
(2725, 11, 33, 112, 'yes', 1, '2016-02-29 11:40:27'),
(2726, 11, 33, 152, 'yes', 1, '2016-02-29 11:40:27'),
(2727, 11, 33, 35, 'yes', 1, '2016-02-29 11:40:27'),
(2728, 11, 33, 153, 'yes', 1, '2016-02-29 11:40:27'),
(2729, 11, 33, 77, 'yes', 1, '2016-02-29 11:40:27'),
(2730, 11, 33, 78, 'yes', 1, '2016-02-29 11:40:27'),
(2731, 11, 33, 79, 'yes', 1, '2016-02-29 11:40:27'),
(2732, 11, 33, 80, 'yes', 1, '2016-02-29 11:40:27'),
(2733, 11, 33, 36, 'yes', 1, '2016-02-29 11:40:27'),
(2734, 11, 33, 81, 'yes', 1, '2016-02-29 11:40:27'),
(2735, 11, 33, 83, 'yes', 1, '2016-02-29 11:40:27'),
(2736, 11, 33, 84, 'yes', 1, '2016-02-29 11:40:27'),
(2737, 11, 33, 86, 'yes', 1, '2016-02-29 11:40:27'),
(2738, 11, 33, 37, 'yes', 1, '2016-02-29 11:40:27'),
(2739, 11, 33, 111, 'yes', 1, '2016-02-29 11:40:27'),
(2740, 11, 33, 38, 'yes', 1, '2016-02-29 11:40:27'),
(2741, 11, 33, 109, 'yes', 1, '2016-02-29 11:40:27'),
(2742, 11, 33, 87, 'yes', 1, '2016-02-29 11:40:27'),
(2743, 11, 33, 133, 'yes', 1, '2016-02-29 11:40:27'),
(2744, 11, 33, 134, 'yes', 1, '2016-02-29 11:40:27'),
(2745, 11, 33, 135, 'yes', 1, '2016-02-29 11:40:27'),
(2746, 11, 33, 136, 'yes', 1, '2016-02-29 11:40:27'),
(2747, 11, 33, 137, 'yes', 1, '2016-02-29 11:40:27'),
(2748, 11, 33, 138, 'yes', 1, '2016-02-29 11:40:27'),
(2749, 11, 33, 139, 'yes', 1, '2016-02-29 11:40:27'),
(2750, 11, 33, 140, 'yes', 1, '2016-02-29 11:40:27'),
(2751, 11, 33, 141, 'yes', 1, '2016-02-29 11:40:27'),
(2752, 11, 33, 142, 'yes', 1, '2016-02-29 11:40:27'),
(2753, 11, 33, 143, 'yes', 1, '2016-02-29 11:40:27'),
(2754, 11, 33, 144, 'yes', 1, '2016-02-29 11:40:27'),
(2755, 11, 33, 145, 'yes', 1, '2016-02-29 11:40:27'),
(2756, 11, 33, 146, 'yes', 1, '2016-02-29 11:40:27'),
(2757, 11, 33, 149, 'yes', 1, '2016-02-29 11:40:27'),
(2758, 11, 33, 150, 'yes', 1, '2016-02-29 11:40:27'),
(2759, 11, 33, 151, 'yes', 1, '2016-02-29 11:40:27'),
(2760, 11, 33, 267, 'yes', 1, '2016-02-29 11:40:27'),
(2761, 11, 33, 268, 'yes', 1, '2016-02-29 11:40:27'),
(2764, 11, 11, 56, 'yes', 1, '2016-02-29 11:40:27'),
(2765, 11, 11, 94, 'yes', 1, '2016-02-29 11:40:27'),
(2766, 11, 11, 113, 'yes', 1, '2016-02-29 11:40:27'),
(2767, 11, 11, 25, 'yes', 1, '2016-02-29 11:40:27'),
(2768, 11, 11, 58, 'yes', 1, '2016-02-29 11:40:27'),
(2770, 11, 11, 95, 'yes', 1, '2016-02-29 11:40:27'),
(2771, 11, 11, 114, 'yes', 1, '2016-02-29 11:40:27'),
(2772, 11, 11, 31, 'yes', 1, '2016-02-29 11:40:27'),
(2775, 11, 11, 96, 'yes', 1, '2016-02-29 11:40:27'),
(2781, 11, 12, 43, 'yes', 1, '2016-02-29 11:40:27'),
(2782, 11, 12, 39, 'yes', 1, '2016-02-29 11:40:27'),
(2783, 11, 12, 26, 'yes', 1, '2016-02-29 11:40:27'),
(2784, 11, 12, 44, 'yes', 1, '2016-02-29 11:40:27'),
(2785, 11, 12, 45, 'yes', 1, '2016-02-29 11:40:27'),
(2786, 11, 12, 97, 'yes', 1, '2016-02-29 11:40:27'),
(2787, 11, 12, 108, 'yes', 1, '2016-02-29 11:40:27'),
(2791, 11, 12, 229, 'yes', 1, '2016-02-29 11:40:27'),
(2792, 11, 12, 47, 'yes', 1, '2016-02-29 11:40:27'),
(2793, 11, 12, 48, 'yes', 1, '2016-02-29 11:40:27'),
(2795, 11, 12, 235, 'yes', 1, '2016-02-29 11:40:27'),
(2796, 11, 12, 237, 'yes', 1, '2016-02-29 11:40:27'),
(2797, 11, 12, 238, 'yes', 1, '2016-02-29 11:40:27'),
(2798, 11, 12, 118, 'yes', 1, '2016-02-29 11:40:27'),
(2799, 11, 12, 28, 'yes', 1, '2016-02-29 11:40:27'),
(2800, 11, 12, 116, 'yes', 1, '2016-02-29 11:40:27'),
(2801, 11, 12, 117, 'yes', 1, '2016-02-29 11:40:27'),
(2802, 11, 12, 27, 'yes', 1, '2016-02-29 11:40:27'),
(2803, 11, 12, 50, 'yes', 1, '2016-02-29 11:40:27'),
(2804, 11, 12, 52, 'yes', 1, '2016-02-29 11:40:27'),
(2805, 11, 12, 53, 'yes', 1, '2016-02-29 11:40:27'),
(2806, 11, 12, 54, 'yes', 1, '2016-02-29 11:40:27'),
(2807, 11, 12, 122, 'yes', 1, '2016-02-29 11:40:27'),
(2808, 11, 12, 107, 'yes', 1, '2016-02-29 11:40:27'),
(2811, 11, 12, 254, 'yes', 1, '2016-02-29 11:40:27'),
(2812, 11, 12, 256, 'yes', 1, '2016-02-29 11:40:27'),
(2813, 11, 12, 257, 'yes', 1, '2016-02-29 11:40:27'),
(2814, 11, 12, 258, 'yes', 1, '2016-02-29 11:40:27'),
(2815, 11, 12, 255, 'yes', 1, '2016-02-29 11:40:27'),
(2816, 11, 275, 40, 'yes', 1, '2016-02-29 11:40:27'),
(2817, 11, 275, 42, 'yes', 1, '2016-02-29 11:40:27'),
(2838, 1, 10, 282, 'yes', 1, '2016-05-31 13:38:52'),
(2839, 9, 10, 282, 'no', 1, '2016-05-31 13:38:52'),
(2840, 10, 10, 282, 'no', 1, '2016-05-31 13:38:52'),
(2841, 11, 10, 282, 'no', 1, '2016-05-31 13:38:52');

-- --------------------------------------------------------

--
-- Table structure for table `social_comments`
--

CREATE TABLE IF NOT EXISTS `social_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `email` varchar(256) NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `social_email_subscription`
--

CREATE TABLE IF NOT EXISTS `social_email_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) NOT NULL,
  `email` varchar(265) NOT NULL,
  `website` varchar(250) NOT NULL,
  `message` longtext NOT NULL,
  `inserted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_group`
--

CREATE TABLE IF NOT EXISTS `structure_menu_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `image` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `structure_menu_group`
--

INSERT INTO `structure_menu_group` (`id`, `title`, `alias`, `image`, `description`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 'Main Menu', 'main_menu', '', '', 1, '2016-06-05 15:45:29', 0, '0000-00-00 00:00:00'),
(2, 'Footer menu one', 'footer_menu_one', '', '', 1, '2016-06-05 15:46:00', 0, '0000-00-00 00:00:00'),
(3, 'Footer menu two', 'footer_menu_two', '', '', 1, '2016-06-05 15:46:14', 0, '0000-00-00 00:00:00'),
(4, 'Footer menu three', 'footer_menu_three', '', '', 1, '2016-06-05 15:46:26', 0, '0000-00-00 00:00:00'),
(5, 'social menu', 'social_menu', '', '', 1, '2016-06-05 16:11:37', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link`
--

CREATE TABLE IF NOT EXISTS `structure_menu_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `path_type` enum('post','external','page','event','category') NOT NULL,
  `path` int(11) DEFAULT NULL,
  `external_path` varchar(250) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `icon` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `drop_down` enum('yes','no') NOT NULL,
  `drop_down_style` enum('op1','op2','op3') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `structure_menu_link`
--

INSERT INTO `structure_menu_link` (`id`, `parent_id`, `group_id`, `sorting`, `path_type`, `path`, `external_path`, `status`, `icon`, `image`, `drop_down`, `drop_down_style`, `inserted_by`, `inserted_date`, `update_by`, `last_update`, `description`) VALUES
(1, 0, 1, 1, 'post', 2, '', 'publish', '', '../', '', '', 1, '2016-06-05 15:53:46', 1, '2016-06-05 15:54:56', ''),
(2, 0, 5, 0, 'external', 0, '', 'publish', 'facebook', '../', '', '', 1, '2016-06-05 16:11:48', 1, '2016-06-07 11:38:41', ''),
(3, 0, 5, 2, 'external', 0, '#', 'publish', 'twitter', '../', '', '', 1, '2016-06-05 16:12:13', 1, '2016-06-05 16:18:18', ''),
(4, 0, 5, 3, 'external', 0, '#', 'publish', 'youtube', '../', '', '', 1, '2016-06-05 16:12:37', 1, '2016-06-05 16:18:39', ''),
(5, 0, 5, 0, 'external', 0, '#', 'publish', 'google-plus', '', '', '', 1, '2016-06-05 16:13:00', 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link_content`
--

CREATE TABLE IF NOT EXISTS `structure_menu_link_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `structure_menu_link_content`
--

INSERT INTO `structure_menu_link_content` (`id`, `link_id`, `title`, `lang_id`, `description`) VALUES
(1, 1, 'Ø§Ù„Ø§Ø®Ø¨Ø§Ø±', 2, ''),
(2, 1, 'News', 1, ''),
(3, 2, 'facebook', 2, ''),
(4, 2, 'facebook', 1, ''),
(5, 3, 'twitter', 2, ''),
(6, 3, 'twitter', 1, ''),
(7, 4, 'youtube', 2, ''),
(8, 4, 'youtube', 1, ''),
(9, 5, 'google-plus', 2, ''),
(10, 5, 'google-plus', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies`
--

CREATE TABLE IF NOT EXISTS `taxonomies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `cover` varchar(250) NOT NULL,
  `main_menu` enum('no','yes') NOT NULL,
  `show_image` enum('no','yes') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `taxonomies`
--

INSERT INTO `taxonomies` (`id`, `sort`, `parent_id`, `taxonomy_type`, `sorting`, `status`, `cover`, `main_menu`, `show_image`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 0, 0, 'category', 1, 'publish', 'media-library/', 'no', 'no', 1, '2016-06-02 13:08:55', 1, '2016-06-02 13:46:49'),
(2, 0, 0, 'category', 1, 'publish', '', 'no', 'no', 1, '2016-06-07 04:02:37', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies_content`
--

CREATE TABLE IF NOT EXISTS `taxonomies_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxonomy_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `alias` varchar(256) NOT NULL,
  `description` longtext CHARACTER SET utf16 NOT NULL,
  `lang_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `taxonomies_content`
--

INSERT INTO `taxonomies_content` (`id`, `taxonomy_id`, `name`, `alias`, `description`, `lang_id`) VALUES
(1, 1, 'gfgh', 'gfghjk', '', 2),
(2, 1, 'new cat', 'new_cat', '', 1),
(3, 2, 'News', 'news', '', 2),
(4, 2, 'News', 'news', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `source` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `version`, `author`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(2, 'main', '1.0', 'diva', 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes_layouts`
--

CREATE TABLE IF NOT EXISTS `themes_layouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `layout_name` varchar(100) NOT NULL,
  `layout_cells` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `themes_layouts`
--

INSERT INTO `themes_layouts` (`id`, `theme_id`, `version`, `author`, `layout_name`, `layout_cells`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(4, 2, '1.0', 'diva', 'internal-page-2side', 2, 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model`
--

CREATE TABLE IF NOT EXISTS `theme_layout_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `type` enum('page','post','event') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `theme_layout_model`
--

INSERT INTO `theme_layout_model` (`id`, `name`, `theme_id`, `layout_id`, `type`) VALUES
(1, 'news model', 2, 4, 'post'),
(2, 'event model', 2, 4, 'event'),
(3, 'page model', 2, 4, 'page'),
(4, '', 2, 4, 'page'),
(5, 'events page', 2, 4, 'page');

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model_plugin`
--

CREATE TABLE IF NOT EXISTS `theme_layout_model_plugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `sorting` int(11) NOT NULL,
  `position` enum('left','right') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `theme_layout_model_plugin`
--

INSERT INTO `theme_layout_model_plugin` (`id`, `model_id`, `plugin_id`, `sorting`, `position`) VALUES
(1, 3, 1, 1, 'left'),
(27, 1, 7, 1, 'left'),
(28, 1, 44, 1, 'right'),
(29, 1, 47, 2, 'right'),
(30, 1, 45, 3, 'right'),
(31, 1, 46, 4, 'right'),
(32, 2, 34, 1, 'left'),
(33, 2, 44, 1, 'right'),
(34, 2, 47, 2, 'right'),
(35, 4, 35, 1, 'left'),
(36, 4, 47, 1, 'right'),
(37, 4, 46, 2, 'right'),
(38, 4, 44, 3, 'right'),
(39, 5, 48, 1, 'left'),
(40, 5, 44, 1, 'right'),
(41, 5, 45, 2, 'right');

-- --------------------------------------------------------

--
-- Table structure for table `time_zones`
--

CREATE TABLE IF NOT EXISTS `time_zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `GMT` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=150 ;

--
-- Dumping data for table `time_zones`
--

INSERT INTO `time_zones` (`id`, `GMT`, `name`) VALUES
(1, '-12.0', '(GMT-12:00)-International Date Line West'),
(2, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(3, '-10.0', '(GMT-10:00)-Hawaii'),
(4, '-9.0', '(GMT-09:00)-Alaska'),
(5, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(6, '-7.0', '(GMT-07:00)-Arizona'),
(7, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(8, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(9, '-6.0', '(GMT-06:00)-Central America'),
(10, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(11, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(12, '-6.0', '(GMT-06:00)-Saskatchewan'),
(13, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(14, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(15, '-5.0', '(GMT-05:00)-Indiana (East)'),
(16, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(17, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(18, '-4.0', '(GMT-04:00)-Santiago'),
(19, '-3.5', '(GMT-03:30)-Newfoundland'),
(20, '-3.0', '(GMT-03:00)-Brasilia'),
(21, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(22, '-3.0', '(GMT-03:00)-Greenland'),
(23, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(24, '-1.0', '(GMT-01:00)-Azores'),
(25, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(26, '0.0', '(GMT)-Casablanca, Monrovia'),
(27, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(28, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(29, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(30, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(31, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(32, '1.0', '(GMT+01:00)-West Central Africa'),
(33, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(34, '2.0', '(GMT+02:00)-Bucharest'),
(35, '2.0', '(GMT+02:00)-Cairo'),
(36, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(37, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(38, '2.0', '(GMT+02:00)-Jerusalem'),
(39, '3.0', '(GMT+03:00)-Baghdad'),
(40, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(41, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(42, '3.0', '(GMT+03:00)-Nairobi'),
(43, '3.5', '(GMT+03:30)-Tehran'),
(44, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(45, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(46, '4.5', '(GMT+04:30)-Kabul'),
(47, '5.0', '(GMT+05:00)-Ekaterinburg'),
(48, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(49, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(50, '5.75', '(GMT+05:45)-Kathmandu'),
(51, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(52, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(53, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(54, '6.5', '(GMT+06:30)-Rangoon'),
(55, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(56, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(57, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(58, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(59, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(60, '8.0', '(GMT+08:00)-Perth'),
(61, '8.0', '(GMT+08:00)-Taipei'),
(62, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(63, '9.0', '(GMT+09:00)-Seoul'),
(64, '9.0', '(GMT+09:00)-Vakutsk'),
(65, '9.5', '(GMT+09:30)-Adelaide'),
(66, '9.5', '(GMT+09:30)-Darwin'),
(67, '10.0', '(GMT+10:00)-Brisbane'),
(68, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(69, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(70, '10.0', '(GMT+10:00)-Hobart'),
(71, '10.0', '(GMT+10:00)-Vladivostok'),
(72, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(73, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(74, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(75, '-12.0', '(GMT-12:00)-International Date Line West'),
(76, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(77, '-10.0', '(GMT-10:00)-Hawaii'),
(78, '-9.0', '(GMT-09:00)-Alaska'),
(79, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(80, '-7.0', '(GMT-07:00)-Arizona'),
(81, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(82, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(83, '-6.0', '(GMT-06:00)-Central America'),
(84, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(85, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(86, '-6.0', '(GMT-06:00)-Saskatchewan'),
(87, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(88, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(89, '-5.0', '(GMT-05:00)-Indiana (East)'),
(90, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(91, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(92, '-4.0', '(GMT-04:00)-Santiago'),
(93, '-3.5', '(GMT-03:30)-Newfoundland'),
(94, '-3.0', '(GMT-03:00)-Brasilia'),
(95, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(96, '-3.0', '(GMT-03:00)-Greenland'),
(97, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(98, '-1.0', '(GMT-01:00)-Azores'),
(99, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(100, '0.0', '(GMT)-Casablanca, Monrovia'),
(101, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(102, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(103, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(104, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(105, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(106, '1.0', '(GMT+01:00)-West Central Africa'),
(107, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(108, '2.0', '(GMT+02:00)-Bucharest'),
(109, '2.0', '(GMT+02:00)-Cairo'),
(110, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(111, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(112, '2.0', '(GMT+02:00)-Jerusalem'),
(113, '3.0', '(GMT+03:00)-Baghdad'),
(114, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(115, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(116, '3.0', '(GMT+03:00)-Nairobi'),
(117, '3.5', '(GMT+03:30)-Tehran'),
(118, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(119, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(120, '4.5', '(GMT+04:30)-Kabul'),
(121, '5.0', '(GMT+05:00)-Ekaterinburg'),
(122, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(123, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(124, '5.75', '(GMT+05:45)-Kathmandu'),
(125, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(126, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(127, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(128, '6.5', '(GMT+06:30)-Rangoon'),
(129, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(130, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(131, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(132, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(133, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(134, '8.0', '(GMT+08:00)-Perth'),
(135, '8.0', '(GMT+08:00)-Taipei'),
(136, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(137, '9.0', '(GMT+09:00)-Seoul'),
(138, '9.0', '(GMT+09:00)-Vakutsk'),
(139, '9.5', '(GMT+09:30)-Adelaide'),
(140, '9.5', '(GMT+09:30)-Darwin'),
(141, '10.0', '(GMT+10:00)-Brisbane'),
(142, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(143, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(144, '10.0', '(GMT+10:00)-Hobart'),
(145, '10.0', '(GMT+10:00)-Vladivostok'),
(146, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(147, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(148, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(149, '13.0', '(GMT+13:00)-Nuku''alofa ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `user_level` enum('admin','restaurant_owner','branch_manager','meal_handler','pilot','operation') NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_profile` int(11) NOT NULL,
  `password` varchar(150) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `user_level`, `restaurant_id`, `branch_id`, `email`, `user_profile`, `password`, `inserted_by`, `inserted_date`, `last_update`, `update_by`) VALUES
(1, 'waled.rayan80', 'waleed', 'rayan', 'admin', 0, 0, 'waled@gmail.com', 1, 'fe703d258c7ef5f50b71e06565a65aa07194907f', 4, '2014-04-14 10:45:49', '2015-10-27 15:55:29', 1),
(6, 'AlaaAssem', 'Alaa', 'Assem', 'admin', 0, 0, 'aassem@mls-egypt.org', 1, '889b4f5d5e81327037322672384ec4a719206516', 1, '2016-02-29 11:50:57', '0000-00-00 00:00:00', 0),
(7, 'heba.hussien', 'heba', 'hussien', 'admin', 0, 0, 'hebahussien83@gmail.com', 1, '2bb2125955689088cffd9a68ff483c98a6f694db', 1, '2016-03-01 09:09:03', '0000-00-00 00:00:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
