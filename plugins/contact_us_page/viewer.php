<div class="col-md-12 internal-cover" style="background-image: url('main/main/images/int_cover2.jpg')"><!--start of internal-cover-->
         
      </div><!--end of internal-cover-->

       <article class="col-md-12 internal-hero"><!--start of internal-hero-->
            <div class="col-md-12 internal-hero-title">
              <h1 class="col-md-12 main-title give-color1-bg give-radius"><?php echo $title; ?></h1>
            </div>
            <div class="col-md-12 internal-hero-text give-card-bg give-radius give-shadow">
              <p><?php  echo $get_page_content->meta_description; ?></p>
            </div>
      </article><!--end of internal-hero-->


      <!--this <internal-wrapper> is important for give a min-height if content was not enough for making the website full height in the HD res-->
      <section class="col-md-12 internal-wrapper"><!--start of internal-wrapper-->
          <?php  echo $get_page_content->body?>
          <!--end of container-->

           <div class="col-md-12 give-card-bg feedback">
                <div class="container">
                  <h1 class="col-md-12 give-color1-c">feedback</h1>
                  <form action="" class="row clearfix feedback-form">
                    <div class="form-group col-md-4">
                      <input type="text" class="form-control" placeholder="Full name">
                    </div>

                    <div class="form-group col-md-4">
                      <input type="text" class="form-control" placeholder="Email ">
                    </div>

                     <div class="form-group col-md-4">
                      <input type="text" class="form-control" placeholder="Subject ">
                    </div>
                     <div class="form-group col-md-all">
                      <textarea type="text" class="form-control" placeholder="Subject "></textarea>
                    </div>
                    <div class="form-group align-area col-md-all">
                      <input type="submit" class="btn give-color2-bg" value="Send">
                    </div>
                  </form>
                </div>
          </div>
    
      </section><!--end of internal-wrapper-->