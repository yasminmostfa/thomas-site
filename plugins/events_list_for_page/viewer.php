   <section class="col-m-12 main-content-section"><!--start of main content section-->
  <?php
  $page_all_post = $define_node->front_node_data(null,"event",null,null,$lang_info->id,null,null,null,null, null,null,null,"many",null,null,"no");
  //total posts
  $total_posts = count($page_all_post);
  //get page id if exist
  $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
 //number of posts in one page
  $per_page =20 ;
  $pagination = new Pagination($page_number , $per_page, $total_posts);
  //calculate the offset
  $offset = $pagination->offset();
  $count = $offset+1;
  $List_posts_for_blog = $define_node->front_node_data(null,"event",null,null,$lang_info->id,null,null,null,null, null,null,null,"many",$per_page,$offset,"no");
  $count_show_result = count($List_posts_for_blog);
  ?>
  <header class="clearfix give-mb-med main-content-header"><!--start of main content section header-->
                <p class="col-md-12 give-boldFamily search-result pull-left"><?php echo $showing."&nbsp;"; if($total_posts != 0) { if($count_show_result < $per_page){ echo $count."-".$count_show_result;}else{
						  
						    echo $per_page*$page_number;
						  }}else{ echo 0;} echo "  $of ".$total_posts?>   </p>
                
                <div class="col-md-12 sort-select-wrap pull-right"><!--start of sort  select-->
                 <div class="col-md-12 sort-select give-padding-sm">
                   <p class="col-md-12 sort-selected">Revelance</p>
                   <span class="caret"></span>
                 </div>
                 <ul class="col-md-12 sort-list">
                   <li>Books</li>
                   <li>library</li>
                   <li>section</li>
                 </ul>
               </div><!--end of sort select-->
              <p class="col-md-12 sort-text pull-right">Sort by</p>
              </header>
    
<?php   
echo "<input type='hidden' id='lang' value='$lang'>";
foreach($List_posts_for_blog as $post){
			  $image_src = get_image_src($post->cover_image,"medium");
			  if($lang == "en"){
						  $date = english_date($post->start_publishing);
			 }else{
						  $date = arabic_date($post->start_publishing);
		     }
			 if($post->cover_image){
				 $image_path = get_image_src($post->cover_image,"large");
			 }else{
				 $image_path = "";
			 }
       	   	 echo "<article class='clearfix give-padding give-gray-bg primary-post database-post'><!--start of database post-->
                  <a href='event_details.php?lang=$lang&alias=$post->alias' class='col-md-3 primary-image' style='background-image: url($image_path)'></a>
                  <div class='col-md-9 primary-text'>
                    <h4 class='col-md-12 give-darkRed-c give-boldFamily'><a href='post_details.php?alias=$post->alias' class='add_view' id='$post->id' = name = 'divacms'>$post->title</a></h4>
                    <p>".string_limit_words(strip_tags($post->summary),20)."</p>
                  </div>
                   <ul class='col-md-12 post-meta give-red-bg row give-padding-sm'>
                      
                       <li title='Published'>
                          <p class='fa fa-calendar give-yellow-c'></p> $date
                      </li>
                      </li>
                        <li title='Share'>
                           <a href='#' class='share_class' id='$post->id' name='divacms' data-title='$post->title'><span class='fa fa-share-square-o give-yellow-c'></span>$share</a>
                      </li>
                       <li title='Views' class='hide'>
                          <p class='fa fa-eye give-yellow-c'></p> 300
                      </li>
                       <li title='Save'  class='hide'>
                          <a href=''><span class='fa fa-heart give-yellow-c'></span> Save</a>
                      </li>
                      <li class='col-md-12 post-read'>
                          <a href='event_details.php?lang=$lang&alias=$post->alias' class='btn give-darkRed-bg add_view' id='$post->id' = name = 'divacms' target='_blank'>$read_more</a>
                      </li>
                   </ul>
              </article>";
                
			 
		  //end of foreach
		  
 }//end of page_categories 
 ?>
 
 </section>
 <?php echo "<nav class='col-md-12 pagination-wrap'>";
 if($pagination->total_pages() > 1) {
			echo "<ul class='pagination'>";
			if($pagination->has_previous_page()) { 
			   $previous = $pagination->previous_page();
			   echo "<li ><a href='content.php?alias=$node_alias&lang=$lang&page=$previous' aria-label='Previous'><span  class='fa fa-chevron-left'></span> </a></li>";
			  }
			  for($i=1; $i <= $pagination->total_pages(); $i++) {
				   if($i == $page_number){
					     $active = "  active";
				   }else{
					     $active = " ";
				   }
				   echo "<li class='$active' ><a href='content.php?alias=$node_alias&lang=$lang&page={$i}'>$i</a></li>";
				   
				   
			   }
			  
			  
			  if($pagination->has_next_page()) { 
			    $next = $pagination->next_page();
			    echo "<li><a href='content.php?alias=$node_alias&page=$next&lang=$lang' aria-label='Next'><span  class='fa fa-chevron-right'></span></a></li>";
			  }
			  echo "</ul>";
 }
 echo "</nav>";
 ?>
