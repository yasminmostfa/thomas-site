<?php 
 if($front_session->is_logged() == false){
	 redirect_to('index.php?lang=en');
 }
 
 $get_all_feeds = $define_feeds->get_feeds(null,null,null,"inserted_date","DESC");
?>


<section class="col-m-12 main-content-section" style="min-height:1300px"><!--start of main content section-->
<?php 
if($get_all_feeds){
	echo "<input type='hidden' id='lang' value='$lang'>";
	foreach($get_all_feeds as $feed){
		
		$user_feed_data = CustomerInfo::find_by_id($feed->custome_info_id);
		if($feed->resource_type =="divacms"){
			$feed_all_data = $define_node->front_node_data($feed->resource_id,null,null,null,$lang_info->id,null,null,null,null, null,null,null,"one");
			if($feed_all_data->cover_image){
				 $image_path = get_image_src($feed_all_data->cover_image,"large");
			 }else{
				 $image_path = "";
			 }
			 if($feed_all_data->node_type == "post"){
				 $path = "post_details.php?lang=$lang&alias=$feed_all_data->alias";
			 }else if($feed_all_data->node_type == "page"){
				 $path = "content.php?lang=$lang&alias=$feed_all_data->alias";
			 }else{
				 $path = "event_details.php?lang=$lang&alias=$feed_all_data->alias";
			 }
		 echo " <article class='col-md-12 feeds2-post'>
                    <header class='clearfix feeds2-header'>
                      <div class='col-md-1 feeds2-header-image' style='background-image: url(main/images/dummy-image.jpg);'></div>
                      <div class='col-md-11 feeds2-header-text'>
                        <h4 class='col-md-12 give-red-c give-boldFamily feeds2-title'>".$user_feed_data->first_name." ".$user_feed_data->last_name ." <span class='give-black-c'>$shared </span>$feed_all_data->title</h4>
                        <p class='col-md-12 give-feedstime-c feeds2-time'>".date('h:i:s A',strtotime($feed_all_data->start_publishing))."</p>
                      </div>
                    </header>

                    <div class='col-md-12 feeds2-item'>
                        <a href='$path' class='col-md-12 feeds2-item-image' ><img src='$image_path'></a>
                        <div class='col-md-12 feeds2-item-text give-padding give-gray-bg'>".string_limit_words(strip_tags($feed_all_data->summary),20)."</div>
                    </div>

                   <ul class='col-md-12 post-meta give-red-bg give-flex text-center give-padding-sm'>
                       <li title='Share'>
                           <a href='#' class='share_class' id='$feed_all_data->id' name='$feed->resource_type'><span class='fa fa-share-square-o give-yellow-c'></span>$share</a>
                      </li>
                       <li title='Views' class='hide'>
                          <p class='fa fa-eye give-yellow-c'></p> 300
                      </li>
                      <li title='Save' class='hide'>
                          <a href=''><span class='fa fa-heart give-yellow-c'></span> Save</a>
                      </li>

                       <li title='Like'>
                          <a href='$path'><span class='fa fa-thumbs-up give-yellow-c'></span>$like</a>
                      </li>
                   </ul>
              </article>";
		}else if($feed->resource_type =="feed"){
			$feed_all_data = $define_node->front_node_data($feed->resource_id,null,null,null,$lang_info->id,null,null,null,null, null,null,null,"one");
			if($feed_all_data){
			if($feed_all_data->cover_image){
				 $image_path = "media-library/$feed_all_data->cover_image";
			 }else{
				 $image_path = "";
			 }
			echo " <article class='col-md-12 feeds2-post'>
                    <header class='clearfix feeds2-header'>
                      <div class='col-md-1 feeds2-header-image' style='background-image: url('https://placeholdit.imgix.net/~text?txtsize=23&txt=243%C3%97270&w=243&h=270');'></div>
                      <div class='col-md-11 feeds2-header-text'>
                        <h4 class='col-md-12 give-red-c give-boldFamily feeds2-title'>".$user_feed_data->full_name()." <span class='give-black-c'>$shared </span>$feed_all_data->title</h4>
                        <p class='col-md-12 give-feedstime-c feeds2-time'>".date('h:i:s A',strtotime($feed_all_data->start_publishing))."</p>
                      </div>
                    </header>

                    <div class='col-md-12 feeds2-item'>
                        <a href='$path' class='col-md-12 feeds2-item-image' >".string_limit_words(strip_tags($feed_all_data->summary),20)."</a>
                        <div class='col-md-12 feeds2-item-text give-padding give-gray-bg'>$feed_all_data->body</div>
                    </div>

                   <ul class='col-md-12 post-meta give-red-bg give-flex text-center give-padding-sm'>
                       <li title='Share'>
                           <a href=''  class='share_class' id='$feed_all_data->id' name='$feed->resource_type' name='$feed->resource_type' data-title='$feed_all_data->title'><span class='fa fa-share-square-o give-yellow-c'></span>$share</a>
                      </li>
                       <li title='Views' class='hide'>
                          <p class='fa fa-eye give-yellow-c'></p> 300
                      </li>
                      <li title='Save' class='hide'>
                          <a href=''><span class='fa fa-heart give-yellow-c'></span> Save</a>
                      </li>

                       <li title='Like'  class='hide'>
                          <a href=''><span class='fa fa-thumbs-up give-yellow-c'></span>$like</a>
                      </li>
                   </ul>
              </article>";
			}
			
			
		}else if($feed->resource_type =="vufind"){
			$path = $define_general_setting->vufind_main_url."Record/$feed->resource_id";
			
	    echo " <article class='col-md-12 feeds2-post'>
                    <header class='clearfix feeds2-header'>
                      <div class='col-md-1 feeds2-header-image' style='background-image: url('https://placeholdit.imgix.net/~text?txtsize=23&txt=243%C3%97270&w=243&h=270');'></div>
                      <div class='col-md-11 feeds2-header-text'>
                        <h4 class='col-md-12 give-red-c give-boldFamily feeds2-title'>".$user_feed_data->full_name()." <span class='give-black-c'>$shared </span>$feed->feed_title</h4>
                        <p class='col-md-12 give-feedstime-c feeds2-time'>5 min. ago</p>
                      </div>
                    </header>

                   <ul class='col-md-12 post-meta give-red-bg give-flex text-center give-padding-sm'>
                       <li title='Share'>
                           <a href=''  class='share_class' id='$feed_all_data->id' name='$feed->resource_type' name='$feed->resource_type' data-title='$feed->feed_title'><span class='fa fa-share-square-o give-yellow-c'></span>$share</a>
                      </li>
                       <li title='Views' class='hide'>
                          <p class='fa fa-eye give-yellow-c'></p> 300
                      </li>
                      <li title='Save' class='hide'>
                          <a href=''><span class='fa fa-heart give-yellow-c'></span> Save</a>
                      </li>

                       <li title='Like'  class='hide'>
                          <a href=''><span class='fa fa-thumbs-up give-yellow-c'></span>$like</a>
                      </li>
                   </ul>
              </article>";		
			
			
	  }
		
		
	}
	
	
}

?>

              


                


                

            
              
 </section>