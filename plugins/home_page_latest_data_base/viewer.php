<section class="col-m-12 main-content-section"><!--start of main content section-->
  <?php $data = new HomePageLayout();
           
        $data_result =$data->get_by_plugin($section_data->plugin,$section_data->position);
		$user_saves = UsersSaves::find_all_by_custom_filed("custome_info_id",$front_session->user_id);
		$user_saves_ids = array();
		if($user_saves){
			foreach($user_saves as $user_save){
				$user_saves_ids[] = $user_save->resource_id;
			}
		}
                          
                         ?>
               <header class="clearfix give-mb-med main-content-header"><!--start of main content section header-->
                <h1 class="col-md-12 give-boldFamily give-title-border give-maintitle-border pull-left">
                <?php echo $data_result->header_title;?></h1>
                <a href="<?php echo $data_result->link_en;?>" target="_blank" class="col-md-12 give-red-c pull-right">View all events <span class="fa fa-long-arrow-right"></span></a>
              </header><!--start of main content section header-->

                          <?php
                          if($_GET['lang'] == 'en'){
                            $books = simplexml_load_file($data_result->link_en.'&view=rss&skip_rss_sort=1');
                          }else{
                            $books = simplexml_load_file($data_result->link_ar.'&view=rss&skip_rss_sort=1');

                          }
						 
                        $items= array();
                        // for ($i = 0; $i <$data_result->link_limit; $i++) {
                        //     $items[$i]= $books->channel->item[$i];
                        // }
                        $counter = 1;
                        foreach ($books->channel->item as $item) {
							$item_link = explode("/",$item->link);
							$count_link_array = count($item_link);
							$item_id = $item_link[$count_link_array-1];
							$check_view = $define_view->get_views("vufind",$item_id);
							


                       
                            ?>
               <article class="clearfix give-padding give-gray-bg primary-post database-post"><!--start of database post-->
                  <a href="" class="col-md-3 primary-image" style="background-image: url('https://placeholdit.imgix.net/~text?txtsize=18&txt=190%C3%97140&w=190&h=140');"></a>
                  <div class="col-md-9 primary-text">
                      <h4 class="col-md-12 give-darkRed-c give-boldFamily"><a href="<?php  echo $item->link;?>"  class="add_view" target="_blank"  id="<?php echo $item_id ?>" name = "vufind"> <?php echo $item->title; ?></a></h4>
                   <p></p>
                  </div>
                   <ul class="col-md-12 post-meta give-red-bg row give-padding-sm">
                      
                       <li title="Published">
                      <?php   if ($_GET['lang'] == 'ar'){?>
                          <p class="fa fa-calendar give-yellow-c"></p> <?php echo arabic_date($item->pubDate);}else{?>
                          <p class="fa fa-calendar give-yellow-c"></p> <?php echo english_date($item->pubDate);} ?>
                      </li>
                      <?php   if($front_session->is_logged()){ ?>
                       <li title="Share">
                           <a href="#"  class="share_class"  id='<?php echo $item_id ?>' name='vufind' data-title = "<?php echo $item->title;  ?>" ><span class="fa fa-share-square-o give-yellow-c"></span> <?php echo $share ?></a>
                      </li>
                      <?php  } ?>
                       <li title="Views" >
                          <p class="fa fa-eye give-yellow-c"></p><i  id="view_number_<?php echo $item_id ?>"><?php if($check_view){ echo $check_view->number_views; }else{echo 0;} ?></i> 
                      </li>
                       <?php   if($front_session->is_logged()){ ?>
                       <li class="<?php  if(in_array($item_id,$user_saves_ids)){
							        echo "active";
							        
							      }else{echo "";}
						   ?> "  id="id="save_label<?php echo $item_id ?>">
                          <a href="#"  class="save_class_cms"  id='<?php echo $item_id ?>' name='<?php echo $item->link;  ?>' data-title = "<?php echo $item->title;  ?>"  <?php  if(in_array($item_id,$user_saves_ids)){
							        echo "style='pointer-events:none'";
							        
							      }else{echo "";}
						   ?>"><span class="fa fa-heart give-yellow-c"></span> 
                          <i id="save_label<?php echo $item_id ?>">
                          <?php  if(in_array($item_id,$user_saves_ids)){
							        echo "Saved";
							        
							      }else{echo "Save";}
						   ?>
                          
                          
                          </i></a>
                      </li>
                      <?php } ?>
                      <li class="col-md-12 post-read">
                          <a href="<?php  echo $item->link;?>" class="btn give-darkRed-bg add_view " target="_blank" id="<?php echo $item_id ?>" name = "vufind" ><?php echo $read_more ?></a>
                      </li>
                   </ul>
              </article><!--end of database post-->
              
              
              <?php
                      $counter++;
                      if($counter>$data_result->link_limit){
                        break;
                      }
                        }?>
                      
                           
             
             
               
            </section>