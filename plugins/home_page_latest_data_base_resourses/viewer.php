<section class="col-m-12 main-content-section"><!--start of main content section-->
 <?php $data = new HomePageLayout();
                                
                          $data_result =$data->get_by_plugin($section_data->plugin,$section_data->position);


                          ?>
               <header class="clearfix give-mb-med main-content-header"><!--start of main content section header-->
                <h1 class="col-md-12 give-boldFamily give-title-border give-maintitle-border pull-left"><?php
                echo $data_result->header_title;?></h1>
                <a href="" class="col-md-12 give-red-c pull-right">View all events <span class="fa fa-long-arrow-right"></span></a>
              </header><!--end of main content section header-->
               <div class="row clearfix"><!--start of row-->
              <?php
                          if($_GET['lang'] == 'en'){
                            $books = simplexml_load_file($data_result->link_en.'&view=rss&skip_rss_sort=1');
                          }else{
                            $books = simplexml_load_file($data_result->link_ar.'&view=rss&skip_rss_sort=1');
                          }
                        $items= array();
                        
                        $counter = 1;
                        foreach ($books->channel->item as $item) {
                          
                      
                            ?>

             
                
                  <article class="col-md-4 database-cover">
                    <a href="" class="col-md-12 database-cover-image give-border" style="background-image: url('https://placeholdit.imgix.net/~text?txtsize=23&txt=243%C3%97270&w=243&h=270');"></a>
                    <div class="col-md-12 database-cover-text give-gray-bg give-padding">
                        <h2 class="col-md-12 give-red-c give-boldFamily"><a title="<?php echo substr($item->title, 0,-1)?>" href="<?php  echo $item->link;?>"><?php   echo string_limit_words($item->title,4);?>....</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                         <ul class="col-md-12 post-meta give-block">
                         <?php  if ($item->author) {?>
                           <li title="Author">

                              <a href=""><span class="fa fa-user"></span> <?php echo $item->author ?></a>
                          </li>
                          <?php
                         }?>
                           
                           <li title="Date">
                            <?php   if ($_GET['lang'] == 'ar'){?>
                          <p class="fa fa-calendar"></p> <?php echo arabic_date($item->pubDate);}else{?>
                          <p class="fa fa-calendar"></p> <?php echo english_date($item->pubDate);} ?>
                          </li>
                           <li title="isbn" class="hide">
                              <a href=""><span class="fa fa-book"></span> #145523</a>
                          </li>
                          <li title="Views" class="hide">
                              <p class="fa fa-eye"></p> 300
                           </li>
                        </ul>
                    </div>
                  </article>
                   <?php
                      $counter++;
                      if($counter>$data_result->link_limit){
                        break;
                      }
                        }?>
                     

             </div><!--end of row-->
            </section>