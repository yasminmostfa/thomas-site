<?php
if(!empty($plugin_value)){ 
if($header_title){
	  $explode_header = explode("||",$header_title);
	  if($lang == "en"){
		  $pugin_title = $explode_header[0];
	  }else{
		  $pugin_title = $explode_header[1];
	  }
  }else{
	  $pugin_title = "";
  }
$pugin_value_url  = Taxonomies::find_by_id($plugin_value);
if($pugin_value_url){
	$url = $pugin_value_url->url;
}else{
	$url = "";
}

?>

<section class='col-m-12 main-content-section'><!--start of main content section-->
               <header class='clearfix give-mb-med main-content-header'><!--start of main content section header-->
                <h1 class='col-md-12 give-boldFamily give-title-border give-maintitle-border pull-left'><?php echo $pugin_title; ?></h1>
                <a href='<?php echo $url ?>' class='col-md-12 give-red-c pull-right'  target="_blank"><?php  echo $view_all?> <span class='fa fa-long-arrow-right'></span></a>
              </header><!--end of main content section header-->

              <div class='row clearfix'><!--start of row-->
                <!--end of training post-->
              <?php
			  echo "<input type='hidden' id='lang' value='$lang'>";  
			  $courses = $define_node->front_node_data(null,"event",null,null,$lang_info->id,"yes",null,null,null, null,null,null,"many",2,null,"yes",$plugin_value);
			  if($courses){
				  foreach($courses as $course){
					  $check_view = $define_view->get_views("divacms",$course->id);
					  $image_path = get_image_src($course->cover_image,"large");
					  $course_details =  EventDetails::find_by_custom_filed("event_id",$course->id);
					  if($lang == "en"){
						  $start_date = english_date($course_details->start_date);
						  $end_date  = english_date($course_details->end_date);
					  }else{
						  $start_date = arabic_date($course_details->start_date);
						  $end_date  = arabic_date($course_details->end_date);
					  }
					  if($check_view){ $view_number = $check_view->number_views; }else{$view_number= 0;}
					  echo "<article class='col-md-6 secondary-post traninig-post '><!--start of training post-->
                 <div class='col-md-12 secondary-post-inner give-border'>
                    <div  class='col-md-12 secondary-image' style='background-image: url($image_path);'>
                    <ul class='col-md-12 post-meta give-red-bg give-padding-sm give-flex text-center give-absolute-bottom'>";
                    if($front_session->is_logged()){
                       echo "<li title='Share'>
                           <a href='#' class='share_class' id='$course->id' name='divacms' data-title='$course->title'><span class='fa fa-share-square-o give-yellow-c'></span>$share</a>
                      </li>";
					 }
					  echo "
                       <li title='Views'>
                          <p class='fa fa-eye give-yellow-c'></p> <i  id='view_number_$course->id'>$view_number</i>
                      </li>
                       
                      
                   </ul>
                  </div>
                  <div class='col-md-12 secondary-text give-gray-bg give-padding'>
                     <h4 class='col-md-12 give-darkRed-c give-boldFamily'><a href='event_details.php?lang=$lang&alias=$course->alias'  class='add_view' id='$course->id' = name = 'divacms'>$course->title</a></h4>
                     <ul class='col-md-12 post-list'>
                       <li>
                          <p class='col-md-12 give-darkRed-c give-boldFamily post-list-title'>$duration :</p>
                           $from $start_date,  $to $end_date
                      </li>
                       <li>
                          <p class='col-md-12 give-darkRed-c give-boldFamily post-list-title'>$traniner :</p>
                           $course_details->instructor
                      </li>
                      <li>
                          <p class='col-md-12 give-darkRed-c give-boldFamily post-list-title'>$place :</p> $course_details->place
                          
                      </li>
                      <li>
                         <a href='event_details.php?lang=$lang&alias=$course->alias' class='btn give-red-bg add_view' id='$course->id' = name = 'divacms'>$read_more</a>
                      </li>
                   </ul>
                  </div>
                 </div>
               </article>";
				  }
			  }
			  
			  ?>
                <!--end of training post-->

             </div><!--end of row-->
            </section>
            <?php } ?>