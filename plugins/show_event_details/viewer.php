<?php
$event_details = EventDetails::find_by_custom_filed("event_id",$get_event_content->id);
 if($lang =="en"){
	$post_date = english_date($event_details->start_date);
	
 }else{
   $post_date = arabic_date($event_details->start_date);

}
 $image_path = get_image_src($get_event_content->cover_image,"large");

 ?>


 <article class="col-md-12 internal-post"><!--start of internal-post-->
              <div class="col-md-12 secondary-image give-mb-med" style="background-image: url('<?php echo $image_path ?>');">
                <ul class="col-md-12 post-meta give-red-bg  give-padding-sm give-absolute-bottom">
                     <li title="Published">
                        <p class="fa fa-calendar give-yellow-c"></p> <?php echo $post_date; ?>
                    </li>
                     <?php
					 echo "<input type='hidden' id='lang' value='$lang'>";   
					 $check_view = $define_view->get_views("divacms",$get_event_content->id);
					   if($check_view){ $view_number = $check_view->number_views; }else{$view_number= 0;}
					 echo "
					  <li title='Share'>
                           <a href='#' class='share_class' id='$get_event_content->id' name='divacms'><span class='fa fa-share-square-o give-yellow-c'></span>$share</a>
                      </li>
					   <li title='Views'>
                          <p class='fa fa-eye give-yellow-c'></p> <i >$view_number</i>
                      </li>
					
					";
					 
					 ?>                  
                 </ul>
              </div>

              <h4 class="col-md-12 give-boldFamily give-darkRed-c"><?php echo $get_event_content->title ?></h4>

              <p class="col-md-12 give-yellow-c"><?php echo $post_date?></p>
              <?php 
				//process body and separate between plugin name and value
				$fullText = $get_event_content->body;
				//add plugin
				$add_plugin = preg_replace_callback("/\[.+\]/U", "plugin_callback", $fullText)."\n";
				echo $add_plugin;
				//print final
				//echo $add_plugin;
				function plugin_callback($match){
					global $node_id;
					global $node_type;
					global $define_image_gallery;
					$query = substr($match[0],1,-1);                           
					//delete [ and ]
					$text_manipulation = preg_replace('/[\[\]]/s', '', $match[0]);
					ob_start();
					$separate_text = explode('@',$text_manipulation);
					if($separate_text[0] == "cms_plugin"){
						$plugin_value = $separate_text[2];
						if(file_exists("plugins/$separate_text[1]/viewer.php")){
							include "plugins/".$separate_text[1]."/viewer.php";
						}else{
							echo "File Not Exist";	
						}
						$pluginContent = ob_get_contents();
						ob_end_clean();
						return $pluginContent;		
					}
				}
  
 ?> 

              </article>

