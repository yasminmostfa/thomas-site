<?php if(!empty($plugin_value)){ 

$posts = $define_node->front_node_data(null,"post",null,null,$lang_info->id,"no",null,"yes",$plugin_value, null,null,null,"many");

if($posts){

  if($header_title){

	  $explode_header = explode("||",$header_title);

	  if($lang == "en"){

		  $pugin_title = $explode_header[0];

	  }else{

		  $pugin_title = $explode_header[1];

	  }

  }else{

	  $pugin_title = "";

  }



?>

<div class="clearfix news-slider-wrapper"><!--start of recent news-->

                    <h2 class="col-md-12 give-boldFamily give-title-border give-sidetitle-border give-darkRed-c give-mb-med pull-left"><?php echo $pugin_title;?></h2>

                    <div class="col-md-12 pull-right news-controls-wrapper">

                       <a href="#news-slider" class="carousel-control left" data-slide="prev"><span class="fa fa-chevron-left"></span></a>

                        <a href="#news-slider" class="carousel-control right" data-slide="next"><span class="fa fa-chevron-right"></span></a>

                    </div>

                   </div>

     

         

      <div class="carousel slide give-mb-med" id="news-slider">

                     <div class="carousel-inner">

                       <?php 

	   $offset = 0;

	   foreach($posts as $post){

		  if($offset == 0){

			 $class = "active";

		  }else{

			  $class = "";

		  }

		  $image_path = get_image_src($post->cover_image,"large");

		  if($lang =="en"){

			  $post_date = english_date($post->start_publishing);

		   }else{

			   $post_date = arabic_date($post->start_publishing);

		   }

		   echo "<div class='item $class'>

           <div class='col-md-12 news-image' style='background-image: url($image_path)'></div>

           <div class='col-m-12 news-text'>

             <p class='col-md-12 give-red-c'>$post_date</p>

             <h4 class='col-md-12 give-darkRed-c give-boldFamily'><a href='post_details.php?lang=$lang&alias=$post->alias' class='add_view' id='$post->id' = name = 'divacms'>$post->title</a></h4>

           </div>

         </div>";

		 $offset++;

	   }

	   

	   ?>

                       

        </div>

     </div>

<?php }

}?>