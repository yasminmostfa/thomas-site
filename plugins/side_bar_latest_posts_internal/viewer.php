<?php 
$get_plugin_id = Plugins::find_by_custom_filed('source','side_bar_latest_posts_internal');
$get_selected_category_for_node = NodesPluginsValues::get_plugin_values($get_plugin_id->id,$lang_info->id,$node_id,$node_type);	

if($get_selected_category_for_node){
	if($get_selected_category_for_node->content != 0 || !empty($get_selected_category_for_node->content)){
		$posts = $define_node->front_node_data(null,"post",null,null,$lang_info->id,"no",null,"yes",$get_selected_category_for_node->content, null,null,null,"many");
?>
<div class="clearfix news-slider-wrapper"><!--start of recent news-->
                    <h2 class="col-md-12 give-boldFamily give-title-border give-sidetitle-border give-darkRed-c give-mb-med pull-left"><?php echo $get_selected_category_for_node->title;?></h2>
                    <div class="col-md-12 pull-right news-controls-wrapper">
                       <a href="#news-slider" class="carousel-control left" data-slide="prev"><span class="fa fa-chevron-left"></span></a>
                        <a href="#news-slider" class="carousel-control right" data-slide="next"><span class="fa fa-chevron-right"></span></a>
             </div>
         </div>
     
         
      <div class="carousel slide give-mb-med" id="news-slider">
                     <div class="carousel-inner">
                       <?php 
	   $offset = 0;
	   foreach($posts as $post){
		  if($offset == 0){
			 $class = "active";
		  }else{
			  $class = "";
		  }
		  $image_path = get_image_src($post->cover_image,"large");
		  if($lang =="en"){
			  $post_date = english_date($post->start_publishing);
		   }else{
			   $post_date = arabic_date($post->start_publishing);
		   }
		   echo "<div class='item $class'>
           <div class='col-md-12 news-image' style='background-image: url($image_path)'></div>
           <div class='col-m-12 news-text'>
             <p class='col-md-12 give-red-c'>$post_date</p>
             <h4 class='col-md-12 give-darkRed-c give-boldFamily'><a href='post_details.php?lang=$lang&alias=$post->alias' class='add_view' id='$post->id' = name = 'divacms'>$post->title</a></h4>
           </div>
         </div>";
		 $offset++;
	   }
	   
	   ?>
                       
        </div>
     </div>
     <?php }
}?>
