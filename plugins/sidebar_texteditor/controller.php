<?php
	//get plugin data from plguin node options
	$get_plugin_id = Plugins::find_by_custom_filed('source','sidebar_texteditor');
    $get_plugin_value = NodesPluginsValues::get_plugin_values($get_plugin_id->id,$lang->id,$node_id,$node_type);
?>
<div class="form-group">
<label class="col-lg-3">Section Title-<?php echo ucfirst($lang->label)?>:</label>
<div class="col-lg-4">
<input type="text" class="form-control" name="sidebar_texteditor_title_<?php echo $lang->label?>" value="<?php echo $get_plugin_value->title?>" autocomplete="off">
</div>
</div>
<div class="form-group">
  <label  class="col-lg-2">Sidebar Text-<?php echo ucfirst($lang->label)?> :</label>
  <div class="col-lg-8">
    <textarea class="form-control" name="sidebar_texteditor_<?php echo $lang->label?>" placeholder=" " rows="10"><?php echo $get_plugin_value->content?></textarea>
  </div>
</div>