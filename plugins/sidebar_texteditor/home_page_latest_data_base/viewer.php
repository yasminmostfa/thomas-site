<section class="col-m-12 main-content-section"><!--start of main content section-->
  <?php $data = new HomePageLayout();
                            
                        
                          $data_result =$data->get_by_plugin($section_data->plugin,$section_data->position);
                          
                         ?>
               <header class="clearfix give-mb-med main-content-header"><!--start of main content section header-->
                <h1 class="col-md-12 give-boldFamily give-title-border give-maintitle-border pull-left">
                <?php echo $data_result->header_title;?></h1>
                <a href="" class="col-md-12 give-red-c pull-right">View all events <span class="fa fa-long-arrow-right"></span></a>
              </header><!--start of main content section header-->

                          <?php
                          if($_GET['lang'] == 'en'){
                            $books = simplexml_load_file($data_result->link_en.'&view=rss&skip_rss_sort=1');
                          }else{
                            $books = simplexml_load_file($data_result->link_ar.'&view=rss&skip_rss_sort=1');

                          }
                        $items= array();
                        // for ($i = 0; $i <$data_result->link_limit; $i++) {
                        //     $items[$i]= $books->channel->item[$i];
                        // }
                        $counter = 1;
                        foreach ($books->channel->item as $item) {


                       
                            ?>
               <article class="clearfix give-padding give-gray-bg primary-post database-post"><!--start of database post-->
                  <a href="" class="col-md-3 primary-image" style="background-image: url('https://placeholdit.imgix.net/~text?txtsize=18&txt=190%C3%97140&w=190&h=140');"></a>
                  <div class="col-md-9 primary-text">
                      <h4 class="col-md-12 give-darkRed-c give-boldFamily"><a href="<?php  echo $item->link;?>"> <?php echo $item->title; ?></a></h4>
                    <p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking    </p>
                  </div>
                   <ul class="col-md-12 post-meta give-red-bg row give-padding-sm">
                      
                       <li title="Published">
                          <p class="fa fa-calendar give-yellow-c"></p> <?php echo $item->pubDate;?>
                      </li>
                       <li title="Share">
                           <a href=""><span class="fa fa-share-square-o give-yellow-c"></span> Share</a>
                      </li>
                       <li title="Views">
                          <p class="fa fa-eye give-yellow-c"></p> 300
                      </li>
                       <li title="Save">
                          <a href=""><span class="fa fa-heart give-yellow-c"></span> Save</a>
                      </li>
                      <li class="col-md-12 post-read">
                          <a href="" class="btn give-darkRed-bg " target="_blank">Read More</a>
                      </li>
                   </ul>
              </article><!--end of database post-->
              
              
              <?php
                      $counter++;
                      if($counter>$data_result->link_limit){
                        break;
                      }
                        }?>
                      
                           
             
             
               
            </section>