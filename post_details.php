<?php
require_once("initialize_classes_files.php"); 
require_once("main/layout_parts/header.php");
require_once('main/layout_parts/nav.php');
	if(empty($node_alias)){
		redirect_to('index.php');
	}
	//retrive layout 
	$get_post_content = $define_node->front_node_data(null,"post",$node_alias,null,$lang_info->id,null,null,null,null, null,null,null,"one");
	$node_type = 'post';
	if($get_post_content){				
		$post_layout_info = $define_node->front_get_model($get_post_content->id);
		
		//layout
		require_once("main/layout_templates/".$post_layout_info->layout_name.".php");
	}else{
		echo "<h1>Page not found</h1>
			<p>We don't think you came to the right page. Please use the website navigation to reach what you want.</p>";	
	}
	require_once("main/layout_parts/footer.php")
?>