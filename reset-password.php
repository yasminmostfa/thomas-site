<?php require_once("initialize_classes_files.php");?>
<?php require_once("main/layout_parts/header.php"); ?>

 
		  <?php require_once("initialize_classes_files.php");?>
<?php require_once("main/layout_parts/header.php"); ?>

<?php 
$define_customers = new CustomerInfo(); 
$define_customers->enable_relation(); 
$define_customers_activities = new CustomerActivites(); 
$define_customers_experience = new CustomerExperience(); 
$define_customers_education = new CustomerEducation(); 

$CustomerInfo = $define_customers->customer_data(36, null, null,null,null, null,null,null); 

$CustomerActivities = $define_customers_activities->customer_data($CustomerInfo->id);
$CustomerExpereince = $define_customers_experience->customer_experience($CustomerInfo->id); 
$CustomerEducation = $define_customers_education->customer_education($CustomerInfo->id); 

$lang = $_GET['lang']; 
$code = $_GET['code']; 

$customer_id = 36 ; 

?>
<!--==============content Goes here==============================-->
     <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="index.php?lang=<?php echo $lang ; ?>"><?php echo $home ;  ?></a></li>
              <li class="active"><?php echo $reset_password ;  ?></li>
            </ol>
          </div>
      </section><!--end of internal-cover-->
	


      <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-all main-content"><!--start of main content-->
            <section class="col-m-12 main-content-section"><!--start of main content section-->
               
                  <h2 class="col-md-12 give-darkRed-c give-boldFamily"><?php echo $reset_password_text ;  ?></h2>
                

                    <!-- Nav tabs -->
                    

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="home">
                          <form action="" id="reset_password_form" class="col-md-12 primary-form">
              
                          <input type="hidden" id="lang" value="<?php echo $lang ; ?>">     
                          <input type="hidden" id="code" value="<?php echo $code ; ?>">     
                        
                            <div class="row clearfix give-mb-lg">

                              <div class="form-group col-md-4">
                            
                                <input type="password" id="password" class="form-control"    placeholder="<?php echo $password ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vpass ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>

                              <div class="form-group col-md-4">
                            
                                <input type="password" id="confirm_password" class="form-control"    placeholder="<?php echo $r_password ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vpass_missmatch ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>
                                
                             
                               

                              
                            
                            
                           <?php 

                            $class =""; 
                            if($_GET['lang'] == 'en'){

                              $class = "text-left"; 
                            }else{
                              $class = "text-right" ;
                            }

                           ?>
                          <div class="<?php echo $class ; ?> form-group col-md-8">
                          <input type="submit" class="btn give-red-bg primary-form-save " id="reset_pass_btn" value="<?php echo $save ; ?>" >
                           
                            <!-- <button class="btn give-red-bg primary-form-edit" id="edit" type="button"><?php echo $edit;  ?></button> -->
                      
                            <div id="general_info_reset">
                              
                            </div>

                          </div>
                  
                          </div>
                           
                          </form>

                         

                     

                  </div>
            
              
            </section><!--end of main content section-->
           
          </div><!--end of main content-->
        </div><!--end of row-->
      </div><!--end of main content-->

    <!--==============content Goes here==============================-->

<script>
  
    var password = document.getElementById("password"); 
    var confirm_password = document.getElementById("confirm_password");  

     function validatePassword(){

        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("<?php echo $vpass_missmatch ;  ?>");
        }else{
            confirm_password.setCustomValidity('');
        }

    }

    function val_pass_field(){

         if(password.value==""){
              password.setCustomValidity("<?php echo $vpass ; ?>") ; 
        }else{
              password.setCustomValidity("") ; 
        }

    }


      password.onfocus = val_pass_field; 
      password.onchange = val_pass_field; 
      password.onchange = validatePassword; 
      confirm_password.onfocus = validatePassword;


</script>

<?php require_once("main/layout_parts/footer.php");



 
