<?php require_once("initialize_classes_files.php");?>
<?php require_once("main/layout_parts/header.php"); ?>
<?php $lang = $_GET['lang'] ;  ?>
<?php 
//activity id : 
$activity_id = $_GET['id'] ; 

$get_data =  CustomerActivites::find_by_custom_filed('id' , $activity_id) ; 

//getting customer id based on activityid . 
$customer_id = $get_data->customer_id ; 





?>
<!--==============content Goes here==============================-->
      <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="index.php?lang=<?php echo $lang ; ?>"><?php echo $home ;  ?></a></li>
              <li><a href="user-profile.php?lang=<?php echo $lang ; ?>"><?php echo $user_profile_bread ;  ?></a></li>
              <li class="active"><?php echo $update_activity_bread ;  ?></li>
            </ol>
          </div>
      </section><!--end of internal-cover-->


       <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-all main-content"><!--start of main content-->
            <section class="col-m-12 main-content-section"><!--start of main content section-->
               
                  <h2 class="col-md-12 give-darkRed-c give-boldFamily"><?php echo $update_activity ;  ?></h2>
                  <div class="col-md-12 primary-tabs">

                  

                    <!-- Tab panes -->
                    <div class="tab-content">
                     
                       <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $activities ; ?></h2>
                            <div class="row clearfix give-mb-lg">
                            <form   id="activity_form_update">
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="activity_name"  value="<?php echo $get_data->activity_name ; ?> "  placeholder="Activity Name" required="required" oninvalid="this.setCustomValidity('<?php echo $vactivity_name ; ?>')" onchange="this.setCustomValidity('')">
                                <input type="hidden" value="<?php echo $lang  ; ?>" id="lang">
                                <input type="hidden" value="<?php echo $customer_id ; ?>" id="customer_id">
                              </div>
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="activity_type"  value="<?php echo $get_data->activity_type ?>"  placeholder="Title" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_type ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                              
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="datetimepicker2"    value=" <?php echo $get_data->from_date ;  ?>" placeholder="Date" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_date_from ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                               </div> 


                                <div class="form-group col-md-4">
                                <input type="text" class="form-control datetimepicker_class" id="datetimepicker3"   value="<?php echo $get_data->to_date ;  ?> "  placeholder="Date" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_date_to ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                               </div>

                                <div class="form-group col-md-8">
                                <input type="text" class="form-control" id="role"    value="<?php echo $get_data->role ;  ?> "  placeholder="role" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_role ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                              <div class="form-group col-md-all">
                                <textarea name=""  class="form-control"    id="description"    placeholder="Description" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_description ; ?>')" onchange="this.setCustomValidity('')"><?php echo $get_data->description ;  ?></textarea>
                              </div>

                              <div class="form-group col-md-all">
                                <input type="submit" class="btn give-red-bg" id="update_activity" value="<?php echo $update_activity ; ?>">
                                <div id="updating_current_activity">
                                  
                                </div>
                              </div>
                              </form>

                  </div>
            
              
            </section><!--end of main content section-->
           
          </div><!--end of main content-->
        </div><!--end of row-->
      </div><!--end of main content-->




<?php require_once("main/layout_parts/footer.php");