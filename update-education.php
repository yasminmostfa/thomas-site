<?php require_once("initialize_classes_files.php");?>
<?php require_once("main/layout_parts/header.php"); ?>
<?php $lang = $_GET['lang'] ;  ?>
<?php 
//activity id : 
$education_id = $_GET['id'] ; 

$get_data =  CustomerEducation::find_by_custom_filed('id' , $education_id) ; 

//getting customer id based on activityid . 
$customer_id = $get_data->customer_id ; 





?>
<!--==============content Goes here==============================-->
      <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="">Home</a></li>
              <li class="active">Databases resources</li>
            </ol>
          </div>
      </section><!--end of internal-cover-->


       <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-all main-content"><!--start of main content-->
            <section class="col-m-12 main-content-section"><!--start of main content section-->
               
                  <h2 class="col-md-12 give-darkRed-c give-boldFamily"><?php echo $update_education ;  ?></h2>
                  <div class="col-md-12 primary-tabs">

                  

                    <!-- Tab panes -->
                    <div class="tab-content">
                     
                       <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $education ; ?></h2>
                            <div class="row clearfix give-mb-lg">
                            <form id="education_form_update" >
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="university"  value="<?php echo $get_data->university ; ?> "   placeholder="<?php echo $university_name ; ?>" required 
                                oninvalid="this.setCustomValidity('<?php echo $veducation_university ; ?>')" onchange="this.setCustomValidity('')">
                                <input type="hidden" value="<?php echo $lang  ; ?>" id="lang">
                                <input type="hidden" value="<?php echo $education_id ; ?>" id="education_id">
                              </div>
                               <div class="form-group col-md-4">
                                <select name="" class="form-control" id="education_level" required 
                                   oninvalid="this.setCustomValidity('<?php echo $veducation_degree_level ; ?>')" onchange="this.setCustomValidity('')" >
                                 <!-- <option name="edu" value="<?php //echo  $degree_type1 ;  ?>" <?php //if($get_data->degree_level == 'degree type 1'){echo 'selected' ; } ?>><?php //echo  $degree_level1 ; ?></option>  -->
                                 <option name="edu" value="type2" <?php if($get_data->degree_level == 'type2'){echo 'selected' ; } ?>><?php echo  $degree_type2 ;   ?></option>
                                 <option name="edu" value="type3" <?php if($get_data->degree_level == 'type3'){echo 'selected' ; } ?>><?php echo  $degree_type3 ;  ?></option>
                                 <option name="edu" value="type4" <?php if($get_data->degree_level == 'type4'){echo 'selected' ; } ?>><?php echo  $degree_type4 ;   ?></option>
                                </select>
                                
                              </div>
                                
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="fileds_of_study"    value="<?php echo $get_data->fileds_of_study ;  ?> "   placeholder="<?php echo $fields_of_study ;  ?>" required 
                                oninvalid="this.setCustomValidity('<?php echo $veducation_fields_study ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                              
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control datetimepicker_class" id="datetimepicker6"    value=" <?php echo $get_data->from_date ;  ?>" placeholder="<?php echo $education_date_from ;  ?>" required
                                   oninvalid="this.setCustomValidity('<?php echo $veducation_from ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                               </div> 


                                <div class="form-group col-md-4">
                                <input type="text" class="form-control datetimepicker_class" id="datetimepicker7"   value="<?php echo $get_data->to_date ;  ?> "   placeholder="<?php echo $education_date_to ;  ?>" required
                                   oninvalid="this.setCustomValidity('<?php echo $veducation_to ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                               </div>

                               
                              <div class="form-group col-md-all">
                                <input type="submit" class="btn give-red-bg" id="update_education" value="<?php echo $update_education ; ?>">
                                <div id="updating_current_education">
                                  
                                </div>
                              </div>
                              </form>

                  </div>
            
              
            </section><!--end of main content section-->
           
          </div><!--end of main content-->
        </div><!--end of row-->
      </div><!--end of main content-->




<?php require_once("main/layout_parts/footer.php");