<?php require_once("initialize_classes_files.php");?>
<?php require_once("main/layout_parts/header.php"); ?>
<?php $lang = $_GET['lang'] ;  ?>
<?php 
//activity id : 
$experience_id = $_GET['id'] ; 
$get_data =  CustomerExperience::find_by_custom_filed('id' , $experience_id) ; 
//getting customer id based on activityid . 
$customer_id = $get_data->customer_id ; 
 
?>
<!--==============content Goes here==============================-->
      <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="">Home</a></li>
              <li class="active">Databases resources</li>
            </ol>
          </div>
      </section><!--end of internal-cover-->


       <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-all main-content"><!--start of main content-->
            <section class="col-m-12 main-content-section"><!--start of main content section-->
               
                  <h2 class="col-md-12 give-darkRed-c give-boldFamily"><?php echo $update_experience ;  ?></h2>
                  <div class="col-md-12 primary-tabs">

                  

                    <!-- Tab panes -->
                    <div class="tab-content">
                     
                       <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $expereinces ; ?></h2>
                            <div class="row clearfix give-mb-lg">
                            <form id="experience_form_update">
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="job_title"  value="<?php echo $get_data->job_title ; ?> "  placeholder="<?php echo $job_title ;  ?>"  required 
                                oninvalid="this.setCustomValidity('<?php echo $vexpereince_job_title ; ?>')" onchange="this.setCustomValidity('')">
                                <input type="hidden" value="<?php echo $lang  ; ?>" id="lang">
                                <input type="hidden" value="<?php echo $experience_id ; ?>" id="experience_id">
                                <input type="hidden" value="<?php echo $customer_id ; ?>" id="customer_id">
                              </div>
                               <div class="form-group col-md-4">
                               <select name="" class="form-control" id="job_role" required oninvalid="this.setCustomValidity('<?php echo $vexpereince_job_role ; ?>')" onchange="this.setCustomValidity('')">
                                 <option name="ro" value="role1" <?php if($get_data->job_role == 'role1'){echo 'selected' ; } ?>><?php echo $role1 ; ?></option>
                                 <option name="ro" value="role2" <?php if($get_data->job_role == 'role2'){echo 'selected' ; } ?>><?php echo $role2 ;   ?></option>
                                 <option name="ro" value="role3" <?php if($get_data->job_role == 'role3'){echo 'selected' ; } ?>><?php echo  $role3 ;  ?></option>
                                 <option name="ro" value="role4" <?php if($get_data->job_role == 'role4'){echo 'selected' ; } ?>><?php echo $role4 ;   ?></option>
                               </select>
                               
                              </div>

                                <div class="form-group col-md-4">
                               <select class="form-control" id="career_level" required oninvalid="this.setCustomValidity('<?php echo $vexpereince_career ; ?>')" onchange="this.setCustomValidity('')">
                                 <option name="ca" value="level1" <?php if($get_data->career_level == 'level1'){echo 'selected' ; } ?>><?php echo $level1 ; ?></option>
                                 <option name="ca" value="level2" <?php if($get_data->career_level == 'level2'){echo 'selected' ; } ?>><?php echo $level2 ;   ?></option>
                                 <option name="ca" value="level3" <?php if($get_data->career_level == 'level3'){echo 'selected' ; } ?>><?php echo  $level3 ;  ?></option>
                                 <option name="ca" value="level4" <?php if($get_data->career_level == 'level4'){echo 'selected' ; } ?>><?php echo $level4 ;   ?></option>
                               </select>
                               
                              </div>


                               <div class="form-group col-md-4">
                               <select class="form-control" id="experience_type" required  oninvalid="this.setCustomValidity('<?php echo $vexpereince_extype ; ?>')" onchange="this.setCustomValidity('')">
                                 <option name="ex" value="type1" <?php if($get_data->experience_type == 'type1'){echo 'selected' ; } ?>><?php echo $type1 ; ?></option>
                                 <option name="ex" value="type2" <?php if($get_data->experience_type == 'type2'){echo 'selected' ; } ?>><?php echo $type2 ;   ?></option>
                                 <option name="ex" value="type3" <?php if($get_data->experience_type == 'type3'){echo 'selected' ; } ?>><?php echo $type3 ;  ?></option>
                                 <option name="ex" value="type4" <?php if($get_data->experience_type == 'type4'){echo 'selected' ; } ?>><?php echo $type4 ;   ?></option>
                               </select>
                               
                              </div>
                              
                              
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="company_name"    value=" <?php echo $get_data->company_name ;  ?>" placeholder="<?php echo $company_name ;  ?>" required 
                                 oninvalid="this.setCustomValidity('<?php echo $vexpereince_company_name ; ?>')" onchange="this.setCustomValidity('')" >
                               
                               </div> 


                                <div class="form-group col-md-4">
                                <input type="text" class="form-control datetimepicker_class" id="datetimepicker4"   value="<?php echo $get_data->from_date ;  ?> "  placeholder="<?php echo $date_from ;  ?>" required 
                                   oninvalid="this.setCustomValidity('<?php echo $vexpereince_date_from ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                               </div>

                                <div class="form-group col-md-4">
                                <input type="text" class="form-control datetimepicker_class" id="datetimepicker5"    value="<?php echo $get_data->to_date ;  ?> "  placeholder="<?php echo $date_to ;  ?>" required 
                                    oninvalid="this.setCustomValidity('<?php echo $vexpereince_date_to ; ?>')" onchange="this.setCustomValidity('')">
                                 <p class="fa fa-calendar primary-birthday"></p>
                              </div>
                              

                              <div class="form-group col-md-all">
                                <input type="submit" class="btn give-red-bg" id="update_experience" value="<?php echo $update_experience ; ?>">
                                <div id="updating_current_experience">
                                  
                                </div>
                              </div>
                              </form>

                  </div>
            
              
            </section><!--end of main content section-->
           
          </div><!--end of main content-->
        </div><!--end of row-->
      </div><!--end of main content-->




<?php require_once("main/layout_parts/footer.php");