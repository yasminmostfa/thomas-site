<?php require_once("initialize_classes_files.php");?>
<?php require_once("main/layout_parts/header.php");
require_once("main/layout_parts/nav.php");

 ?>

<?php 
$define_customers = new CustomerInfo(); 
$define_customers->enable_relation(); 
$define_customers_activities = new CustomerActivites(); 
$define_customers_experience = new CustomerExperience(); 
$define_customers_education = new CustomerEducation(); 

$CustomerInfo = $define_customers->customer_data($front_session->user_id, null, null,null,null, null,null,null); 

$CustomerActivities = $define_customers_activities->customer_data($CustomerInfo->id);
$CustomerExpereince = $define_customers_experience->customer_experience($CustomerInfo->id); 
$CustomerEducation = $define_customers_education->customer_education($CustomerInfo->id); 

$lang = $_GET['lang']; 
$customer_id = $front_session->user_id ; 

?>
<!--==============content Goes here==============================-->
      <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="index.php?lang=<?php echo $lang ; ?>"><?php echo $home ;  ?></a></li>
              <li class="active"><?php echo $user_profile_bread ;  ?></li>
            </ol>
          </div>
      </section><!--end of internal-cover-->


      <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-all main-content"><!--start of main content-->
            <section class="col-m-12 main-content-section"><!--start of main content section-->
               
                  <h2 class="col-md-12 give-darkRed-c give-boldFamily"><?php echo $user_profile ;  ?></h2>
                  <div class="col-md-12 primary-tabs">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" id="nav" role="tablist">
                      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo $first_tab ;  ?></a></li>
                      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php echo $second_tab ;  ?></a></li>
                      <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><?php echo $third_tab ; ?></a></li>
                      <!-- <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><?php echo $fourth_tab; ?></a></li> -->
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="home">
                          <form action="" id="general_info" class="col-md-12 primary-form">
                            <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $personal_info ; ?></h2>
                            <input type="hidden" value="<?php echo $lang  ; ?>" id="lang">
                            <input type="hidden" value="<?php echo $customer_id  ; ?>" id="customer_id">
                            <div class="row clearfix give-mb-lg">
                              <div class="form-group col-md-4">
                                <input type="text" id="first_name" class="form-control" disabled="" value="<?php echo $CustomerInfo->first_name ;  ?>" placeholder="<?php echo $fname ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vfirstname ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>
                               <div class="form-group col-md-4">
                                <input type="text" id="last_name" class="form-control" disabled="" value="<?php echo $CustomerInfo->last_name ;  ?>" placeholder="<?php echo $lname ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vlastname ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control"  id="datetimepicker" disabled="" value="<?php echo $CustomerInfo->birth_date ;  ?>"  placeholder="<?php echo $birthday ;  ?>" required oninvalid="this.setCustomValidity('<?php echo $vbirthdate ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                              </div>
                               <div class="form-group col-md-4">
                                <select name="" id="gender" disabled class="form-control" required oninvalid="this.setCustomValidity('<?php echo $vgender ; ?>')" onchange="this.setCustomValidity('')">
                                  <option value=""  ><?php echo $select_gender ; ?></option>
                                  <option value="male" <?php if($CustomerInfo->gender == 'male'){ echo 'selected'; }?>  name="gn"><?php echo $male ;  ?></option>
                                  <option value="female" <?php if($CustomerInfo->gender == 'female') {echo 'selected' ; } ?> name="gn"><?php echo $female;  ?></option>
                                </select>
                              </div>
                                <div class="form-group col-md-4">
                                
                                 <select name="nationality" disabled="" id="countries" class="form-control" required oninvalid="this.setCustomValidity('<?php echo $vnationality ; ?>')" onchange="this.setCustomValidity('')">
                                   <?php require_once('countries.php');  ?>
                                </select>
                              </div>


                               <div class="form-group col-md-4">
                                <select name=""  disabled id="colleage" class="form-control" required oninvalid="this.setCustomValidity('<?php echo $vdept ; ?>')" onchange="this.setCustomValidity('')">
                                  <option  name="dept" value="dept1" <?php if($CustomerInfo->colleage_department == 'dept1'){echo 'selected' ; } ?> > <?php echo $dept1;  ?> </option>
                                  <option  name="dept" value="dept2" <?php if($CustomerInfo->colleage_department == 'dept2'){echo 'selected' ; } ?> > <?php echo $dept2;  ?></option>
                                  <option name="dept" value="dept3" <?php if($CustomerInfo->colleage_department == 'dept3'){echo 'selected' ; } ?> > <?php echo $dept3;  ?></option>
                                  <option name="dept" value="dept4" <?php if($CustomerInfo->colleage_department == 'dept4'){echo 'selected' ; } ?> > <?php echo $dept4;  ?> </option>
                                  <option name="dept" value="dept5" <?php if($CustomerInfo->colleage_department == 'dept5'){echo 'selected' ; } ?> > <?php echo $dept5;  ?></option>
                                  <option name="dept" value="dept6" <?php if($CustomerInfo->colleage_department == 'dept6'){echo 'selected' ; } ?>>  <?php echo $dept6;  ?> </option>     
                                </select>
                              </div>
                            </div>

                             <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $contact_info ; ?> </h2>
                            <div class="row clearfix give-mb-lg">
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="mobile" value='<?php echo $CustomerInfo->mobile ; ?>' disabled placeholder="<?php echo $mobile ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vusermobile ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="other_mobile" value='<?php echo $CustomerInfo->otherMobile ; ?>' disabled  placeholder="<?php echo $mobile_other ;  ?>" >
                              </div>
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="email" value='<?php echo $CustomerInfo->email ; ?>' disabled  placeholder="<?php echo $email ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vemail ; ?>')" onchange="this.setCustomValidity('')" >
                                
                              </div>
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="city" value='<?php echo $CustomerInfo->city ; ?>' disabled placeholder="<?php echo $city ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vcity ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="area" value='<?php echo $CustomerInfo->area ; ?>' disabled placeholder="<?php echo $area ; ?>" required oninvalid="this.setCustomValidity('<?php echo $varea ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control " id="street" value='<?php echo $CustomerInfo->street ; ?>' disabled  placeholder="<?php echo $street ; ?>" required oninvalid="this.setCustomValidity('<?php echo $vstreet ; ?>')" onchange="this.setCustomValidity('')">
                                
                              </div>
                            </div>
                            
                           <?php 

                            $class =""; 
                            if($_GET['lang'] == 'en'){

                              $class = "text-left"; 
                            }else{
                              $class = "text-right" ;
                            }

                           ?>
                          <div class="<?php echo $class ; ?> form-group">
                          <input type="submit" class="btn give-red-bg primary-form-save hide" id="save_general" value="<?php echo $save ; ?>" >
                           
                            <button class="btn give-red-bg primary-form-edit" id="edit" type="button"><?php echo $edit;  ?></button>
                      
                            <div id="general_info_notify">
                              
                            </div>
                          </div>

                           
                          </form>

                         
                          <form action="" method="" id="activity_form" >
                         
                           
                            
                         <h2 class='col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border'><?php echo $activities ; ?></h2>
                          
                            <div class="row clearfix give-mb-lg">
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="activity_name"   placeholder="<?php echo $activity_name;  ?>" required="required" 
                                 oninvalid="this.setCustomValidity('<?php echo $vactivity_name ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="activity_type"   placeholder="<?php echo $activity_type;  ?>" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_type ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                              
                               <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="datetimepicker2"    placeholder="<?php echo $from_date;  ?>" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_date_from ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                               </div> 


                                <div class="form-group col-md-4">
                                <input type="text" class="form-control datetimepicker_class" id="datetimepicker3"    placeholder="<?php echo $to_date ; ?>" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_date_to ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                               </div>

                                <div class="form-group col-md-8">
                                <input type="text" class="form-control" id="role"   placeholder="<?php echo $role ; ?>" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_role ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                              <div class="form-group col-md-all">
                                <textarea name=""  class="form-control"     id="description"    placeholder="<?php echo $description ; ?>" required="required"
                                oninvalid="this.setCustomValidity('<?php echo $vactivity_description ; ?>')" onchange="this.setCustomValidity('')"></textarea>
                              </div>
              

                              <div class="form-group col-md-all">
                                <input type="submit" class="btn give-red-bg" id="add_new_activity" value="<?php echo $add_activity ; ?>">
                                <div id="adding_new_activity">
                                  
                                </div>
                              </div>
                            </div>
                            </form>
                              <?php 
                              $all_activities = $define_customers_activities->get_activities($CustomerInfo->id); 
                              if(count($all_activities) > 0){
                              ?>
                             <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $your_activities_2 ; ?></h2>
                             <?php } ?> 
                             <div class="col-md-12 primary-table-wrap">
                               <table class="col-md-12 primary-table give-gray-bg">

                                
                                 <?php
                                  $lang = $_GET['lang']; 
                                    $all_activities = $define_customers_activities->get_activities($CustomerInfo->id); 
                                    if($all_activities){
                                    foreach($all_activities as $one_activity){

                                        echo "<tr>
                                                <td class='col-md-12 primary-big'>"; 

                                        echo "<h5>$one_activity->activity_name</h5>"; 
                                        echo "<ul class='col-md-12 activity-title'>"; 
                                        echo "<li>$one_activity->activity_type</li>"; 
                                        echo "<li>$from : $one_activity->from_date</li>" ; 
                                        echo "<li>$to : $one_activity->to_date</li>"; 
                                        echo "</ul>"; 
                                        echo "<p>$one_activity->description</p>"; 
                                        echo "</td>" ; 
                                        echo " </td>
                                                 <td class='col-md-12 text-center'>
                                                  <a href='update-activity.php?lang=$lang&id=$one_activity->id'  class='fa fa-edit fa-remove'></a>
                                                 </td>
                                                 <td class='col-md-12 text-center'>
                                                   <a id='delete_activity' data-activity-id='$one_activity->id' data-lang='$lang' href='' class='fa fa-trash  '></a>
                                                 </td>"; 
                                        echo "</tr>";          

                                    }
                                    }
                                 ?>

                                 <!-- delete-activity.php?lang=$lang&id=$one_activity->id -->
                                
                             </table>
                             <div id="activity_delete"></div>
                           </div>


                      </div>
                      <div role="tabpanel" class="tab-pane" id="profile">
                          <form action="" id="experience_form" class="col-md-12 primary-form">
                            <input type="hidden" id="lang" value="<?php echo $lang ;  ?>">
                            <input type="hidden" id="customer_id" value="<?php echo $customer_id ;  ?>">
                            <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $experience_title ; ?></h2>
                            <div class="row clearfix">
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control"  id="job_title" placeholder="<?php echo $job_title ;  ?>" required 
                                oninvalid="this.setCustomValidity('<?php echo $vexpereince_job_title ; ?>')" onchange="this.setCustomValidity('')">
                              </div>
                               <div class="form-group col-md-4">
                                <select name="" id="job_role"   class="form-control" required oninvalid="this.setCustomValidity('<?php echo $vexpereince_job_role ; ?>')" onchange="this.setCustomValidity('')" >
                                  <option name="ro" value="" selected disabled><?php echo $job_role  ; ?></option>
                                  <option name="ro" value="role1" ><?php echo $role1 ; ?></option>
                                  <option name="ro" value="role2" ><?php echo $role2 ; ?></option>
                                  <option name="ro" value="role3" ><?php echo $role3 ; ?></option>
                                  <option name="ro" value="role4" ><?php echo $role4 ; ?></option>
                                </select>
                              </div>
                              <div class="form-group col-md-4">
                                   <select name="" id="career_level"   class="form-control" required oninvalid="this.setCustomValidity('<?php echo $vexpereince_career ; ?>')" onchange="this.setCustomValidity('')" >
                                  <option name="le" value="" selected disabled><?php echo $career ; ?></option>
                                  <option name="le" value="level1" ><?php echo $level1 ; ?></option>
                                  <option name="le" value="level2" ><?php echo $level2 ; ?></option>
                                  <option name="le" value="level3" ><?php echo $level3 ; ?></option>
                                  <option name="le" value="level4" ><?php echo $level4 ; ?></option>
                                </select>
                              </div>

                               <div class="form-group col-md-4">
                                   <select name="" id="ex_type"   class="form-control" required  oninvalid="this.setCustomValidity('<?php echo $vexpereince_extype ; ?>')" onchange="this.setCustomValidity('')">
                                  <option value="" selected disabled><?php echo $ex_type ; ?></option>
                                  <option name="ty" value="type1"><?php echo $type1 ; ?></option>
                                  <option name="ty" value="type2"><?php echo $type2 ; ?></option>
                                  <option name="ty" value="type3"><?php echo $type3 ; ?></option>
                                  <option name="ty" value="type4"><?php echo $type4 ; ?></option>
                                </select>
                              </div>
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="company_name" placeholder="<?php echo $company_name ;  ?>" required 
                                 oninvalid="this.setCustomValidity('<?php echo $vexpereince_company_name ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>
                               <div class="form-group col-md-4">
                                   <input type="text" class="form-control datetimepicker_class " id="datetimepicker4" placeholder="<?php echo $date_from ;  ?>" required 
                                   oninvalid="this.setCustomValidity('<?php echo $vexpereince_date_from ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                              </div>
                               <div class="form-group col-md-4">
                                   <input type="text" class="form-control datetimepicker_class" id="datetimepicker5" placeholder="<?php echo $date_to ;  ?>" required 
                                    oninvalid="this.setCustomValidity('<?php echo $vexpereince_date_to ; ?>')" onchange="this.setCustomValidity('')">
                                <p class="fa fa-calendar primary-birthday"></p>
                              </div>
                              <div class="form-group col-md-all">
                                <!--  <button class="btn give-red-bg" type="button" id="add_new_experience"><?php echo $add_new_experince ?></button> -->
                                 <input type="submit" class="btn give-red-bg" id="add_new_experience" value="<?php echo $add_new_experince ; ?>">
                                 <div id="notify_adding_new_experience"></div>
                              </div>
                            </div>
                            
                            
                            

                            <?php 

                            $class =""; 
                            if($_GET['lang'] == 'en'){

                              $class = "text-left"; 
                            }else{
                              $class = "text-right" ;
                            }

                           ?>


                          <div class="<?php echo $class ;  ?> form-group">
                         

                           <div id="experience_notify">
                             
                           </div>
                          </div>
                          </form>
                          <?php
                           $all_experiences = $define_customers_experience->get_experiences($CustomerInfo->id);  
                           if(count($all_experiences) > 0 ){

                           ?>
                             <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $yours_expereinces ; ?></h2>
                           <?php 

                           }
                           ?>
                           <div class="col-md-12 primary-table-wrap">
                               <table class="col-md-12 primary-table give-gray-bg">

                              <?php
                                    $all_experiences = $define_customers_experience->get_experiences($CustomerInfo->id); 
                                    if($all_experiences){
                                    foreach($all_experiences as $one_experience){

                                        echo "<tr>
                                                <td class='col-md-12 primary-big'>"; 

                                        echo "<h5>$one_experience->job_title</h5>"; 
                                        echo "<ul class='col-md-12 activity-title'>"; 
                                        echo "<li>$one_experience->experience_type</li>"; 
                                        echo "<li>$from : $one_experience->from_date</li>" ; 
                                        echo "<li>$to : $one_experience->to_date</li>"; 
                                        echo "</ul>"; 
                                        echo "<p>$one_experience->company_name</p>"; 
                                        echo "</td>" ; 
                                        echo " </td>
                                                 <td class='col-md-12 text-center'>
                                                  <a href='update-experience.php?lang=$lang&id=$one_experience->id' class='fa fa-edit'></a>
                                                 </td>
                                                 <td class='col-md-12 text-center'>
                                                   <a id='delete_experience' data-experience-id = '$one_experience->id' data-lang='$lang' href='' class='fa fa-trash'></a>
                                                 </td>"; 
                                        echo "</tr>";          

                                    }
                                    }
                                 ?>

                               <!-- delete-experience.php?lang=$lang&id=$one_experience->id -->
                             </table>
                             <div id="experience_deleted"></div>
                           </div>
                      </div>
                      <div role="tabpanel" class="tab-pane" id="messages">
                          <form action="" id="education_form" class="col-md-12 primary-form">
                            <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $education_title ; ?></h2>
                            <div class="row clearfix">
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="university"   placeholder="<?php echo $university_name ; ?>" required 
                                oninvalid="this.setCustomValidity('<?php echo $veducation_university ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>
                            
                             
                               <div class="form-group col-md-4">
                                   <select name="" id="education_level"  class="form-control" required 
                                   oninvalid="this.setCustomValidity('<?php echo $veducation_degree_level ; ?>')" onchange="this.setCustomValidity('')" >
                                  <option value="" selected disabled="" ><?php echo $degree_level  ; ?></option>
                                  <option value="type1" ><?php echo $degree_type1 ; ?></option>
                                  <option value="type2"  ><?php echo $degree_type2 ; ?></option>
                                  <option value="type3"  ><?php echo $degree_type3 ; ?></option>
                                  <option value="type4" ><?php echo $degree_type4 ; ?></option>
                                </select>
                              </div>
                              
                              <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="field_of_study"   placeholder="<?php echo $fields_of_study ;  ?>" required 
                                oninvalid="this.setCustomValidity('<?php echo $veducation_fields_study ; ?>')" onchange="this.setCustomValidity('')" >
                              </div>
                               <div class="form-group col-md-4">
                                   <input type="text" class="form-control datetimepicker_class" id="datetimepicker6"   placeholder="<?php echo $education_date_from ;  ?>" required
                                   oninvalid="this.setCustomValidity('<?php echo $veducation_from ; ?>')" onchange="this.setCustomValidity('')" >
                                <p class="fa fa-calendar primary-birthday"></p>
                              </div>
                               <div class="form-group col-md-4">
                                   <input type="text" class="form-control datetimepicker_class" id="datetimepicker7"    placeholder="<?php echo $education_date_to ;  ?>" required
                                   oninvalid="this.setCustomValidity('<?php echo $veducation_to ; ?>')" onchange="this.setCustomValidity('')" >
                                <p class="fa fa-calendar primary-birthday"></p>
                              </div>
                              <div class="form-group col-md-all">
                                 <button class="btn give-red-bg" id="add_new_education" type="submit"><?php echo $add_new_education ?></button>
                                 <div id="notify_new_education"></div>
                              </div>
                            </div>
                            <?php 
                             $all_education = $define_customers_education->get_education($CustomerInfo->id); 
                             if(count($all_education)){
                            ?>
                              <h2 class="col-md-12 give-darkRed-c give-boldFamily give-title-border give-sidetitle-border"><?php echo $yours_education ; ?></h2>
                            <?php } ?>
                             <div class="col-md-12 primary-table-wrap">
                               <table class="col-md-12 primary-table give-gray-bg">

                              <?php
                                    $all_education = $define_customers_education->get_education($CustomerInfo->id); 
                                    if($all_education){
                                    foreach($all_education as $one_education){

                                        echo "<tr>
                                                <td class='col-md-12 primary-big'>"; 

                                        echo "<h5>$one_education->degree_level</h5>"; 
                                        echo "<ul class='col-md-12 activity-title'>"; 
                                        echo "<li>$one_education->university</li>"; 
                                        echo "<li>$from : $one_education->from_date</li>" ; 
                                        echo "<li>$to : $one_education->to_date</li>"; 
                                        echo "</ul>"; 
                                        echo "<p>$one_education->fileds_of_study</p>"; 
                                        echo "</td>" ; 
                                        echo " </td>
                                                 <td class='col-md-12 text-center'>
                                                  <a href='update-education.php?lang=$lang&id=$one_education->id' class='fa fa-edit'></a>
                                                 </td>
                                                 <td class='col-md-12 text-center'>
                                                   <a id='delete_education' data-education-id = '$one_education->id' data-lang='$lang' href='' class='fa fa-trash'></a>
                                                 </td>"; 
                                        echo "</tr>";          

                                    }
                                    }
                                 ?>

                               <!-- delete-education.php?lang=$lang&id=$one_education->id -->
                             </table>
                             <div id="education_deleted"></div>
                           </div>
                            
                          <div class="text-right form-group">
                          
                            <div id="notify_education">
                              
                            </div>
                          </div>
                          </form>

                      </div>
                      <div role="tabpanel" class="tab-pane" id="settings">...</div>
                    </div>

                  </div>
            
              
            </section><!--end of main content section-->
           
          </div><!--end of main content-->
        </div><!--end of row-->
      </div><!--end of main content-->

    <!--==============content Goes here==============================-->



<?php require_once("main/layout_parts/footer.php");

