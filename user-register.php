<?php require_once("initialize_classes_files.php");?>



<?php require_once("main/layout_parts/header.php");
 
 $lang = $_GET['lang']; 

?>

    <section class="col-md-12 internal-cover" style="background-image: url(main/images/dummy-image.jpg);"><!--start of internal-cover-->
          <div class="container">
             <ol class="breadcrumb internal-path-crumb">
              <li><a href="index.php?lang=<?php echo $lang ;  ?>"><?php echo $home ;  ?></a></li>
              <li class="active"><?php echo $user_register_bread  ;  ?></li>
            </ol>
          </div>
      </section><!--end of internal-cover-->


     <div class="container"><!--start of main content-->
        <div class="row clearfix"><!--start of row-->
           <div class="col-md-8 main-content"><!--start of main content-->
            <section class="col-m-12 main-content-section"><!--start of main content section-->

               
                  <h2 class="col-md-12 give-darkRed-c give-boldFamily"><?php echo $registration ;  ?></h2>
                   <form action="" id="user_register" class="row clearfix primary-form">
                        <input type="hidden" id="lang" value="<?php echo $lang ?>" />
                        <div class="form-group col-md-6">
                          <input type="text" id="first_name" class="form-control" 
                          oninvalid="this.setCustomValidity('<?php echo $vfname ; ?>')" onchange="this.setCustomValidity('')"
                           placeholder="<?php echo $fname ; ?>" required>
                        </div>
                         <div class="form-group col-md-6">
                          <input type="text" id="last_name" class="form-control" oninvalid="this.setCustomValidity('<?php echo $vlname ; ?>')" onchange="this.setCustomValidity('')"
                           placeholder="<?php echo $lname ; ?>" required>
                        </div>
                         <div class="form-group col-md-6">
                          <input type="text" id="email" class="form-control" placeholder="<?php echo $email ; ?>" required>
                        </div>
                         <div class="form-group col-md-6">
                          <input type="text" id="email_compare" class="form-control"  placeholder="<?php echo $r_email ; ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                          <input type="password" id="password" class="form-control" placeholder="<?php echo $password ;  ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                          <input type="password" id="password_compare" class="form-control" placeholder="<?php echo $r_password ; ?>" required>
                        </div>
                         <div class="form-group col-md-6">
                          <input type="text" id="city" class="form-control" oninvalid="this.setCustomValidity('<?php echo $vcity ; ?>')" onchange="this.setCustomValidity('')"
                          placeholder="<?php echo $city ; ?>" required  >
                        </div>
                         <div class="form-group col-md-6">
                          <input type="text" id="area" class="form-control" oninvalid="this.setCustomValidity('<?php echo $varea ; ?>')" onchange="this.setCustomValidity('')"
                          placeholder="<?php echo $area ; ?>" required>
                        </div>
                         <div class="form-group col-md-6">
                          <input type="text" id="street" class="form-control" oninvalid="this.setCustomValidity('<?php echo $vstreet ; ?>')" onchange="this.setCustomValidity('')"
                          placeholder="<?php echo $street ; ?>" required>
                        </div>
                         <div class="form-group col-md-6">
                          <select name="" id="gender" class="form-control" required  oninvalid="this.setCustomValidity('<?php echo $vgender ; ?>')" onchange="this.setCustomValidity('')">
                            <option value="" selected disabled><?php echo $gender ;  ?></option>
                            <option value="<?php echo $male ; ?>" name="gn" ><?php echo $male ; ?></option>
                            <option value="<?php echo $female ; ?>" name="gn" ><?php echo $female ; ?></option>
                          </select>
                        </div>
                         <div class="form-group col-md-6">
                          <input type="text" class="form-control" id="datetimepicker" placeholder="<?php echo $birthday ;  ?>" required oninvalid="this.setCustomValidity('<?php echo $vdatetimepicker ; ?>')" onchange="this.setCustomValidity('')" >
                          <p class="fa fa-calendar primary-birthday"></p>
                        </div>
                        <div class="col-md-all form-group">
                          <input type="submit" id="submit" class="btn give-red-bg" value="<?php echo $submit ; ?>">
                          <div id="validation_message_reg"></div>
                        </div>
                        <br>
                        
                      </form>
            

            
              
            </section><!--end of main content section-->
           
          </div><!--end of main content-->

         
        </div><!--end of row-->
      </div><!--end of main content-->
    <!--==============content Goes here==============================-->

<?php require_once("main/layout_parts/footer.php");?>



<script type="text/javascript">

    var password = document.getElementById("password"); 
    var confirm_password = document.getElementById("password_compare"); 
    var email = document.getElementById("email");
    var email_compare = document.getElementById("email_compare");


     function validateEmailEmpty(){
      if(email.value == ''){
         email.setCustomValidity("<?php echo $vemail ;  ?>");
      }else{
        email.setCustomValidity("");
      }
    }


    function validateEmails(){
      if(email.value != email_compare.value ){
        email_compare.setCustomValidity("<?php echo $vemail_compare ;  ?>");
      }else{
         email_compare.setCustomValidity('') ; 
      }
    }

   


    function validatePassword(){

        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("<?php echo $vpass_missmatch ;  ?>");
        }else{
            confirm_password.setCustomValidity('');
        }

    }

    function val_pass_field(){

         if(password.value==""){
              password.setCustomValidity("<?php echo $vpass ; ?>") ; 
        }else{
              password.setCustomValidity("") ; 
        }

    }


    email.onfocus = validateEmailEmpty ; 
    email.onchange = validateEmailEmpty ; 
    email.onkeyup = validateEmails ;
    email_compare.onchange = validateEmails ; 


    password.onfocus = val_pass_field;  
    confirm_password.onfocus = validatePassword;

</script>

